<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Front_model extends CI_Model
{

    public function get_posts()
    {

        $this->db->select('ps.id as id, 
                            ps.content as content,
                            ps.updated_at as post_date,
                            cu.id as user_id, 
                            cu.name_surname as name_surname');

        $this->db->join('client_users cu', 'ps.user_id=cu.id');
        $this->db->from('posts ps');
        return $this->db->get()->result_array();
    }

    public function get_comments($page_ref_id)
    {
        $this->db->limit(4);
        $this->db->order_by('id', 'RANDOM');
        $this->db->where('page_ref_id', $page_ref_id);
        return $this->db->get('pages_comments')->result_array();
    }

    public function get_random_pages($category_id = NULL)
    {
        $this->db->limit(15);
        $this->db->order_by('pg.id', 'RANDOM');
        if ($category_id != NULL)
            $this->db->where('pg.category_ref_id', $category_id);
        $this->db->where('pg.language', $this->session->userdata('app_lang_id'));
        $this->db->select('pg.title, CONCAT(lng.code,"/",pg.permalink) as permalink');
        $this->db->join('languages lng', 'pg.language=lng.id', 'left');
        $this->db->from('pages pg');
        return $this->db->get()->result_array();
    }

}