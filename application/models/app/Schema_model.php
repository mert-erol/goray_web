<?php

class Schema_model extends CI_Model
{


    public function get_columns($table)
    {

        $query = $this->schema->select('COLUMN_NAME name,COLUMN_COMMENT comment,IS_NULLABLE is_null,DATA_TYPE type')
            ->where('COLUMN_COMMENT!=', '')
            ->where('TABLE_SCHEMA', $this->db->database)
            ->where('TABLE_NAME', $table)
            ->get('COLUMNS')
            ->result_array();

        $list = array();
        foreach ($query as $row):
            $list[$row['name']] = $row;
        endforeach;

        return $list;
    }

    public function get_columns_datatables($table)
    {

        $query = $this->schema->select('
        COLUMN_NAME name,
        COLUMN_COMMENT comment,
        IS_NULLABLE is_null
        ')
            ->where('TABLE_SCHEMA', $this->db->database)
            ->where_not_in('COLUMN_NAME', ['language', 'ref_id'])
            ->where('TABLE_NAME', $table)
            ->get('COLUMNS')
            ->result_array();
        return $query;
    }

}