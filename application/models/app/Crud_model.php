<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Crud_model extends CI_Model
{

    public function get($table, $where = NULL, $limit = NULL, $order = NULL, $like = NULL)
    {

        $like != NULL ? $this->db->like($like[0], $like[1]) : NULL;
        $where != NULL ? $this->db->where($where) : NULL;
        $limit != NULL ? $this->db->limit($limit) : NULL;
        $order != NULL ? $this->db->order_by($order) : $this->db->order_by('id', 'desc');
        $this->db->from($table);

        return $this->db->get()->result_array();
    }

    public function set($table, $data, $insert_id = FALSE)
    {
        $insert = $this->db->insert($table, $data);
        return $insert_id == FALSE ? $insert : $this->db->insert_id();
    }

    public function update($table, $where, $data, $limit = NULL)
    {
        if ($limit != NULL):
            $this->db->limit($limit);
        endif;
        if ($where != NULL):
            $this->db->where($where);
            return $this->db->update($table, $data);
        else:
            return 0;
        endif;
    }

    public function delete($table, $where)
    {
        return $this->db->where($where)->delete($table);
    }

}
