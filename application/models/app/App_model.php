<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class App_model extends CI_Model
{

    public function getOrderDetails($orderId)
    {
        $this->db->where('ord.id', $orderId);
        $this->db->select('ord.id as id,
ord.order_data as order_data,
ord.discount as discount,
ord.status as status,
ord.created_at as created_at,
ord.updated_at as updated_at,
cst.title as customer,
usr.name_surname as client_user');

        $this->db->join('customers cst', 'ord.customer=cst.id', 'left');
        $this->db->join('client_users usr', 'ord.client_user=usr.id', 'left');
        $this->db->from('orders ord');


        return $this->db->get()->row_array();
    }

    public function get_devices_credits()
    {
        $this->db->select('dc.id, dc.process as process, dv.name as device, dc.credit,dc.created_at, dc.updated_at');
        $this->db->join('devices dv', 'dc.device=dv.id');
        $this->db->from('devices_credits dc');
        return $this->db->get();
    }

    public function get_employees($where = NULL)
    {

        if ($where != NULL) {
            $this->db->where($where);
        }

        $this->db->select('cu.id, cu.username, cu.name_surname, ti.title, cu.phone, cu.phone_2, cu.date');
        $this->db->limit(10);
        $this->db->where('ti.language', $this->session->userdata('app_lang_id'));
        $this->db->where('dp.language', $this->session->userdata('app_lang_id'));
        $this->db->from('client_users cu');
        $this->db->join('titles ti', 'cu.title_ref_id=ti.ref_id', 'left');
        $this->db->join('departments dp', 'ti.department=dp.ref_id', 'left');
        return $this->db->get()->result_array();
    }

}
