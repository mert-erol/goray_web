<?php

function view_engine($modules = array(), $sizes = array(), $viewData = array())
{
    $ci = &get_instance();

    $viewData['mainSettings'] = $ci->crud_model->get('settings', array('id' => 1))[0];
    $viewData['navMenu'] = $ci->crud_model->get('applications', array('is_menu' => 1));
    $viewData['languages'] = $ci->crud_model->get('languages', ['status!=' => 0]);
    $viewData['logs'] = $ci->crud_model->get('logs', NULL, 20, 'id DESC');
    $viewData['modal'] = TRUE;
    $ci->input->set_cookie('app_last_page', current_url(), time() + (10 * 365 * 24 * 60 * 60));

    if (!$ci->input->get('modal')) {
        $viewData['modal'] = FALSE;
        $ci->load->view('app/base/header', $viewData);
        $ci->load->view('app/base/aside', $viewData);
    }

    foreach ($modules as $key => $module):
        !isset($sizes[$key]) ? $viewData['size'] = 6 : $viewData['size'] = $sizes[$key];
        $ci->load->view($module, $viewData);
    endforeach;

    if (!$ci->input->get('modal')) {
        $ci->load->view('app/base/footer', $viewData);
    }
}

function lang_data($id)
{
    $ci = &get_instance();
    return $ci->crud_model->get('languages', array('id' => $id))[0];
}

function app_auth_check($app_name)
{
    $ci = &get_instance();
    $enc = json_decode($ci->session->userdata('app_user_data')['auths_list']);
    return !is_null($enc) ? (property_exists($enc,$app_name) ? 1 : 0) : 0;
}

function app_method_check($class, $method)
{
    $app_name = mb_strtolower($method . '-' . $class);
    return app_auth_check($app_name) ? '' : redirect('App/Dashboard');
}

function get_app_name($class, $method)
{
    return mb_strtolower($method . '-' . $class);
}

function front_view_engine($modules = array(), $viewData = array(), $modal = FALSE)
{
    $ci = &get_instance();

    $viewData['mainSettings'] = $ci->crud_model->get('settings', array('id' => 1))[0];
    $viewData['languages'] = $ci->crud_model->get('languages', ['id!=' => $ci->session->userdata('app_lang_id')]);

    $viewData['userSettings'] = $ci->session->userdata('front_user_data');
    $viewData['translationSettings'] = [
        'languages' => $ci->crud_model->get('translation_languages', ['language' => $ci->session->userdata('app_lang_id'), 'status' => 1]),
        'serviceTypes' => $ci->crud_model->get('service_types', ['language' => $ci->session->userdata('app_lang_id'), 'status' => 1])
    ];

    $ci->input->set_cookie('front_last_page', current_url(), time() + (10 * 365 * 24 * 60 * 60));

    if ($modal == FALSE) {
        if (!$ci->input->get('modal')) {
            $ci->load->view('site/base/header', $viewData);
            $ci->load->view('site/base/navigation', $viewData);
        }
    }

    foreach ($modules as $key => $module):
        $ci->load->view($module, $viewData);
    endforeach;

    if ($modal == FALSE) {
        if (!$ci->input->get('modal') || $modal == FALSE) {
            $ci->load->view('site/base/footer', $viewData);
        }
    }
}

function app_login_check()
{
    $ci = &get_instance();
    language_selector(NULL, 'NO');
    $ci->session->userdata('app_login') != 1 ? redirect(base_url('login')) : 0;
}

function language_selector($lang_id = NULL, $redirect = NULL)
{
    $ci = &get_instance();

    if ($lang_id != NULL) {
        $langData = $ci->crud_model->get('languages', ['id' => $lang_id]);
        if (count($langData) > 0) {
            $ci->session->set_userdata('app_lang_code', mb_strtoupper($langData[0]['code']));
            $ci->session->set_userdata('app_lang_id', $langData[0]['id']);
            $ci->input->set_cookie('app_lang_dir', $langData[0]['directory'], time() + (10 * 365 * 24 * 60 * 60));
            $ci->load->language($langData[0]['directory'] . '_lang', $langData[0]['directory']);
            $ci->config->set_item('language', $langData[0]['directory']);
        }
    } else {
        if (strlen(get_cookie('app_lang_dir')) > 0) {
            $langData = $ci->crud_model->get('languages', ['directory' => get_cookie('app_lang_dir')]);
            $ci->session->set_userdata('app_lang_code', mb_strtoupper($langData[0]['code']));
            $ci->session->set_userdata('app_lang_id', $langData[0]['id']);
            $ci->load->language(get_cookie('app_lang_dir') . '_lang', get_cookie('app_lang_dir'));
            $ci->config->set_item('language', get_cookie('app_lang_dir'));
        } else {
            $ci->session->set_userdata('app_lang_code', 'EN');
            $ci->session->set_userdata('app_lang_id', 2);
            $ci->input->set_cookie('app_lang_dir', 'english', time() + (10 * 365 * 24 * 60 * 60));
            $ci->load->language('english_lang', 'english');
            $ci->config->set_item('language', 'english');
        }
    }

    $redirect == NULL || $redirect == 'NO' ? '' : redirect(base_url($redirect));

}


function front_login_check()
{
    $ci = &get_instance();
    language_selector(NULL, 'NO');
    $ci->session->userdata('front_login') != 1 ? redirect(base_url('login')) : 0;
}

function my_logs($text, $type = 'success', $table = NULL, $relation_id = NULL)
{
    $ci = &get_instance();
    $ci->load->model('crud_model');
    $text = "<span class='text-$type'>$text</span>";

    $dataSet = array('text' => $text);

    if ($table != NULL) {
        $dataSet['table'] = $table;
    }

    if ($ci->session->userdata('app_login') == 1) {
        $dataSet['user_id'] = app_usr('id');
    }


    if ($relation_id != NULL) {
        $dataSet['relation_id'] = $relation_id;
    }

    $ci->crud_model->set('logs', $dataSet);
}

function status_map($type, $text = NULL, $array = FALSE)
{

    if ($text == NULL) {
        $types = array(
            0 => array('warning', 'BEKLİYOR'),
            1 => array('success', 'ONAYLANDI'),
            2 => array('success', 'TAMAMLANDI'),
            3 => array('danger', 'İPTAL EDİLDİ'),
            4 => array('info', 'HAZIRLANIYOR')
        );
    } else {
        $types = array(
            0 => array('warning', $text),
            1 => array('success', $text),
            2 => array('success', $text),
            3 => array('danger', $text),
            4 => array('info', $text)
        );
    }


    return !$array ? '<span class="badge badge-' . $types[$type][0] . '">' . $types[$type][1] . '</span>' : $types;
}

function prod_url($id)
{
    $ci = &get_instance();
    $ci->load->model('crud_model');
    $data = $ci->crud_model->get('products', array('id' => $id))[0];

    return base_url('urun/' . $id . '/' . permalink($data['product_name']));
}

function cat_url($id)
{
    $ci = &get_instance();
    $ci->load->model('crud_model');
    $data = $ci->crud_model->get('product_categories', array('id' => $id))[0];

    return base_url('kategori/' . $id . '/' . permalink($data['title']));
}

function featured_image($prod_id)
{
    $ci = &get_instance();
    $ci->load->model('crud_model');
    $data = $ci->crud_model->get('multimedia', array('relation_id' => $prod_id, 'table' => 'products', 'status' => 1));

    if (count($data) > 0) {
        return base_url('uploads/products/' . $data[0]['name']);
    } else {
        return base_url() . '/uploads/settings/logo.png';
    }
}

function app_usr($col, $tag = NULL)
{
    $ci = &get_instance();
    $userSessData = $ci->session->userdata('app_user_data');
    return $tag == NULL ? $userSessData[$col] : "<$tag>" . $userSessData[$col] . "</$tag>";
}

function my_curl($url, $params = array())
{

    $postData = '';
    foreach ($params as $k => $v) {
        $postData .= $k . '=' . $v . '&';
    }
    rtrim($postData, '&');

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_POST, count($params));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    $output = curl_exec($ch);
    curl_close($ch);

    return $output;
}

function my_curl2($url, $params)
{
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $params,
        CURLOPT_HTTPHEADER => array(
            "Content-Type: application/x-www-form-urlencoded"
        ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    return $response;
}

function app_order_stats($stat, $tag = 'button', $id = NULL, $extra = NULL)
{
    /*
     *  0 => array('warning', 'BEKLİYOR'),
      1 => array('success', 'ONAYLANDI'),
      2 => array('success', 'TAMAMLANDI'),
      3 => array('danger', 'İPTAL EDİLDİ'),
      4 => array('info', 'HAZIRLANIYOR')
     * */


    $stat_map = array(
        0 => array('BEKLİYOR', 'warning', 'la la-spin la-spinner'),
        1 => array('ONAYLANDI', 'success', 'la la-check'),
        2 => array('TAMAMLANDI', 'success', 'la la-check'),
        3 => array('İPTAL EDİLDİ', 'danger', 'la la-times'),
        4 => array('HAZIRLANIYOR', 'info', 'la la-spin la-spinner')
    );

    return "<$tag id='$id'" . 'class="btn btn-elevate btn-' . $stat_map[$stat][1] . ' btn-elevate"  ' . $extra . '><i
                            class="' . $stat_map[$stat][2] . '"></i>' . $stat_map[$stat][0] . "</$tag>";
}

function permalink($str, $options = array())
{
    $str = mb_convert_encoding((string)$str, 'UTF-8', mb_list_encodings());
    $defaults = array(
        'delimiter' => '-',
        'limit' => null,
        'lowercase' => true,
        'replacements' => array(),
        'transliterate' => true
    );
    $options = array_merge($defaults, $options);
    $char_map = array(
        // Latin
        'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'AE', 'Ç' => 'C',
        'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
        'Ð' => 'D', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ő' => 'O',
        'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ű' => 'U', 'Ý' => 'Y', 'Þ' => 'TH',
        'ß' => 'ss',
        'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'ae', 'ç' => 'c',
        'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
        'ð' => 'd', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ő' => 'o',
        'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ű' => 'u', 'ý' => 'y', 'þ' => 'th',
        'ÿ' => 'y',
        // Latin symbols
        '©' => '(c)',
        // Greek
        'Α' => 'A', 'Β' => 'B', 'Γ' => 'G', 'Δ' => 'D', 'Ε' => 'E', 'Ζ' => 'Z', 'Η' => 'H', 'Θ' => '8',
        'Ι' => 'I', 'Κ' => 'K', 'Λ' => 'L', 'Μ' => 'M', 'Ν' => 'N', 'Ξ' => '3', 'Ο' => 'O', 'Π' => 'P',
        'Ρ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'Y', 'Φ' => 'F', 'Χ' => 'X', 'Ψ' => 'PS', 'Ω' => 'W',
        'Ά' => 'A', 'Έ' => 'E', 'Ί' => 'I', 'Ό' => 'O', 'Ύ' => 'Y', 'Ή' => 'H', 'Ώ' => 'W', 'Ϊ' => 'I',
        'Ϋ' => 'Y',
        'α' => 'a', 'β' => 'b', 'γ' => 'g', 'δ' => 'd', 'ε' => 'e', 'ζ' => 'z', 'η' => 'h', 'θ' => '8',
        'ι' => 'i', 'κ' => 'k', 'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => '3', 'ο' => 'o', 'π' => 'p',
        'ρ' => 'r', 'σ' => 's', 'τ' => 't', 'υ' => 'y', 'φ' => 'f', 'χ' => 'x', 'ψ' => 'ps', 'ω' => 'w',
        'ά' => 'a', 'έ' => 'e', 'ί' => 'i', 'ό' => 'o', 'ύ' => 'y', 'ή' => 'h', 'ώ' => 'w', 'ς' => 's',
        'ϊ' => 'i', 'ΰ' => 'y', 'ϋ' => 'y', 'ΐ' => 'i',
        // Turkish
        'Ş' => 'S', 'İ' => 'I', 'Ç' => 'C', 'Ü' => 'U', 'Ö' => 'O', 'Ğ' => 'G',
        'ş' => 's', 'ı' => 'i', 'ç' => 'c', 'ü' => 'u', 'ö' => 'o', 'ğ' => 'g',
        // Russian
        'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'Yo', 'Ж' => 'Zh',
        'З' => 'Z', 'И' => 'I', 'Й' => 'J', 'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O',
        'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
        'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sh', 'Ъ' => '', 'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'Yu',
        'Я' => 'Ya',
        'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'yo', 'ж' => 'zh',
        'з' => 'z', 'и' => 'i', 'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',
        'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c',
        'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sh', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu',
        'я' => 'ya',
        // Ukrainian
        'Є' => 'Ye', 'І' => 'I', 'Ї' => 'Yi', 'Ґ' => 'G',
        'є' => 'ye', 'і' => 'i', 'ї' => 'yi', 'ґ' => 'g',
        // Czech
        'Č' => 'C', 'Ď' => 'D', 'Ě' => 'E', 'Ň' => 'N', 'Ř' => 'R', 'Š' => 'S', 'Ť' => 'T', 'Ů' => 'U',
        'Ž' => 'Z',
        'č' => 'c', 'ď' => 'd', 'ě' => 'e', 'ň' => 'n', 'ř' => 'r', 'š' => 's', 'ť' => 't', 'ů' => 'u',
        'ž' => 'z',
        // Polish
        'Ą' => 'A', 'Ć' => 'C', 'Ę' => 'e', 'Ł' => 'L', 'Ń' => 'N', 'Ó' => 'o', 'Ś' => 'S', 'Ź' => 'Z',
        'Ż' => 'Z',
        'ą' => 'a', 'ć' => 'c', 'ę' => 'e', 'ł' => 'l', 'ń' => 'n', 'ó' => 'o', 'ś' => 's', 'ź' => 'z',
        'ż' => 'z',
        // Latvian
        'Ā' => 'A', 'Č' => 'C', 'Ē' => 'E', 'Ģ' => 'G', 'Ī' => 'i', 'Ķ' => 'k', 'Ļ' => 'L', 'Ņ' => 'N',
        'Š' => 'S', 'Ū' => 'u', 'Ž' => 'Z',
        'ā' => 'a', 'č' => 'c', 'ē' => 'e', 'ģ' => 'g', 'ī' => 'i', 'ķ' => 'k', 'ļ' => 'l', 'ņ' => 'n',
        'š' => 's', 'ū' => 'u', 'ž' => 'z'
    );
    $str = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);
    if ($options['transliterate']) {
        $str = str_replace(array_keys($char_map), $char_map, $str);
    }
    $str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);
    $str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);
    $str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');
    $str = trim($str, $options['delimiter']);
    return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;
}

function media_permalink($file)
{
    $exploded = explode('.', $file);
    $extension = end($exploded);
    $file_name = str_replace($extension, '', $file);
    return permalink($file_name) . ".$extension";
}

function pr2($val)
{
    echo '<pre>';
    print_r($val);
}

function cur($amount, $code = "TL", $reverse = FALSE)
{
    $amount = number_format((float)$amount, 2, '.', '');
    return !$reverse ? "$amount $code" : "$code $amount";
}

function phn($n)
{
    $n = "$n";
    $n = trim($n);
    $n = preg_replace("/[^0-9.]/", "", $n);
    $n = substr($n, -10, 10);
    return strlen($n) == 10 ? '90' . $n : 0;
}

function ln($key)
{
    $ci = &get_instance();
    return $ci->lang->line($key);
}

function date_std($date, $time = TRUE, $format = 'd-m-Y')
{
    $ci = &get_instance();
    $locale = $ci->session->userdata('app_lang_code');
    $locale = strtolower($locale) . '_' . mb_strtoupper($locale);
    setlocale(LC_TIME, $locale . ".UTF-8");
    $create_date = date($format . ($time ? ' H:i' : ''), strtotime($date));

    return strftime('%e %B %A' . ($time ? ' %H:%M' : ''), strtotime($date));
}

function date2db($date)
{
    $date = str_replace('/', '.', $date);
    return date('Y-m-d H:i:s', strtotime($date));
}

function timeago($date)
{
    $timestamp = strtotime($date);

    $strTime = array(ln("TIME.second"), ln('TIME.minute'), ln('TIME.hour'), ln('TIME.day'), ln('TIME.month'), ln('TIME.year'));
    $length = array("60", "60", "24", "30", "12", "10");

    $currentTime = time();
    if ($currentTime >= $timestamp) {
        $diff = time() - $timestamp;
        for ($i = 0; $diff >= $length[$i] && $i < count($length) - 1; $i++) {
            $diff = $diff / $length[$i];
        }

        $diff = round($diff);
        return $diff . " " . $strTime[$i] . ln('TIME.ago');
    } else {
        $diff = time() - $timestamp;
        for ($i = 0; $diff >= $length[$i] && $i < count($length) - 1; $i++) {
            $diff = $diff / $length[$i];
        }

        $diff = round($diff);
        return $diff . " " . $strTime[$i] . ln('TIME.ago');
    }
}

function foreign_currency()
{
    $doviz = simplexml_load_file('http://www.tcmb.gov.tr/kurlar/today.xml');

    return [
        'dolar_alis' => $doviz->Currency[0]->BanknoteBuying,
        'dolar_satis' => $doviz->Currency[0]->BanknoteSelling,
        'euro_alis' => $doviz->Currency[3]->BanknoteBuying,
        'euro_satis' => $doviz->Currency[3]->BanknoteSelling,
        'pound_alis' => $doviz->Currency[4]->BanknoteBuying,
        'pound_satis' => $doviz->Currency[4]->BanknoteSelling,
        'japon_yeni_alis' => $doviz->Currency[11]->BanknoteBuying,
        'japon_yeni_satis' => $doviz->Currency[11]->BanknoteSelling

    ];
}

function altin_currency()
{
    $altin = simplexml_load_file('http://www.kulcealtin.com/altinxml/');
    $showItems = [];
    foreach ($altin->altin as $item) {
        $fields = ['Y22', 'C', 'Y', 'T', 'GA'];
        if (in_array($item->field, $fields)) {
            $showItems[] = [
                'adi' => $item->adi,
                'al' => $item->al,
                'sat' => $item->sat
            ];
        }
    }
    return $showItems;
}

function upload_dir()
{
    return base_url('uploads/');
}

function get_page($id)
{
    $ci = &get_instance();
    $get = $ci->crud_model->get('pages', ['ref_id' => $id, 'language' => $ci->session->userdata('app_lang_id')]);
    return !empty($get) ? $get[0] : [];
}


function weather()
{
    $get_xml = simplexml_load_file('https://www.mgm.gov.tr/FTPDATA/bolgesel/istanbul/sonSOA.xml');
    return $get_xml;
}

function send_mail($viewData)
{
    $ci = &get_instance();
    $ci->load->library('phpmailer_lib');
    $mail = $ci->phpmailer_lib->load();

    $mail->IsSMTP();
    $mail->Mailer = "smtp";
    $mail->CharSet = 'UTF-8';
    $mail->Host = 'smtp.office365.com';
    $mail->Port = "25"; // 8025, 587 and 25 can also be used. Use Port 465 for SSL.
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = 'tls';
    $mail->Username = "portal@ihidalgakiran.com";
    $mail->Password = "ihiDalga2018+";
    $mail->From = $viewData['email'];
    $mail->FromName = $viewData['name_surname'];
    $mail->AddAddress($viewData['email'], $viewData['name_surname']);
    $mail->AddReplyTo($viewData['email'], $viewData['name_surname']);
    $mail->SMTPDebug = 1;
    $mail->Subject = 'Merhaba, bur bir test mesajıdır.';
    $mail->Body = 'Lütfen cevap vermeyiniz';
    $mail->WordWrap = 50;

    if (!$mail->Send()) {
        $a = 'Message was not sent.';
        return $a . 'Mailer error: ' . $mail->ErrorInfo;
        exit;
    } else {
        return 'Message has been sent.';
    }
}


function send_mail_old($viewData)
{

    $ci = &get_instance();
    $ci->load->library('phpmailer_lib');
    $mail = $ci->phpmailer_lib->load();

    $mail->isSMTP();

    $mail->SMTPAuth = true;
    $mail->CharSet = 'utf-8';
    $mail->Host = 'tls://smtp-mail.outlook.com';

    $mail->Username = 'portal@ihidalgakiran.com';
    $mail->Password = 'ihiDalga2018+';
    $mail->SMTPSecure = 'tls';
    $mail->Port = 587;
    $mail->SMTPDebug = 0;
    $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        )
    );

    $mail->From = $viewData['email'];
    $mail->FromName = $viewData['name_surname'];
    $mail->AddAddress($viewData['email'], $viewData['name_surname']);
    $mail->AddReplyTo($viewData['email'], $viewData['name_surname']);

    $mail->Subject = 'WEB SİTESİ FORMLARI ' . $viewData['name_surname'];

    $mail->isHTML(true);

    // Email body content
    // $mailContent = $this->load->view('site/email', $viewData, true);
    $mailContent = 'TEST MAILI' . $viewData['name_surname'];
    $mail->Body = $mailContent;


    // Send email
    if (!$mail->send()) {

        // echo 'Message could not be sent.';
        return 'Mailer Error: ' . $mail->ErrorInfo;
        //  echo '<pre>';

    } else {
        return true;
        // echo 'Message has been sent';
    }
}


function doc_counter($file_dir)
{
    exec("python3 ../../../home/ubuntu/CounterProjects/counter2.py $file_dir", $result);
    return $result;
}

function guid()
{
    if (function_exists('com_create_guid') === true) {
        return trim(com_create_guid(), '{}');
    }

    return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
}


function estimated_date_old($word_count)
{
    date_default_timezone_set('Europe/Istanbul');
    $hourly_words = 10;
    $add_hours = round($word_count / $hourly_words);
    $over_hours = round($add_hours % 24);
    $estimated_date = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . "+$add_hours hours"));

    $estimated_hour = intval(date('H', strtotime($estimated_date)));
    if ($estimated_hour > 18) {
        $estimated_date = date('Y-m-d', strtotime($estimated_date . '+1 days')) . ' 09:00:00';
        $estimated_date = date('Y-m-d H:i:s', strtotime($estimated_date . " +$over_hours hours"));

    }

    $estimated_hour = intval(date('H', strtotime($estimated_date)));
    if ($estimated_hour < 9) {
        $estimated_date = date('Y-m-d', strtotime($estimated_date)) . ' 09:00:00';
        $estimated_date = date('Y-m-d H:i:s', strtotime($estimated_date . " +$over_hours hours"));
    }

    $estimated_hour = intval(date('H', strtotime($estimated_date)));
    if ($estimated_hour <= 18 || $estimated_hour >= 9) {

    }

    return date_std($estimated_date);
}

function estimated_date($word_count)
{


    date_default_timezone_set('Europe/Istanbul');
    $date = date('Y-m-d H:i');
    $time = date('H:i');

    //$word_count = 820;
    $hourly_words = 75;
    $working_hours = 8;

    $add_hours = floor($word_count / $hourly_words) % 8;
    $add_days = floor($word_count / $hourly_words / 8);


    if (intval(date('H', strtotime($date))) > 18) {
        $date = date('Y-m-d', strtotime($date . " +1 weekdays 09:00"));
    }

    if (intval(date('H', strtotime($date))) < 9) {
        $date = date('Y-m-d', strtotime($date)) . ' 09:00';
    }


    $date = date('Y-m-d H:i:s', strtotime($date . " +$add_days weekdays 09:00"));


    $date = date('Y-m-d H:i', strtotime($date . " +$add_hours hours"));

    if (date('N', strtotime($date)) == 6) {
        $date = date('Y-m-d H:i', strtotime($date . " +1 days"));
    }


    if (date('N', strtotime($date)) == 7) {
        $date = date('Y-m-d H:i', strtotime($date . " +2 days"));
    }

    return date_std($date);
}

function get_coupon()
{
    $seed = str_split(
        'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        . '0123456789');
    shuffle($seed);
    $rand = '';
    foreach (array_rand($seed, 6) as $k) $rand .= $seed[$k];
    return $rand;
}

function available_langs()
{
    $ci = &get_instance();

    return [
        'lang_data' => $ci->crud_model->get('languages', ['status!=' => 0]),
        'default_lang' => $ci->crud_model->get('languages', ['default' => 1])[0]
    ];
}

function get_sess($name, $type = 0)
{
    $ci = &get_instance();
    if ($type == 0) {
        if ($ci->session->flashdata($name)) {
            return $ci->session->flashdata($name);
        }
    } else {
        if ($ci->session->userdata($name)) {
            return $ci->session->userdata($name);
        }
    }


    return "";
}

function get_pages($options = ['category' => NULL, 'slug' => NULL, 'page_id' => NULL, 'limit' => 999])
{
    $ci = &get_instance();
    $where = [];
    if (isset($options['limit']))
        $ci->db->limit($options['limit']);
    if (isset($options['category']))
        for ($i = 0; $i < count($options['category']); $i++) {
            $ci->db->or_like('pg.category_ref_id', '"' . $options['category'][$i] . '"', 'both');
        }
    if (isset($options['slug']))
        $where = array_merge($where, ['pg.permalink' => $options['slug']]);
    if (isset($options['page_id']))
        $where = array_merge($where, ['pg.id' => $options['page_id']]);

    if (!is_null($where)):
        $ci->db->where($where);
        $ci->db->where('pg.language', $ci->session->userdata('app_lang_id'));
        $ci->db->select('pg.title, CONCAT(lng.code,"/",pg.permalink) as permalink');
        $ci->db->join('languages lng', 'pg.language=lng.id', 'left');
        $ci->db->from('pages pg');
        return $ci->db->get()->result_array();
    else:
        return [];
    endif;
}

function get_permalink($page_id)
{
    $ci = &get_instance();
}


