<?php

$lang['APPLICATIONS.Managers'] = "Managers";
$lang['APPLICATIONS.Add New Manager'] = "Add New Manager";
$lang['APPLICATIONS.Manager List'] = "Manager List";
$lang['APPLICATIONS.Departments'] = "Departments";
$lang['APPLICATIONS.Pages Categories'] = "Pages Categories";
$lang['APPLICATIONS.Translation Languages'] = "Translation Languages";
$lang['APPLICATIONS.Service Types'] = "Service Types";
$lang['APPLICATIONS.Discounts'] = "Discount Coupons";
$lang['APPLICATIONS.Translation Scenarios'] = "Translation Scenarios";
$lang['APPLICATIONS.Add Department'] = "Add Department";
$lang['APPLICATIONS.Department List'] = "Department List";
$lang['APPLICATIONS.Add Language'] = "Add Language";
$lang['APPLICATIONS.Language List'] = "Language List";
$lang['APPLICATIONS.Add Service'] = "Add Service";
$lang['APPLICATIONS.Service List'] = "Service List";
$lang['APPLICATIONS.Add Discount'] = "Add Discount";
$lang['APPLICATIONS.Discount List'] = "Discount List";
$lang['APPLICATIONS.Add Scenario'] = "Add Scenario";
$lang['APPLICATIONS.Scenario List'] = "Scenario List";
$lang['APPLICATIONS.Add Category'] = "Add Category";
$lang['APPLICATIONS.Category List'] = "Category List";
$lang['APPLICATIONS.Products'] = "Products";
$lang['APPLICATIONS.Product List'] = "Product List";
$lang['APPLICATIONS.Add New Product'] = "Add New Product";
$lang['APPLICATIONS.Users'] = "Employees";
$lang['APPLICATIONS.Add New User'] = "Add New Employee";
$lang['APPLICATIONS.User List'] = "Employee List";
$lang['APPLICATIONS.Orders'] = "Orders";
$lang['APPLICATIONS.Order List'] = "Order List";
$lang['APPLICATIONS.Add New Order'] = "Add New Order";
$lang['APPLICATIONS.Categories'] = "Categories";
$lang['APPLICATIONS.Settings'] = "Settings";
$lang['APPLICATIONS.Payment Methods'] = "Payment Methods";
$lang['APPLICATIONS.Departments'] = "Departments";
$lang['APPLICATIONS.Food List'] = "Food List";
$lang['APPLICATIONS.List by Daily'] = "List by Daily";
$lang['APPLICATIONS.Education'] = "Education";
$lang['APPLICATIONS.New Education'] = "New Education";
$lang['APPLICATIONS.Education List'] = "Education List";
$lang['APPLICATIONS.Leaves'] = "Leaves";
$lang['APPLICATIONS.Leave Apply'] = "Leave Apply";
$lang['APPLICATIONS.Applies'] = "Applies";
$lang['APPLICATIONS.Events'] = "Events";
$lang['APPLICATIONS.Event Calendar'] = "Event Calendar";
$lang['APPLICATIONS.New Event'] = "New Event";
$lang['APPLICATIONS.Event List'] = "Event List";
$lang['APPLICATIONS.Campaigns'] = "Campaigns";
$lang['APPLICATIONS.New Campaigns'] = "New Campaign";
$lang['APPLICATIONS.Campaigns List'] = "All Campaigns";
$lang['APPLICATIONS.News'] = "News";
$lang['APPLICATIONS.Add News'] = "Add News";
$lang['APPLICATIONS.News List'] = "News List";
$lang['APPLICATIONS.Announcements'] = "Announcements";
$lang['APPLICATIONS.New Announcements'] = "New Announcement";
$lang['APPLICATIONS.Job Titles'] = "Job Titles";
$lang['APPLICATIONS.Add Title'] = "Add Title";
$lang['APPLICATIONS.Title List'] = "Title List";
$lang['APPLICATIONS.Surveys'] = "Surveys";
$lang['APPLICATIONS.Survey List'] = "Survey List";
$lang['APPLICATIONS.Create New Survey'] = "Create New Survey";
$lang['APPLICATIONS.Meetings'] = "Meetings";
$lang['APPLICATIONS.Create Meeting'] = "Create Meeting";
$lang['APPLICATIONS.Meeting List'] = "Meeting List";
$lang['APPLICATIONS.Pages'] = "Pages";
$lang['APPLICATIONS.Add Pages'] = "Add Pages";
$lang['APPLICATIONS.Pages List'] = "Pages List";
$lang['APPLICATIONS.PageSigns'] = "Accepted Documents";
$lang['APPLICATIONS.Requests'] = "Requests";
$lang['APPLICATIONS.Expense Bill'] = "Expense Bill";
$lang['APPLICATIONS.Near Miss'] = "Near Miss";
$lang['APPLICATIONS.Wish box'] = "Wish Box";
$lang['APPLICATIONS.Suggestion Box'] = "Complaint Box";
$lang['APPLICATIONS.Pages Comments'] = "Pages Comments";
$lang['APPLICATIONS.Add Comment'] = "Add Comment";
$lang['APPLICATIONS.Comment List'] = "Comment List";
$lang['APPLICATIONS.Edit Comment'] = "Edit Comment";
$lang['APPLICATIONS.Transaction Stocks'] = "Transaction Stocks";
$lang['APPLICATIONS.Add Stocks'] = "Add Stocks";
$lang['APPLICATIONS.Stocks List'] = "Stocks List";
$lang['APPLICATIONS.Edit Stocks'] = "Edit Stocks";

$lang['departments.message'] = "Department";
$lang['departments.code'] = "Code";
$lang['departments.title'] = "Department Name";
$lang['departments.title_2'] = "Content";
$lang['departments.phone'] = "Phone/Internal";
$lang['departments.created_at'] = "Created";
$lang['departments.updated_at'] = "Updated";
$lang['departments.language'] = "Language";

$lang['pages_categories.message'] = "Category";
$lang['pages_categories.title'] = "Category Name";
$lang['pages_categories.created_at'] = "Created";
$lang['pages_categories.updated_at'] = "Updated";
$lang['pages_categories.language'] = "Language";

$lang['pages_comments.message'] = "Comment";
$lang['pages_comments.page_ref_id'] = "Page";
$lang['pages_comments.author'] = "Author";
$lang['pages_comments.star'] = "Star";
$lang['pages_comments.comment'] = "Comment";
$lang['pages_comments.created_at'] = "Created";
$lang['pages_comments.updated_at'] = "Updated";

$lang['orders.message'] = "Order";
$lang['orders.origin_language'] = "Origin Language";
$lang['orders.target_languages'] = "Target Languages";
$lang['orders.free_text'] = "Text";
$lang['orders.client_user'] = "User";
$lang['orders.user_files'] = "User Files";
$lang['orders.word_count'] = "Word Count";
$lang['orders.total_amount'] = "Total Amount";
$lang['orders.service_types'] = "Service Types";
$lang['orders.estimated_date'] = "Estimated Date";
$lang['orders.discount'] = "Discount";
$lang['orders.notes'] = "Notes";
$lang['orders.status'] = "Status";
$lang['orders.created_at'] = "Created";
$lang['orders.updated_at'] = "Updated";

$lang['translation_languages.message'] = "Translation Languages";
$lang['translation_languages.title'] = "Language Name";
$lang['translation_languages.status'] = "Status";
$lang['translation_languages.created_at'] = "Created";
$lang['translation_languages.updated_at'] = "Updated";
$lang['translation_languages.language'] = "Language";

$lang['service_types.message'] = "Service Type";
$lang['service_types.title'] = "Service Name";
$lang['service_types.method'] = "Method";
$lang['service_types.method_value'] = "Method Value";
$lang['service_types.status'] = "Status";
$lang['service_types.created_at'] = "Created";
$lang['service_types.expired_at'] = "Expired";
$lang['service_types.language'] = "Language";

$lang['discounts.message'] = "Discount Coupon";
$lang['discounts.title'] = "Coupon Code";
$lang['discounts.method'] = "Discount Type";
$lang['discounts.method_value'] = "Discount Value";
$lang['discounts.status'] = "Status";
$lang['discounts.created_at'] = "Created";
$lang['discounts.expired_at'] = "Expired";
$lang['discounts.language'] = "Language";

$lang['translation_scenarios.message'] = "Scenario";
$lang['translation_scenarios.origin_lang_ref_id'] = "Origin Language";
$lang['translation_scenarios.target_lang_ref_id'] = "Target Language";
$lang['translation_scenarios.word_fee'] = "Word Fee";
$lang['translation_scenarios.status'] = "Status";
$lang['translation_scenarios.created_at'] = "Created";
$lang['translation_scenarios.updated_at'] = "Updated";
$lang['translation_scenarios.language'] = "Language";

$lang['transaction_stocks.product_id'] = "Product_id";
$lang['transaction_stocks.type'] = "Type";
$lang['transaction_stocks.header_id'] = "Header_id";
$lang['transaction_stocks.amount'] = "Amount";

$lang['requests.message'] = "Requests";
$lang['requests.start_date'] = "Start Date";
$lang['requests.end_date'] = "End Date";
$lang['requests.content'] = "Content";
$lang['requests.status'] = "Status";
$lang['requests.created_at'] = "Created";
$lang['requests.updated_at'] = "Updated";
$lang['requests.status_0'] = "Pending";
$lang['requests.status_1'] = "Approval";
$lang['requests.status_2'] = "Declined";
$lang['requests.employee_id'] = "Employee";
$lang['requests.type'] = "Type";
$lang['requests.type_1'] = "Leave Apply";
$lang['requests.type_2'] = "Expense Bill";
$lang['requests.type_3'] = "Near Miss";
$lang['requests.type_4'] = "Wish box";
$lang['requests.type_5'] = "Complaint Box";

$lang['client_users.message'] = "Employees";
$lang['client_users.id'] = "ID";
$lang['client_users.title_ref_id'] = "Title";
$lang['client_users.username'] = "E-mail";
$lang['client_users.name_surname'] = "Name Surname";
$lang['client_users.password'] = "Password";
$lang['client_users.date'] = "Birthday";
$lang['client_users.phone'] = "Phone";
$lang['client_users.phone_2'] = "Internal";
$lang['client_users.locale'] = "Locale";
$lang['client_users.status'] = "Status";
$lang['client_users.file'] = "Photo";
$lang['client_users.created_at'] = "Created";
$lang['client_users.updated_at'] = "Updated";

$lang['food_list.message'] = "Food List";
$lang['food_list.date'] = "Date";
$lang['food_list.content'] = "Content";
$lang['food_list.status'] = "Status";
$lang['food_list.created_at'] = "Created";
$lang['food_list.updated_at'] = "Updated";

$lang['education.message'] = "Education";
$lang['education.department'] = "Department";
$lang['education.title'] = "Title";
$lang['education.start_date'] = "Start Date";
$lang['education.end_date'] = "End Date";
$lang['education.location'] = "Location";
$lang['education.content'] = "Content";
$lang['education.status'] = "Status";
$lang['education.created_at'] = "Created";
$lang['education.updated_at'] = "Updated";

$lang['leaves.message'] = "Education";
$lang['leaves.department'] = "Department";
$lang['leaves.title'] = "Title";
$lang['leaves.start_date'] = "Start Date";
$lang['leaves.end_date'] = "End Date";
$lang['leaves.location'] = "Location";
$lang['leaves.content'] = "Content";
$lang['leaves.status'] = "Status";
$lang['leaves.created_at'] = "Created";
$lang['leaves.updated_at'] = "Updated";

$lang['events.message'] = "Events";
$lang['events.department'] = "Department";
$lang['events.title'] = "Title";
$lang['events.start_date'] = "Start Date";
$lang['events.end_date'] = "End Date";
$lang['events.location'] = "Location";
$lang['events.content'] = "Content";
$lang['events.status'] = "Status";
$lang['events.created_at'] = "Created";
$lang['events.updated_at'] = "Updated";

$lang['campaigns.message'] = "Campaigns";
$lang['campaigns.department'] = "Department";
$lang['campaigns.title'] = "Title";
$lang['campaigns.start_date'] = "Start Date";
$lang['campaigns.end_date'] = "End Date";
$lang['campaigns.location'] = "Foundation & Firm";
$lang['campaigns.content'] = "Content";
$lang['campaigns.status'] = "Status";
$lang['campaigns.created_at'] = "Created";
$lang['campaigns.updated_at'] = "Updated";

$lang['surveys.message'] = "Survey";
$lang['surveys.department'] = "Department";
$lang['surveys.title'] = "Title";
$lang['surveys.start_date'] = "Start Date";
$lang['surveys.end_date'] = "End Date";
$lang['surveys.location'] = "Foundation & Firm";
$lang['surveys.content'] = "Content";
$lang['surveys.status'] = "Status";
$lang['surveys.created_at'] = "Created";
$lang['surveys.updated_at'] = "Updated";

$lang['survey_items.title'] = "Title";
$lang['survey_items.survey_id'] = "Survey";
$lang['survey_items.options'] = "Options";

$lang['meetings.message'] = "Survey";
$lang['meetings.department'] = "Department";
$lang['meetings.title'] = "Title";
$lang['meetings.start_date'] = "Start Date";
$lang['meetings.end_date'] = "End Date";
$lang['meetings.location'] = "Foundation & Firm";
$lang['meetings.content'] = "Content";
$lang['meetings.status'] = "Status";
$lang['meetings.created_at'] = "Created";
$lang['meetings.updated_at'] = "Updated";

$lang['meeting_items.title'] = "Title";
$lang['meeting_items.survey_id'] = "Meeting";
$lang['meeting_items.options'] = "Decisions";

$lang['news.message'] = "News";
$lang['news.department'] = "Department";
$lang['news.title'] = "Title";
$lang['news.content'] = "Content";
$lang['news.file'] = "Photo";
$lang['news.user_id'] = "User";
$lang['news.status'] = "Status";
$lang['news.created_at'] = "Created";
$lang['news.updated_at'] = "Updated";

$lang['pages.message'] = "Pages";
$lang['pages.department'] = "Department";
$lang['pages.category_ref_id'] = "Category";
$lang['pages.title'] = "Title";
$lang['pages.permalink'] = "Permalink";
$lang['pages.origin'] = "Origin Lang";
$lang['pages.target'] = "Target Lang";
$lang['pages.service'] = "Service";
$lang['pages.faq'] = "F.A.Q";
$lang['pages.keywords'] = "Meta Keywords";
$lang['pages.description'] = "Meta Description";
$lang['pages.content'] = "Content";
$lang['pages.files'] = "Attachments";
$lang['pages.status'] = "Status";
$lang['pages.created_at'] = "Created";
$lang['pages.updated_at'] = "Updated";

$lang['page_signs.client_user_id'] = "Employee";
$lang['page_signs.page_id'] = "Document";
$lang['page_signs.created_at'] = "Created";
$lang['page_signs.updated_at'] = "Updated";

$lang['announcements.message'] = "Announcements";
$lang['announcements.department'] = "Department";
$lang['announcements.title'] = "Title";
$lang['announcements.content'] = "Content";
$lang['announcements.status'] = "Status";
$lang['announcements.created_at'] = "Created";
$lang['announcements.updated_at'] = "Updated";

$lang['users.username'] = "Username";
$lang['users.name_surname'] = "Full Name";
$lang['users.e_mail'] = "E-mail";
$lang['users.mobile'] = "Mobile";
$lang['users.password'] = "Password";
$lang['users.status'] = "Status";
$lang['users.auths_list'] = "Auths";
$lang['users.created_at'] = "Created";
$lang['users.updated_at'] = "Updated";

$lang['titles.title'] = "Title";
$lang['titles.department'] = "Department";
$lang['titles.status'] = "Status";
$lang['titles.auths_list'] = "Permissions";
$lang['titles.created_at'] = "Created";
$lang['titles.updated_at'] = "Updated";

$lang['shelfs.type'] = "Shelf Type";
$lang['shelfs.name'] = "Shelf Name";
$lang['APPLICATIONS.Shelfs'] = "Shelfs";
$lang['APPLICATIONS.Add Shelfs'] = "Add Shelfs";
$lang['APPLICATIONS.Shelfs List'] = "Shelfs List";
$lang['APPLICATIONS.Edit Shelfs'] = "Edit Shelfs";
$lang['shelfs.shelf'] = "Shelf";
$lang['shelfs.section'] = "Section";
$lang['shelfs.parent'] = "Parent Section";

$lang['warehouses.name'] = "Warehouse Name";
$lang['APPLICATIONS.Warehouses'] = "Warehouses";
$lang['APPLICATIONS.Add Warehouses'] = "Add Warehouses";
$lang['APPLICATIONS.Warehouses List'] = "Warehouses List";
$lang['APPLICATIONS.Edit Warehouses'] = "Edit Warehouses";

$lang['TEXT.Social Experience'] = 'Social Experience';
$lang['TEXT.Min'] = 'Min';
$lang['TEXT.Max'] = 'Max';

$lang['AUTH.Login'] = 'Login';
$lang['AUTH.Log out'] = 'Log out';
$lang['AUTH.Log out_sub'] = 'Log out from your account.';
$lang['AUTH.Forgot password?'] = 'Forgot password?';

$lang['BASE.turkish'] = 'Türkçe';
$lang['BASE.english'] = 'English';
$lang['BASE.add'] = 'Add';
$lang['BASE.edit'] = 'Edit';
$lang['BASE.update'] = 'Update';
$lang['BASE.all'] = 'All';
$lang['BASE.close'] = 'Close';
$lang['BASE.active'] = 'Active';
$lang['BASE.passive'] = 'Passive';
$lang['BASE.search'] = 'Search';
$lang['BASE.filter'] = 'Filter';
$lang['BASE.clear'] = 'Clear';
$lang['BASE.renew'] = 'Renew';
$lang['BASE.select'] = 'Select';
$lang['BASE.translations'] = 'Translations';
$lang['BASE.apply'] = 'Apply';
$lang['BASE.send'] = 'Send';
$lang['BASE.profile'] = 'My Profile';
$lang['BASE.hello'] = 'Hello';

$lang['ACTION.add'] = 'Add';
$lang['ACTION.update'] = 'Update';
$lang['ACTION.delete'] = 'Delete';
$lang['ACTION.add_row'] = 'Add Row';

$lang['MESSAGE.Processing'] = 'Processing';
$lang['MESSAGE.Wait'] = 'Please wait awhile';
$lang['MESSAGE.Added'] = 'Successfully added!';
$lang['MESSAGE.Applied'] = 'Successfully applied!';
$lang['MESSAGE.Updated'] = 'Successfully updated!';
$lang['MESSAGE.SysError'] = 'System Error!';
$lang['MESSAGE.Duplicated'] = 'Successfully duplicated!';
$lang['MESSAGE.Error'] = 'Error!';
$lang['MESSAGE.Login Successfully !'] = 'Login Successfully !';
$lang['MESSAGE.Redirecting'] = 'Redirecting';
$lang['MESSAGE.SendPasswordMail'] = 'The new password has been sent to ';
$lang['MESSAGE.InvalidMail'] = 'The e-mail is invalid!';
$lang['MESSAGE.Signature'] = 'Read and Accept';

$lang['FRONT.Feed'] = 'Feed';
$lang['FRONT.Panel'] = 'Management';
$lang['FRONT.Menu'] = 'Menu';
$lang['FRONT.News'] = 'News';
$lang['FRONT.News_sub'] = 'Department News';
$lang['FRONT.Education'] = 'Education';
$lang['FRONT.Events'] = 'Events';
$lang['FRONT.Events_sub'] = 'Planned Events';
$lang['FRONT.Food List'] = 'Food List';
$lang['FRONT.Food List_sub'] = 'Monthly';
$lang['FRONT.Leave Apply'] = 'Leave Apply';
$lang['FRONT.Expense bill'] = 'Expense Bill';
$lang['FRONT.Requests'] = 'Requests';
$lang['FRONT.Leave Apply_sub'] = 'Track apply';
$lang['FRONT.My Department'] = 'My Department';
$lang['FRONT.My Department_sub'] = 'Employees, Notifications';

$lang['FRONT.Campaign'] = 'Campaigns';
$lang['FRONT.Notices'] = 'Notices';
$lang['FRONT.Birthdays'] = 'Birthdays';
$lang['FRONT.Employees'] = 'Employees';

$lang['FRONT.Publish'] = 'Publish';
$lang['FRONT.Media'] = 'Media';
$lang['FRONT.Video'] = 'Video';

$lang['FRONT.Load More'] = 'Load More';
$lang['FRONT.Read More'] = 'Read More';

$lang['FRONT.newjob_title'] = ' <h4>%1$s</h4> <br> <p>started to work with us as <br> %2$s</p>';
$lang['FRONT.You might like'] = 'You might like';
$lang['FRONT.Exchange Rate'] = 'Exchange Rate';
$lang['FRONT.Exchange Rate_buy'] = 'BUY';
$lang['FRONT.Exchange Rate_sell'] = 'SELL';


$lang['COMPOSE.write line'] = 'Write something about you...';
$lang['COMPOSE.Time Line'] = 'Time Line';
$lang['COMPOSE.SHARE.Public'] = 'Public';
$lang['COMPOSE.SHARE.My Department'] = 'My Department';
$lang['COMPOSE.SHARE.Publish Content'] = 'Publish Content';

$lang['TIME.second'] = 'second';
$lang['TIME.minute'] = 'minute';
$lang['TIME.hour'] = 'hour';
$lang['TIME.day'] = 'day';
$lang['TIME.month'] = 'month';
$lang['TIME.year'] = 'year';
$lang['TIME.ago'] = '(s) ago ';





