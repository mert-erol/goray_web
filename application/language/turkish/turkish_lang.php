<?php

$lang['APPLICATIONS.Managers'] = "Yetkililer";
$lang['APPLICATIONS.Add New Manager'] = "Yeni Yetkili Ekle";
$lang['APPLICATIONS.Manager List'] = "Yetkili Listesi";
$lang['APPLICATIONS.Departments'] = "Departmanler";
$lang['APPLICATIONS.Pages Categories'] = "Sayfa Kategorileri";
$lang['APPLICATIONS.Translation Languages'] = "Çeviri Dilleri";
$lang['APPLICATIONS.Service Types'] = "Hizmet Tipleri";
$lang['APPLICATIONS.Discounts'] = "İndirim Kuponları";
$lang['APPLICATIONS.Translation Scenarios'] = "Çeviri Senaryoları";
$lang['APPLICATIONS.Add Department'] = "Departman Ekle";
$lang['APPLICATIONS.Department List'] = "Departman Listesi";
$lang['APPLICATIONS.Add Language'] = "Dil Ekle";
$lang['APPLICATIONS.Language List'] = "Dil Listesi";
$lang['APPLICATIONS.Add Service'] = "Hizmet Ekle";
$lang['APPLICATIONS.Service List'] = "Hizmet Listesi";
$lang['APPLICATIONS.Add Discount'] = "Kupon Ekle";
$lang['APPLICATIONS.Discount List'] = "Kupon Listesi";
$lang['APPLICATIONS.Add Category'] = "Kategori Ekle";
$lang['APPLICATIONS.Add Scenario'] = "Senaryo Ekle";
$lang['APPLICATIONS.Scenario List'] = "Senaryo Listesi";
$lang['APPLICATIONS.Category List'] = "Kategori Listesi";
$lang['APPLICATIONS.Products'] = "Ürünler";
$lang['APPLICATIONS.Product List'] = "Ürün Listesi";
$lang['APPLICATIONS.Add New Product'] = "Yeni Ürün Ekle";
$lang['APPLICATIONS.Users'] = "Personeller";
$lang['APPLICATIONS.Add New User'] = "Yeni Personel Ekle";
$lang['APPLICATIONS.User List'] = "Personel Listesi";
$lang['APPLICATIONS.Orders'] = "Siparişler";
$lang['APPLICATIONS.Order List'] = "Sipariş Listesi";
$lang['APPLICATIONS.Add New Order'] = "Yeni Sipariş Ekle";
$lang['APPLICATIONS.Categories'] = "Kategoriler";
$lang['APPLICATIONS.Settings'] = "Ayarlar";
$lang['APPLICATIONS.Payment Methods'] = "Ödeme Yöntemleri";
$lang['APPLICATIONS.Departments'] = "Departmanlar";
$lang['APPLICATIONS.Food List'] = "Yemek Listesi";
$lang['APPLICATIONS.List by Daily'] = "Günlük Liste";
$lang['APPLICATIONS.Education'] = "Eğitim";
$lang['APPLICATIONS.New Education'] = "Yeni Eğitim Ekle";
$lang['APPLICATIONS.Education List'] = "Eğitim Listesi";
$lang['APPLICATIONS.Leaves'] = "İzinler";
$lang['APPLICATIONS.Leave Apply'] = "Yeni İzin";
$lang['APPLICATIONS.Applies'] = "Başvurular";
$lang['APPLICATIONS.Events'] = "Etkinlikler";
$lang['APPLICATIONS.Event Calendar'] = "Etkinlik Takvimi";
$lang['APPLICATIONS.New Event'] = "Yeni Etkinlik";
$lang['APPLICATIONS.Event List'] = "Etkinlik Listesi";
$lang['APPLICATIONS.Report'] = "Bütçe Raporları";
$lang['APPLICATIONS.New Campaigns'] = "Yeni Kampanya";
$lang['APPLICATIONS.Campaigns List'] = "Bütçe Raporu";
$lang['APPLICATIONS.News'] = "Haberler";
$lang['APPLICATIONS.Add News'] = "Yeni Haber Ekle";
$lang['APPLICATIONS.News List'] = "Tüm Haberler";
$lang['APPLICATIONS.Announcements'] = "Duyurular";
$lang['APPLICATIONS.New Announcements'] = "Yeni Duyuru";
$lang['APPLICATIONS.Job Titles'] = "İş Ünvanları";
$lang['APPLICATIONS.Add Title'] = "Ünvan Ekle";
$lang['APPLICATIONS.Title List'] = "Ünvan Listesi";
$lang['APPLICATIONS.Surveys'] = "Anketler";
$lang['APPLICATIONS.Survey List'] = "Anket Listesi";
$lang['APPLICATIONS.Create New Survey'] = "Yeni Anket Oluştur";
$lang['APPLICATIONS.Meetings'] = "Toplantılar";
$lang['APPLICATIONS.Create Meeting'] = "Toplantı Oluştur";
$lang['APPLICATIONS.Meeting List'] = "Toplantı Listesi";
$lang['APPLICATIONS.Pages'] = "Sayfalar";
$lang['APPLICATIONS.Add Pages'] = "Sayfa Ekle";
$lang['APPLICATIONS.Pages List'] = "Sayfa Listesi";
$lang['APPLICATIONS.PageSigns'] = "Onaylanmış Dökümanlar";
$lang['APPLICATIONS.Requests'] = "Talepler";
$lang['APPLICATIONS.Expense Bill'] = "Gider Fişi";
$lang['APPLICATIONS.Near Miss'] = "Ramak Kala";
$lang['APPLICATIONS.Wish box'] = "Dilek Kutusu";
$lang['APPLICATIONS.Suggestion Box'] = "Şikayet Kutusu";
$lang['APPLICATIONS.Pages Comments'] = "Sayfa Yorumları";
$lang['APPLICATIONS.Add Comment'] = "Yorum Ekle";
$lang['APPLICATIONS.Comment List'] = "Yorum Listesi";
$lang['APPLICATIONS.Edit Comment'] = "Yorum Düzenle";
$lang['APPLICATIONS.Transaction Stocks'] = "Stok Yönetimi";
$lang['APPLICATIONS.Add Stocks'] = "Stok Ekle";
$lang['APPLICATIONS.Stocks List'] = "Stokları Listele";
$lang['APPLICATIONS.Edit Stocks'] = "Stokları Düzenle";

$lang['departments.message'] = "Departman";
$lang['departments.code'] = "Kod";
$lang['departments.title'] = "Departman Adı";
$lang['departments.title_2'] = "Departman Açıklaması";
$lang['departments.phone'] = "Telefon/Dahili";
$lang['departments.created_at'] = "Oluşturma";
$lang['departments.updated_at'] = "Güncelleme";
$lang['departments.language'] = "Dil";

$lang['pages_categories.message'] = "Kategori";
$lang['pages_categories.title'] = "Kategori Adı";
$lang['pages_categories.created_at'] = "Oluşturma";
$lang['pages_categories.updated_at'] = "Güncelleme";
$lang['pages_categories.language'] = "Dil";

$lang['pages_comments.message'] = "Yorum";
$lang['pages_comments.page_ref_id'] = "Sayfa";
$lang['pages_comments.author'] = "Yazar";
$lang['pages_comments.star'] = "Yıldız";
$lang['pages_comments.comment'] = "Yorum";
$lang['pages_comments.created_at'] = "Oluşturma";
$lang['pages_comments.updated_at'] = "Güncelleme";

$lang['translation_languages.message'] = "Çeviri Dilleri";
$lang['translation_languages.title'] = "Dil Adı";
$lang['translation_languages.status'] = "Durum";
$lang['translation_languages.created_at'] = "Oluşturma";
$lang['translation_languages.updated_at'] = "Güncelleme";
$lang['translation_languages.language'] = "Dil";

$lang['service_types.message'] = "Hizmet Tipi";
$lang['service_types.title'] = "Hizmet Adı";
$lang['service_types.method'] = "Metod";
$lang['service_types.method_value'] = "Metod Değeri";
$lang['service_types.status'] = "Durum";
$lang['service_types.created_at'] = "Oluşturma";
$lang['service_types.updated_at'] = "Güncelleme";
$lang['service_types.language'] = "Dil";

$lang['discounts.message'] = "İndirim Kuponu";
$lang['discounts.title'] = "Kupon Kodu";
$lang['discounts.method'] = "İndirim Tipi";
$lang['discounts.method_value'] = "İndirim Değeri";
$lang['discounts.status'] = "Durum";
$lang['discounts.created_at'] = "Oluşturma";
$lang['discounts.expired_at'] = "Geçerlilik";
$lang['discounts.language'] = "Dil";

$lang['translation_scenarios.message'] = "Senaryo";
$lang['translation_scenarios.origin_lang_ref_id'] = "Ana Dil";
$lang['translation_scenarios.target_lang_ref_id'] = "Hedef Dil";
$lang['translation_scenarios.word_fee'] = "Kelime Ücreti";
$lang['translation_scenarios.status'] = "Durum";
$lang['translation_scenarios.created_at'] = "Oluşturma";
$lang['translation_scenarios.updated_at'] = "Güncelleme";
$lang['translation_scenarios.language'] = "Dil";

$lang['transaction_stocks.product_id'] = "Ürün Numarası";
$lang['transaction_stocks.type'] = "Tip";
$lang['transaction_stocks.header_id'] = "Header Numarası";
$lang['transaction_stocks.amount'] = "Miktar";

$lang['requests.message'] = "Talepler";
$lang['requests.start_date'] = "Başlangıç Tarihi";
$lang['requests.end_date'] = "Bitiş Tarihi";
$lang['requests.content'] = "Açıklama";
$lang['requests.status'] = "Durum";
$lang['requests.created_at'] = "Oluşturma";
$lang['requests.updated_at'] = "Güncelleme";
$lang['requests.status_0'] = "Bekliyor";
$lang['requests.status_1'] = "Onaylandı";
$lang['requests.status_2'] = "Reddedildi";
$lang['requests.employee_id'] = "Çalışan";
$lang['requests.type'] = "Tip";
$lang['requests.type_1'] = "İzin Talebi";
$lang['requests.type_2'] = "Gider Fişi";
$lang['requests.type_3'] = "Ramak Kala";
$lang['requests.type_4'] = "Dilek Kutusu";
$lang['requests.type_5'] = "Şikayet Kutusu";

$lang['client_users.message'] = "Çalışanlar";
$lang['client_users.id'] = "ID";
$lang['client_users.title_ref_id'] = "Başlık";
$lang['client_users.username'] = "E-mail";
$lang['client_users.name_surname'] = "İsim Soyisim";
$lang['client_users.password'] = "Parola";
$lang['client_users.date'] = "Doğumgünü";
$lang['client_users.phone'] = "Telefon";
$lang['client_users.phone_2'] = "Dahili";
$lang['client_users.locale'] = "Dil";
$lang['client_users.status'] = "Durum";
$lang['client_users.file'] = "Fotoğraf";
$lang['client_users.created_at'] = "Oluşturma";
$lang['client_users.updated_at'] = "Güncelleme";

$lang['food_list.message'] = "Yemek Listesi";
$lang['food_list.date'] = "Tarih";
$lang['food_list.content'] = "İçerik";
$lang['food_list.status'] = "Durum";
$lang['food_list.created_at'] = "Oluşturma";
$lang['food_list.updated_at'] = "Güncelleme";

$lang['education.message'] = "Etkinlik";
$lang['education.department'] = "Departman";
$lang['education.title'] = "Başlık";
$lang['education.start_date'] = "Başlangıç Tarihi";
$lang['education.end_date'] = "Bitiş Tarihi";
$lang['education.location'] = "Lokasyon";
$lang['education.content'] = "İçerik";
$lang['education.status'] = "Durum";
$lang['education.created_at'] = "Oluşturma";
$lang['education.updated_at'] = "Güncelleme";

$lang['leaves.message'] = "Etkinlik";
$lang['leaves.department'] = "Departman";
$lang['leaves.title'] = "Başlık";
$lang['leaves.start_date'] = "Başlangıç Tarihi";
$lang['leaves.end_date'] = "Bitiş Tarihi";
$lang['leaves.location'] = "Lokasyon";
$lang['leaves.content'] = "İçerik";
$lang['leaves.status'] = "Durum";
$lang['leaves.created_at'] = "Oluşturma";
$lang['leaves.updated_at'] = "Güncelleme";

$lang['events.message'] = "Etkinlikler";
$lang['events.department'] = "Departman";
$lang['events.title'] = "Başlık";
$lang['events.start_date'] = "Başlangıç Tarihi";
$lang['events.end_date'] = "Bitiş Tarihi";
$lang['events.location'] = "Lokasyon";
$lang['events.content'] = "İçerik";
$lang['events.status'] = "Durum";
$lang['events.created_at'] = "Oluşturma";
$lang['events.updated_at'] = "Güncelleme";

$lang['campaigns.message'] = "Kampanyalar";
$lang['campaigns.department'] = "Departman";
$lang['campaigns.title'] = "Başlık";
$lang['campaigns.start_date'] = "Başlangıç Tarihi";
$lang['campaigns.end_date'] = "Bitiş Tarihi";
$lang['campaigns.location'] = "Vakıf ve Firma";
$lang['campaigns.content'] = "İçerik";
$lang['campaigns.status'] = "Durum";
$lang['campaigns.created_at'] = "Oluşturma";
$lang['campaigns.updated_at'] = "Güncelleme";

$lang['surveys.message'] = "Anket";
$lang['surveys.department'] = "Departman";
$lang['surveys.title'] = "Başlık";
$lang['surveys.start_date'] = "Başlangıç Tarihi";
$lang['surveys.end_date'] = "Bitiş Tarihi";
$lang['surveys.location'] = "Vakıf ve Firma";
$lang['surveys.content'] = "İçerik";
$lang['surveys.status'] = "Durum";
$lang['surveys.created_at'] = "Oluşturma";
$lang['surveys.updated_at'] = "Güncelleme";

$lang['survey_items.title'] = "Başlık";
$lang['survey_items.survey_id'] = "Anket";
$lang['survey_items.options'] = "Seçenekler";

$lang['meetings.message'] = "Anket";
$lang['meetings.department'] = "Departman";
$lang['meetings.title'] = "Başlık";
$lang['meetings.start_date'] = "Başlangıç Tarihi";
$lang['meetings.end_date'] = "Bitiş Tarihi";
$lang['meetings.location'] = "Vakıf ve Firma";
$lang['meetings.content'] = "İçerik";
$lang['meetings.status'] = "Durum";
$lang['meetings.created_at'] = "Oluşturma";
$lang['meetings.updated_at'] = "Güncelleme";

$lang['meeting_items.title'] = "Başlık";
$lang['meeting_items.survey_id'] = "Toplantı";
$lang['meeting_items.options'] = "Kararlar";

$lang['news.message'] = "Haberler";
$lang['news.department'] = "Departman";
$lang['news.title'] = "Başlık";
$lang['news.content'] = "İçerik";
$lang['news.file'] = "Görsel";
$lang['news.user_id'] = "Oluşturan";
$lang['news.status'] = "Durum";
$lang['news.created_at'] = "Oluşturma";
$lang['news.updated_at'] = "Güncelleme";

$lang['pages.message'] = "Sayfalar";
$lang['pages.department'] = "Departman";
$lang['pages.category_ref_id'] = "Kategori";
$lang['pages.title'] = "Başlık";
$lang['pages.permalink'] = "Permalink";
$lang['pages.origin'] = "Kaynak Dil";
$lang['pages.target'] = "Hedef Dil";
$lang['pages.service'] = "Hizmet";
$lang['pages.faq'] = "S.S.S";
$lang['pages.keywords'] = "Meta Keywords";
$lang['pages.description'] = "Meta Açıklama";
$lang['pages.content'] = "İçerik";
$lang['pages.files'] = "Ekler";
$lang['pages.status'] = "Durum";
$lang['pages.created_at'] = "Oluşturma";
$lang['pages.updated_at'] = "Güncelleme";

$lang['page_signs.client_user_id'] = "Çalışan";
$lang['page_signs.page_id'] = "Döküman";
$lang['page_signs.created_at'] = "Oluşturma";
$lang['page_signs.updated_at'] = "Güncelleme";

$lang['announcements.message'] = "Duyurular";
$lang['announcements.department'] = "Departman";
$lang['announcements.title'] = "Başlık";
$lang['announcements.content'] = "İçerik";
$lang['announcements.status'] = "Durum";
$lang['announcements.created_at'] = "Oluşturma";
$lang['announcements.updated_at'] = "Güncelleme";

$lang['users.username'] = "Kullanıcı adı";
$lang['users.name_surname'] = "Ad Soyad";
$lang['users.e_mail'] = "E-mail";
$lang['users.mobile'] = "Cep";
$lang['users.password'] = "Parola";
$lang['users.status'] = "Durum";
$lang['users.auths_list'] = "Yetkilendirmeler";
$lang['users.created_at'] = "Oluşturma";
$lang['users.updated_at'] = "Güncelleme";

$lang['titles.title'] = "Başlık";
$lang['titles.department'] = "Departman";
$lang['titles.status'] = "Durum";
$lang['titles.auths_list'] = "İzinler";
$lang['titles.created_at'] = "Oluşturma";
$lang['titles.updated_at'] = "Güncelleme";

$lang['transaction_count.id'] = "ID";
$lang['transaction_count.barcode'] = "Barkod";
$lang['transaction_count.created_at'] = "Son Güncelleme";
$lang['transaction_count.counted'] = "Sayılan";

$lang['shelfs.type'] = "Raf Tipi";
$lang['shelfs.name'] = "Raf İsmi";
$lang['APPLICATIONS.Shelfs'] = "Raflar";
$lang['APPLICATIONS.Add Shelfs'] = "Raf Ekle";
$lang['APPLICATIONS.Shelfs List'] = "Rafları Listele";
$lang['APPLICATIONS.Edit Shelfs'] = "Rafları Düzenle";
$lang['shelfs.shelf'] = "Raf";
$lang['shelfs.section'] = "Bölüm";
$lang['shelfs.parent'] = "Bağlı Bölüm";

$lang['warehouses.warehouse_name'] = "Depo İsmi";
$lang['APPLICATIONS.Warehouses'] = "Depolar";
$lang['APPLICATIONS.Add Warehouses'] = "Depo Ekle";
$lang['APPLICATIONS.Warehouses List'] = "Depoları Listele";
$lang['APPLICATIONS.Edit Warehouses'] = "Depoları Düzenle";


$lang['TEXT.Social Experience'] = 'Sosyal İntranet';
$lang['TEXT.Min'] = 'En düşük';
$lang['TEXT.Max'] = 'En yüksek';

$lang['AUTH.Login'] = 'Giriş Yap';
$lang['AUTH.Log out'] = 'Çıkış Yap';
$lang['AUTH.Log out_sub'] = 'Hesabınızdan çıkış yapın.';
$lang['AUTH.Forgot password?'] = 'Şifremi Unuttum?';

$lang['BASE.turkish'] = 'Türkçe';
$lang['BASE.english'] = 'İngilizce';
$lang['BASE.add'] = 'Ekle';
$lang['BASE.edit'] = 'Düzenle';
$lang['BASE.update'] = 'Güncelle';
$lang['BASE.all'] = 'Tümü';
$lang['BASE.close'] = 'Kapat';
$lang['BASE.active'] = 'Aktif';
$lang['BASE.passive'] = 'Pasif';
$lang['BASE.search'] = 'Arama';
$lang['BASE.filter'] = 'filtre';
$lang['BASE.clear'] = 'Temizle';
$lang['BASE.renew'] = 'Yenile';
$lang['BASE.select'] = 'Seç';
$lang['BASE.translations'] = 'Çeviriler';
$lang['BASE.apply'] = 'Başvur';
$lang['BASE.send'] = 'Gönder';
$lang['BASE.profile'] = 'Profilim';
$lang['BASE.hello'] = 'Merhaba';

$lang['ACTION.add'] = 'Ekle';
$lang['ACTION.update'] = 'Güncelle';
$lang['ACTION.delete'] = 'Sil';
$lang['ACTION.add_row'] = 'Satır Ekle';

$lang['MESSAGE.Processing'] = 'İşleniyor';
$lang['MESSAGE.Wait'] = 'Lütfen biraz bekleyin';
$lang['MESSAGE.Added'] = 'Başarıyla Eklendi!';
$lang['MESSAGE.Applied'] = 'Başvuru Tamamlandı!';
$lang['MESSAGE.Updated'] = 'Başarıyla güncellendi!';
$lang['MESSAGE.SysError'] = 'Sistem hatası!';
$lang['MESSAGE.Duplicated'] = 'Başarıyla Kopyalandı!';
$lang['MESSAGE.Error'] = 'Hata!';
$lang['MESSAGE.Login Successfully !'] = 'Giriş Başarılı !';
$lang['MESSAGE.Redirecting'] = 'Yönlendiriliyor';
$lang['MESSAGE.SendPasswordMail'] = 'Yeni şifre mail adresine gönderildi ';
$lang['MESSAGE.InvalidMail'] = 'Geçersiz e-posta adresi!';
$lang['MESSAGE.Signature'] = 'Okudum Onayladım';

$lang['FRONT.Feed'] = 'Ana Sayfa';
$lang['FRONT.Panel'] = 'Yönetim';
$lang['FRONT.Menu'] = 'Menü';
$lang['FRONT.News'] = 'Haberler';
$lang['FRONT.News_sub'] = 'Departman Haberleri';
$lang['FRONT.Education'] = 'Eğitim';
$lang['FRONT.Events'] = 'Etkinlikler';
$lang['FRONT.Events_sub'] = 'Planlanan Etkinlikler ';
$lang['FRONT.Food List'] = 'Yemek Listesi';
$lang['FRONT.Food List_sub'] = 'Aylık';
$lang['FRONT.Leave Apply'] = 'İzin Talebi';
$lang['FRONT.Expense bill'] = 'Gider Fişi';
$lang['FRONT.Requests'] = 'Talepler';
$lang['FRONT.Leave Apply_sub'] = 'Başvuru Takibi';
$lang['FRONT.My Department'] = 'Ailemiz';
$lang['FRONT.My Department_sub'] = 'Çalışanlar, Bildirimler';

$lang['FRONT.Campaign'] = 'Kampanyalar';
$lang['FRONT.Notices'] = 'Bildirimler';
$lang['FRONT.Birthdays'] = 'Doğumgünleri';
$lang['FRONT.Employees'] = 'Çalışan';

$lang['FRONT.Publish'] = 'Yayınla';
$lang['FRONT.Media'] = 'Medya';
$lang['FRONT.Video'] = 'Video';

$lang['FRONT.Load More'] = 'Daha fazla yükle';
$lang['FRONT.Read More'] = 'Daha fazla oku';

$lang['FRONT.newjob_title'] = ' <h4>%1$s</h4> <br> <p> %2$s <br> olarak aramıza katıldı</p>';
$lang['FRONT.You might like'] = 'İlgini çekebilir';
$lang['FRONT.Exchange Rate'] = 'Döviz Kurları';
$lang['FRONT.Exchange Rate_buy'] = 'ALIŞ';
$lang['FRONT.Exchange Rate_sell'] = 'SATIŞ';

$lang['COMPOSE.write line'] = 'Birşeyler yaz...';
$lang['COMPOSE.Time Line'] = 'Zaman çizelgesi';
$lang['COMPOSE.SHARE.Public'] = 'Genel';
$lang['COMPOSE.SHARE.My Department'] = 'Departmanım';
$lang['COMPOSE.SHARE.Publish Content'] = 'İçeriği Yayınla';

$lang['TIME.second'] = 'ikinci';
$lang['TIME.minute'] = 'dakika';
$lang['TIME.hour'] = 'saat';
$lang['TIME.day'] = 'gün';
$lang['TIME.month'] = 'ay';
$lang['TIME.year'] = 'yıl';
$lang['TIME.ago'] = ' önce ';



