<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Language extends CI_Controller
{
    public function Change($lang_id, $redirect = NULL)
    {
        language_selector($lang_id, $redirect);
    }

    public function index()
    {
        redirect(base_url('home'));
    }

    public function Referrer()
    {

    }


    public function show_404()
    {

        $this->output->set_status_header('404');
        front_view_engine(['site/base/404']);

    }

}