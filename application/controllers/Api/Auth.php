<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    public function Login()
    {
        $response = ['status'=>1, 'message'=>'Login başarılı'];

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($response));
    }
}