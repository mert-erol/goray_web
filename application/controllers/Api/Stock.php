<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Stock extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function Count($barcode, $quantity = 0)
    {
        $response = ['status' => 0, 'message' => ''];
        $product = $this->crud_model->get('products', ['barcode' => $barcode]);

        if (count($product) > 0) {

            $this->crud_model->set('transaction_counts', [
                'user_id' => 1,
                'warehouse_id' => 1,
                'amount' => 1,
                'product_id' => $product[0]['id']
            ]);

            $response = ['status' => 1, 'message' => 'Ürün Sayım Kuyruğuna Eklendi'];

        } else {
            $response = ['status' => 0, 'message' => 'Ürün Bulunamadı'];
        }

        $this->output->set_content_type('application/json')
            ->set_output(json_encode($response));
    }

    public function Sync($barcode)
    {
        $response = ['status' => 0, 'message' => ''];
        $product = $this->crud_model->get('products', ['barcode' => $barcode]);

        if (count($product) > 0) {

            $this->db->where('product_id', $product[0]['id']);
            $this->db->select('sum(amount) counted');
            $total_counted_product = $this->db->get('transaction_counts');
            $total_counted_product = intval($total_counted_product->counted);

            if($this->StockControl($barcode) != $total_counted_product){
                $response = ['status' => 0, 'message' => 'Ürün sayısı eşit değildir'];
            }
            else{
                if ($total_counted_product > 0) {

                    $this->crud_model->set('transaction_stocks', [
                        'product_id' => $product[0]['id'],
                        'type' => 3,
                        'amount' => $total_counted_product
                    ]);

                    $this->crud_model->set('transaction_stocks', [
                        'product_id' => $product[0]['id'],
                        'type' => 4,
                        'amount' => $total_counted_product
                    ]);
                }
                $response = ['status' => 1, 'message' => 'Ürün Eşitleme Tamamlandı'];
            }
        }
        else {
            $response = ['status' => 0, 'message' => 'Ürün Eşitlenemedi'];
        }
    }

    private function StockControl($barcode)
    {
        $response = ['status' => 0, 'message' => ''];
        $product = $this->crud_model->get('products', ['barcode' => $barcode]);

        $this->db->where('product_id', $product[0]['id']);
        $stocks = $this->db->get('transaction_stocks')->result();

        $product_available_stock = 0;
        foreach ($stocks as $stock) {
            /*
             * 1:giriş
             * 2: çıkış
             *
             * */

            if ($stock->type == 1) {
                $product_available_stock += $stock->amount;
            }

            if ($stock->type == 2) {
                $product_available_stock -= $stock->amount;
            }
        }
        return $product_available_stock;
    }
}