<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Sitemap extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $xmlString = '<?xml version="1.0" encoding="UTF-8"?>
                    <urlset
                 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                 xmlns:image="http://www.google.com/schemas/sitemap-image/1.1"
                 xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"
                 xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

        $pages = get_pages();
        foreach ($pages as $page) {
            $xmlString .= '<url>';
            $xmlString .= '<loc>' . base_url($page['permalink']) . '</loc>';
            $xmlString .= '</url>';
        }


        $xmlString .= '</urlset>';



        $dom = new DOMDocument;
        $dom->preserveWhiteSpace = FALSE;
        $dom->loadXML($xmlString);
        if ($dom->save(FCPATH . '/sitemap.xml')) {
            echo "<h2>Site Map Created SuccessFully</h2>";
        } else {
            echo "<h2>Site Map Created Failed</h2>";
        }
    }

}