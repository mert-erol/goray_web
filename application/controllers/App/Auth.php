<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Auth extends CI_Controller
{

    private $mainDb = 'users';

    public function index()
    {
        redirect(base_url('App/Dashboard'));
        $this->load->view('app/base/login', array('appName' => 'login-form'));
    }


    public function Login()
    {

        redirect(base_url('Front/Auth'));

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $this->form_validation->set_rules('username', 'Kullanıcı Adı', 'required');
            $this->form_validation->set_rules('password', 'Şifre', 'required');


            if ($this->form_validation->run() == FALSE) {

                $res = array('result' => 0, 'message' => validation_errors());

            } else {

                $isUser = $this->crud_model->get($this->mainDb, array(
                    'username' => $this->input->post('username'),
                    'password' => md5($this->input->post('password')),
                    'status' => 1

                ));

                if (count($isUser) > 0) {
                    $res = array('result' => 1, 'message' => 'Giriş Yapıldı! Yönlendiriliyor');
                    $this->session->set_userdata('app_login', TRUE);
                    $this->session->set_userdata('app_user_data', $isUser[0]);
                    my_logs('<b>' . $this->input->post('username') . '</b> Giriş Yaptı!');
                } else {
                    $res = array('result' => 0, 'message' => 'Hatalı Giriş!');
                    my_logs('<span class="text-danger"><b>' . $this->input->post('username') . ' </b>Hatalı Giriş!</span> ');
                }
            }
            echo json_encode($res);
        } else {
            redirect('App');
        }
    }

    public function Referrer()
    {
        /*
        if (strlen(get_cookie('app_last_page')) > 0) {
            redirect(get_cookie('app_last_page'));
        } else {
            redirect('App/Dashboard');
        }*/
    }

    public function Logout()
    {
        redirect(base_url('Front/Auth/Logout'));
        my_logs('<b>' . app_usr('username') . '</b> Çıkış Yaptı!');
        $this->session->set_userdata('app_login', FALSE);
        $this->session->set_userdata('app_user_data', array());
        redirect(base_url('App'));
    }


}