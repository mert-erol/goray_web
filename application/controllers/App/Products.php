<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller
{
    private $mainDb = 'products';
    private $categoriesDb = 'product_categories';
    private $variationsDb = 'product_variations';
    private $mediasDb = 'multimedia';


    public function __construct()
    {
        parent::__construct();
        app_login_check();
        $this->cols = $this->schema_model->get_columns($this->mainDb);
        $this->colsCats = $this->schema_model->get_columns($this->categoriesDb);
        $this->colsVars = $this->schema_model->get_columns($this->variationsDb);
        $this->colsDt = $this->schema_model->get_columns_datatables($this->mainDb);
        $this->colsDtCats = $this->schema_model->get_columns_datatables($this->categoriesDb);
        $this->colsDtVars = $this->schema_model->get_columns_datatables($this->variationsDb);
    }


    public function index()
    {
        view_engine(array('app/products/list'), array(12), array('cols' => $this->colsDt, 'appName' => 'list-product'));
    }


    public function Set()
    {

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            foreach ($this->input->post() as $col => $val) {
                $this->form_validation->set_rules($col, $this->cols[$col]['comment'], $this->cols[$col]['is_null'] == 'NO' ? 'required|trim' : '');
            }

            if ($this->form_validation->run() == FALSE) {
                $res = array('result' => 0, 'message' => validation_errors());
            } else {
                $query = $this->crud_model->set($this->mainDb, $this->input->post());
                $res = array('result' => $query, 'message' => $query ? ln('MESSAGE.Added') : ln('MESSAGE.SysError'), 'last_id' => $this->db->insert_id());
            }

            echo json_encode($res);

        } else {

            view_engine(array('app/products/new'), array(12), array('cols' => $this->cols, 'appName' => 'new-product'));
        }
    }

    public function Show($id)
    {


        if ($this->input->server('REQUEST_METHOD') == 'POST') {


        } else {

            $product = $this->crud_model->get($this->mainDb, array('id' => $id));
            $product[0]['variations'] = $this->crud_model->get($this->variationsDb, array('product' => $id));


            view_engine(
                array('app/products/search-products'), array(12),
                array('cols' => $this->colsDtCats,
                    'appName' => 'search-products',
                    'data' => $product));

        }

    }


    public function SetVariations($productId)
    {

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $this->form_validation->set_rules('type', 'Varyasyon Tipi', 'required|trim');
            $this->form_validation->set_rules('unit', 'Varyasyon Birimi', 'required|trim');
            $this->form_validation->set_rules('name', 'Varyasyon Açıklaması', 'required|trim');
            $this->form_validation->set_rules('price', 'Varyasyon Fiyatı', 'required|trim');

            if ($this->form_validation->run() == FALSE) {
                $res = array('result' => 0, 'message' => validation_errors());
            } else {
                $_POST['product'] = $productId;
                $query = $this->crud_model->set($this->variationsDb, $this->input->post());
                $res = array('result' => $query, 'message' => $query ? 'Varyasyon Eklendi !' : ln('MESSAGE.SysError'), 'last_id' => $this->db->insert_id());
            }

            echo json_encode($res);

        }
    }

    public function Edit($id)
    {


        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            foreach ($this->input->post() as $col => $val) {
                $this->form_validation->set_rules($col, $this->cols[$col]['comment'], $this->cols[$col]['is_null'] == 'NO' ? 'required|trim' : '');
            }

            if ($this->form_validation->run() == FALSE) {
                $res = array('result' => 0, 'message' => validation_errors());
            } else {
                $query = $this->crud_model->update($this->mainDb, array('id' => $id), $this->input->post());
                log_message('error', $this->db->last_query());
                $res = array('result' => $query, 'message' => $query ? ln('MESSAGE.Updated') : ln('MESSAGE.SysError'));
            }

            echo json_encode($res);

        } else {
            $data = $this->crud_model->get($this->mainDb, array('id' => $id));
            view_engine(array('app/products/edit', 'app/products/variations/variations', 'app/products/medias', 'app/products/new_media'), array(8, 4, 6, 6), array('cols' => $this->cols, 'appName' => 'edit-products', 'id' => $id, 'data' => $data[0]));
        }
    }

    public function Datatables()
    {

        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $search = $this->input->post("search");
        $search = $search['value'];
        $col = 0;
        $dir = "";

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }


        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }
        $valid_columns = array();

        foreach ($this->colsDt as $cName => $cDt):
            $valid_columns[] = $cDt['name'];
        endforeach;

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }
        if ($order != null) {
            $this->db->order_by($order, $dir);
        }


        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $this->db->like($sterm, $search);
                } else {
                    $this->db->or_like($sterm, $search);
                }
                $x++;
            }
        }


        $this->db->limit($length, $start);
        $getDtData = $this->db->get($this->mainDb);

        $data = $getDtData->result();

        $total = $this->db->get($this->mainDb)->num_rows();

        $result = array(
            'draw' => $draw,
            'iTotalRecords' => $total,
            'iTotalDisplayRecords' => count($oData),
            'sEcho' => 0,
            'sColumns' => 0,
            'aaData' => $data
        );

        echo json_encode($result);
        exit();
    }

    public function CategoriesDatatables()
    {

        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $search = $this->input->post("search");
        $search = $search['value'];
        $col = 0;
        $dir = "";

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }


        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        $valid_columns = array();

        foreach ($this->colsDtCats as $cName => $cDt):
            $valid_columns[] = $cDt['name'];
        endforeach;

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }
        if ($order != null) {
            $this->db->order_by($order, $dir);
        }


        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $this->db->like($sterm, $search);
                } else {
                    $this->db->or_like($sterm, $search);
                }
                $x++;
            }
        }


        $this->db->limit($length, $start);
        $getDtData = $this->db->get($this->categoriesDb);

        $data = $getDtData->result();

        $total = $this->db->get($this->categoriesDb)->num_rows();

        $result = array(
            'draw' => $draw,
            'iTotalRecords' => $total,
            'iTotalDisplayRecords' => count($oData),
            'sEcho' => 0,
            'sColumns' => 0,
            'aaData' => $data
        );

        echo json_encode($result);
        exit();
    }

    public function VariationsDatatables($productId)
    {

        $draw = intval($this->input->post("draw"));
        $start = 0;
        $length = 10;
        $order = $this->input->post("order");
        $search = $this->input->post("search");
        $search = $search['value'];
        $col = 0;
        $dir = "";

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }


        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        $valid_columns = array();

        foreach ($this->colsDtVars as $cName => $cDt):
            $valid_columns[] = $cDt['name'];
        endforeach;

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }
        if ($order != null) {
            $this->db->order_by($order, $dir);
        }


        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $this->db->like($sterm, $search);
                } else {
                    $this->db->or_like($sterm, $search);
                }
                $x++;
            }
        }


        $this->db->limit($length, $start);
        $getDtData = $this->db->where('product', $productId)->get($this->variationsDb);

        $data = $getDtData->result();

        $total = $this->db->where('product', $productId)->get($this->variationsDb)->num_rows();

        $result = array(
            'draw' => $draw,
            'iTotalRecords' => $total,
            'iTotalDisplayRecords' => count($oData),
            'sEcho' => 0,
            'sColumns' => 0,
            'aaData' => $data
        );

        echo json_encode($result);
        exit();
    }


    public function Media($id = NULL)
    {
        if ($_FILES) {
            $files = $_FILES['files'];
            $file_path = $files['tmp_name'][0];
            $file_name = date('YmdHis') . '_' . media_permalink($_POST['name']);
            move_uploaded_file($file_path, "./uploads/$this->mainDb/" . $file_name);
            $query = $this->crud_model->set('multimedia', array('table' => $this->mainDb, 'relation_id' => $id, 'name' => $file_name));
        }

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $this->db->select('id, name, status');
            $this->db->where(array('relation_id' => $id, 'table' => $this->input->post('app_name')));
            $data = $this->db->get('multimedia')->result();


            $result = array(
                'meta' => array(
                    'page' => 1,
                    'pages' => 1,
                    'perpage' => -1,
                    'total' => 50,
                    'sort' => 'asc',
                    'field' => 'id'
                ),

                'data' => $data
            );

            echo json_encode($result);
        }
    }

    public function Categories()
    {

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            foreach ($this->input->post() as $col => $val) {
                $this->form_validation->set_rules($col, $this->colsCats[$col]['comment'], $this->colsCats[$col]['is_null'] == 'NO' ? 'required|trim' : '');
            }

            if ($this->form_validation->run() == FALSE) {
                $res = array('result' => 0, 'message' => validation_errors());
            } else {
                $query = $this->crud_model->set($this->categoriesDb, $this->input->post());
                $res = array('result' => $query, 'message' => $query ? ln('MESSAGE.Added') : ln('MESSAGE.SysError'), 'last_id' => $this->db->insert_id());
            }

            echo json_encode($res);

        } else {

            view_engine(array('app/products/categories/new', 'app/products/categories/list'), array(12, 12), array('cols' => $this->colsDtCats, 'appName' => 'new-category'));

        }

    }

    public function Category($cat_id, $delete = FALSE)
    {

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            if ($delete === FALSE) {
                $this->form_validation->set_rules('title', 'Kategori Adı', 'required|trim');

                if ($this->form_validation->run() == FALSE) {
                    $res = array('result' => 0, 'message' => validation_errors());
                } else {

                    $query = $this->crud_model->update($this->categoriesDb, array('id' => $cat_id), $this->input->post());

                    $res = array('result' => $query, 'message' => $query ? 'Kategori Güncellendi !' : ln('MESSAGE.SysError'));
                }

                echo json_encode($res);
            }

            if ($delete == 'Delete') {

                $this->form_validation->set_rules('id', 'Kategori ID', 'required|trim');
                $id = $this->input->post('id');

                $res = 0;
                if ($this->form_validation->run() !== FALSE) {
                    $data = $this->crud_model->get($this->categoriesDb, ['id' => $id]);
                    my_logs(app_usr('username', 'strong') . ' kullanıcısı <strong>' . $data[0]['title'] . '</strong> kategorisini sildi.', 'danger');
                    $res = $id > 0 ? $this->crud_model->delete($this->categoriesDb, array('id' => $id)) : 0;
                }

                echo $res;
            }


        } else {

            $data = $this->crud_model->get($this->categoriesDb, ['id' => $cat_id]);

            view_engine(array('app/products/categories/edit'), array(12), array('cols' => $this->colsDtCats, 'appName' => 'update-category', 'data' => $data[0]));

        }

    }

    public function EditVariations($var_id)
    {


        if ($this->input->server('REQUEST_METHOD') == 'POST') {


            $this->form_validation->set_rules('type', 'Varyasyon Tipi', 'required|trim');
            $this->form_validation->set_rules('unit', 'Varyasyon Birimi', 'required|trim');
            $this->form_validation->set_rules('name', 'Varyasyon Açıklaması', 'required|trim');
            $this->form_validation->set_rules('price', 'Varyasyon Fiyatı', 'required|trim');

            if ($this->form_validation->run() == FALSE) {
                $res = array('result' => 0, 'message' => validation_errors());
            } else {

                $query = $this->crud_model->update($this->variationsDb, array('id' => $var_id), $this->input->post());
                $res = array('result' => $query, 'message' => $query ? 'Varyasyon Güncellendi !' : ln('MESSAGE.SysError'), 'last_id' => $this->db->insert_id());
            }

            echo json_encode($res);


        } else {

            $data = $this->crud_model->get($this->variationsDb, array('id' => $var_id));

            view_engine(array('app/products/variations/edit'), array(12), array('cols' => $this->colsDtVars, 'appName' => 'edit-variations', 'data' => $data[0]));

        }

    }

    public function Delete()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $id = intval($this->input->post('prodId'));
            $data = $this->crud_model->get($this->mainDb, array('id' => $id));

            my_logs(app_usr('username', 'strong') . ' kullanıcısı <strong>' . $data[0]['product_name'] . '</strong> ürününü sildi.', 'danger');

            echo $id > 0 ? $this->crud_model->delete($this->mainDb, array('id' => $id)) : 0;
            $this->crud_model->delete($this->variationsDb, array('product' => $id));
            $this->crud_model->delete($this->mediasDb, array('relation_id' => $id, 'table' => 'products'));

        }
    }

    public function DeleteVariation()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $varId = intval($this->input->post('varId'));
            $varData = $this->crud_model->get($this->variationsDb, array('id' => $varId));
            my_logs(app_usr('username', 'strong') . ' kullanıcısı <strong>' . $varData[0]['name'] . '</strong> varyasyonunu sildi.', 'danger');
            echo $varId > 0 ? $this->crud_model->delete($this->variationsDb, array('id' => $varId)) : 0;

        }
    }

    public function SetCategory()
    {


        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            foreach ($this->input->post() as $col => $val) {
                $this->form_validation->set_rules($col, $this->colsCats[$col]['comment'], $this->colsCats[$col]['is_null'] == 'NO' ? 'required|trim' : '');
            }

            if ($this->form_validation->run() == FALSE) {
                $res = array('result' => 0, 'message' => validation_errors());
            } else {
                $query = $this->crud_model->set($this->categoriesDb, $this->input->post());
                $res = array('result' => $query, 'message' => $query ? ln('MESSAGE.Added') : ln('MESSAGE.SysError'), 'last_id' => $this->db->insert_id());
            }

            echo json_encode($res);

        } else {

            view_engine(array('app/products/categories/new', 'app/products/categories/list'), array(12, 12), array('cols' => $this->colsDtCats, 'appName' => 'new-category'));

        }

    }


    public function GetCategories()
    {
        $cats = $this->crud_model->get($this->categoriesDb, array('status' => 1));
        sort($cats);
        print_r(json_encode($cats));

    }

    public function GetProducts()
    {
        $data = $this->crud_model->get($this->mainDb);
        sort($data);
        print_r(json_encode($data));

    }


}

