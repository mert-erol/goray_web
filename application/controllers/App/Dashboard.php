<?php

class Dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        app_login_check();

    }

    public function index()
    {

        $auth_list = $this->session->userdata('app_user_data')['auths_list'];
        $auth_list = json_decode($auth_list, TRUE);


        $dashCards = [];
        foreach ($auth_list as $itm => $stat) {
            $exploded = explode('-', $itm);
            if (strpos($exploded[0], 'list') !== FALSE) {
                $get_app = $this->crud_model->get('applications', ['name' => $itm])[0];
                $parent = $this->crud_model->get('applications', ['id' => $get_app['parent']])[0];
                $get_app['icon'] = $parent['icon'];
                $get_app['parent_title'] = $parent['title'];
                $dashCards[] = $get_app;
            }
        }


        view_engine(array('app/dashboard/card'), array(3), [
            'cardData' => $dashCards
        ]);
    }

    public function test()
    {

      phpinfo();
        

    }

}