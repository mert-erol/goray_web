<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PageSigns extends CI_Controller
{
    private $mainDb = 'page_signs';

    public function __construct()
    {
        parent::__construct();
        app_login_check();
        $this->cols = $this->schema_model->get_columns($this->mainDb);
        $this->colsDt = $this->schema_model->get_columns_datatables($this->mainDb);
    }

    public function index($client_user_id = NULL)
    {
        app_method_check('ClientUsers', 'list');
        view_engine(array('app/page-signs/list'), array(12), array('cols' => $this->colsDt, 'mainTbl' => $this->mainDb, 'client_user_id' => $client_user_id));
    }

    public function Datatables($client_user_id = NULL)
    {
        app_method_check('ClientUsers', 'list');

        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $search = $this->input->post("search");
        $search = $search['value'];
        $col = 0;
        $dir = "";

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }


        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }
        $valid_columns = array();

        foreach ($this->colsDt as $cName => $cDt):
            $valid_columns[] = $cDt['name'];
        endforeach;

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }
        if ($order != null) {
            $this->db->order_by($order, $dir);
        }


        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $this->db->like($sterm, $search);
                } else {
                    $this->db->or_like($sterm, $search);
                }
                $x++;
            }
        }


        $this->db->limit($length, $start);
        if ($client_user_id != NULL) {
            $this->db->where('ed.client_user_id', intval($client_user_id));
        }
        $this->db->select('ed.id, cu.name_surname as client_user_id, pg.title as page_id, ed.created_at, ed.updated_at ');
        $this->db->join('pages pg', 'ed.page_id=pg.id', 'left');
        $this->db->join('client_users cu', 'ed.client_user_id=cu.id', 'left');
        $this->db->from($this->mainDb . ' ed');
        $getDtData = $this->db->get();
        $data = $getDtData->result_array();

        $total = $this->db->get($this->mainDb)->num_rows();

        $result = array(
            'draw' => $draw,
            'iTotalRecords' => $total,
            'iTotalDisplayRecords' => count($data),
            'sEcho' => 0,
            'sColumns' => 0,
            'aaData' => $data
        );

        echo json_encode($result);
        exit();
    }
}

