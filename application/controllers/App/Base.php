<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 28.10.2019
 * Time: 14:06
 */

class Base extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        app_login_check();
    }

    public function DeleteMedia()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $mediaId = intval($this->input->post('mediaId'));

            $getFile = $this->crud_model->get('multimedia', array('id' => $mediaId));
            unlink('./uploads/' . $getFile[0]['table'] . '/' . $getFile[0]['name']);

            echo $mediaId > 0 ? $this->crud_model->delete('multimedia', array('id' => $mediaId)) : 0;
        }
    }

    public function SetFeaturedMedia()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $mediaId = intval($this->input->post('mediaId'));
            $getRelation = $this->crud_model->get('multimedia', array('id' => $mediaId));


            $mediaId > 0 ? $this->crud_model->update('multimedia', array('id!=' => $mediaId, 'table' => $getRelation[0]['table'], 'relation_id' => $getRelation[0]['relation_id']), array('status' => 0)) : 0;

            $mediaId > 0 ? $this->crud_model->update('multimedia', array('id' => $mediaId), array('status' => 1)) : 0;

            echo 1;

        }
    }

    public function Languages()
    {
        $data = $this->crud_model->get('languages');
        sort($data);
        print_r(json_encode($data));

    }

    public function sess2()
    {
        pr2($_SESSION);
    }

    public function test()
    {

        exit();

        include APPPATH . 'third_party/SimpleXLSX.php';

        echo '<h1>Parse *.xslx</h1><pre>';

        if ($xlsx = SimpleXLSX::parse('./uploads/files/cari.xlsx')) {

            $rows = $xlsx->rows();

            for ($i = 1; $i < count($rows); $i++) {

                $nRow = array();

                //echo count($rows[$i]).'<br>';

                for ($x = 0; $x < count($rows[$i]); $x++) {

                    $nRow[$rows[0][$x]] = $rows[$i][$x];
                }

                print_r($nRow);

                // $this->crud_model->set('customers', $nRow);

            }

        } else {
            echo SimpleXLSX::parseError();
        }
        echo '<pre>';
    }

    public function curl_t()
    {


        echo my_curl(base_url('App/Base/api2'), array('ad' => 'sefa'));
    }


    public function api2()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            echo json_encode($_POST);

        } else {
            echo 'gett';
        }
    }


}