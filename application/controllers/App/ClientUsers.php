<?php

class ClientUsers extends CI_Controller
{
    private $mainDb = 'client_users';

    public function __construct()
    {
        parent::__construct();
        app_login_check();
        $this->cols = $this->schema_model->get_columns($this->mainDb);
        $this->colsDt = $this->schema_model->get_columns_datatables($this->mainDb);
        $this->data['locales'] = $this->crud_model->get('languages');
    }

    public function index()
    {
        app_method_check(__CLASS__, 'list');
        view_engine(array('app/client-users/list'), array(12), array('cols' => $this->colsDt, 'mainTbl' => $this->mainDb));
    }

    public function Set($customerID = NULL)
    {
        app_method_check(__CLASS__, 'new');

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            foreach ($this->input->post() as $col => $val) {
                $this->form_validation->set_rules($col, $this->cols[$col]['comment'], $this->cols[$col]['is_null'] == 'NO' ? 'required' : 'trim');
            }

            if ($this->form_validation->run() == FALSE) {
                $res = array('result' => 0, 'message' => validation_errors());
            } else {

                if (count($this->crud_model->get($this->mainDb, array('username' => $this->input->post('username')))) > 0) {
                    $res = array('result' => 0, 'message' => 'Kullanıcı adı mevcut!');
                } else {

                    $_POST['password'] = md5($this->input->post('password'));
                    $_POST['date'] = date2db($_POST['date']);

                    $query = $this->crud_model->set($this->mainDb, $this->input->post());

                    if ($query) {
                        $res = array('result' => $query, 'message' => ln('MESSAGE.Added'), 'last_id' => $this->db->insert_id());
                    } else {
                        $res = array('result' => $query, 'message' => ln('MESSAGE.SysError'));
                    }
                }
            }

            echo json_encode($res);

        } else {


            view_engine(array('app/client-users/new'), array(12), array('cols' => $this->cols, 'appName' => 'new-client-user', 'customerID' => $customerID, 'mainTbl' => $this->mainDb));
        }
    }

    public function Edit($id)
    {
        app_method_check(__CLASS__, 'edit');


        $data = $this->crud_model->get($this->mainDb, array('id' => $id));

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            foreach ($this->input->post() as $col => $val) {
                $this->form_validation->set_rules($col, $this->cols[$col]['comment'], $this->cols[$col]['is_null'] == 'NO' ? 'required' : 'trim');
            }

            if ($this->form_validation->run() == FALSE) {
                $res = array('result' => 0, 'message' => validation_errors());
            } else {

                $config['upload_path'] = './uploads/';
                $config['allowed_types'] = 'gif|jpg|png|pdf|doc|docx|xls|xlsx|ppt|pptx';
                $config['max_size'] = 3048;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('file')) {
                    $error = array('error' => $this->upload->display_errors());
                } else {
                    $up_success = array('upload_data' => $this->upload->data());
                    $_POST['file'] = $up_success['upload_data']['file_name'];
                }

                $_POST['date'] = date2db($_POST['date']);
                $_POST['password'] = $data[0]['password'] == $this->input->post('password') ? $this->input->post('password') : md5($this->input->post('password'));
                $query = $this->crud_model->update($this->mainDb, array('id' => $id), $this->input->post());
                $res = array('result' => $query, 'message' => $query ? ln('MESSAGE.Updated') : ln('MESSAGE.SysError'));
            }

            echo json_encode($res);

        } else {

            // $applications = $this->crud_model->get('applications');
            view_engine(array('app/client-users/edit'), array(12),
                array('cols' => $this->cols,
                    'appName' => 'edit-customer',
                    'id' => $id,
                    'data' => $data[0], 'mainTbl' => $this->mainDb));
        }
    }

    public function Datatables()
    {
        app_method_check(__CLASS__, 'list');

        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $search = $this->input->post("search");
        $search = $search['value'];
        $col = 0;
        $dir = "";

        $columns = $this->input->post("columns");

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }


        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }
        $valid_columns = array();

        foreach ($this->colsDt as $cName => $cDt):
            $valid_columns[] = $cDt['name'];
        endforeach;

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }
        if ($order != null) {
            $this->db->order_by($order, $dir);
        }


        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                $sterm = $sterm != 'title_ref_id' ? 'usr.' . $sterm : 'ti.title';

                if ($x == 0) {
                    $this->db->like($sterm, $search);
                } else {
                    $this->db->or_like($sterm, $search);
                }
                $x++;
            }
        }

        if (!empty($columns)) {
            foreach ($columns as $k => $column) {
                $search_val = $column['search']['value'];
                if (strlen($search_val) > 0) {
                    $db_col = $column['data'];
                    $db_col = $db_col != 'title_ref_id' ? 'usr.' . $db_col : 'ti.title';
                    $this->db->like($db_col, $search_val);
                }
            }
        }


        $this->db->limit($length, $start);

        $this->db->select('usr.id, ti.title as title_ref_id, usr.username,usr.name_surname,usr.date, usr.password, usr.phone, usr.phone_2, ln.title as locale, usr.status, usr.created_at, usr.updated_at ');
        $this->db->join('titles ti', 'usr.title_ref_id=ti.ref_id AND language=' . $this->session->userdata('app_lang_id'), 'left');
        $this->db->join('languages ln', 'usr.locale=ln.id', 'left');
        $this->db->from($this->mainDb . ' usr');
        $getDtData = $this->db->get();

        $data = $getDtData->result();
        $total = $this->db->get($this->mainDb)->num_rows();

        $result = array(
            'draw' => $draw,
            'iTotalRecords' => count($data),
            'iTotalDisplayRecords' => $total,
            'sEcho' => 0,
            'sColumns' => 0,
            'aaData' => $data
        );


        echo json_encode($result);
        exit();
    }

    public function SetAuths($id)
    {
        app_method_check(__CLASS__, 'edit');
        $query = $this->crud_model
            ->update($this->mainDb, array('id' => $id), array('auths_list' => json_encode($this->input->post())));

        $response = $query ? array('result' => 1, 'message' => 'Yetkiler Güncellendi!') :
            array('result' => 0, 'message' => 'Lütfen sistem yöneticinize başvurunuz!');

        echo json_encode($response);
    }

    public function GetClientUsers()
    {
        //app_method_check(__CLASS__, 'list');
        $data = $this->crud_model->get($this->mainDb, ['title_ref_id' => 74]);
        sort($data);
        print_r(json_encode($data));
    }

}