<?php

class Users extends CI_Controller
{
    private $mainDb = 'users';


    public function __construct()
    {
        parent::__construct();
        app_login_check();
        $this->cols = $this->schema_model->get_columns($this->mainDb);
        $this->colsDt = $this->schema_model->get_columns_datatables($this->mainDb);
    }

    public function index()
    {
        //  app_method_check(__CLASS__, 'list');
        view_engine(array('app/users/list'), array(12), array('cols' => $this->colsDt, 'mainTbl' => $this->mainDb));
    }

    public function Set()
    {
        //  app_method_check(__CLASS__, 'new');

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            foreach ($this->input->post() as $col => $val) {
                $this->form_validation->set_rules($col, $this->cols[$col]['comment'], $this->cols[$col]['is_null'] == 'NO' ? 'required' : 'trim');

            }

            if ($this->form_validation->run() == FALSE) {

                $res = array('result' => 0, 'message' => validation_errors());

            } else {

                if (count($this->crud_model->get($this->mainDb, array('username' => $this->input->post('username')))) > 0) {
                    $res = array('result' => 0, 'message' => 'Kullanıcı adı mevcut!');
                } else {

                    $_POST['password'] = md5($this->input->post('password'));
                    $_POST['auths_list'] = json_encode(array());
                    $query = $this->crud_model->set($this->mainDb, $this->input->post());

                    if ($query) {
                        $res = array('result' => $query, 'message' => ln('MESSAGE.Added'), 'last_id' => $this->db->insert_id());
                    } else {
                        $res = array('result' => $query, 'message' => ln('MESSAGE.SysError'));
                    }
                }
            }

            echo json_encode($res);

        } else {


            view_engine(array('app/users/new'), array(12), array('cols' => $this->cols, 'appName' => 'new-user'));
        }
    }

    public function Edit($id = NULL)
    {
        //  app_method_check(__CLASS__, 'edit');

        if ($id == NULL) {
            $id = $this->session->userdata('userSettings')['id'];
        }

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            foreach ($this->input->post() as $col => $val) {
                $this->form_validation->set_rules($col, $this->cols[$col]['comment'], $this->cols[$col]['is_null'] == 'NO' ? 'required' : 'trim');
            }

            if ($this->form_validation->run() == FALSE) {
                $res = array('result' => 0, 'message' => validation_errors());
            } else {
                $query = $this->crud_model->update($this->mainDb, array('id' => $id), $this->input->post());

                $res = array('result' => $query, 'message' => $query ? ln('MESSAGE.Updated') : ln('MESSAGE.SysError'));
            }

            echo json_encode($res);

        } else {

            $applications = $this->crud_model->get('applications');


            $data = $this->crud_model->get($this->mainDb, array('id' => $id));
            view_engine(array('app/users/edit', 'app/users/auth'), array(12, 12),
                array('cols' => $this->cols,
                    'appName' => 'edit-user',
                    'id' => $id,
                    'data' => $data[0],
                    'applications' => $applications));
        }
    }

    public function Datatables()
    {
        //  app_method_check(__CLASS__, 'list');

        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $search = $this->input->post("search");
        $search = $search['value'];
        $col = 0;
        $dir = "";

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }


        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }
        $valid_columns = array();

        foreach ($this->colsDt as $cName => $cDt):
            $valid_columns[] = $cDt['name'];
        endforeach;

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }
        if ($order != null) {
            $this->db->order_by($order, $dir);
        }


        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $this->db->like($sterm, $search);
                } else {
                    $this->db->or_like($sterm, $search);
                }
                $x++;
            }
        }


        $this->db->limit($length, $start);
        $getDtData = $this->db->get($this->mainDb);

        $data = $getDtData->result();

        $total = $this->db->get($this->mainDb)->num_rows();

        $result = array(
            'draw' => $draw,
            'iTotalRecords' => $total,
            'iTotalDisplayRecords' => count($oData),
            'sEcho' => 0,
            'sColumns' => 0,
            'aaData' => $data
        );

        echo json_encode($result);
        exit();
    }

    public function SetAuths($id)
    {
        $query = $this->crud_model
            ->update($this->mainDb, array('id' => $id), array('auths_list' => json_encode($this->input->post())));

        $response = $query ? array('result' => 1, 'message' => 'Yetkiler Güncellendi!') :
            array('result' => 0, 'message' => 'Lütfen sistem yöneticinize başvurunuz!');

        echo json_encode($response);

    }

    public function GetTitles($lang_id = 0)
    {
        if ($lang_id != 0) {
            $this->db->where('language', $lang_id);
        }

        $data = $this->crud_model->get($this->mainDb);
        sort($data);
        print_r(json_encode($data));
    }


}