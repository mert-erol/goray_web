<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Controller
{
    private $mainDb = 'orders';

    public function __construct()
    {
        parent::__construct();
        app_login_check();
        $this->cols = $this->schema_model->get_columns($this->mainDb);
        $this->colsDt = $this->schema_model->get_columns_datatables($this->mainDb);
    }

    public function index()
    {

        app_method_check(__CLASS__, 'list');
        view_engine(array('app/orders/list'), array(12), array('cols' => $this->colsDt, 'mainTbl' => $this->mainDb));
    }

    public function Set($lang = NULL, $ref_id = NULL)
    {

        app_method_check(__CLASS__, 'new');

        if ($lang != NULL && $ref_id != NULL) {
            $main_data = $this->crud_model->get($this->mainDb, ['id' => $ref_id]);
            $is_exist = $this->crud_model->get($this->mainDb, ['ref_id' => $ref_id, 'language' => $lang]);
            if (count($is_exist) > 0) {
                redirect(base_url('App/' . __CLASS__ . '/Edit/' . $is_exist[0]['id']));
            } elseif (count($main_data) > 0) {
                unset($main_data[0]['id']);
                $main_data[0]['title'] = '';
                $main_data[0]['language'] = $lang;
                $main_data[0]['ref_id'] = $ref_id;
                $insert_id = $this->crud_model->set($this->mainDb, $main_data[0], TRUE);
                redirect(base_url('App/' . __CLASS__ . '/Edit/' . $insert_id));
            }
        }

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            foreach ($this->input->post() as $col => $val) {
                $this->form_validation->set_rules($col, ln($this->mainDb . '.' . $this->cols[$col]['name']), $this->cols[$col]['is_null'] == 'NO' ? 'required' : 'trim');
            }

            if ($this->form_validation->run() == FALSE) {
                $res = array('result' => 0, 'message' => validation_errors());
            } else {
                $_POST['language'] = $this->session->userdata('app_lang_id');
                $query = $this->crud_model->set($this->mainDb, $this->input->post());
                if ($query) {
                    $insert_id = $this->db->insert_id();
                    $this->crud_model->update($this->mainDb, ['id' => $insert_id], ['ref_id' => $insert_id]);
                    $res = array('result' => $query, 'message' => ln('MESSAGE.Added'), 'last_id' => $insert_id);
                } else {
                    $res = array('result' => $query, 'message' => ln('MESSAGE.SysError'));
                }
            }

            echo json_encode($res);

        } else {

            view_engine(array('app/orders/new'), array(12), array('cols' => $this->cols, 'appName' => 'new-customer', 'mainTbl' => $this->mainDb));
        }
    }

    public function Edit($id)
    {
        app_method_check(__CLASS__, 'edit');

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            foreach ($this->input->post() as $col => $val) {
                $this->form_validation->set_rules($col, $this->cols[$col]['comment'], $this->cols[$col]['is_null'] == 'NO' ? 'required' : 'trim');
            }

            if ($this->form_validation->run() == FALSE) {
                $res = array('result' => 0, 'message' => validation_errors());
            } else {
                $query = $this->crud_model->update($this->mainDb, array('id' => $id), $this->input->post());
                $res = array('result' => $query, 'message' => $query ? ln('MESSAGE.Updated') : ln('MESSAGE.SysError'));
            }

            echo json_encode($res);

        } else {

            $order_data = $this->crud_model->get('orders', ['id' => $id]);
            foreach ($order_data as $key => $ord) {
                $order_data[$key]['origin_language'] = $this->crud_model->get('translation_languages', ['ref_id' => $ord['origin_language'], 'language' => 2])[0]['title'];
                $order_data[$key]['target_languages'] = '';
                foreach (json_decode($ord['target_languages'], FALSE) as $k => $tlgn) {
                    $order_data[$key]['target_languages'] .= $this->crud_model->get('translation_languages', ['ref_id' => $tlgn, 'language' => 2])[0]['title'] . ($k != count(json_decode($ord['target_languages'], FALSE)) - 1 ? ',' : '');
                }
                $order_data[$key]['service_types'] = '';

                if ($ord['service_types'] !== 'null') {
                    foreach (json_decode($ord['service_types'], FALSE) as $k => $srv) {
                        $order_data[$key]['service_types'] .= $this->crud_model->get('service_types', ['ref_id' => $srv, 'language' => 2])[0]['title'] . ($k != count(json_decode($ord['service_types'], FALSE)) - 1 ? ',' : '');
                    }
                }

                $user_data = $this->crud_model->get('client_users', ['id' => $ord['client_user']]);
                $order_data[$key]['client_user'] = count($user_data) > 0 ? $user_data[0] : [];
                $assign_data = $this->crud_model->get('order_assigns', ['order_id' => $ord['id']]);

                $exist_files = [];
                $initialPreview = [];
                $initialPreviewConfig = [];

                $order_data[$key]['assigns'] = $assign_data;

                $files = NULL;
                if (count($assign_data) > 0)
                    $files = json_decode($assign_data[0]['files'], FALSE);


                if (!is_null($files)) {
                    if (count($files) > 0) {
                        foreach ($files as $file) {

                            $exist_files[] = base_url("uploads/translated_files/" . md5($id) . "/$file");
                            $initialPreview[] = base_url("uploads/translated_files/" . md5($id) . "/$file");
                            $file_explode = explode('.', $file);
                            $file_extension = end($file_explode);

                            $known_extensions = [
                                'html' => 'html',
                                'txt' => 'text',
                                'pdf' => 'pdf',
                                'doc' => 'office',
                                'docx' => 'office',
                                'xls' => 'office',
                                'xlsx' => 'office',
                                'ppt' => 'office',
                                'pptx' => 'office',
                                'tif' => 'gdocs',
                                'ai' => 'gdocs',
                                'eps' => 'gdocs',
                                'mp4' => 'video'
                            ];

                            $fu_config = ['caption' => $file, 'size' => filesize("uploads/translated_files/" . md5($id) . "/$file"), 'width' => 120, 'url' => base_url('App/Orders/FileDelete/' . $id), 'key' => $file];

                            if (isset($known_extensions[$file_extension]))
                                $fu_config['type'] = $known_extensions[$file_extension];
                            $initialPreviewConfig[] = $fu_config;

                        }
                    }
                }


                $order_data[$key]['exist_files'] = $exist_files;
                $order_data[$key]['initialPreview'] = json_encode($initialPreview);
                $order_data[$key]['initialPreviewConfig'] = json_encode($initialPreviewConfig);

                view_engine(
                    array('app/orders/documents', 'app/orders/customer-detail', 'app/orders/order-detail', 'app/orders/assign', 'app/orders/upload-file'),
                    array(12),
                    array('cols' => $this->cols, 'appName' => 'edit-order', 'id' => $id, 'order_data' => $order_data[0], 'mainTbl' => $this->mainDb));

            }
        }
    }

    public function Datatables()
    {
        app_method_check(__CLASS__, 'list');

        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $search = $this->input->post("search");
        $search = $search['value'];
        $col = 0;
        $dir = "";

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }


        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }
        $valid_columns = array();

        foreach ($this->colsDt as $cName => $cDt):
            $valid_columns[] = $cDt['name'];
        endforeach;

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }
        if ($order != null) {
            $this->db->order_by('ord.' . $order, $dir);
        }


        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($sterm == 'target_lang_ref_id') $sterm = 'tlng.title';
                if ($sterm == 'origin_lang_ref_id') $sterm = 'olng.title';
                if ($x == 0) {
                    $this->db->like($sterm, $search);
                } else {
                    $this->db->or_like($sterm, $search);
                }
                $x++;
            }
        }


        $this->db->limit($length, $start);

        app_auth_check('edit-assign') == 0 ? $this->db->where('asg.client_user', $this->session->userdata('app_user_data')['id']) : '';
        $this->db->select('ord.id, olng.title as origin_language, ord.target_languages, clu.name_surname as client_user, ord.word_count, ord.total_amount, ord.service_types, ord.estimated_date, ord.discount, ord.status, ord.created_at, ord.updated_at');
        $this->db->join('client_users clu', 'ord.client_user=clu.id', 'left');
        $this->db->join('translation_languages olng', 'ord.origin_language=olng.ref_id AND olng.language=2', 'left');
        app_auth_check('edit-assign') == 0 ? $this->db->join('order_assigns asg', 'ord.id=asg.order_id', 'left') : '';
        $this->db->from($this->mainDb . ' ord');
        $getDtData = $this->db->get();

        $data = $getDtData->result_array();

        foreach ($data as $k => $dt) {
            $trlngs = json_decode($dt['target_languages']);
            $data[$k]['target_languages'] = '';
            foreach ($trlngs as $kt => $trlng) {
                $data[$k]['target_languages'] .=
                    $this->db->where(['ref_id' => $trlng, 'language' => 2])->select('title')->get('translation_languages')->row_array()['title'] . ',';
            }
            $data[$k]['target_languages'] = rtrim($data[$k]['target_languages'], ',');

            if ($dt['service_types'] != 'null') {
                $stypes = json_decode($dt['service_types']);
                $data[$k]['service_types'] = '';
                foreach ($stypes as $kt => $stype) {
                    $data[$k]['service_types'] .=
                        $this->db->where(['ref_id' => $stype, 'language' => 2])->select('title')->get('service_types')->row_array()['title'] . ',';
                }
                $data[$k]['service_types'] = rtrim($data[$k]['service_types'], ',');
            } else {
                $data[$k]['service_types'] = '';
            }


        }

        $total = $this->db->where('language', $this->session->userdata('app_lang_id'))->get($this->mainDb)->num_rows();

        $result = array(
            'draw' => $draw,
            'iTotalRecords' => $total,
            'iTotalDisplayRecords' => count($data),
            'sEcho' => 0,
            'sColumns' => 0,
            'aaData' => $data
        );

        echo json_encode($result);
        exit();
    }

    public function GetTypes($lang_id = 0)
    {
        if ($lang_id != 0) {
            $this->db->where('language', $lang_id);
        }

        $data = $this->crud_model->get($this->mainDb);
        sort($data);
        print_r(json_encode($data));
    }

    public function Download($order_id)
    {
        $order_data = $this->crud_model->get('orders', ['id' => $order_id]);
        if (count($order_data) > 0) {
            $order_data = $order_data[0];
            $user_files = json_decode($order_data['user_files'], FALSE);
            if (count($user_files) > 0) {
                $this->load->library('zip');
                $guid = $order_data['guid'];
                if (is_dir('./uploads/' . $order_data['guid'])) {
                    $this->zip->read_dir("./uploads/$guid/", FALSE);
                    $this->zip->download(permalink($order_data['id'] . '_' . date('YmdHis') . '.zip'));
                }
            }
        }
    }

    public function AssignTo()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $client_user = trim($this->input->post('employee_id'));
            $order_id = trim($this->input->post('order_id'));
            $is_exist = $this->crud_model->get('order_assigns', ['order_id' => $order_id]);
            if (count($is_exist) == 0) $this->crud_model->set('order_assigns', ['order_id' => $order_id, 'client_user' => $client_user, 'status' => 0]);
        }
    }

    public function UpdateAssign()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $order_id = trim($this->input->post('order_id'));
            $this->crud_model->update('order_assigns', ['order_id' => $order_id], ['notes' => trim($this->input->post('notes')), 'status' => $this->input->post('status')]);
        }
    }

    public function FileUpload($order_id)
    {

        if ($_FILES['files']) {
            $filesCount = count($_FILES['files']['name']);
            $uploadData = [];
            for ($i = 0; $i < $filesCount; $i++) {
                $_FILES['file']['name'] = $_FILES['files']['name'][$i];
                $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                $_FILES['file']['size'] = $_FILES['files']['size'][$i];


                $config['upload_path'] = './uploads/translated_files/' . md5($order_id);
                !file_exists($config['upload_path']) ? mkdir($config['upload_path'], 0755, TRUE) : 0;
                $config['allowed_types'] = 'gif|jpg|png|pdf|doc|docx|xls|xlsx|ppt|pptx';
                $config['max_size'] = 50000;
                $this->load->library('upload', $config);

                $this->upload->initialize($config);
                if ($this->upload->do_upload('file')) {
                    $fileData = $this->upload->data();
                    $uploadData[$i] = $fileData['file_name'];
                }
            }
            $files = json_encode($uploadData);

            $order_assigns_data = $this->crud_model->get('order_assigns', ['order_id' => $order_id]);
            if (strlen($order_assigns_data[0]['files']) > 0) {

                $exist_files = json_decode($order_assigns_data[0]['files'], FALSE);
                if (count($exist_files) > 0) {
                    $files = json_encode(array_merge(json_decode($files, FALSE), $exist_files));
                }
            }

            $this->crud_model->update('order_assigns', ['order_id' => $order_id], ['files' => $files]);

        }


        $response = ['nothing' => TRUE, 'data' => $uploadData];


        echo json_encode($response);
    }

    public function FileDelete($order_id)
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $folder = md5($order_id);
            $file_name = $this->input->post('key');


            unlink(FCPATH . "uploads/translated_files/$folder/$file_name");

            $order_assigns_data = $this->crud_model->get('order_assigns', ['order_id' => $order_id]);
            $new_files = [];
            foreach (json_decode($order_assigns_data[0]['files'], FALSE) as $file) {
                if ($file != $file_name)
                    $new_files[] = $file;
            }
            $this->crud_model->update('order_assigns', ['order_id' => $order_id], ['files' => json_encode($new_files)]);


            echo json_encode(['message' => "File $file_name deleted", 'url' => "./uploads/translated_files/$folder/$file_name"]);

        }
    }

    public function ChangeStatus($order_id)
    {

        $res = ['status' => 0];
        if (intval($order_id) > 0) {
            if ($this->input->server('REQUEST_METHOD') == 'POST') {
                $update = $this->crud_model->update('orders', ['id' => intval($order_id)], ['status' => $this->input->post('status')]);
                $res['status'] = $update ? 1 : 0;
            }
        }

        echo json_encode($res);
    }


}

