<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

    private $queueDb = 'messages_sender_queue';

    public function __construct() {
        parent::__construct();
    }

    public function Devices() {

        $devices = $this->crud_model->get('devices', ['status' => 1], 20);

        foreach ($devices as $key => $dev) {
            $api_data = $this->crud_model->get('api_bulk_send', ['device' => $dev['id'], 'status' => 0], 1);
            if (count($api_data) > 0) {
                $devices[$key]['link'] = $api_data[0]['link'];
                $devices[$key]['api_id'] = $api_data[0]['id'];
                $devices[$key]['interval'] = intval($api_data[0]['interval'])*1000;
                $devices[$key]['url'] = explode('/', $dev['link'])[2];
            } else {
                unset($devices[$key]);
            }
        }

        $newStruct = [];
        foreach ($devices as $dv) {
            $newStruct[] = $dv;
        }

        echo json_encode($newStruct);
    }

    public function SendOld($device_id) {
        $message = $this->input->post('msg');
        $count = $this->input->post('qt');
        if ($device_id > 0) {
            $numbers = '';
            if ($message == NULL) {
                $get_message = $this->crud_model->get('messages', ['device' => $device_id], 1);
            } else {
                $get_message[0]['message'] = $message;
            }
            $get_bulk_numbers = $this->crud_model->get('numbers', ['status' => '0'], $count);
            $count_number = count($get_bulk_numbers);
            foreach ($get_bulk_numbers as $key => $number) {
                $numbers.=$number['content'] . ($key + 1 != $count_number ? ';' : '');
            }
            $device = $this->crud_model->get('devices', ['id' => $device_id], 1);
            if (count($device) > 0 && count($get_message) > 0) {
                $device[0]['link'] = str_replace('[telefon21]', $numbers, $device[0]['link']);
                $device[0]['link'] = str_replace('[mesaj21]', $get_message[0]['message'], $device[0]['link']);


                $this->crud_model->set('api_bulk_send', ['device' => $device_id, 'link' => $device[0]['link']]);
                $this->crud_model->set('api_bulk_logs', ['device' => $device_id, 'process' => 1, 'credit' => $count]);
                echo json_encode(['status' => 1, 'data' => $device[0]]);
            } elseif (count($get_message) == 0) {
                echo json_encode(['status' => 0, 'message' => 'Cihaza Eklenmiş Mesaj Yok !']);
            } else {
                echo json_encode(['status' => 0, 'message' => 'Gönderme Hatası !']);
            }
        }
    }

    public function Send($device_id) {

        $message = ''; // $this->input->post('msg');
        $count = 10; // $this->input->post('qt');
        if ($device_id > 0) {
            $this->crud_model->update($this->queueDb, ['device=' => 0], ['device' => 1], $count);

            $message_queue = $this->crud_model->get($this->queueDb, ['device' => $device_id], $count);

            $new_struct = [];
            foreach ($message_queue as $que) {
                $message = $this->crud_model->get('messages', ['id' => $que['message']])['0']['message'];
                $new_struct[$message][] = $this->crud_model->get('numbers', ['id' => $que['number']])['0']['content'];
            }
            $device = $this->crud_model->get('devices', ['id' => $device_id], 1);
            $links = [];
            foreach ($new_struct as $message => $numbers) {
                $numbers_to_string = '';
                foreach ($numbers as $key => $num) {
                    $numbers_to_string.=$num . ($key + 1 != count($numbers) ? ';' : '');
                }
                $url = str_replace('[telefon21]', $numbers_to_string, $device[0]['link']);
                $url = str_replace('[mesaj21]', $message, $url);
                $links[] = $url;
                $this->crud_model->set('api_bulk_send', ['device' => $device_id, 'link' => $url]);
            }
            count($message_queue) > 0 ? $this->crud_model->set('devices_credits', ['process' => 2, 'device' => $device_id, 'credit' => count($message_queue)]) : 0;
   
            
        }
    }
    
    public function Stop($device_id) {

        if ($device_id > 0) {
            $this->crud_model->update('api_bulk_send', ['device' => $device_id, 'status!=' => 0], ['status' => 2]);
            $this->crud_model->set('api_bulk_logs', ['device' => $device_id, 'process' => 2]);

            echo json_encode(['status' => 1, 'message' => 'Cihazın Gönderimi Durduruldu']);
        }
    }

    public function UpdateBulkQueue($api_id) {
        if ($api_id > 0) {
            $this->crud_model->update('api_bulk_send', ['id' => $api_id], ['status' => 1]);
            $data = $this->crud_model->get('api_bulk_send', ['id' => $api_id]);
            $device_id = $data[0]['device'];
            $this->crud_model->set('devices_credits', ['device' => $device_id, 'process' => 2, 'credit' => 250]);
        }
    }

}
