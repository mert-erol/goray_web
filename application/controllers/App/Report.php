<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller
{
    private $mainDb = 'campaigns';


    
    public function __construct()
    {
        parent::__construct(); 
        app_login_check();



        $this->cols = [
            'CostCenterDescription','glaccdescription','Year','Month','MonthName','BDescription','DOC_Debit','Doc_Credit','Hedef'
        ];
        $this->colsDt = [
            'CostCenterDescription','glaccdescription','Year','Month','MonthName','BDescription','DOC_Debit','Doc_Credit','Hedef'
        ];
    }

    public function index()
    {
        app_method_check(__CLASS__, 'list');
        view_engine(array('app/campaigns/list'), array(12), array('cols' => $this->colsDt, 'mainTbl' => $this->mainDb));
    }

    public function test2(){
       pr2( $this->cols = $this->schema_model->get_columns($this->mainDb));
    }

    public function Set($lang = NULL, $ref_id = NULL)
    {
        app_method_check(__CLASS__, 'new');

        if ($lang != NULL && $ref_id != NULL) {
            $main_data = $this->crud_model->get($this->mainDb, ['id' => $ref_id]);
            $is_exist = $this->crud_model->get($this->mainDb, ['ref_id' => $ref_id, 'language' => $lang]);
            if (count($is_exist) > 0) {
                redirect(base_url('App/' . __CLASS__ . '/Edit/' . $is_exist[0]['id']));
            } elseif (count($main_data) > 0) {
                unset($main_data[0]['id']);
                $main_data[0]['title'] = '';
                $main_data[0]['language'] = $lang;
                $main_data[0]['ref_id'] = $ref_id;
                $insert_id = $this->crud_model->set($this->mainDb, $main_data[0], TRUE);
                redirect(base_url('App/' . __CLASS__ . '/Edit/' . $insert_id));
            }
        }

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            foreach ($this->input->post() as $col => $val) {
                $this->form_validation->set_rules($col, ln($this->mainDb . '.' . $this->cols[$col]['name']), $this->cols[$col]['is_null'] == 'NO' ? 'required' : 'trim');
            }

            if ($this->form_validation->run() == FALSE) {
                $res = array('result' => 0, 'message' => validation_errors());
            } else {

                $_POST['start_date'] = date2db($_POST['start_date']);
                $_POST['end_date'] = date2db($_POST['end_date']);
                $_POST['language'] = $this->session->userdata('app_lang_id');
                $query = $this->crud_model->set($this->mainDb, $this->input->post());
                if ($query) {
                    $insert_id = $this->db->insert_id();
                    $this->crud_model->update($this->mainDb, ['id' => $insert_id], ['ref_id' => $insert_id]);
                    $res = array('result' => $query, 'message' => ln('MESSAGE.Added'), 'last_id' => $insert_id);
                } else {
                    $res = array('result' => $query, 'message' => ln('MESSAGE.SysError'));
                }
            }

            echo json_encode($res);

        } else {
            view_engine(array('app/campaigns/new'), array(12), array('cols' => $this->cols, 'appName' => 'new-education', 'mainTbl' => $this->mainDb));
        }
    }

    public function Edit($id)
    {
        app_method_check(__CLASS__, 'edit');

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            foreach ($this->input->post() as $col => $val) {
                $this->form_validation->set_rules($col, $this->cols[$col]['comment'], $this->cols[$col]['is_null'] == 'NO' ? 'required' : 'trim');
            }

            if ($this->form_validation->run() == FALSE) {
                $res = array('result' => 0, 'message' => validation_errors());
            } else {

                $_POST['start_date'] = date2db($_POST['start_date']);
                $_POST['end_date'] = date2db($_POST['end_date']);
                $query = $this->crud_model->update($this->mainDb, array('id' => $id), $this->input->post());
                log_message('error', $this->db->last_query());
                $res = array('result' => $query, 'message' => $query ? ln('MESSAGE.Updated') : ln('MESSAGE.SysError'));
            }

            echo json_encode($res);

        } else {

            $data = $this->crud_model->get($this->mainDb, array('id' => $id));
            view_engine(array('app/campaigns/edit'), array(12), array('cols' => $this->cols, 'appName' => 'edit-education', 'id' => $id, 'data' => $data[0], 'mainTbl' => $this->mainDb));
        }
    }

    public function Datatables()
    {
        app_method_check(__CLASS__, 'list');

        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $search = $this->input->post("search");
        $search = $search['value'];
        $col = 0;
        $dir = "";

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }


        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }
        $valid_columns = array();

        foreach ($this->colsDt as $cName => $cDt):
            $valid_columns[] = $cDt;
        endforeach;

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }
        if ($order != null) {
            $this->db->order_by($order, $dir);
        }


        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                $sterm = $sterm != 'title_ref_id' ? 'ed.' . $sterm : 'ed.title';
                if ($x == 0) {
                    $this->db->like($sterm, $search);
                } else {
                    $this->db->or_like($sterm, $search);
                }
                $x++;
            }
        }



        $datas = $this->data();




        $oData =  $datas;
//        $oData = $getDtData->result_array();


        $total =count($datas);

        $result = array(
            'draw' => $draw,
            'iTotalRecords' => $total,
            'iTotalDisplayRecords' => count($oData),
            'sEcho' => 0,
            'sColumns' => 0,
            'aaData' => $datas
        );

        echo json_encode($result);
        exit();
    }


    public function data(){

      $this->load->library('nebim');

      $response_array = array();
    
      $response =  $this->nebim->Budget();


      //pr2($response); exit;

      foreach ($response as $key => $value) {
            $ra_length  = count($response_array);

            if($ra_length > 0){

                $ra_cost_center = $response_array[count($response_array)-1]['CostCenterDescription'];
                $val_cost_center = $value['CostCenterDescription'];

                if($ra_cost_center==$val_cost_center){
                    continue;
                }   else{
                    array_push($response_array,$value);
                }
            }   else{
                array_push($response_array,$value);
            }
      $response_array = $this->addToArray($response,$response_array);
           
      }

  






      return $response_array;
    }



    private function addToArray($response,$response_array){
        $ra_length_last_index  = count($response_array) -1 ;

        $total_DOC_Debit  = 0 ; 
        $total_Doc_Credit  = 0 ; 
        $total_Target  = 0 ;

        foreach ($response as $k2 => $v2) {
             $ra_cost_center_v2 = $response_array[$ra_length_last_index]['CostCenterDescription'];
            $val_cost_center_v2 = $v2['CostCenterDescription'];
            if($ra_cost_center_v2==$val_cost_center_v2){
                   //if(!isset($v2['DOC_Debit']))
                    $total_DOC_Debit = $total_DOC_Debit + $v2['DOC_Debit'];
                   // if(!isset($v2['Doc_Credit']))
                    $total_Doc_Credit = $total_Doc_Credit + $v2['Doc_Credit'];

                    if($v2['Hedef']!='' || $v2['Hedef']!=null)
                    $total_Target = $total_Target + $v2['Hedef'];
                }
    
        }

        $response_array[$ra_length_last_index]['DOC_Debit']  = $total_DOC_Debit ; 
        $response_array[$ra_length_last_index]['Doc_Credit']  = $total_Doc_Credit ; 
        $response_array[$ra_length_last_index]['Hedef']  = $total_Target ; 

       // pr2($response);

        return $response_array;
    }


}

