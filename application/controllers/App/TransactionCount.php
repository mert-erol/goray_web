<?php
defined('BASEPATH') or exit('No direct script access allowed');

class TransactionCount extends CI_Controller
{
    private $mainDb = 'transaction_counts';

    public function __construct()
    {
        parent::__construct();
        app_login_check();
        $this->cols = $this->schema_model->get_columns($this->mainDb);
        $this->colsDt = $this->schema_model->get_columns_datatables($this->mainDb);
    }


    public function Datatables()
    {
        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $search = $this->input->post("search");
        $search = $search['value'];
        $col = 0;
        $dir = "";

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }


        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }
        $valid_columns = array();



        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }
        if ($order != null) {
            $this->db->order_by($order, $dir);
        }


        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $this->db->like($sterm, $search);
                } else {
                    $this->db->or_like($sterm, $search);
                }
                $x++;
            }
        }


        $this->db->limit($length, $start);
        $this->db->group_by('trcs1.product_id');
        $this->db->order_by('created_at', 'desc');

        $this->db->select('trcs1.id,prd.barcode,trcs1.created_at,
                            (SELECT SUM(amount) AS counted
                            FROM transaction_counts
                            WHERE product_id=trcs1.product_id) AS counted');

        $this->db->join('products prd', 'trcs1.product_id=prd.id', 'left');
        $this->db->from($this->mainDb.' trcs1');
        $getDtData = $this->db->get();

        $data = $getDtData->result();


        $total = $this->db->get($this->mainDb)->num_rows();

        $result = array(
            'draw' => $draw,
            'iTotalRecords' => $total,
            'iTotalDisplayRecords' => $total,
            'sEcho' => 0,
            'sColumns' => 0,
            'aaData' => $data
        );

        echo json_encode($result);
        exit();
    }

    public function GetLanguages($lang_id = 0)
    {
        if ($lang_id != 0) {
            $this->db->where('language', $lang_id);
        }

        $data = $this->crud_model->get($this->mainDb);
        sort($data);
        print_r(json_encode($data));
    }


}

