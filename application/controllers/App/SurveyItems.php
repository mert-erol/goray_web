<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SurveyItems extends CI_Controller
{
    private $mainDb = 'survey_items';

    public function __construct()
    {
        parent::__construct();
        app_login_check();
        $this->cols = $this->schema_model->get_columns($this->mainDb);
        $this->colsDt = $this->schema_model->get_columns_datatables($this->mainDb);
    }

    public function index()
    {
        app_method_check('Surveys', 'list');
        view_engine(array('app/surveys/items/list'), array(12), array('cols' => $this->colsDt, 'mainTbl' => $this->mainDb));
    }

    public function Set($survey_id = 0)
    {
        app_method_check('Surveys', 'new');


        if ($this->input->server('REQUEST_METHOD') == 'POST') {


            if ($survey_id == 0) {
                redirect(base_url('App/Surveys'));
            }

            foreach ($this->input->post() as $col => $val) {
                $this->form_validation->set_rules($col, ln($this->mainDb . '.' . $this->cols[$col]['name']), $this->cols[$col]['is_null'] == 'NO' ? 'required' : 'trim');
            }

            if ($this->form_validation->run() == FALSE) {
                $res = array('result' => 0, 'message' => validation_errors());
            } else {
                $_POST['survey_id'] = $survey_id;
                $query = $this->crud_model->set($this->mainDb, $this->input->post());
                if ($query) {
                    $insert_id = $this->db->insert_id();
                    $res = array('result' => $query, 'message' => ln('MESSAGE.Added'), 'last_id' => $insert_id);
                } else {
                    $res = array('result' => $query, 'message' => ln('MESSAGE.SysError'));
                }
            }

            echo json_encode($res);

        } else {
            view_engine(array('app/surveys/items/new'), array(12), array('cols' => $this->cols, 'appName' => 'new-education', 'mainTbl' => $this->mainDb, 'surveyID' => $survey_id));
        }
    }

    public function Edit($id)
    {
        app_method_check('Surveys', 'edit');

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            foreach ($this->input->post() as $col => $val) {
                $this->form_validation->set_rules($col, $this->cols[$col]['comment'], $this->cols[$col]['is_null'] == 'NO' ? 'required' : 'trim');
            }

            if ($this->form_validation->run() == FALSE) {
                $res = array('result' => 0, 'message' => validation_errors());
            } else {


                $query = $this->crud_model->update($this->mainDb, array('id' => $id), $this->input->post());
                $res = array('result' => $query, 'message' => $query ? ln('MESSAGE.Updated') : ln('MESSAGE.SysError'));
            }

            echo json_encode($res);

        } else {

            $data = $this->crud_model->get($this->mainDb, array('id' => $id));
            view_engine(array('app/surveys/items/edit'), array(12), array('cols' => $this->cols, 'appName' => 'edit-education', 'id' => $id, 'data' => $data[0], 'mainTbl' => $this->mainDb));
        }
    }

    public function Datatables($survey_id = 0)
    {
        app_method_check('Surveys', 'list');


        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $search = $this->input->post("search");
        $search = $search['value'];
        $col = 0;
        $dir = "";

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }


        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }
        $valid_columns = array();

        foreach ($this->colsDt as $cName => $cDt):
            $valid_columns[] = $cDt['name'];
        endforeach;

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }
        if ($order != null) {
            $this->db->order_by($order, $dir);
        }


        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $this->db->like($sterm, $search);
                } else {
                    $this->db->or_like($sterm, $search);
                }
                $x++;
            }
        }


        $this->db->limit($length, $start);
        $this->db->where('survey_id', $survey_id);
        $this->db->select('*');
        $this->db->from($this->mainDb . ' ed');
        $getDtData = $this->db->get();

        $oData = $getDtData->result_array();
        $data = [];
        foreach ($oData as $row) {
            $nRow = [];
            foreach ($this->colsDt as $colRow) {
                $nRow[$colRow['name']] = $row[$colRow['name']];
            }
            $data[] = $nRow;
        }

        $total = $this->db->where('survey_id', $survey_id)->get($this->mainDb)->num_rows();

        $result = array(
            'draw' => $draw,
            'iTotalRecords' => $total,
            'iTotalDisplayRecords' => count($oData),
            'sEcho' => 0,
            'sColumns' => 0,
            'aaData' => $data
        );

        echo json_encode($result);
        exit();
    }

    public function GetDepartments()
    {
        $data = $this->crud_model->get($this->mainDb);
        sort($data);
        print_r(json_encode($data));

    }


}

