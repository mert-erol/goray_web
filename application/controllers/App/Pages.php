<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller
{
    private $mainDb = 'pages';

    public function __construct()
    {
        parent::__construct();
        app_login_check();
        $this->cols = $this->schema_model->get_columns($this->mainDb);
        $this->colsDt = $this->schema_model->get_columns_datatables($this->mainDb);
    }

    public function index()
    {
        app_method_check(__CLASS__, 'list');
        view_engine(array('app/pages/list'), array(12), array('cols' => $this->colsDt, 'mainTbl' => $this->mainDb));
    }

    public function Set($lang = NULL, $ref_id = NULL)
    {
        app_method_check(__CLASS__, 'new');

        if ($lang != NULL && $ref_id != NULL) {
            $main_data = $this->crud_model->get($this->mainDb, ['id' => $ref_id]);
            $is_exist = $this->crud_model->get($this->mainDb, ['ref_id' => $ref_id, 'language' => $lang]);
            if (count($is_exist) > 0) {
                redirect(base_url('App/' . __CLASS__ . '/Edit/' . $is_exist[0]['id']));
            } elseif (count($main_data) > 0) {
                unset($main_data[0]['id']);
                $main_data[0]['title'] = '';
                $main_data[0]['language'] = $lang;
                $main_data[0]['ref_id'] = $ref_id;
                $insert_id = $this->crud_model->set($this->mainDb, $main_data[0], TRUE);
                redirect(base_url('App/' . __CLASS__ . '/Edit/' . $insert_id));
            }
        }

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            foreach ($this->input->post() as $col => $val) {
                $this->form_validation->set_rules($col, ln($this->mainDb . '.' . $this->cols[$col]['name']), $this->cols[$col]['is_null'] == 'NO' ? 'required' : 'trim');
            }

            if ($this->form_validation->run() == FALSE) {
                $res = array('result' => 0, 'message' => validation_errors());
            } else {
                $_POST['language'] = $this->session->userdata('app_lang_id');
                $query = $this->crud_model->set($this->mainDb, $this->input->post());
                if ($query) {
                    $insert_id = $this->db->insert_id();
                    $this->crud_model->update($this->mainDb, ['id' => $insert_id], ['ref_id' => $insert_id]);
                    $res = array('result' => $query, 'message' => ln('MESSAGE.Added'), 'last_id' => $insert_id);
                } else {
                    $res = array('result' => $query, 'message' => ln('MESSAGE.SysError'));
                }
            }

            echo json_encode($res);

        } else {
            view_engine(array('app/pages/new'), array(12), array('cols' => $this->cols, 'appName' => 'new-education', 'mainTbl' => $this->mainDb));
        }
    }

    public function Edit($id)
    {
        app_method_check(__CLASS__, 'edit');

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            foreach ($this->input->post() as $col => $val) {
                if ($col != 'faq')
                    $this->form_validation->set_rules($col, $this->cols[$col]['comment'], $this->cols[$col]['is_null'] == 'NO' ? 'required' : 'trim');
            }

            if ($this->form_validation->run() == FALSE) {
                $res = array('result' => 0, 'message' => validation_errors());
            } else {

                if ($_FILES['files']) {
                    $filesCount = count($_FILES['files']['name']);
                    $uploadData = [];
                    for ($i = 0; $i < $filesCount; $i++) {
                        $_FILES['file']['name'] = $_FILES['files']['name'][$i];
                        $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                        $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                        $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                        $_FILES['file']['size'] = $_FILES['files']['size'][$i];

                        $config['upload_path'] = './uploads/';
                        $config['allowed_types'] = 'gif|jpg|png|pdf|doc|docx|xls|xlsx|ppt|pptx';
                        $config['max_size'] = 3048;
                        $this->load->library('upload', $config);


                        $this->upload->initialize($config);
                        if ($this->upload->do_upload('file')) {
                            $fileData = $this->upload->data();
                            $uploadData[$i] = $fileData['file_name'];
                        }
                    }
                    $_POST['files'] = json_encode($uploadData);
                }


                //$_POST['category_ref_id'] = json_encode($_POST['category_ref_id']);
                $query = $this->crud_model->update($this->mainDb, array('id' => $id), $this->input->post());
                $res = array('result' => $query, 'message' => $query ? ln('MESSAGE.Updated') : ln('MESSAGE.SysError'));
            }

            echo json_encode($res);

        } else {

            $data = $this->crud_model->get($this->mainDb, array('id' => $id));
            $lang_data = $this->crud_model->get('languages', ['id' => $data[0]['language']])[0];
            view_engine(array('app/pages/edit'), array(12), array('cols' => $this->cols, 'appName' => 'edit-education', 'id' => $id, 'data' => $data[0], 'mainTbl' => $this->mainDb, 'langData' => $lang_data));
        }
    }

    public function Datatables()
    {
        app_method_check(__CLASS__, 'list');

        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $search = $this->input->post("search");
        $search = $search['value'];
        $col = 0;
        $dir = "";

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }


        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }
        $valid_columns = array();

        foreach ($this->colsDt as $cName => $cDt):
            $valid_columns[] = $cDt['name'];
        endforeach;

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }
        if ($order != null) {
            $this->db->order_by($order, $dir);
        }


        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $this->db->like($sterm, $search);
                } else {
                    $this->db->or_like($sterm, $search);
                }
                $x++;
            }
        }


        $this->db->limit($length, $start);
        $this->db->where('ed.language', $this->session->userdata('app_lang_id'));

        $this->db->select('ed.id, ed.title,dp.title as department, ct.title as category_ref_id, ed.content, ed.status, ed.created_at, ed.updated_at ');
        $this->db->join('departments dp', 'ed.department=dp.ref_id AND dp.language=' . $this->session->userdata('app_lang_id'), 'left');
        $this->db->join('pages_categories ct', 'ed.category_ref_id=ct.ref_id AND ct.language=' . $this->session->userdata('app_lang_id'), 'left');
        $this->db->from($this->mainDb . ' ed');
        $getDtData = $this->db->get();

        $data = $getDtData->result_array();


        $total = $this->db->get($this->mainDb)->num_rows();

        $result = array(
            'draw' => $draw,
            'iTotalRecords' => $total,
            'iTotalDisplayRecords' => $total,
            'sEcho' => 0,
            'sColumns' => 0,
            'aaData' => $data
        );

        echo json_encode($result);
        exit();
    }

    public function SetPermalink($id)
    {
        app_method_check(__CLASS__, 'edit');

        $res = ['status' => 0, 'title' => ''];
        if ($this->input->server('REQUEST_METHOD') == 'POST' && $id != NULL && intval($id) > 0) {
            $title = trim($this->input->post('title'));
            $create_permalink = permalink($title);
            $permalink = $create_permalink;
            $counter = 0;
            $get_page = $this->crud_model->get($this->mainDb, ['id' => $id])[0];
            $control = $this->crud_model->get($this->mainDb, ['permalink' => $permalink, 'language' => $get_page['language']]);

            $log = [];
            if (count($control) > 0) {
                $count_control = count($control);
                $new_permalink = $permalink;
                while ($count_control > 0) {
                    $new_permalink = $permalink;
                    $counter++;
                    $control = $this->crud_model->get($this->mainDb, ['permalink' => $new_permalink, 'language' => $get_page['language']]);
                    $new_permalink = "$permalink-$counter";
                    if (count($control) > 0) {
                        if ($control[0]['id'] == $id) {
                            $log[] = 'aynı dönüyor';
                            $count_control = 0;
                            $new_permalink = $permalink;
                        } else {
                            $count_control = 1;
                        }
                    } else {
                        $count_control = 0;
                    }
                }

                $permalink = $new_permalink;
            }


            $this->crud_model->update($this->mainDb, ['id' => $id], ['permalink' => $permalink]);

            $res = ['status' => 1, 'permalink' => $permalink, 'log' => $log];
        } else {

        }

        echo json_encode($res);
    }

    public function Duplicate($db_id)
    {
        $res = ['status' => 0];
        if (intval($db_id) > 0) {
            $get_page = $this->crud_model->get($this->mainDb, ['id' => $db_id]);
            unset($get_page[0]['id']);
            $insert_id = $this->crud_model->set($this->mainDb, $get_page[0], TRUE);
            $this->crud_model->update($this->mainDb, ['id' => $insert_id], ['ref_id' => $insert_id]);

            if ($insert_id > 0) {
                $res = ['status' => 1, 'insert_id' => $insert_id];
            }
        }

        echo json_encode($res);
    }

    public function Upload($db_id)
    {
        app_method_check(__CLASS__, 'edit');
        if ($_FILES['file']) {
            $uploadData = [];

            $config['upload_path'] = './uploads/pages/';
            $config['allowed_types'] = '*';
            $config['max_size'] = 3048;
            $this->load->library('upload', $config);


            $this->upload->initialize($config);
            if ($this->upload->do_upload('file')) {
                $fileData = $this->upload->data();
                $fileData = $this->upload->data();
                echo base_url('uploads/pages/' . $fileData['file_name']);
            }
        }
    }

    public function GetPages($lang_id = 0)
    {
        if ($lang_id != 0) {
            $this->db->where('language', $lang_id);
        }

        $data = $this->crud_model->get($this->mainDb);
        sort($data);
        print_r(json_encode($data));
    }

    public function Delete($page_id)
    {
        $res = ['status' => 0];
        if (intval($page_id) > 0) {
            $status = $this->crud_model->delete('pages', ['id' => $page_id]);
            $res = ['status' => $status];
        } else {
            $res = ['status' => 0];
        }

        echo json_encode($res);
    }
}

