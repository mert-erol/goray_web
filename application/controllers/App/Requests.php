<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Requests extends CI_Controller
{
    private $mainDb = 'requests';

    public function __construct()
    {
        parent::__construct();
        app_login_check();
        $this->cols = $this->schema_model->get_columns($this->mainDb);
        $this->colsDt = $this->schema_model->get_columns_datatables($this->mainDb);
    }

    public function index($type = 0)
    {
        if ($type == 0) redirect(base_url('App'));
        app_method_check(__CLASS__ . "-$type", 'list');
        view_engine(array('app/requests/list'), array(12), array('cols' => $this->colsDt, 'mainTbl' => $this->mainDb, 'type' => $type));
    }


    public function Edit($id)
    {
        app_method_check('request', 'edit');

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            foreach ($this->input->post() as $col => $val) {
                $this->form_validation->set_rules($col, $this->cols[$col]['comment'], $this->cols[$col]['is_null'] == 'NO' ? 'required' : 'trim');
            }

            if ($this->form_validation->run() == FALSE) {
                $res = array('result' => 0, 'message' => validation_errors());
            } else {

                $_POST['start_date'] = date2db($_POST['start_date']);
                $_POST['end_date'] = date2db($_POST['end_date']);
                $query = $this->crud_model->update($this->mainDb, array('id' => $id), $this->input->post());
                $res = array('result' => $query, 'message' => $query ? ln('MESSAGE.Updated') : ln('MESSAGE.SysError'));
            }

            echo json_encode($res);

        } else {

            $data = $this->crud_model->get($this->mainDb, array('id' => $id));
            $getEmployee = $this->crud_model->get('client_users', ['id' => $data[0]['employee_id']])[0];
            $userData = [
                'name_surname' => $getEmployee['name_surname'],
                'title' => $this->crud_model->get('titles', ['ref_id' => $getEmployee['title_ref_id'], 'language' => $this->session->userdata('app_lang_id')])[0]['title']
            ];

            view_engine(array('app/requests/edit'), array(12), array('cols' => $this->cols, 'appName' => 'edit-education', 'id' => $id, 'data' => $data[0], 'mainTbl' => $this->mainDb, 'userData' => $userData));
        }
    }

    public function Datatables($type = 1)
    {
        app_method_check(__CLASS__ . "-$type", 'list');

        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $search = $this->input->post("search");
        $search = $search['value'];
        $col = 0;
        $dir = "";

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }


        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }
        $valid_columns = array();

        foreach ($this->colsDt as $cName => $cDt):
            $valid_columns[] = $cDt['name'];
        endforeach;

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }
        if ($order != null) {
            $this->db->order_by($order, $dir);
        }


        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                $sterm = $sterm != 'employee_id' ? 'ed.' . $sterm : 'usr.name_surname';
                if ($x == 0) {
                    $this->db->like($sterm, $search);
                } else {
                    $this->db->or_like($sterm, $search);
                }
                $x++;
            }
        }


        $this->db->limit($length, $start);
        $this->db->where('ed.type', $type);
        $this->db->select('ed.id, usr.name_surname as employee_id, ed.start_date, ed.end_date, ed.content, ed.status, ed.created_at, ed.updated_at ');
        $this->db->join('client_users usr', 'ed.employee_id=usr.id', 'left');
        $this->db->from($this->mainDb . ' ed');
        $getDtData = $this->db->get();

        $oData = $getDtData->result_array();
        $data = [];
        foreach ($oData as $row) {
            $nRow = [];
            foreach ($this->colsDt as $colRow) {
                if ($colRow['name'] != 'type') {
                    $nRow[$colRow['name']] = $row[$colRow['name']];
                }
            }
            $nRow['start_date'] = date_std($row['start_date']);
            $nRow['end_date'] = date_std($row['end_date']);
            $nRow['created_at'] = date_std($row['created_at']);
            $nRow['updated_at'] = date_std($row['updated_at']);
            $data[] = $nRow;
        }

        $total = $this->db->get($this->mainDb)->num_rows();

        $result = array(
            'draw' => $draw,
            'iTotalRecords' => $total,
            'iTotalDisplayRecords' => count($oData),
            'sEcho' => 0,
            'sColumns' => 0,
            'aaData' => $data
        );

        echo json_encode($result);
        exit();
    }


}

