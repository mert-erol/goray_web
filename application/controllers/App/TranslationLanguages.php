<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TranslationLanguages extends CI_Controller
{
    private $mainDb = 'translation_languages';

    public function __construct()
    {
        parent::__construct();
        app_login_check();
        $this->cols = $this->schema_model->get_columns($this->mainDb);
        $this->colsDt = $this->schema_model->get_columns_datatables($this->mainDb);
    }

    public function index()
    {

        app_method_check(__CLASS__, 'list');
        view_engine(array('app/translation-languages/list'), array(12), array('cols' => $this->colsDt, 'mainTbl' => $this->mainDb));
    }

    public function Set($lang = NULL, $ref_id = NULL)
    {

        app_method_check(__CLASS__, 'new');

        if ($lang != NULL && $ref_id != NULL) {
            $main_data = $this->crud_model->get($this->mainDb, ['id' => $ref_id]);
            $is_exist = $this->crud_model->get($this->mainDb, ['ref_id' => $ref_id, 'language' => $lang]);
            if (count($is_exist) > 0) {
                redirect(base_url('App/' . __CLASS__ . '/Edit/' . $is_exist[0]['id']));
            } elseif (count($main_data) > 0) {
                unset($main_data[0]['id']);
                $main_data[0]['title'] = '';
                $main_data[0]['language'] = $lang;
                $main_data[0]['ref_id'] = $ref_id;
                $insert_id = $this->crud_model->set($this->mainDb, $main_data[0], TRUE);
                redirect(base_url('App/' . __CLASS__ . '/Edit/' . $insert_id));
            }
        }

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            foreach ($this->input->post() as $col => $val) {
                $this->form_validation->set_rules($col, ln($this->mainDb . '.' . $this->cols[$col]['name']), $this->cols[$col]['is_null'] == 'NO' ? 'required' : 'trim');
            }

            if ($this->form_validation->run() == FALSE) {
                $res = array('result' => 0, 'message' => validation_errors());
            } else {
                $_POST['language'] = $this->session->userdata('app_lang_id');
                $query = $this->crud_model->set($this->mainDb, $this->input->post());
                if ($query) {
                    $insert_id = $this->db->insert_id();
                    $this->crud_model->update($this->mainDb, ['id' => $insert_id], ['ref_id' => $insert_id]);
                    $res = array('result' => $query, 'message' => ln('MESSAGE.Added'), 'last_id' => $insert_id);
                } else {
                    $res = array('result' => $query, 'message' => ln('MESSAGE.SysError'));
                }
            }

            echo json_encode($res);

        } else {

            view_engine(array('app/translation-languages/new'), array(12), array('cols' => $this->cols, 'appName' => 'new-customer', 'mainTbl' => $this->mainDb));
        }
    }

    public function Edit($id)
    {
        app_method_check(__CLASS__, 'edit');

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            foreach ($this->input->post() as $col => $val) {
                $this->form_validation->set_rules($col, $this->cols[$col]['comment'], $this->cols[$col]['is_null'] == 'NO' ? 'required' : 'trim');
            }

            if ($this->form_validation->run() == FALSE) {
                $res = array('result' => 0, 'message' => validation_errors());
            } else {
                $query = $this->crud_model->update($this->mainDb, array('id' => $id), $this->input->post());
                $res = array('result' => $query, 'message' => $query ? ln('MESSAGE.Updated') : ln('MESSAGE.SysError'));
            }

            echo json_encode($res);

        } else {

            $data = $this->crud_model->get($this->mainDb, array('id' => $id));
            view_engine(array('app/translation-languages/edit'), array(12), array('cols' => $this->cols, 'appName' => 'edit-customer', 'id' => $id, 'data' => $data[0], 'mainTbl' => $this->mainDb));
        }
    }

    public function Datatables()
    {
        app_method_check(__CLASS__, 'list');

        $draw = intval($this->input->post("draw"));
        $start = intval($this->input->post("start"));
        $length = intval($this->input->post("length"));
        $order = $this->input->post("order");
        $search = $this->input->post("search");
        $search = $search['value'];
        $col = 0;
        $dir = "";

        if (!empty($order)) {
            foreach ($order as $o) {
                $col = $o['column'];
                $dir = $o['dir'];
            }
        }


        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }
        $valid_columns = array();

        foreach ($this->colsDt as $cName => $cDt):
            $valid_columns[] = $cDt['name'];
        endforeach;

        if (!isset($valid_columns[$col])) {
            $order = null;
        } else {
            $order = $valid_columns[$col];
        }
        if ($order != null) {
            $this->db->order_by($order, $dir);
        }


        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $sterm) {
                if ($x == 0) {
                    $this->db->like($sterm, $search);
                } else {
                    $this->db->or_like($sterm, $search);
                }
                $x++;
            }
        }


        $this->db->limit($length, $start);
        $this->db->select('id,title,status,created_at,updated_at');
        $this->db->where('language', $this->session->userdata('app_lang_id'));
        $getDtData = $this->db->get($this->mainDb);

        $data = $getDtData->result();

        $total = $this->db->where('language',$this->session->userdata('app_lang_id'))->get($this->mainDb)->num_rows();

        $result = array(
            'draw' => $draw,
            'iTotalRecords' => $total,
            'iTotalDisplayRecords' => $total,
            'sEcho' => 0,
            'sColumns' => 0,
            'aaData' => $data
        );

        echo json_encode($result);
        exit();
    }

    public function GetLanguages($lang_id = 0)
    {
        if ($lang_id != 0) {
            $this->db->where('language', $lang_id);
        }

        $data = $this->crud_model->get($this->mainDb);
        sort($data);
        print_r(json_encode($data));
    }


}

