<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        language_selector();
        $this->session->set_userdata('sess_guid', ($this->session->userdata('sess_guid') ? $this->session->userdata('sess_guid') : guid()));
    }

    public function index()
    {
        $guid = $this->session->userdata('sess_guid');
        $exist_files = array();
        $initialPreview = array();
        $initialPreviewConfig = array();

        if ($this->session->userdata('front_login')) {
            $order_data = $this->crud_model->get('orders', ['guid' => $guid, 'client_user' => $this->session->userdata('front_user_data')['id'], 'status' => 0]);
            if (count($order_data) == 0) {
                $order_data = $this->crud_model->get('orders', ['guid' => $guid, 'client_user' => NULL, 'status' => 0]);
            }
        } else {
            $order_data = $this->crud_model->get('orders', ['guid' => $guid, 'client_user' => NULL, 'status' => 0]);
        }

        if (count($order_data) > 0) {
            if ($order_data[0]['status'] != 0) {
                $this->session->set_userdata('sess_guid', guid()); // eğer 0 değilse yeni bir guid
            }
        }

        if (is_dir("./uploads/" . $guid)) {
            $files = scandir("./uploads/" . $guid);
            foreach ($files as $file) {
                if (!is_dir("./uploads/" . $file)) {
                    $exist_files[] = base_url("uploads/$guid/$file");
                    $initialPreview[] = base_url("uploads/$guid/$file");
                    $initialPreviewConfig[] = ['caption' => $file, 'size' => filesize("./uploads/$guid/$file"), 'width' => 120, 'url' => base_url('order/file-delete'), 'key' => $file];
                }
            }
        }

        front_view_engine(
            ['site/order/order'],
            [
                'exist_files' => $exist_files,
                'initialPreview' => json_encode($initialPreview),
                'initialPreviewConfig' => json_encode($initialPreviewConfig),
                'order_data' => count($order_data) > 0 ? $order_data[0] : []
            ]
        );
    }

    public function GetScenarios($origin_lang_ref_id)
    {

        $sys_lng_id = $this->session->userdata('app_lang_id');
        $error_level = array(99);// sistem error
        $org_langs = array();
        $response = ['status' => 0, 'error_level' => $error_level];

        $get_scenarios = $this->crud_model->get('translation_scenarios', ['origin_lang_ref_id' => $origin_lang_ref_id]);
        if (count($get_scenarios) > 0) {
            $org_langs = array();
            foreach ($get_scenarios as $scenario) {
                $org_lng_data = $this->crud_model->get('translation_languages',
                    [
                        'language' => $sys_lng_id = $this->session->userdata('app_lang_id'),
                        'ref_id' => $scenario['target_lang_ref_id']
                    ]);
                if (count($org_lng_data) > 0) {
                    $org_langs[] = ['ref_id' => $org_lng_data[0]['ref_id'], 'title' => $org_lng_data[0]['title']];
                }
            }
        } else $error_level[] = 1; // senaryo yok

        echo json_encode($org_langs);
    }

    public function Calculate()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            //$word_count = $this->input->post('std_word_count') != -1 ? intval($this->input->post('std_word_count')) : 0;
            $word_count = 0;
            $user_files = array();
            if ($this->input->post('std_word_count') != -1) {
                $word_count = intval($this->input->post('std_word_count'));
            } else {
                $file_data = $this->WordCount();
                $word_count = $file_data['word_count'];
                $user_files = $file_data['user_files'];
            }

            $order_id = 0;
            $guid = $this->session->userdata('sess_guid');
            $estimated_date = estimated_date($word_count);
            $total_amount = 0;
            $start_price = 8;
            $total_amount += $start_price;
            $error_level = array();
            $trg_lng_data = array();
            $response = ['total_amout' => $total_amount, 'error_level' => $error_level, 'total_words' => $word_count];
            $sys_lng_id = $this->session->userdata('app_lang_id');
            $org_lang = $this->input->post('origin_language');
            $trg_langs = $this->input->post('target_languages');
            $coupon_code = trim($this->input->post('coupon_code'));
            $discount = 0;

            if (strlen(intval($org_lang))) {
                $org_lng_data = $this->crud_model->get('translation_languages', ['ref_id' => $org_lang, 'language' => $sys_lng_id]);
                if (count($org_lng_data) > 0) {
                    if (count($trg_langs) > 0) {
                        foreach ($trg_langs as $tr_l) {
                            $true_trg_lng = $this->crud_model->get('translation_languages', ['ref_id' => $tr_l, 'language' => $sys_lng_id]);
                            if (count($true_trg_lng) > 0) {
                                $get_word_fee = $this->crud_model->get('translation_scenarios',
                                    [
                                        'origin_lang_ref_id' => $org_lang,
                                        'target_lang_ref_id' => $true_trg_lng[0]['ref_id']
                                    ]);
                                if (count($get_word_fee) > 0) {
                                    $total_amount += $word_count * floatval($get_word_fee[0]['word_fee']); // toplam ücret belirlendi
                                    $error_level[] = 0;
                                    if (floatval($total_amount) > 0) {
                                        if ($this->input->post('service_types')) { // servis tipleri için
                                            $stypes = $this->input->post('service_types');
                                            if (count($stypes) > 0) {
                                                foreach ($stypes as $stype) {
                                                    $get_stype = $this->crud_model->get('service_types', ['ref_id' => $stype]);
                                                    if (count($get_stype) > 0) {
                                                        if ($get_stype[0]['method'] == '%') {
                                                            $total_amount += (($total_amount / 100) * $get_stype[0]['method_value']);
                                                        }
                                                        if ($get_stype[0]['method'] == '+') {
                                                            $total_amount += floatval($get_stype[0]['method_value']);
                                                        }
                                                    } else $error_level[] = 7; // servis tipi yok
                                                }
                                            } else $error_level[] = 7; // servis tipi yok
                                        }
                                    }
                                } else $error_level[] = 6; // dil senaryosu yok
                            } else $error_level[] = 4; // target dillerinden herhangi biri yoksa
                        }
                    } else $error_level[] = 2; // hedef dil yok
                } else $error_level[] = 3; // ana dil yok
            } else $error_level[] = 1; // ana dil yok

            if (count($error_level) == 0 || in_array(0, $error_level)) {    // eğer hiçbir hata yoksa

                if (strlen($coupon_code) > 0) { // kupon kodu alanı
                    $get_coupon = $this->crud_model->get('discounts', ['title' => $coupon_code, 'status' => 1, 'order_id' => 0]);

                    if (count($get_coupon) == 0) {
                        $get_coupon = $this->crud_model->get('discounts', ['title' => $coupon_code, 'status' => 1, 'client_user' => $this->session->userdata('front_user_data')['id']]);
                    }

                    if (count($get_coupon) > 0) {
                        $this->crud_model->update('discounts', ['id' => $get_coupon[0]['id']], ['client_user' => $this->session->userdata('front_user_data')['id']]);
                        if ($get_coupon[0]['method'] == '%') {
                            $discount = (($total_amount / 100) * $get_coupon[0]['method_value']);
                            $total_amount -= $discount;
                        }
                        if ($get_coupon[0]['method'] == '-') {
                            $discount = floatval($get_coupon[0]['method_value']);
                            $total_amount -= $discount;
                        }
                    } else {
                        $error_level[] = 80; // indirim kuponu yok / hatalı
                    }
                }

                $order_data = [
                    'guid' => $guid,
                    'origin_language' => $org_lang,
                    'target_languages' => json_encode($trg_langs),
                    'notes' => $this->input->post('order_notes'),
                    'free_text' => $this->input->post('free_text'),
                    'service_types' => json_encode($this->input->post('service_types')),
                    'user_files' => json_encode($user_files),
                    'word_count' => $word_count,
                    'total_amount' => round($total_amount, 2),
                    'client_user' => $this->session->userdata('front_login') ? $this->session->userdata('front_user_data')['id'] : NULL,
                    'estimated_date' => date2db($estimated_date),
                    'discount' => $discount,
                    'status' => 2

                ];

                $is_order_exist = $this->crud_model->get('orders', ['guid' => $guid]);
                if (count($is_order_exist) > 0) {
                    $order_id = $is_order_exist[0]['id'];
                    $this->crud_model->update('orders', ['guid' => $guid], $order_data);
                } else {
                    $order_id = $this->crud_model->set('orders', $order_data, TRUE);
                }

                if ($order_id != 0) {
                    if ($discount > 0) {
                        $this->crud_model->update('discounts', ['id' => $get_coupon[0]['id']], ['order_id' => $order_id]);
                    }
                }


                $response = ['total_amount' => $total_amount, 'error_level' => $error_level, 'total_words' => $word_count, 'trg_lg' => $trg_langs, 'estimated_date' => $estimated_date, 'guid' => $guid, 'discount' => $discount];
            } else $response = ['total_amount' => $total_amount, 'error_level' => $error_level, 'total_words' => $word_count, 'trg_lg' => $trg_langs, 'estimated_date' => $estimated_date, 'guid' => $guid, 'discount' => $discount];
            echo json_encode($response);
        }
    }

    private function WordCount()
    {
        $guid = $this->session->userdata('sess_guid');
        $word_counter = 0;
        $user_files = array();
        if (is_dir("./uploads/" . $guid)) {
            $files = scandir("./uploads/" . $guid);
            foreach ($files as $file) {
                if (!is_dir("./uploads/$guid/$file")) {
                    exec("python3 ../../../home/ubuntu/CounterProjects/counter2.py ./uploads/$guid/$file", $doc_count);
                    if (count($doc_count) > 0) $word_counter += intval($doc_count[0]);
                    $user_files[] = $file;
                }
            }
        }

        return ['word_count' => $word_counter, 'user_files' => $user_files];
    }

    public function FileDelete()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $guid = $this->session->userdata('sess_guid');
            $file_name = $this->input->post('key');
            if (file_exists("./uploads/$guid/$file_name")) {
                unlink("./uploads/$guid/$file_name");
                echo json_encode(['message' => "File $file_name deleted"]);
            } else {
                echo json_encode(['message' => "File $file_name not found"]);
            }
        }
    }

    public function test()
    {
        date_default_timezone_set('Europe/Istanbul');
        $date = date('Y-m-d H:i');
        $time = date('H:i');

        $word_count = 820;
        $hourly_words = 10;
        $working_hours = 8;

        $add_hours = floor($word_count / $hourly_words) % 8;
        $add_days = floor($word_count / $hourly_words / 8);


        if (intval(date('H', strtotime($date))) > 18) {
            $date = date('Y-m-d', strtotime($date . " +1 weekdays 09:00"));

        }

        if (intval(date('H', strtotime($date))) < 9) {
            $date = date('Y-m-d', strtotime($date)) . ' 09:00';
        }


        $date = date('Y-m-d H:i:s', strtotime($date . " +$add_days weekdays 09:00"));


        $date = date('Y-m-d H:i', strtotime($date . " +$add_hours hours"));

        if (date('N', strtotime($date)) == 6) {
            $date = date('Y-m-d H:i', strtotime($date . " +1 days"));
        }


        if (date('N', strtotime($date)) == 7) {
            $date = date('Y-m-d H:i', strtotime($date . " +2 days"));
        }

        $date = date_std($date);
        echo $date;
    }


}