<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class File extends CI_Controller
{

    public function index()
    {

    }

    public function Upload()
    {

        if ($_FILES['files']) {
            $filesCount = count($_FILES['files']['name']);
            $uploadData = [];
            for ($i = 0; $i < $filesCount; $i++) {
                $_FILES['file']['name'] = $_FILES['files']['name'][$i];
                $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                $_FILES['file']['size'] = $_FILES['files']['size'][$i];


                $config['upload_path'] = './uploads/' . $this->session->userdata('sess_guid');
                !file_exists($config['upload_path']) ? mkdir($config['upload_path'], 0755, TRUE) : 0;
                $config['allowed_types'] = 'gif|jpg|png|pdf|doc|docx|xls|xlsx|ppt|pptx';
                $config['max_size'] = 50000;
                $this->load->library('upload', $config);

                $this->upload->initialize($config);
                if ($this->upload->do_upload('file')) {
                    $fileData = $this->upload->data();
                    $uploadData[$i] = $fileData['file_name'];
                }
            }
            $_POST['files'] = json_encode($uploadData);
        }


        $response = ['nothing' => TRUE, 'data' => $uploadData];


        echo json_encode($response);
    }

    public function WordCount_old()
    {

        $guid = $this->session->userdata('sess_guid');

        $word_counter = 0;
        $files = scandir("./uploads/" . $guid);
        foreach ($files as $file) {
            if (!is_dir("./uploads/" . $file)) {
                $doc_count = doc_counter("../../../var/www/cevir.io/uploads/$guid/$file");
                if (count($doc_count) > 0) {
                    $word_counter += intval($doc_count[0]);
                }
            }
        }

        echo $word_counter;
    }

    public function test()
    {
        mkdir('./uploads/sefa', 0755, TRUE);
    }

}