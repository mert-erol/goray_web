<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        front_login_check();
        $this->data['userSettings'] = $this->session->userdata('front_user_data');
    }

    public function MyAccount()
    {
        front_view_engine(['site/account/profile']);
    }

    public function Orders()
    {
        $guid = $this->session->userdata('sess_guid');
        $this->crud_model->update('orders', ['guid' => $guid], ['client_user' => $this->data['userSettings']['id']]);

        $order_data = $this->crud_model->get('orders', ['client_user' => $this->data['userSettings']['id']]);

        foreach ($order_data as $key => $ord) {
            $order_data[$key]['origin_language'] = $this->crud_model->get('translation_languages', ['ref_id' => $ord['origin_language'], 'language' => 2])[0]['title'];
            $order_data[$key]['target_languages'] = '';
            foreach (json_decode($ord['target_languages'], FALSE) as $k => $tlgn) {
                $order_data[$key]['target_languages'] .= $this->crud_model->get('translation_languages', ['ref_id' => $tlgn, 'language' => 2])[0]['title'] . ($k != count(json_decode($ord['target_languages'], FALSE)) - 1 ? ',' : '');
            }

            $order_data[$key]['service_types'] = '';


            if ($ord['service_types'] !== 'null') {
                foreach (json_decode($ord['service_types'], FALSE) as $k => $srv) {
                    $order_data[$key]['service_types'] .= $this->crud_model->get('service_types', ['ref_id' => $srv, 'language' => 2])[0]['title'] . ($k != count(json_decode($ord['service_types'], FALSE)) - 1 ? ',' : '');
                }
            }
        }

        $this->data['order_data'] = $order_data;

        front_view_engine(['site/account/orders'], $this->data);
    }

    public function Order($order_id)
    {
        $this->crud_model->update('orders', ['id' => $order_id], ['client_user' => $this->data['userSettings']['id']]);

        $order_data = $this->crud_model->get('orders', ['client_user' => $this->data['userSettings']['id']]);
        if (count($order_data) == 0) redirect(base_url('orders'));

        foreach ($order_data as $key => $ord) {
            $order_data[$key]['origin_language'] = $this->crud_model->get('translation_languages', ['ref_id' => $ord['origin_language'], 'language' => 2])[0]['title'];
            $order_data[$key]['target_languages'] = '';
            foreach (json_decode($ord['target_languages'], FALSE) as $k => $tlgn) {
                $order_data[$key]['target_languages'] .= $this->crud_model->get('translation_languages', ['ref_id' => $tlgn, 'language' => 2])[0]['title'] . ($k != count(json_decode($ord['target_languages'], FALSE)) - 1 ? ',' : '');
            }

            $order_data[$key]['service_types'] = '';


            if ($ord['service_types'] !== 'null') {
                foreach (json_decode($ord['service_types'], FALSE) as $k => $srv) {
                    $order_data[$key]['service_types'] .= $this->crud_model->get('service_types', ['ref_id' => $srv, 'language' => 2])[0]['title'] . ($k != count(json_decode($ord['service_types'], FALSE)) - 1 ? ',' : '');
                }
            }
        }

        $this->data['order_data'] = $order_data[0];

        front_view_engine(['site/account/order-detail'], $this->data);
    }


}