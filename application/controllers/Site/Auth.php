<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Auth extends CI_Controller
{

    private $mainDb = 'client_users';

    public function __construct()
    {
        parent::__construct();
        language_selector();
    }

    public function index()
    {
        redirect(base_url('login'));
    }

    public function Login()
    {
        if ($this->session->userdata('front_login')) {
            redirect(base_url());
        }

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $this->form_validation->set_rules('username', ln('users.username'), 'required');
            $this->form_validation->set_rules('password', ln('users.password'), 'required');


            if ($this->form_validation->run() == FALSE) {

                $res = array('result' => 0, 'message' => validation_errors());

            } else {


                $isUser = $this->crud_model->get($this->mainDb, array(
                    'username' => $this->input->post('username'),
                    'password' => md5($this->input->post('password')),
                    'status' => 1

                ));

                if (count($isUser) > 0) {
                    $res = array('result' => 1, 'message' => ln('MESSAGE.Login Successfully !') . ' ' . ln('MESSAGE.Redirecting'));

                    $this->session->set_userdata('front_login', TRUE);
                    $this->session->set_userdata('front_user_data', $isUser[0]);

                    $permissions = $this->crud_model->get('titles', ['id' => $isUser[0]['title_ref_id']])[0];

                    if (strlen($permissions['auths_list']) > 0) {
                        $isUser[0]['auths_list'] = $permissions['auths_list'];
                        $this->session->set_userdata('app_login', TRUE);
                        $this->session->set_userdata('app_user_data', $isUser[0]);
                    }

                    $department = $this->crud_model->get('departments', ['id' => $permissions['department']]);
                    if (count($department) > 0) $this->session->set_userdata('department', $department[0]);
                    $langData = $this->crud_model->get('languages', ['id' => $isUser[0]['locale']]);
                    $this->input->set_cookie('app_lang_dir', $langData[0]['directory'], time() + (10 * 365 * 24 * 60 * 60));

                    my_logs('<b>' . $isUser[0]['name_surname'] . '</b> Giriş Yaptı!');

                } else {
                    $res = array('result' => 0, 'message' => ln('MESSAGE.Error'));
                    my_logs('<span class="text-danger"><b>' . $this->input->post('username') . ' </b>Hatalı Giriş!</span> ');
                }
            }

            echo json_encode($res);

        } else {
            front_view_engine(['site/auth/login'], []);
        }
    }

    public function Logout()
    {
        my_logs('<b>' . app_usr('username') . '</b> Çıkış Yaptı!');
        $this->session->set_userdata('front_login', FALSE);
        $this->session->set_userdata('front_user_data', array());

        if ($this->session->userdata('app_login')) {
            $this->session->set_userdata('app_login', FALSE);
            $this->session->set_userdata('app_user_data', array());
        }

        redirect(base_url('home'));
    }

    public function ResetPassword()
    {

        $res = array('result' => 0, 'message' => ln('MESSAGE.SysError'));
        $res = array('result' => 1, 'message' => ln('MESSAGE.SendPasswordMail'));
        echo json_encode($res);
    }

    public function Register()
    {
        if ($this->session->userdata('front_login')) {
            redirect(base_url());
        }

        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $this->form_validation->set_rules('username', ln('users.username'), 'required');
            $this->form_validation->set_rules('password', ln('users.password'), 'required');
            $this->form_validation->set_rules('password_confirm', 'Password Confirm', 'required');
            $this->form_validation->set_rules('name_surname', 'Full Name', 'required');

            if ($this->form_validation->run() == FALSE) {
                $res = array('result' => 0, 'message' => validation_errors());
            } else {

                $isUser = $this->crud_model->get($this->mainDb, array(
                    'username' => $this->input->post('username'),
                    'status' => 1

                ));

                if (count($isUser) > 0) {
                    $res = array('result' => 0, 'message' => 'This account already register');
                } else {

                    $add_new_user = [
                        'username' => $this->input->post('username'),
                        'name_surname' => $this->input->post('name_surname'),
                        'password' => md5($this->input->post('password')),
                        'locale' => 2,
                        'status' => 1,
                        'title_ref_id' => 73
                    ];

                    $add = $this->crud_model->set($this->mainDb, $add_new_user, TRUE);

                    if ($add > 0) {
                        $get_new_user_data = $this->crud_model->get($this->mainDb, ['id' => $add]);

                        $this->session->set_userdata('front_login', TRUE);
                        $this->session->set_userdata('front_user_data', $get_new_user_data[0]);

                        $permissions = $this->crud_model->get('titles', ['id' => $get_new_user_data[0]['title_ref_id']])[0];

                        if (strlen($permissions['auths_list']) > 0) {
                            $get_new_user_data[0]['auths_list'] = $permissions['auths_list'];
                            $this->session->set_userdata('app_login', TRUE);
                            $this->session->set_userdata('app_user_data', $get_new_user_data[0]);
                        }

                        $department = $this->crud_model->get('departments', ['id' => $permissions['department']]);
                        if (count($department) > 0) $this->session->set_userdata('department', $department[0]);
                        $langData = $this->crud_model->get('languages', ['id' => $get_new_user_data[0]['locale']]);
                        $this->input->set_cookie('app_lang_dir', $langData[0]['directory'], time() + (10 * 365 * 24 * 60 * 60));

                        $res = array('result' => 1, 'message' => 'Successfully Sign up ! Redirecting ...');

                    } else $res = array('result' => 0, 'message' => 'System Error');
                }
            }

            echo json_encode($res);

        } else front_view_engine(['site/auth/register'], []);


    }


    public function Referrer($lang = NULL, $uri = NULL)
    {
        if($lang==NULL && $uri==NULL){
            redirect(base_url());
        } else{

        }
    }

}