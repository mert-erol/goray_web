<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Site extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        language_selector();
        $this->data['userSettings'] = $this->session->userdata('front_user_data');

    }

    public function index()
    {
        front_view_engine(['site/home/index'], $this->data);
    }

    public function order()
    {
        front_view_engine(['site/auth/order'], $this->data);
    }

    public function pages()
    {
        $total_segments = $this->uri->total_segments();
        $default_lang = available_langs()['default_lang']['code'];


        $this->session->set_flashdata('title', 'Home');
        $page = 'home';
        if ($total_segments == 0) {
            front_view_engine(['site/home/index'], $this->data);
        }

        if ($total_segments == 1) {
            front_view_engine(['site/home/index'], $this->data);
        }


        if ($total_segments == 2) {
            if ($this->uri->segment(2) == 'home') {
                $this->data['page_data'] = $this->crud_model->get('pages', ['permalink' => 'home']);
                front_view_engine(['site/home/index'], $this->data);
            } else {

                $lang_data = $this->crud_model->get('languages', ['code' => $this->uri->segment(1)]);
                if (count($lang_data) > 0) {
                    $get_page = $this->crud_model->get('pages', ['language' => $lang_data[0]['id'], 'permalink' => $this->uri->segment(2)]);
                    if (count($get_page) > 0) {
                        $this->data['page_data'] = $get_page[0];
                        $this->data['page_comments'] = $this->front_model->get_comments($get_page[0]['ref_id']);
                        $this->session->set_flashdata('title', $get_page[0]['title']);
                        $this->session->set_flashdata('meta_keywords', $get_page[0]['keywords']);
                        $this->session->set_flashdata('meta_description', $get_page[0]['description']);
                        front_view_engine(['site/base/page'], $this->data);
                    } else {
                        front_view_engine(['site/home/index'], $this->data);

                    }
                }

            }

        }

    }

    public function login()
    {
        front_view_engine(['site/base/page'], $this->data);
    }

    public function orders()
    {
        front_view_engine(['site/auth/orders'], $this->data);
    }


}