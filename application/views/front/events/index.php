<div class="view-wrapper">

    <!--Wrapper-->
    <div id="events-page" class="events-wrapper">
        <!--Events Sidebar-->
        <div class="left-panel">
            <div class="left-panel-inner has-slimscroll">

                <?php foreach ($events as $event): ?>
                    <a href="#event-<?= $event['id'] ?>" data-event-id="event-<?= $event['id'] ?>" class="scroll-link">
                            <span class="date-block">
                                <i data-feather="calendar"></i>
                                <span class="month"><?= $event['title'] ?></span>
                            </span>
                        <span class="meta-block">
                                <span class="time"><?= date_std($event['start_date'], FALSE) ?></span>
                            </span>
                    </a>

                <?php endforeach; ?>
            </div>
        </div>

        <!--Event List-->
        <div class="wrapper-inner">
            <div id="event-list" class="event-list">
                <?php foreach ($events as $event): ?>
                    <div id="event-<?= $event['id'] ?>" class="event-item">
                        <div class="event-inner-wrap">

                            <h2 class="event-title"><?= $event['title'] ?></h2>
                            <div class="event-subtitle">
                                <i data-feather="map-pin"></i>
                                <h3><?= $event['location'] ?> | <?= date_std($event['start_date']) ?> - <?= date_std($event['end_date']) ?></h3>
                            </div>
                            <div class="event-content">
                                <div class="event-description content">
                                    <?= $event['content'] ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

    </div>
</div>