<div class="view-wrapper is-full">

    <div class="stories-wrapper is-home">

        <!-- Stories -->
        <div class="inner-wrapper">
            <a class="mobile-sidebar-trigger is-story-post is-home-v2">
                <i data-feather="menu"></i>
            </a>

            <!-- Story -->
            <div class="story-post-wrapper">
                <div class="story-post">
                    <div class="post-title">
                        <h2><?= $detail['title'] ?></h2>
                    </div>
                    <div class="post-meta">
                        <div class="post-author">
                            <div class="meta">
                                <span><?= $user['name_surname'] ?></span>
                                <span><?= $user['title'] ?></span>
                            </div>
                        </div>

                        <div class="post-stats">
                            <div class="meta">
                                <span><?= date_std($detail['created_at'], FALSE) ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="post-text content">
                        <?= $detail['content'] ?>
                    </div>
                </div>
            </div>

            <!--Story post sidebar-->
            <div class="story-post-sidebar">
                <div class="header">
                    <h2><?= ln('FRONT.You might like') ?></h2>
                </div>
                <div class="related-posts">
                    <a class="related-post">
                        <div class="meta">
                            <h3>Beaches you want to discover and experiment in 2020</h3>
                            <div class="user-line">
                                <span>Elise Walker</span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

        </div>
    </div>
</div>