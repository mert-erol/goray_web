<div class="view-wrapper is-full">

    <?php $this->load->view('front/news/slider', $this->data) ?>

    <div class="section is-news">
        <div class="news-grid">
            <div class="news-card is-default">
                <div class="news-content">
                    <h3 class="news-title">Ethereum is collapsing hard on crypto exchange platforms</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quid de Pythagora? At iam decimum annum
                        in spelunca
                        iacet.
                    </p>
                    <div class="button-wrap">
                        <a class="button"><?=ln('FRONT.Read More')?></a>
                    </div>
                </div>
            </div>

        </div>

        <!-- Load more posts -->
        <div class=" load-more-wrap has-text-centered">
            <a href="#" class="load-more-button">Load More</a>
        </div>
    </div>
</div>