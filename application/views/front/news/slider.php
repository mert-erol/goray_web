<div id="news-layout" class="news-header">

    <?php foreach ($news as $nw): ?>
        <div id="top-<?= $nw['id'] ?>" class="news-hero-wrapper has-background-image is-accent fadeIn"
             data-background="https://via.placeholder.com/1600x900"
             data-demo-background="<?= base_url('assets/front') ?>/img/demo/news/bg6.jpg">
            <div class="overlay-layer">
                <div class="text-layer">
                    <div class="hero-text">
                        <h2><?= $nw['title'] ?></h2>
                        <p>
                            <?php substr($nw['content'], 0, 100) ?>
                        </p>
                        <div class="cta-wrap">
                            <a href="<?= base_url('Front/News/Detail/' . $nw['id']) ?>"
                               class="button"><?= ln('FRONT.Read More') ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>

    <!--Menu-->
    <div class="news-hero-menu">
        <div class="stories">
            <?php foreach ($news as $k => $nw): ?>
                <a data-story="top-<?= $nw['id'] ?>" class="story is-accent is-active">
                    <div class="avatar-wrap"> <?= $k + 1 ?>
                    </div>
                </a>
            <?php endforeach; ?>
        </div>
    </div>
</div>
