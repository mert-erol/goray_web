<?php if (count($newEmployees) > 0):
    foreach ($newEmployees as $em):
        ?>
        <div class="card is-new-job-card has-background-image"
             data-background="<?= base_url('assets/front') ?>/img/illustrations/cards/job-bg.svg">
            <div class="card-heading">
                <i data-feather="briefcase"></i>
            </div>
            <div class="card-body">
                <div>
                    <div class="job-content">
                        <?php printf(ln('FRONT.newjob_title'), $em['name_surname'], $em['title']); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; endif; ?>