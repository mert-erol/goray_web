<?php foreach ($posts as $post):

    ?>
    <div class="card is-post is-simple">
        <!-- Main wrap -->
        <div class="content-wrap">
            <!-- Header -->
            <div class="card-heading">
                <!-- User image -->
                <div class="user-block">
                    <div class="user-info">
                        <a href="#"><?=$post['name_surname']?></a>
                        <span class="time"><?=date_std($post['post_date'])?></span>
                    </div>
                </div>

                <div class="dropdown is-spaced is-right is-neutral dropdown-trigger">
                    <div>
                        <div class="button">
                            <i data-feather="more-vertical"></i>
                        </div>
                    </div>
                    <div class="dropdown-menu" role="menu">
                        <div class="dropdown-content">
                            <a href="#" class="dropdown-item">
                                <div class="media">
                                    <i data-feather="bookmark"></i>
                                    <div class="media-content">
                                        <h3>Bookmark</h3>
                                        <small>Add this post to your bookmarks.</small>
                                    </div>
                                </div>
                            </a>
                            <a class="dropdown-item">
                                <div class="media">
                                    <i data-feather="bell"></i>
                                    <div class="media-content">
                                        <h3>Notify me</h3>
                                        <small>Send me the updates.</small>
                                    </div>
                                </div>
                            </a>
                            <hr class="dropdown-divider">
                            <a href="#" class="dropdown-item">
                                <div class="media">
                                    <i data-feather="flag"></i>
                                    <div class="media-content">
                                        <h3>Flag</h3>
                                        <small>In case of inappropriate content.</small>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Header -->

            <!-- Post body -->
            <div class="card-body">
                <!-- Post body text -->
                <div class="post-text">
                    <?= $post['content'] ?>
                </div>

            </div>
            <!-- /Post body -->


        </div>
        <!-- /Main wrap -->

        <!-- Post #6 comments -->
        <div class="comments-wrap is-hidden">
            <!-- Header -->
            <div class="comments-heading">
                <h4>
                    Comments
                    <small>(0)</small>
                </h4>
                <div class="close-comments">
                    <i data-feather="x"></i>
                </div>
            </div>
            <!-- /Header -->

            <!-- Comments body -->
            <div class="comments-body has-slimscroll">
                <div class="comments-placeholder">
                    <img src="<?= base_url('assets/front') ?>/img/icons/feed/bubble.svg" alt="">
                    <h3>Nothing in here yet</h3>
                    <p>Be the first to post a comment.</p>
                </div>
            </div>
            <!-- /Comments body -->

            <!-- Comments footer -->
            <div class="card-footer">
                <div class="media post-comment">
                    <!-- User image -->
                    <div class="media-left">
                        <p class="image is-48x48">
                            <img src="https://via.placeholder.com/300x300"
                                 data-demo-src="<?= base_url('assets/front') ?>/img/avatars/jenna.png"
                                 data-user-popover="8" alt="">
                        </p>
                    </div>
                    <!-- Textarea -->
                    <div class="media-content">
                        <div class="field">
                            <p class="control">
                                                <textarea class="textarea comment-textarea" rows="5"
                                                          placeholder="Write a comment..."></textarea>
                            </p>
                        </div>
                        <!-- Additional actions -->
                        <div class="actions">
                            <div class="action is-auto">
                                <i data-feather="at-sign"></i>
                            </div>
                            <div class="action is-upload">
                                <i data-feather="camera"></i>
                                <input type="file">
                            </div>
                            <a class="button primary-button is-solid raised">Post Comment</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Comments footer -->
        </div>
        <!-- /Post #6 comments -->
    </div>
<?php endforeach; ?>