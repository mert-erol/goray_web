<div class="card has-background-image" style="background-color: #FF645E"
     data-background="<?= base_url('assets/front') ?>/img/illustrations/cards/birthday-bg.svg">
    <div class="card-heading is-bordered">
        <h4 style="color:white"><?= ln('FRONT.Birthdays') ?></h4>
    </div>
    <div class="card-body no-padding">
        <?php
        foreach ($birtdays as $fl): ?>
            <div class="add-friend-block transition-block">
                <div class="page-meta">
                    <span><?= date_std($fl['date'], FALSE, 'd M D') ?></span>
                    <span style="color:white"><?= $fl['name_surname'] ?></span>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>