<div class="view-wrapper">


    <!-- Container -->
    <div id="main-feed" class="container">


        <!-- Content placeholders at page load -->
        <!-- this holds the animated content placeholders that show up before content -->
        <div id="shadow-dom" class="view-wrap">
            <div class="columns">
                <div class="column is-3">
                    <!-- Placeload element -->
                    <div class="placeload weather-widget-placeload">
                        <div class="header">
                            <div class="inner-wrap">
                                <div class="img loads"></div>
                                <div class="content-shape loads"></div>
                                <div class="content-shape loads"></div>
                            </div>
                        </div>
                        <div class="body">
                            <div class="inner-wrap">
                                <div class="rect loads"></div>
                                <div class="content-shape loads"></div>
                                <div class="content-shape loads"></div>
                            </div>
                        </div>
                    </div>
                    <!-- Placeload element -->
                    <div class="placeload list-placeload">
                        <div class="header">
                            <div class="content-shape loads"></div>
                        </div>
                        <div class="body">
                            <div class="flex-block">
                                <div class="img loads"></div>
                                <div class="inner-wrap">
                                    <div class="content-shape loads"></div>
                                    <div class="content-shape loads"></div>
                                </div>
                            </div>
                            <div class="flex-block">
                                <div class="img loads"></div>
                                <div class="inner-wrap">
                                    <div class="content-shape loads"></div>
                                    <div class="content-shape loads"></div>
                                </div>
                            </div>
                            <div class="flex-block">
                                <div class="img loads"></div>
                                <div class="inner-wrap">
                                    <div class="content-shape loads"></div>
                                    <div class="content-shape loads"></div>
                                </div>
                            </div>
                            <div class="flex-block">
                                <div class="img loads"></div>
                                <div class="inner-wrap">
                                    <div class="content-shape loads"></div>
                                    <div class="content-shape loads"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="column is-6">


                    <!-- Placeload element -->
                    <div class="placeload compose-placeload">
                        <div class="header">
                            <div class="content-shape is-lg loads"></div>
                            <div class="content-shape is-lg loads"></div>
                            <div class="content-shape is-lg loads"></div>
                        </div>
                        <div class="body">
                            <div class="img loads"></div>
                            <div class="inner-wrap">
                                <div class="content-shape loads"></div>
                                <div class="content-shape loads"></div>
                            </div>
                        </div>
                    </div>
                    <!-- Placeload element -->
                    <div class="placeload post-placeload">
                        <div class="header">
                            <div class="img loads"></div>
                            <div class="header-content">
                                <div class="content-shape loads"></div>
                                <div class="content-shape loads"></div>
                            </div>
                        </div>
                        <div class="image-placeholder loads"></div>
                        <div class="placeholder-footer">
                            <div class="footer-block">
                                <div class="img loads"></div>
                                <div class="inner-wrap">
                                    <div class="content-shape loads"></div>
                                    <div class="content-shape loads"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Placeload element -->
                    <div class="placeload post-placeload">
                        <div class="header">
                            <div class="img loads"></div>
                            <div class="header-content">
                                <div class="content-shape loads"></div>
                                <div class="content-shape loads"></div>
                            </div>
                        </div>
                        <div class="image-placeholder loads"></div>
                        <div class="placeholder-footer">
                            <div class="footer-block">
                                <div class="img loads"></div>
                                <div class="inner-wrap">
                                    <div class="content-shape loads"></div>
                                    <div class="content-shape loads"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Placeload element -->
                    <div class="placeload post-placeload">
                        <div class="header">
                            <div class="img loads"></div>
                            <div class="header-content">
                                <div class="content-shape loads"></div>
                                <div class="content-shape loads"></div>
                            </div>
                        </div>
                        <div class="image-placeholder loads"></div>
                        <div class="placeholder-footer">
                            <div class="footer-block">
                                <div class="img loads"></div>
                                <div class="inner-wrap">
                                    <div class="content-shape loads"></div>
                                    <div class="content-shape loads"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Placeload element -->
                    <div class="placeload post-placeload">
                        <div class="header">
                            <div class="img loads"></div>
                            <div class="header-content">
                                <div class="content-shape loads"></div>
                                <div class="content-shape loads"></div>
                            </div>
                        </div>
                        <div class="image-placeholder loads"></div>
                        <div class="placeholder-footer">
                            <div class="footer-block">
                                <div class="img loads"></div>
                                <div class="inner-wrap">
                                    <div class="content-shape loads"></div>
                                    <div class="content-shape loads"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="column is-3">

                    <!-- Placeload element -->
                    <div class="placeload stories-placeload">
                        <div class="header">
                            <div class="content-shape loads"></div>
                        </div>
                        <div class="body">
                            <div class="flex-block">
                                <div class="img loads"></div>
                                <div class="inner-wrap">
                                    <div class="content-shape loads"></div>
                                    <div class="content-shape loads"></div>
                                </div>
                            </div>
                            <div class="flex-block">
                                <div class="img loads"></div>
                                <div class="inner-wrap">
                                    <div class="content-shape loads"></div>
                                    <div class="content-shape loads"></div>
                                </div>
                            </div>
                            <div class="flex-block">
                                <div class="img loads"></div>
                                <div class="inner-wrap">
                                    <div class="content-shape loads"></div>
                                    <div class="content-shape loads"></div>
                                </div>
                            </div>
                            <div class="flex-block">
                                <div class="img loads"></div>
                                <div class="inner-wrap">
                                    <div class="content-shape loads"></div>
                                    <div class="content-shape loads"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Placeload element -->
                    <div class="placeload mini-widget-placeload">
                        <div class="body">
                            <div class="inner-wrap">
                                <div class="img loads"></div>
                                <div class="content-shape loads"></div>
                                <div class="content-shape loads"></div>
                                <div class="content-shape loads"></div>
                                <div class="button-shape loads"></div>
                            </div>
                        </div>
                    </div>
                    <!-- Placeload element -->
                    <div class="placeload list-placeload">
                        <div class="header">
                            <div class="content-shape loads"></div>
                        </div>
                        <div class="body">
                            <div class="flex-block">
                                <div class="img loads"></div>
                                <div class="inner-wrap">
                                    <div class="content-shape loads"></div>
                                    <div class="content-shape loads"></div>
                                </div>
                            </div>
                            <div class="flex-block">
                                <div class="img loads"></div>
                                <div class="inner-wrap">
                                    <div class="content-shape loads"></div>
                                    <div class="content-shape loads"></div>
                                </div>
                            </div>
                            <div class="flex-block">
                                <div class="img loads"></div>
                                <div class="inner-wrap">
                                    <div class="content-shape loads"></div>
                                    <div class="content-shape loads"></div>
                                </div>
                            </div>
                            <div class="flex-block">
                                <div class="img loads"></div>
                                <div class="inner-wrap">
                                    <div class="content-shape loads"></div>
                                    <div class="content-shape loads"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- Feed page main wrapper -->
        <div id="activity-feed" class="view-wrap true-dom is-hidden">
            <div class="columns">
                <div class="column is-12">
                    <a href="https://ihidalgakiran.com/" target="_blank">
                        <img src="<?= base_url('assets/front/img') ?>/idk-blog-banner-02.png" alt="">
                    </a>
                </div>
            </div>
            <div class="columns">
                <!-- Left side column -->
                <div class="column is-3 is-hidden-mobile">
                    <div class="card is-weather-card has-background-image"
                         data-background="<?= base_url('assets/front') ?>/img/illustrations/cards/weather-bg.svg">

                        <div class="card-body">
                            <div class="temperature">
                                <span>18</span>
                            </div>
                            <div class="weather-icon">
                                <div>
                                    <i data-feather="sun"></i>
                                    <h4>Sunny</h4>
                                    <div class="details">
                                        <span>Real Feel: 78° </span>
                                        <span>Rain Chance: 5%</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="card">
                        <div class="card-heading is-bordered">
                            <h4><?= ln('FRONT.Menu') ?></h4>

                        </div>
                        <div class="card-body no-padding">
                            <!-- Recommended Page -->
                            <a href="<?= base_url('Front/News') ?>">
                                <div class="page-block transition-block">
                                    <div class="page-meta">
                                        <span><?= ln('FRONT.News') ?></span>
                                        <span><?= ln('FRONT.News_sub') ?></span>
                                    </div>
                                    <div class="add-page add-transition">
                                        <i data-feather="bookmark"></i>
                                    </div>
                                </div>
                            </a>
                            <!-- Recommended Page -->
                            <a href="<?= base_url('Front/Events') ?>">
                                <div class="page-block transition-block">

                                    <div class="page-meta">
                                        <span><?= ln('FRONT.Events') ?></span>
                                        <span><?= ln('FRONT.Events_sub') ?></span>
                                    </div>
                                    <div class="add-page add-transition">
                                        <i data-feather="bookmark"></i>
                                    </div>
                                </div>
                            </a>

                            <a href="<?= base_url('Front/FoodList') ?>">
                                <div class="page-block transition-block">
                                    <div class="page-meta">
                                        <span><?= ln('FRONT.Food List') ?></span>
                                        <span><?= ln('FRONT.Food List_sub') ?></span>
                                    </div>
                                    <div class="add-page add-transition">
                                        <i data-feather="bookmark"></i>
                                    </div>
                                </div>
                            </a>

                            <a href="<?= base_url('Front/Profile/Requests') ?>">
                                <div class="page-block transition-block">
                                    <div class="page-meta">
                                        <span><?= ln('FRONT.Requests') ?></span>
                                        <span><?= ln('FRONT.Leave Apply_sub') ?></span>
                                    </div>
                                    <div class="add-page add-transition">
                                        <i data-feather="bookmark"></i>
                                    </div>
                                </div>
                            </a>

                            <a href="<?= base_url('Front/Profile/Department') ?>">
                                <div class="page-block transition-block">
                                    <div class="page-meta">
                                        <span><?= ln('FRONT.My Department') ?></span>
                                        <span><?= ln('FRONT.My Department_sub') ?></span>
                                    </div>
                                    <div class="add-page add-transition">
                                        <i data-feather="bookmark"></i>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div id="latest-activity-1" class="card">
                        <div class="card-heading is-bordered">
                            <h4><?= ln('FRONT.Campaign') ?></h4>
                        </div>
                        <div class="card-body no-padding">

                            <?php foreach ($campaigns as $cp): ?>

                                <div class="page-block">
                                    <div class="page-meta">
                                        <span><?= $cp['title'] ?></span>
                                        <span><?= date_std($cp['start_date'], FALSE) ?>
                                            - <?= date_std($cp['end_date'], FALSE) ?></span>
                                    </div>
                                </div>

                            <?php endforeach; ?>


                        </div>
                    </div>


                    <a href="<?= base_url('Front/Requests/Apply/5') ?>">
                        <div class="card stc-card">
                            <img src="<?= base_url('assets/front/img/dilek-sikayet.png') ?>" alt="">

                        </div>
                    </a>

                    <a href="<?= base_url('Front/Requests/Apply/3') ?>">
                        <div class="card stc-card">
                            <img src="<?= base_url('assets/front/img/ramak-kala.png') ?>" alt="">
                        </div>
                    </a>

                    <a href="<?= base_url('Front/FoodList') ?>">
                        <div class="card stc-card">
                            <img src="<?= base_url('assets/front/img/yemek-listesi.png') ?>" alt="">
                        </div>
                    </a>


                </div>
                <!-- /Left side column -->

                <!-- Middle column -->
                <div class="column is-6">

                    <?php $this->load->view('front/news/slider', ['news' => $news]); ?>

                    <?php $this->load->view('front/feed/post/compose'); ?>

                    <div id="timeline"></div>

                    <div class=" load-more-wrap narrow-top has-text-centered">
                        <a href="#" class="load-more-button">Load More</a>
                    </div>
                    <!-- /Load more posts -->

                </div>
                <!-- /Middle column -->

                <!-- Right side column -->
                <div class="column is-3">

                    <!-- Stories widget -->
                    <!-- /partials/widgets/stories-widget.html -->
                    <div class="card">
                        <div class="card-heading is-bordered">
                            <h4><?= ln('FRONT.Notices') ?></h4>

                        </div>
                        <div class="card-body no-padding">

                            <?php foreach ($announcements as $an): ?>
                                <!-- Story block -->
                                <div class="story-block">
                                    <div class="story-meta">
                                        <span><?= $an['title'] ?></span>
                                        <span><?= timeago($an['updated_at']) ?></span>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>


                    <?php $this->load->view('front/feed/birthday/list', $this->data); ?>
                    <?php $this->load->view('front/foodlist/list', $this->data); ?>
                    <?php $this->load->view('front/feed/new-job', $this->data); ?>


                    <div class="card">
                        <div class="card-heading is-bordered">
                            <h4><?= ln('FRONT.Exchange Rate') ?></h4>

                        </div>


                            <table class="table is-hoverable is-fullwidth is-size-7">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th><?= ln('FRONT.Exchange Rate_buy') ?></th>
                                    <th><?= ln('FRONT.Exchange Rate_sell') ?></th>
                                </tr>
                                </thead>
                                <tbody>

                                <tr>
                                    <td>USD</td>
                                    <td><?= $foreignCurrency['dolar_alis'] ?></td>
                                    <td><?= $foreignCurrency['dolar_satis'] ?></td>
                                </tr>

                                <tr>
                                    <td>EURO</td>
                                    <td><?= $foreignCurrency['euro_alis'] ?></td>
                                    <td><?= $foreignCurrency['euro_satis'] ?></td>
                                </tr>

                                <tr>
                                    <td>POUND</td>
                                    <td><?= $foreignCurrency['pound_alis'] ?></td>
                                    <td><?= $foreignCurrency['pound_satis'] ?></td>
                                </tr>

                                </tbody>
                            </table>

                    </div>


                </div>
                <!-- /Right side column -->
            </div>
        </div>
        <!-- /Feed page main wrapper -->
    </div>
    <!-- /Container -->

    <!-- Create group modal in compose card -->
    <!-- /partials/pages/feed/modals/create-group-modal.html -->
    <div id="create-group-modal" class="modal create-group-modal is-light-bg">
        <div class="modal-background"></div>
        <div class="modal-content">

            <div class="card">
                <div class="card-heading">
                    <h3>Create group</h3>
                    <!-- Close X button -->
                    <div class="close-wrap">
                                <span class="close-modal">
                                    <i data-feather="x"></i>
                                </span>
                    </div>
                </div>
                <!-- Modal subheading -->
                <div class="subheading">
                    <!-- Group avatar -->
                    <div class="group-avatar">
                        <input id="group-avatar-upload" type="file">
                        <div class="add-photo">
                            <i data-feather="plus"></i>
                        </div>
                    </div>
                    <!-- Group name -->
                    <div class="control">
                        <input type="text" class="input" placeholder="Give the group a name">
                    </div>
                </div>
                <div class="card-body">
                    <div class="inner">
                        <div class="left-section">
                            <div class="search-subheader">
                                <div class="control">
                                    <input type="text" class="input" placeholder="Search for friends to add">
                                    <span class="icon">
                                                <i data-feather="search"></i>
                                            </span>
                                </div>
                            </div>
                            <div id="new-group-list" class="user-list has-slimscroll">

                                <!-- Friend -->
                                <div class="friend-block" data-ref="ref-1">
                                    <img class="friend-avatar" src="https://via.placeholder.com/300x300"
                                         data-demo-src="<?= base_url('assets/front') ?>/img/avatars/dan.jpg" alt="">
                                    <div class="friend-name">Dan Walker</div>
                                    <div class="round-checkbox is-small">
                                        <div>
                                            <input type="checkbox" id="checkbox-group-1">
                                            <label for="checkbox-group-1"></label>
                                        </div>
                                    </div>
                                </div>
                                <!-- Friend -->
                                <div class="friend-block" data-ref="ref-2">
                                    <img class="friend-avatar" src="https://via.placeholder.com/300x300"
                                         data-demo-src="<?= base_url('assets/front') ?>/img/avatars/daniel.jpg" alt="">
                                    <div class="friend-name">Daniel Wellington</div>
                                    <div class="round-checkbox is-small">
                                        <div>
                                            <input type="checkbox" id="checkbox-group-2">
                                            <label for="checkbox-group-2"></label>
                                        </div>
                                    </div>
                                </div>
                                <!-- Friend -->
                                <div class="friend-block" data-ref="ref-3">
                                    <img class="friend-avatar" src="https://via.placeholder.com/300x300"
                                         data-demo-src="<?= base_url('assets/front') ?>/img/avatars/stella.jpg" alt="">
                                    <div class="friend-name">Stella Bergmann</div>
                                    <div class="round-checkbox is-small">
                                        <div>
                                            <input type="checkbox" id="checkbox-group-3">
                                            <label for="checkbox-group-3"></label>
                                        </div>
                                    </div>
                                </div>
                                <!-- Friend -->
                                <div class="friend-block" data-ref="ref-4">
                                    <img class="friend-avatar" src="https://via.placeholder.com/300x300"
                                         data-demo-src="<?= base_url('assets/front') ?>/img/avatars/david.jpg" alt="">
                                    <div class="friend-name">David Kim</div>
                                    <div class="round-checkbox is-small">
                                        <div>
                                            <input type="checkbox" id="checkbox-group-4">
                                            <label for="checkbox-group-4"></label>
                                        </div>
                                    </div>
                                </div>
                                <!-- Friend -->
                                <div class="friend-block" data-ref="ref-5">
                                    <img class="friend-avatar" src="https://via.placeholder.com/300x300"
                                         data-demo-src="<?= base_url('assets/front') ?>/img/avatars/nelly.png" alt="">
                                    <div class="friend-name">Nelly Schwartz</div>
                                    <div class="round-checkbox is-small">
                                        <div>
                                            <input type="checkbox" id="checkbox-group-5">
                                            <label for="checkbox-group-5"></label>
                                        </div>
                                    </div>
                                </div>
                                <!-- Friend -->
                                <div class="friend-block" data-ref="ref-6">
                                    <img class="friend-avatar" src="https://via.placeholder.com/300x300"
                                         data-demo-src="<?= base_url('assets/front') ?>/img/avatars/elise.jpg" alt="">
                                    <div class="friend-name">Elise Walker</div>
                                    <div class="round-checkbox is-small">
                                        <div>
                                            <input type="checkbox" id="checkbox-group-6">
                                            <label for="checkbox-group-6"></label>
                                        </div>
                                    </div>
                                </div>
                                <!-- Friend -->
                                <div class="friend-block" data-ref="ref-7">
                                    <img class="friend-avatar" src="https://via.placeholder.com/300x300"
                                         data-demo-src="<?= base_url('assets/front') ?>/img/avatars/bobby.jpg" alt="">
                                    <div class="friend-name">Bobby Brown</div>
                                    <div class="round-checkbox is-small">
                                        <div>
                                            <input type="checkbox" id="checkbox-group-7">
                                            <label for="checkbox-group-7"></label>
                                        </div>
                                    </div>
                                </div>
                                <!-- Friend -->
                                <div class="friend-block" data-ref="ref-8">
                                    <img class="friend-avatar" src="https://via.placeholder.com/300x300"
                                         data-demo-src="<?= base_url('assets/front') ?>/img/avatars/lana.jpeg" alt="">
                                    <div class="friend-name">Lana Henrikssen</div>
                                    <div class="round-checkbox is-small">
                                        <div>
                                            <input type="checkbox" id="checkbox-group-8">
                                            <label for="checkbox-group-8"></label>
                                        </div>
                                    </div>
                                </div>
                                <!-- Friend -->
                                <div class="friend-block" data-ref="ref-9">
                                    <img class="friend-avatar" src="https://via.placeholder.com/300x300"
                                         data-demo-src="<?= base_url('assets/front') ?>/img/avatars/gaelle.jpeg" alt="">
                                    <div class="friend-name">Gaelle Morris</div>
                                    <div class="round-checkbox is-small">
                                        <div>
                                            <input type="checkbox" id="checkbox-group-9">
                                            <label for="checkbox-group-9"></label>
                                        </div>
                                    </div>
                                </div>
                                <!-- Friend -->
                                <div class="friend-block" data-ref="ref-10">
                                    <img class="friend-avatar" src="https://via.placeholder.com/300x300"
                                         data-demo-src="<?= base_url('assets/front') ?>/img/avatars/mike.jpg" alt="">
                                    <div class="friend-name">Mike Lasalle</div>
                                    <div class="round-checkbox is-small">
                                        <div>
                                            <input type="checkbox" id="checkbox-group-10">
                                            <label for="checkbox-group-10"></label>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="right-section has-slimscroll">
                            <div class="selected-count">
                                <span>Selected</span>
                                <span id="selected-friends-count">0</span>
                            </div>

                            <div id="selected-list" class="selected-list"></div>

                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="button" class="button is-solid grey-button close-modal">Cancel</button>
                    <button type="button" class="button is-solid accent-button close-modal">Create a Group</button>
                </div>
            </div>

        </div>
    </div>
    <!-- Albums onboarding modal -->
    <!-- /partials/pages/feed/modals/albums-help-modal.html -->
    <div id="albums-help-modal" class="modal albums-help-modal is-xsmall has-light-bg">
        <div class="modal-background"></div>
        <div class="modal-content">

            <div class="card">
                <div class="card-heading">
                    <h3>Add Photos</h3>
                    <!-- Close X button -->
                    <div class="close-wrap">
                                <span class="close-modal">
                                    <i data-feather="x"></i>
                                </span>
                    </div>
                </div>
                <div class="card-body">
                    <div class="content-block is-active">
                        <img src="<?= base_url('assets/front') ?>/img/illustrations/cards/albums.svg" alt="">
                        <div class="help-text">
                            <h3>Manage your photos</h3>
                            <p>Lorem ipsum sit dolor amet is a dummy text used by the typography industry and the web
                                industry.</p>
                        </div>
                    </div>

                    <div class="content-block">
                        <img src="<?= base_url('assets/front') ?>/img/illustrations/cards/upload.svg" alt="">
                        <div class="help-text">
                            <h3>Upload your photos</h3>
                            <p>Lorem ipsum sit dolor amet is a dummy text used by the typography industry and the web
                                industry.</p>
                        </div>
                    </div>

                    <div class="slide-dots">
                        <div class="dot is-active"></div>
                        <div class="dot"></div>
                    </div>
                    <div class="action">
                        <button type="button" class="button is-solid accent-button next-modal raised"
                                data-modal="albums-modal">Next
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Album upload modal -->
    <!-- /partials/pages/feed/modals/albums-modal.html -->
    <div id="albums-modal" class="modal albums-modal is-xxl has-light-bg">
        <div class="modal-background"></div>
        <div class="modal-content">

            <div class="card">
                <div class="card-heading">
                    <h3>New album</h3>
                    <div class="button is-solid accent-button fileinput-button">
                        <i class="mdi mdi-plus"></i>
                        Add pictures/videos
                    </div>

                    <!-- Close X button -->
                    <div class="close-wrap">
                                <span class="close-modal">
                                    <i data-feather="x"></i>
                                </span>
                    </div>
                </div>
                <div class="card-body">
                    <div class="left-section">
                        <div class="album-form">
                            <div class="control">
                                <input type="text" class="input is-sm no-radius is-fade" placeholder="Album name">
                                <div class="icon">
                                    <i data-feather="camera"></i>
                                </div>
                            </div>
                            <div class="control">
                                <textarea class="textarea is-fade no-radius is-sm" rows="3"
                                          placeholder="describe your album ..."></textarea>
                            </div>
                            <div class="control">
                                <input type="text" class="input is-sm no-radius is-fade" placeholder="Place">
                                <div class="icon">
                                    <i data-feather="map-pin"></i>
                                </div>
                            </div>
                        </div>

                        <div id="album-date" class="album-date">
                            <div class="head">
                                <h4>Change the date</h4>
                                <button type="button" class="button is-solid dark-grey-button icon-button">
                                    <i data-feather="plus"></i>
                                </button>
                            </div>

                            <p>Set a date for your album. You can always change it later.</p>
                            <div class="control is-hidden">
                                <input id="album-datepicker" type="text" class="input is-sm is-fade"
                                       placeholder="Select a date">
                                <div class="icon">
                                    <i data-feather="calendar"></i>
                                </div>
                            </div>
                        </div>

                        <div id="tagged-in-album" class="tagged-in-album">
                            <div class="head">
                                <h4>Tag friends in this album</h4>
                                <button type="button" class="button is-solid dark-grey-button icon-button">
                                    <i data-feather="plus"></i>
                                </button>
                            </div>

                            <p>You can tag friends in this album. Tagged friends can only see the photos they are tagged
                                in.</p>
                            <div class="field is-autocomplete is-hidden">
                                <div class="control">
                                    <input id="create-album-friends-autocpl" type="text" class="input is-sm is-fade"
                                           placeholder="Search for friends">
                                    <div class="icon">
                                        <i data-feather="search"></i>
                                    </div>
                                </div>
                            </div>

                            <div id="album-tag-list" class="album-tag-list"></div>

                        </div>

                        <div class="shared-album">
                            <div class="head">
                                <h4>Allow friends to add photos</h4>
                                <div class="basic-checkbox">
                                    <input class="styled-checkbox" id="styled-checkbox-1" type="checkbox"
                                           value="value1">
                                    <label for="styled-checkbox-1"></label>
                                </div>
                            </div>

                            <p>If you enable this feature, your tagged friends will be able to share content inside this
                                album.</p>
                        </div>

                    </div>
                    <div class="right-section has-slimscroll">

                        <div class="modal-uploader">
                            <div id="actions" class="columns is-multiline no-mb">
                                <div class="column is-12">
                                            <span class="button has-icon is-solid grey-button fileinput-button">
                                                <i data-feather="plus"></i>
                                            </span>
                                    <button type="submit" class="button start is-hidden">
                                        <span>Upload</span>
                                    </button>
                                    <button type="reset" class="button is-solid grey-button cancel">
                                        <span>Clear all</span>
                                    </button>
                                    <span class="file-count">
                                                <span id="modal-uploader-file-count">0</span> file(s) selected
                                            </span>
                                </div>

                                <div class="column is-12 is-hidden">
                                    <!-- The global file processing state -->
                                    <div class="fileupload-process">
                                        <div id="total-progress" class="progress progress-striped active"
                                             role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                                            <div class="progress-bar progress-bar-success" style="width:0%;"
                                                 data-dz-uploadprogress></div>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="columns is-multiline" id="previews">

                                <div id="template" class="column is-4 is-template">
                                    <div class="preview-box">
                                        <!-- This is used as the file preview template -->
                                        <div class="remove-button" data-dz-remove>
                                            <i class="mdi mdi-close"></i>
                                        </div>
                                        <div>
                                            <span class="preview"><img src="https://via.placeholder.com/120x120"
                                                                       data-dz-thumbnail alt=""/></span>
                                        </div>
                                        <div class="preview-body">
                                            <div class="item-meta">
                                                <div>
                                                    <p class="name" data-dz-name></p>
                                                    <strong class="error text-danger" data-dz-errormessage></strong>
                                                </div>
                                                <small class="size" data-dz-size></small>
                                            </div>
                                            <div class="upload-item-progress">
                                                <div class="progress active" role="progressbar" aria-valuemin="0"
                                                     aria-valuemax="100" aria-valuenow="0">
                                                    <div class="progress-bar progress-bar-success"
                                                         data-dz-uploadprogress></div>
                                                </div>
                                            </div>
                                            <div class="upload-item-description">
                                                <div class="control">
                                                    <textarea class="textarea is-small is-fade no-radius" rows="4"
                                                              placeholder="Describe this photo ..."></textarea>
                                                </div>
                                            </div>
                                            <div class="upload-item-actions is-hidden">
                                                <button class="button start is-hidden">
                                                    <span>Start</span>
                                                </button>
                                                <button data-dz-remove class="button cancel is-hidden">
                                                    <span>Cancel</span>
                                                </button>
                                                <button data-dz-remove class="button delete is-hidden">
                                                    <span>Delete</span>
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <!-- Dropdown menu -->
                    <div class="dropdown is-up is-spaced is-modern is-neutral is-right dropdown-trigger">
                        <div>
                            <button class="button" aria-haspopup="true">
                                <i class="main-icon" data-feather="smile"></i>
                                <span>Friends</span>
                                <i class="caret" data-feather="chevron-down"></i>
                            </button>
                        </div>
                        <div class="dropdown-menu" role="menu">
                            <div class="dropdown-content">
                                <a href="#" class="dropdown-item">
                                    <div class="media">
                                        <i data-feather="globe"></i>
                                        <div class="media-content">
                                            <h3>Public</h3>
                                            <small>Anyone can see this publication.</small>
                                        </div>
                                    </div>
                                </a>
                                <a class="dropdown-item">
                                    <div class="media">
                                        <i data-feather="users"></i>
                                        <div class="media-content">
                                            <h3>Friends</h3>
                                            <small>only friends can see this publication.</small>
                                        </div>
                                    </div>
                                </a>
                                <a class="dropdown-item">
                                    <div class="media">
                                        <i data-feather="user"></i>
                                        <div class="media-content">
                                            <h3>Specific friends</h3>
                                            <small>Don't show it to some friends.</small>
                                        </div>
                                    </div>
                                </a>
                                <hr class="dropdown-divider">
                                <a class="dropdown-item">
                                    <div class="media">
                                        <i data-feather="lock"></i>
                                        <div class="media-content">
                                            <h3>Only me</h3>
                                            <small>Only me can see this publication.</small>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <button type="button" class="button is-solid accent-button close-modal">Create album</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Live video onboarding modal -->
    <!-- /partials/pages/feed/modals/videos-help-modal.html -->
    <div id="videos-help-modal" class="modal videos-help-modal is-xsmall has-light-bg">
        <div class="modal-background"></div>
        <div class="modal-content">

            <div class="card">
                <div class="card-heading">
                    <h3>Add Photos</h3>
                    <!-- Close X button -->
                    <div class="close-wrap">
                                <span class="close-modal">
                                    <i data-feather="x"></i>
                                </span>
                    </div>
                </div>
                <div class="card-body">
                    <div class="content-block is-active">
                        <img src="<?= base_url('assets/front') ?>/img/illustrations/cards/videotrip.svg" alt="">
                        <div class="help-text">
                            <h3>Share live videos</h3>
                            <p>Lorem ipsum sit dolor amet is a dummy text used by the typography industry and the web
                                industry.</p>
                        </div>
                    </div>

                    <div class="content-block">
                        <img src="<?= base_url('assets/front') ?>/img/illustrations/cards/videocall.svg" alt="">
                        <div class="help-text">
                            <h3>To build your audience</h3>
                            <p>Lorem ipsum sit dolor amet is a dummy text used by the typography industry and the web
                                industry.</p>
                        </div>
                    </div>

                    <div class="slide-dots">
                        <div class="dot is-active"></div>
                        <div class="dot"></div>
                    </div>
                    <div class="action">
                        <button type="button" class="button is-solid accent-button next-modal raised"
                                data-modal="videos-modal">Next
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Live video modal -->
    <!-- /partials/pages/feed/modals/videos-modal.html -->
    <div id="videos-modal" class="modal videos-modal is-xxl has-light-bg">
        <div class="modal-background"></div>
        <div class="modal-content">

            <div class="card">
                <div class="card-heading">
                    <h3>Go live</h3>
                    <div id="stop-stream" class="button is-solid accent-button is-hidden" onclick="stopWebcam();">
                        <i class="mdi mdi-video-off"></i>
                        Stop stream
                    </div>
                    <div id="start-stream" class="button is-solid accent-button" onclick="startWebcam();">
                        <i class="mdi mdi-video"></i>
                        Start stream
                    </div>


                    <!-- Close X button -->
                    <div class="close-wrap">
                                <span class="close-modal">
                                    <i data-feather="x"></i>
                                </span>
                    </div>
                </div>
                <div class="card-body">
                    <div class="inner">
                        <div class="left-section">
                            <div class="video-wrapper">
                                <div class="video-wrap">
                                    <div id="live-indicator" class="live is-vhidden">Live</div>
                                    <video id="video" width="400" height="240" controls autoplay></video>
                                </div>
                            </div>
                        </div>
                        <div class="right-section">
                            <div class="header">
                                <img src="https://via.placeholder.com/300x300"
                                     data-demo-src="<?= base_url('assets/front') ?>/img/avatars/jenna.png" alt="">
                                <div class="user-meta">
                                    <span>Jenna Davis <small>is live</small></span>
                                    <span><small>right now</small></span>
                                </div>
                                <button type="button" class="button">Follow</button>
                                <div class="dropdown is-spaced is-right dropdown-trigger">
                                    <div>
                                        <div class="button">
                                            <i data-feather="more-vertical"></i>
                                        </div>
                                    </div>
                                    <div class="dropdown-menu" role="menu">
                                        <div class="dropdown-content">
                                            <div class="dropdown-item is-title">
                                                Who can see this ?
                                            </div>
                                            <a href="#" class="dropdown-item">
                                                <div class="media">
                                                    <i data-feather="globe"></i>
                                                    <div class="media-content">
                                                        <h3>Public</h3>
                                                        <small>Anyone can see this publication.</small>
                                                    </div>
                                                </div>
                                            </a>
                                            <a class="dropdown-item">
                                                <div class="media">
                                                    <i data-feather="users"></i>
                                                    <div class="media-content">
                                                        <h3>Friends</h3>
                                                        <small>only friends can see this publication.</small>
                                                    </div>
                                                </div>
                                            </a>
                                            <a class="dropdown-item">
                                                <div class="media">
                                                    <i data-feather="user"></i>
                                                    <div class="media-content">
                                                        <h3>Specific friends</h3>
                                                        <small>Don't show it to some friends.</small>
                                                    </div>
                                                </div>
                                            </a>
                                            <hr class="dropdown-divider">
                                            <a class="dropdown-item">
                                                <div class="media">
                                                    <i data-feather="lock"></i>
                                                    <div class="media-content">
                                                        <h3>Only me</h3>
                                                        <small>Only me can see this publication.</small>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="inner-content">
                                <div class="control">
                                    <input type="text" class="input is-sm is-fade"
                                           placeholder="What is this live about?">
                                    <div class="icon">
                                        <i data-feather="activity"></i>
                                    </div>
                                </div>
                                <div class="live-stats">
                                    <div class="social-count">
                                        <div class="likes-count">
                                            <i data-feather="heart"></i>
                                            <span>0</span>
                                        </div>
                                        <div class="shares-count">
                                            <i data-feather="link-2"></i>
                                            <span>0</span>
                                        </div>
                                        <div class="comments-count">
                                            <i data-feather="message-circle"></i>
                                            <span>0</span>
                                        </div>
                                    </div>
                                    <div class="social-count ml-auto">
                                        <div class="views-count">
                                            <i data-feather="eye"></i>
                                            <span>0</span>
                                            <span class="views"><small>views</small></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="actions">
                                    <div class="action">
                                        <i data-feather="thumbs-up"></i>
                                        <span>Like</span>
                                    </div>
                                    <div class="action">
                                        <i data-feather="message-circle"></i>
                                        <span>Comment</span>
                                    </div>
                                    <div class="action">
                                        <i data-feather="link-2"></i>
                                        <span>Share</span>
                                    </div>
                                    <div class="dropdown is-spaced is-right dropdown-trigger">
                                        <div>
                                            <div class="avatar-button">
                                                <img src="https://via.placeholder.com/300x300"
                                                     data-demo-src="<?= base_url('assets/front') ?>/img/avatars/jenna.png"
                                                     alt="">
                                                <i data-feather="triangle"></i>
                                            </div>
                                        </div>
                                        <div class="dropdown-menu has-margin" role="menu">
                                            <div class="dropdown-content">
                                                <a href="#" class="dropdown-item is-selected">
                                                    <div class="media">
                                                        <img src="https://via.placeholder.com/300x300"
                                                             data-demo-src="<?= base_url('assets/front') ?>/img/avatars/jenna.png"
                                                             alt="">
                                                        <div class="media-content">
                                                            <h3>Jenna Davis</h3>
                                                            <small>Interact as Jenna Davis.</small>
                                                        </div>
                                                        <div class="checkmark">
                                                            <i data-feather="check"></i>
                                                        </div>
                                                    </div>
                                                </a>
                                                <hr class="dropdown-divider">
                                                <a href="#" class="dropdown-item">
                                                    <div class="media">
                                                        <img src="https://via.placeholder.com/478x344"
                                                             data-demo-src="<?= base_url('assets/front') ?>/img/avatars/hanzo.svg"
                                                             alt="">
                                                        <div class="media-content">
                                                            <h3>Css Ninja</h3>
                                                            <small>Interact as Css Ninja.</small>
                                                        </div>
                                                        <div class="checkmark">
                                                            <i data-feather="check"></i>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tabs-wrapper">
                                <div class="tabs is-fullwidth">
                                    <ul>
                                        <li class="is-active">
                                            <a>Comments</a>
                                        </li>
                                        <li>
                                            <a>Upcoming</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="tab-content has-slimscroll">
                                    <div class="media is-comment">
                                        <figure class="media-left">
                                            <p class="image is-32x32">
                                                <img src="https://via.placeholder.com/300x300"
                                                     data-demo-src="<?= base_url('assets/front') ?>/img/avatars/dan.jpg"
                                                     alt=""
                                                     data-user-popover="1">
                                            </p>
                                        </figure>
                                        <div class="media-content">
                                            <div class="username">Dan Walker</div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare
                                                magna
                                                eros.</p>
                                            <div class="comment-actions">
                                                <a href="javascript:void(0);" class="is-inverted">Like</a>
                                                <span>3h</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="media is-comment">
                                        <figure class="media-left">
                                            <p class="image is-32x32">
                                                <img src="https://via.placeholder.com/300x300"
                                                     data-demo-src="<?= base_url('assets/front') ?>/img/avatars/david.jpg"
                                                     alt=""
                                                     data-user-popover="4">
                                            </p>
                                        </figure>
                                        <div class="media-content">
                                            <div class="username">David Kim</div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing.</p>
                                            <div class="comment-actions">
                                                <a href="javascript:void(0);" class="is-inverted">Like</a>
                                                <span>4h</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="media is-comment">
                                        <figure class="media-left">
                                            <p class="image is-32x32">
                                                <img src="https://via.placeholder.com/300x300"
                                                     data-demo-src="<?= base_url('assets/front') ?>/img/avatars/rolf.jpg"
                                                     alt=""
                                                     data-user-popover="17">
                                            </p>
                                        </figure>
                                        <div class="media-content">
                                            <div class="username">Rolf Krupp</div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ornare
                                                magna
                                                eros. Consectetur adipiscing elit. Proin ornare magna eros.</p>
                                            <div class="comment-actions">
                                                <a href="javascript:void(0);" class="is-inverted">Like</a>
                                                <span>4h</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="comment-controls">
                                <div class="controls-inner">
                                    <img src="https://via.placeholder.com/300x300"
                                         data-demo-src="<?= base_url('assets/front') ?>/img/avatars/jenna.png" alt="">
                                    <div class="control">
                                        <textarea class="textarea comment-textarea is-rounded" rows="1"></textarea>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                </div>
            </div>

        </div>
    </div>
    <!-- Share from feed modal -->
    <!-- /partials/pages/feed/modals/share-modal.html -->
    <div id="share-modal" class="modal share-modal is-xsmall has-light-bg">
        <div class="modal-background"></div>
        <div class="modal-content">

            <div class="card">
                <div class="card-heading">
                    <div class="dropdown is-accent share-dropdown">
                        <div>
                            <div class="button">
                                <i class="mdi mdi-format-float-left"></i> <span>Share in your feed</span> <i
                                        data-feather="chevron-down"></i>
                            </div>
                        </div>
                        <div class="dropdown-menu" role="menu">
                            <div class="dropdown-content">
                                <div class="dropdown-item" data-target-channel="feed">
                                    <div class="media">
                                        <i class="mdi mdi-format-float-left"></i>
                                        <div class="media-content">
                                            <h3>Share in your feed</h3>
                                            <small>Share this publication on your feed.</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="dropdown-item" data-target-channel="friend">
                                    <div class="media">
                                        <i class="mdi mdi-account-heart"></i>
                                        <div class="media-content">
                                            <h3>Share in a friend's feed</h3>
                                            <small>Share this publication on a friend's feed.</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="dropdown-item" data-target-channel="group">
                                    <div class="media">
                                        <i class="mdi mdi-account-group"></i>
                                        <div class="media-content">
                                            <h3>Share in a group</h3>
                                            <small>Share this publication in a group.</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="dropdown-item" data-target-channel="page">
                                    <div class="media">
                                        <i class="mdi mdi-file-document-box"></i>
                                        <div class="media-content">
                                            <h3>Share in a page</h3>
                                            <small>Share this publication in a page.</small>
                                        </div>
                                    </div>
                                </div>
                                <hr class="dropdown-divider">
                                <div class="dropdown-item" data-target-channel="private-message">
                                    <div class="media">
                                        <i class="mdi mdi-email-plus"></i>
                                        <div class="media-content">
                                            <h3>Share in message</h3>
                                            <small>Share this publication in a private message.</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Close X button -->
                    <div class="close-wrap">
                                <span class="close-modal">
                                    <i data-feather="x"></i>
                                </span>
                    </div>
                </div>
                <div class="share-inputs">
                    <div class="field is-autocomplete">
                        <div id="share-to-friend" class="control share-channel-control is-hidden">
                            <input id="share-with-friend" type="text"
                                   class="input is-sm no-radius share-input simple-users-autocpl"
                                   placeholder="Your friend's name">
                            <div class="input-heading">
                                Friend :
                            </div>
                        </div>
                    </div>

                    <div class="field is-autocomplete">
                        <div id="share-to-group" class="control share-channel-control is-hidden">
                            <input id="share-with-group" type="text"
                                   class="input is-sm no-radius share-input simple-groups-autocpl"
                                   placeholder="Your group's name">
                            <div class="input-heading">
                                Group :
                            </div>
                        </div>
                    </div>

                    <div id="share-to-page" class="control share-channel-control no-border is-hidden">
                        <div class="page-controls">
                            <div class="page-selection">

                                <div class="dropdown is-accent page-dropdown">
                                    <div>
                                        <div class="button page-selector">
                                            <img src="https://via.placeholder.com/150x150"
                                                 data-demo-src="<?= base_url('assets/front') ?>/img/avatars/hanzo.svg"
                                                 alt="">
                                            <span>Css Ninja</span> <i
                                                    data-feather="chevron-down"></i>
                                        </div>
                                    </div>
                                    <div class="dropdown-menu" role="menu">
                                        <div class="dropdown-content">
                                            <div class="dropdown-item">
                                                <div class="media">
                                                    <img src="https://via.placeholder.com/150x150"
                                                         data-demo-src="<?= base_url('assets/front') ?>/img/avatars/hanzo.svg"
                                                         alt="">
                                                    <div class="media-content">
                                                        <h3>Css Ninja</h3>
                                                        <small>Share on Css Ninja.</small>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="dropdown-item">
                                                <div class="media">
                                                    <img src="https://via.placeholder.com/150x150"
                                                         data-demo-src="<?= base_url('assets/front') ?>/img/icons/logos/nuclearjs.svg"
                                                         alt="">
                                                    <div class="media-content">
                                                        <h3>NuclearJs</h3>
                                                        <small>Share on NuclearJs.</small>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="dropdown-item">
                                                <div class="media">
                                                    <img src="https://via.placeholder.com/150x150"
                                                         data-demo-src="<?= base_url('assets/front') ?>/img/icons/logos/slicer.svg"
                                                         alt="">
                                                    <div class="media-content">
                                                        <h3>Slicer</h3>
                                                        <small>Share on Slicer.</small>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="alias">
                                <img src="https://via.placeholder.com/150x150"
                                     data-demo-src="<?= base_url('assets/front') ?>/img/avatars/jenna.png" alt="">
                            </div>
                        </div>
                    </div>

                    <div class="field is-autocomplete">
                        <div id="share-to-private-message" class="control share-channel-control is-hidden">
                            <input id="share-with-private-message" type="text"
                                   class="input is-sm no-radius share-input simple-users-autocpl"
                                   placeholder="Message a friend">
                            <div class="input-heading">
                                To :
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="control">
                                <textarea class="textarea comment-textarea" rows="1"
                                          placeholder="Say something about this ..."></textarea>
                    </div>
                    <div class="shared-publication">
                        <div class="featured-image">
                            <img id="share-modal-image" src="https://via.placeholder.com/1600x900"
                                 data-demo-src="<?= base_url('assets/front') ?>/img/demo/unsplash/1.jpg" alt="">
                        </div>
                        <div class="publication-meta">
                            <div class="inner-flex">
                                <img id="share-modal-avatar" src="https://via.placeholder.com/300x300"
                                     data-demo-src="<?= base_url('assets/front') ?>/img/avatars/dan.jpg"
                                     data-user-popover="1" alt="">
                                <p id="share-modal-text">Yesterday with <a href="#">@Karen Miller</a> and <a href="#">@Marvin
                                        Stemperd</a> at the
                                    <a href="#">#Rock'n'Rolla</a> concert in LA. Was totally fantastic! People were
                                    really
                                    excited about this one!</p>
                            </div>
                            <div class="publication-footer">
                                <div class="stats">
                                    <div class="stat-block">
                                        <i class="mdi mdi-earth"></i>
                                        <small>Public</small>
                                    </div>
                                    <div class="stat-block">
                                        <i class="mdi mdi-eye"></i>
                                        <small>163 views</small>
                                    </div>
                                </div>
                                <div class="publication-origin">
                                    <small>Friendkit.io</small>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="bottom-share-inputs">

                    <div id="action-place" class="field is-autocomplete is-dropup is-hidden">
                        <div id="share-place" class="control share-bottom-channel-control">
                            <input type="text" class="input is-sm no-radius share-input simple-locations-autocpl"
                                   placeholder="Where are you?">
                            <div class="input-heading">
                                Location :
                            </div>
                        </div>
                    </div>

                    <div id="action-tag" class="field is-autocomplete is-dropup is-hidden">
                        <div id="share-tags" class="control share-bottom-channel-control">
                            <input id="share-friend-tags-autocpl" type="text" class="input is-sm no-radius share-input"
                                   placeholder="Who are you with">
                            <div class="input-heading">
                                Friends :
                            </div>
                        </div>
                        <div id="share-modal-tag-list" class="tag-list no-margin"></div>
                    </div>

                </div>
                <div class="card-footer">
                    <div class="action-wrap">
                        <div class="footer-action" data-target-action="tag">
                            <i class="mdi mdi-account-plus"></i>
                        </div>
                        <div class="footer-action" data-target-action="place">
                            <i class="mdi mdi-map-marker"></i>
                        </div>
                        <div class="footer-action dropdown is-spaced is-neutral dropdown-trigger is-up"
                             data-target-action="permissions">
                            <div>
                                <i class="mdi mdi-lock-clock"></i>
                            </div>
                            <div class="dropdown-menu" role="menu">
                                <div class="dropdown-content">
                                    <a href="#" class="dropdown-item">
                                        <div class="media">
                                            <i data-feather="globe"></i>
                                            <div class="media-content">
                                                <h3>Public</h3>
                                                <small>Anyone can see this publication.</small>
                                            </div>
                                        </div>
                                    </a>
                                    <a class="dropdown-item">
                                        <div class="media">
                                            <i data-feather="users"></i>
                                            <div class="media-content">
                                                <h3>Friends</h3>
                                                <small>only friends can see this publication.</small>
                                            </div>
                                        </div>
                                    </a>
                                    <a class="dropdown-item">
                                        <div class="media">
                                            <i data-feather="user"></i>
                                            <div class="media-content">
                                                <h3>Specific friends</h3>
                                                <small>Don't show it to some friends.</small>
                                            </div>
                                        </div>
                                    </a>
                                    <hr class="dropdown-divider">
                                    <a class="dropdown-item">
                                        <div class="media">
                                            <i data-feather="lock"></i>
                                            <div class="media-content">
                                                <h3>Only me</h3>
                                                <small>Only me can see this publication.</small>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="button-wrap">
                        <button type="button" class="button is-solid dark-grey-button close-modal">Cancel</button>
                        <button type="button" class="button is-solid accent-button close-modal">Publish</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- No Stream modal -->
    <!-- /partials/pages/feed/modals/no-stream-modal.html -->
    <div id="no-stream-modal" class="modal no-stream-modal is-xsmall has-light-bg">
        <div class="modal-background"></div>
        <div class="modal-content">

            <div class="card">
                <div class="card-heading">
                    <h3></h3>
                    <!-- Close X button -->
                    <div class="close-wrap">
                                <span class="close-modal">
                                    <i data-feather="x"></i>
                                </span>
                    </div>
                </div>
                <div class="card-body has-text-centered">

                    <div class="image-wrap">
                        <img src="<?= base_url('assets/front') ?>/img/illustrations/characters/no-stream.svg" alt="">
                    </div>

                    <h3>Streaming Disabled</h3>
                    <p>Streaming is not allowed from mobile web. Please use our mobile apps for mobile streaming.</p>

                    <div class="action">
                        <a href="index.html#demos-section" class="button is-solid accent-button raised is-fullwidth">Got
                            It</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Google places Lib -->
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyAGLO_M5VT7BsVdjMjciKoH1fFJWWdhDPU&amp;libraries=places"></script>
</div>


