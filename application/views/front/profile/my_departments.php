<div class="view-wrapper">

    <!-- /partials/global/options-nav/friends-options-nav.html -->
    <div class="options-nav no-shadow">
        <div class="container is-fluid">
            <div class="nav-inner is-friends">
                <div class="option-tabs is-friends">
                    <a class="option-tab is-active" data-tab="all-friends">
                        <span><?= $this->session->userdata('department')['title'] ?></span>
                    </a>
                    <div class="option-naver"></div>
                </div>
                <div class="end-group">
                    <div class="nav-item is-friend-count"><?= count($employees) ?> <?= ln('FRONT.Employees') ?></div>
                    <div id="subsearch" class="nav-item is-search is-hidden">
                        <div class="control">
                            <input type="text" class="input textFilter-input" placeholder="<?= ln('BASE.search') ?>">
                        </div>
                    </div>
                    <a id="show-subsearch" class="nav-item is-icon">
                        <i data-feather="search"></i>
                    </a>
                    <a id="hide-subsearch" class="nav-item is-icon is-hidden">
                        <i data-feather="x"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="subloader is-grey is-active">
        <div class="loader is-loading"></div>
    </div>

    <div id="friends-page" class="friends-wrapper main-container">

        <!--First tab-->
        <div id="all-friends" class="card-row-wrap is-active">
            <div class="card-row-placeholder is-hidden">
                No matching results
            </div>
            <div class="card-row">

                <?php foreach ($employees as $em): ?>

                    <div class="card-flex friend-card">
                        <div class="friend-info">
                            <h3><?= $em['name_surname'] ?></h3>
                            <p><?= $em['title'] ?></p>
                            <p><a href="mailto:<?= $em['username'] ?>"><?= $em['username'] ?></a></p>
                            <p><?= $em['phone'] ?><?= strlen($em['phone_2'])>0?' / '.$em['phone_2']:'' ?></p>
                            <p><?= date_std($em['date'], FALSE) ?></p>
                        </div>
                    </div>

                <?php endforeach; ?>

            </div>
        </div>

    </div>

</div>