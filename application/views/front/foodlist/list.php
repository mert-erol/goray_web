<div class="card">
    <div class="card-heading is-bordered">
        <h4><?= ln('APPLICATIONS.Food List') ?></h4>
    </div>
    <div class="card-body no-padding">

        <?php
        foreach ($foodlist as $fl): ?>
            <div class="add-friend-block transition-block">
                <div class="page-meta">
                    <span><?= date_std($fl['date'], FALSE, 'd M D') ?></span>
                    <span><?= $fl['content'] ?></span>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>