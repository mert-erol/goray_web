<div class="view-wrapper">
    <!-- Question wrap -->
    <div class="questions-wrap is-smaller">
        <!-- Container -->
        <div class="container">
            <div class="question-content is-large">
                <div id="questions-shadow-dom-categories" class="columns">
                    <div class="column">
                        <div class="categories-header">
                            <h2><?= ln('APPLICATIONS.Food List') ?></h2>
                        </div>
                        <div class="placeload is-bold questions-categories-placeload">
                            <div class="tile is-ancestor categories-tile-grid">
                                <div class="tile is-vertical is-8">
                                    <div class="tile">
                                        <div class="tile is-parent is-vertical">
                                            <div class="tile is-child is-tile-placeload">
                                                <div class="img loads"></div>
                                                <div class="placeload-content">
                                                    <div class="content-shape loads"></div>
                                                    <div class="content-shape loads"></div>
                                                </div>
                                            </div>
                                            <div class="tile is-child is-tile-placeload">
                                                <div class="img loads"></div>
                                                <div class="placeload-content">
                                                    <div class="content-shape loads"></div>
                                                    <div class="content-shape loads"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tile is-parent is-vertical">
                                            <div class="tile is-child is-tile-placeload">
                                                <div class="img loads"></div>
                                                <div class="placeload-content">
                                                    <div class="content-shape loads"></div>
                                                    <div class="content-shape loads"></div>
                                                </div>
                                            </div>
                                            <div class="tile is-child is-tile-placeload">
                                                <div class="img loads"></div>
                                                <div class="placeload-content">
                                                    <div class="content-shape loads"></div>
                                                    <div class="content-shape loads"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tile is-parent">
                                        <div class="tile is-child is-tile-placeload is-card">
                                            <div class="img loads"></div>
                                            <div class="placeload-content">
                                                <div class="content-shape loads"></div>
                                                <div class="content-shape loads"></div>
                                                <div class="content-shape loads"></div>
                                                <div class="content-shape loads"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tile is-parent is-vertical">
                                    <div class="tile is-child is-tile-placeload">
                                        <div class="img loads"></div>
                                        <div class="placeload-content">
                                            <div class="content-shape loads"></div>
                                            <div class="content-shape loads"></div>
                                        </div>
                                    </div>
                                    <div class="tile is-child is-tile-placeload">
                                        <div class="img loads"></div>
                                        <div class="placeload-content">
                                            <div class="content-shape loads"></div>
                                            <div class="content-shape loads"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="columns true-dom is-hidden">
                    <div class="column">
                        <div class="categories-header">
                            <h2><?= ln('APPLICATIONS.Food List') ?></h2>
                        </div>

                        <div class="tile is-ancestor categories-tile-grid">
                            <div class="tile is-vertical">
                                <div class="tile">
                                    <div class="tile is-parent is-vertical">
                                        <?php foreach ($foodlist as $k => $fl): ?>
                                            <a class="tile is-child category-box is-<?= $k == 0 ? 'primary' : 'accent' ?>">
                                                <div class="box-content">
                                                    <h3 class="title is-6"><?= date_std($fl['date'], FALSE) ?></h3>
                                                    <?= $fl['content'] ?>
                                                </div>
                                            </a>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>