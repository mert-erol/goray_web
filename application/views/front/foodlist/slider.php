<div class="stats-wrapper">
    <div class="achievements">
        <div class="header">
            <h3>Achievements</h3>
        </div>

        <div class="achievements-carousel-wrap">
            <div class="achievements-carousel">
                <div class="achievement has-text-centered">
                    <img src="assets/img/icons/questions/upvoted.svg" alt="">
                    <h3>Upvoted</h3>
                    <p>A reward for community leaders.</p>
                </div>
                <div class="achievement has-text-centered">
                    <img src="assets/img/icons/questions/accurate.svg" alt="">
                    <h3>Accurate</h3>
                    <p>A very high rate of best answers.</p>
                </div>
                <div class="achievement has-text-centered">
                    <img src="assets/img/icons/questions/gunslinger.svg" alt="">
                    <h3>Gun Slinger</h3>
                    <p>Answers people fast as hell!</p>
                </div>
                <div class="achievement has-text-centered">
                    <img src="assets/img/icons/questions/rookie.svg" alt="">
                    <h3>Rookie</h3>
                    <p>Posted at least 50 questions</p>
                </div>
                <div class="achievement has-text-centered">
                    <img src="assets/img/icons/questions/contributor.svg" alt="">
                    <h3>Contributor</h3>
                    <p>Posted at least 250 questions</p>
                </div>
            </div>
        </div>
    </div>
</div>
