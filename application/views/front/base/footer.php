<!-- Concatenated js plugins and jQuery -->
<script src="<?= base_url('assets/front') ?>/js/app.js"></script>

<!-- Core js -->
<script src="<?= base_url('assets/front') ?>/data/tipuedrop_content.js"></script>
<script src="<?= base_url('assets/front') ?>/js/global.js"></script>
<script src="<?= base_url('assets/front') ?>/js/main.js"></script>

<!-- Page and UI related js -->
<script src="<?= base_url('assets/front') ?>/js/feed.js"></script>
<script src="<?= base_url('assets/front') ?>/js/stories.js"></script>
<script src="<?= base_url('assets/front') ?>/js/chat.js"></script>
<script src="<?= base_url('assets/front') ?>/js/inbox.js"></script>
<script src="<?= base_url('assets/front') ?>/js/profile.js"></script>
<script src="<?= base_url('assets/front') ?>/js/friends.js"></script>
<script src="<?= base_url('assets/front') ?>/js/events.js"></script>
<script src="<?= base_url('assets/front') ?>/js/explorer.js"></script>
<script src="<?= base_url('assets/front') ?>/js/news.js"></script>
<script src="<?= base_url('assets/front') ?>/js/questions.js"></script>
<script src="<?= base_url('assets/front') ?>/js/videos.js"></script>
<script src="<?= base_url('assets/front') ?>/js/shop.js"></script>
<script src="<?= base_url('assets/front') ?>/js/settings.js"></script>

<!-- Components js -->
<script src="<?= base_url('assets/front') ?>/js/widgets.js"></script>
<script src="<?= base_url('assets/front') ?>/js/autocompletes.js"></script>
<script src="<?= base_url('assets/front') ?>/js/modal-uploader.js"></script>
<script src="<?= base_url('assets/front') ?>/js/popovers-users.js"></script>
<script src="<?= base_url('assets/front') ?>/js/popovers-pages.js"></script>
<script src="<?= base_url('assets/front') ?>/js/go-live.js"></script>
<script src="<?= base_url('assets/front') ?>/js/lightbox.js"></script>
<script src="<?= base_url('assets/front') ?>/js/touch.js"></script>
<script src="<?= base_url('assets/front') ?>/js/tour.js"></script>
<script src="<?= base_url('assets/front') ?>/js/sweetalert2.all.min.js"></script>
<script src="<?= base_url('assets/front') ?>/js/bulma-calendar.min.js"></script>

<script>


    get_posts();

    $(document).on('click', '.load-more-button', function () {
        get_posts();
    });

    $(document).on('click', '#publish-button', function () {
        create_post();
    });


    function get_posts() {
        $.post(window.location.origin + '/Front/Post/Get', {}, function (response) {
            $('#timeline').append(response);
        });
    }

    function create_post() {
        var publish_content = $('#publish').val();
        $.post(window.location.origin + '/Front/Post/Create', {publish_content: publish_content}, function (response) {
            response = JSON.parse(response);
            if (response.result == 1) {
                $('#publish').val('');
                $('span.close-publish').click();
                swal.fire({
                    type: 'success',
                    title: response.message,
                    showConfirmButton: false,
                    timer: 1500
                });

                get_posts();

            } else {
                swal.fire({
                    type: 'error',
                    title: response.message,
                    showConfirmButton: false,
                    timer: 6000
                });
            }
        });
    }

    setInterval(function () {
        console.log('interval ');
        get_posts();
    }, 50000);

    bulmaCalendar.attach('.my-datetimepicker', {
        displayMode: 'dialog',
        lang: 'tr'
    });
</script>

<style>
    .link-icon {
        font-size: 17px;
        margin-right: 5px;
        margin-top: 2px;
    }

</style>
</body>
</html>
