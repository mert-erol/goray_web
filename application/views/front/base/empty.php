<div class="view-wrapper is-full">

    <div class="stories-wrapper is-home">

        <!-- Stories -->
        <div class="inner-wrapper">
            <a class="mobile-sidebar-trigger is-story-post is-home-v2">
                <i data-feather="menu"></i>
            </a>

            <!-- Story -->
            <div class="story-post-wrapper">
                <div class="story-post">
                    <div class="post-title">
                        <h2>Hello from Sunset Beach</h2>
                    </div>
                    <div class="post-image-wrap">
                        <img class="post-image" src="https://via.placeholder.com/1600x900"
                             data-demo-src="<?= base_url('assets/front') ?>/img/demo/unsplash/3.jpg" alt="">
                    </div>
                    <div class="post-meta">
                        <div class="post-author">
                            <div class="meta">
                                <span>Stella Bergmann</span>
                                <span>2 hours ago</span>
                            </div>
                        </div>

                        <div class="post-stats">
                            <div class="meta">
                                <span>2 hours ago</span>
                            </div>
                        </div>
                    </div>
                    <div class="post-text content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quid de Pythagora? Quae est igitur
                            causa istarum angustiarum? Non risu potius quam oratione eiciendum? Duo Reges:
                            constructio interrete. An haec ab eo non dicuntur? Quae cum essent dicta, discessimus.
                        </p>

                        <p>At enim hic etiam dolore. Idem iste, inquam, de voluptate quid sentit? At enim sequor
                            utilitatem. Iam in altera philosophiae parte. Ut optime, secundum naturam affectum esse
                            possit. Hoc sic expositum dissimile est superiori. <b>#holidays</b> <b>#friends</b> <b>#chillout</b>
                        </p>
                    </div>
                </div>
            </div>

            <!--Story post sidebar-->
            <div class="story-post-sidebar">
                <div class="header">
                    <h2>You might like</h2>
                </div>
                <div class="related-posts">
                    <!--Related post-->
                    <a class="related-post">
                        <img class="post-image" src="https://via.placeholder.com/250x250"
                             data-demo-src="<?= base_url('assets/front') ?>/img/demo/unsplash/4.jpg" alt="">
                        <div class="meta">
                            <h3>Beaches you want to discover and experiment in 2020</h3>
                            <div class="user-line">
                                <img src="https://via.placeholder.com/250x250"
                                     data-demo-src="<?= base_url('assets/front') ?>/img/avatars/elise.jpg" data-user-popover="6" alt="">
                                <span>Elise Walker</span>
                            </div>
                        </div>
                    </a>
                    <!--Related post-->
                    <a class="related-post">
                        <img class="post-image" src="https://via.placeholder.com/250x250"
                             data-demo-src="<?= base_url('assets/front') ?>/img/demo/unsplash/17.jpg" alt="">
                        <div class="meta">
                            <h3>Those shoes will make you feel so happy</h3>
                            <div class="user-line">
                                <img src="https://via.placeholder.com/250x250"
                                     data-demo-src="<?= base_url('assets/front') ?>/img/avatars/dan.jpg" data-user-popover="1" alt="">
                                <span>Dan Walker</span>
                            </div>
                        </div>
                    </a>
                    <!--Related post-->
                    <a class="related-post">
                        <img class="post-image" src="https://via.placeholder.com/250x250"
                             data-demo-src="<?= base_url('assets/front') ?>/img/demo/unsplash/32.jpg" alt="">
                        <div class="meta">
                            <h3>Life is all about happiness, it's up to you to go for it</h3>
                            <div class="user-line">
                                <img src="https://via.placeholder.com/250x250"
                                     data-demo-src="<?= base_url('assets/front') ?>/img/avatars/milly.jpg" data-user-popover="7" alt="">
                                <span>Milly Augustine</span>
                            </div>
                        </div>
                    </a>
                    <!--Related post-->
                    <a class="related-post">
                        <img class="post-image" src="https://via.placeholder.com/250x250"
                             data-demo-src="<?= base_url('assets/front') ?>/img/demo/unsplash/18.jpg" alt="">
                        <div class="meta">
                            <h3>My winter trip was absolutely fantastic</h3>
                            <div class="user-line">
                                <img src="https://via.placeholder.com/250x250"
                                     data-demo-src="<?= base_url('assets/front') ?>/img/avatars/jenna.png" data-user-popover="0" alt="">
                                <span>Jenna Davis</span>
                            </div>
                        </div>
                    </a>
                    <!--Related post-->
                    <a class="related-post">
                        <img class="post-image" src="https://via.placeholder.com/250x250"
                             data-demo-src="<?= base_url('assets/front') ?>/img/demo/unsplash/26.jpg" alt="">
                        <div class="meta">
                            <h3>8 Headsets of 2020 compared, check it out before buying</h3>
                            <div class="user-line">
                                <img src="https://via.placeholder.com/250x250"
                                     data-demo-src="<?= base_url('assets/front') ?>/img/avatars/azzouz.jpg" data-user-popover="20" alt="">
                                <span>Azzouz El Paytoun</span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

        </div>
    </div>
</div>