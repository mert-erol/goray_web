<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title> <?= $mainSettings['firm'] ?> | Login</title>
    <link rel="icon" type="image/png" href="<?= base_url('assets/front') ?>/img/favicon.png"/>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet">
    <!-- Core CSS -->
    <link rel="stylesheet" href="<?= base_url('assets/front') ?>/css/bulma.css">
    <link rel="stylesheet" href="<?= base_url('assets/front') ?>/css/app.css">
    <link rel="stylesheet" href="<?= base_url('assets/front') ?>/css/core.css">
</head>

<body class="is-white">


<div class="login-wrapper">

    <!-- Main Wrapper -->
    <div class="login-wrapper columns is-gapless">
        <!--Left Side (Desktop Only)-->
        <div class="column is-6 is-hidden-mobile hero-banner">
            <div class="hero is-fullheight is-login">
                <div class="hero-body">
                    <div class="container">
                        <div class="left-caption">
                            <h2><?= $mainSettings['firm'] ?> <?= ln('TEXT.Social Experience') ?></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Right Side-->
        <div class="column is-6">
            <div class="hero form-hero is-fullheight">
                <!--Logo-->
                <div class="logo-wrap">
                    <div class="wrap-inner">
                        <img src="<?= base_url('assets/front') ?>/img/logo/logo.jpg" alt="">
                    </div>
                </div>
                <!--Login Form-->
                <div class="hero-body">
                    <div class="form-wrapper">
                        <!--Form-->
                        <div class="login-form">
                            <div class="field">
                                <div class="control">
                                    <input class="input email-input" type="text" id="username"
                                           placeholder="mail@mail.com">
                                    <div class="input-icon">
                                        <i data-feather="user"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <input class="input password-input" type="password" id="password"
                                           placeholder="●●●●●●●">
                                    <div class="input-icon">
                                        <i data-feather="lock"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <div class="control">
                                    <button class="button is-solid primary-button raised is-rounded is-fullwidth"
                                            id="login"><?= ln('AUTH.Login') ?></button>
                                </div>
                            </div>
                        </div>
                        <div class="section forgot-password">
                            <div class="has-text-centered">
                                <a href="#" style="float: left;"><?= ln('AUTH.Forgot password?') ?></a>
                                <?php foreach ($languages as $lng): ?>
                                    <a href="<?= base_url('Language/Change/' . $lng['id'] . '/Front') ?>"
                                       style="float:right">
                                        <?= $lng['title'] ?>
                                    </a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Concatenated js plugins and jQuery -->
<script src="<?= base_url('assets/front') ?>/js/app.js"></script>

<!-- Core js -->
<script src="<?= base_url('assets/front') ?>/data/tipuedrop_content.js"></script>
<script src="<?= base_url('assets/front') ?>/js/global.js"></script>
<script src="<?= base_url('assets/front') ?>/js/main.js"></script>
<script src="<?= base_url('assets/front') ?>/js/sweetalert2.all.min.js"></script>

<script>

    document.addEventListener("DOMContentLoaded", function (event) {

        $(document).on('click', '#login', function (e) {

            e.preventDefault();

            swal.fire({
                title: "<?=ln('MESSAGE.Processing')?>",
                text: "<?=ln('MESSAGE.Wait')?>",
                timer: 99999,
                onOpen: function () {
                    swal.showLoading()
                }
            }).then(function (e) {
                "timer" === e.dismiss && console.log("I was closed by the timer")
            });

            var dataset = {
                username: $('#username').val(),
                password: $('#password').val()
            };


            $.post(window.location.origin + '/Front/Auth/Login', dataset, function (response) {

                var response = JSON.parse(response);

                if (response.result == 1) {
                    swal.fire({
                        type: 'success',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 1500
                    });

                    window.location.href = window.location.origin + '/Front/Auth/Referrer';
                } else {
                    swal.fire({
                        type: 'error',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 6000
                    });
                }
            });
        });
    });
</script>
</body>
</html>