<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title> IHIDALGAKIRAN | <?= ln('FRONT.Feed') ?></title>
    <link rel="icon" type="image/png" href="<?= base_url('assets/front') ?>/img/logo/logo.jpg"/>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/fontisto@v3.0.4/css/fontisto/fontisto-brands.min.css" rel="stylesheet">
    <!-- Core CSS -->
    <link rel="stylesheet" href="<?= base_url('assets/front') ?>/css/bulma.css">
    <link rel="stylesheet" href="<?= base_url('assets/front') ?>/css/app.css">
    <link rel="stylesheet" href="<?= base_url('assets/front') ?>/css/core.css">
    <link rel="stylesheet" href="<?= base_url('assets/front') ?>/css/bulma-calendar.min.css">

</head>
<body>

<!-- Pageloader -->
<div class="pageloader"></div>
<div class="infraloader is-active"></div>
<div class="app-overlay"></div>
