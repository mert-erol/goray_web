<div id="main-navbar" class="navbar is-inline-flex is-transparent no-shadow is-hidden-mobile">
    <div class="container is-fluid">
        <div class="navbar-brand">
            <a href="<?= base_url('Front') ?>" class="navbar-item">
                <img src="<?= base_url('assets/front') ?>/img/logo/logo.jpg" width="100" height="60" alt="">
            </a>
        </div>
        <div class="navbar-menu">
            <div class="navbar-start">


                <div id="explorer-trigger" class="navbar-item is-icon">
                    <a href="<?= base_url('Front/Feed') ?>"
                       class="button is-phantom primary-button accent-button <?= current_url() == base_url('Front/Feed') ? 'is-active' : '' ?>">
                        <i class="mdi mdi-home link-icon"></i>
                        <?= ln('FRONT.Feed') ?>
                    </a>
                </div>

                <div id="explorer-trigger" class="navbar-item is-icon">
                    <a href="<?= base_url('Front/News') ?>"
                       class="button is-phantom primary-button accent-button <?= current_url() == base_url('Front/News') ? 'is-active' : '' ?>">
                        <i class="mdi mdi-newspaper link-icon"></i>
                        <?= ln('FRONT.News') ?>
                    </a>
                </div>

                <div id="explorer-trigger" class="navbar-item is-icon">
                    <a href="<?= base_url('Front/Events') ?>"
                       class="button is-phantom primary-button accent-button <?= current_url() == base_url('Front/Events') ? 'is-active' : '' ?>">
                        <i class="mdi mdi-calendar-clock link-icon"></i>
                        <?= ln('FRONT.Events') ?>
                    </a>
                </div>

                <div id="explorer-trigger" class="navbar-item is-icon">
                    <a href="<?= base_url('Front/FoodList') ?>"
                       class="button is-phantom primary-button accent-button <?= current_url() == base_url('Front/FoodList') ? 'is-active' : '' ?>">
                        <i class="mdi mdi-format-list-bulleted link-icon"></i>
                        <?= ln('FRONT.Food List') ?>
                    </a>
                </div>

                <div id="explorer-trigger" class="navbar-item is-icon">
                    <a href="<?= base_url('Front/Profile/Department') ?>"
                       class="button is-phantom primary-button accent-button <?= current_url() == base_url('Front/Profile/Department') ? 'is-active' : '' ?>">
                        <i class="mdi mdi-account-multiple link-icon"></i>
                        <?= ln('FRONT.My Department') ?>
                    </a>
                </div>

                <div id="explorer-trigger" class="navbar-item is-icon">
                    <a href="<?= base_url('Front/Requests') ?>"
                       class="button is-phantom primary-button accent-button <?= current_url() == base_url('Front/Requests') ? 'is-active' : '' ?>">
                        <i class="mdi mdi-walk link-icon"></i>
                        <?= ln('FRONT.Requests') ?>
                    </a>
                </div>

                <?php if ($this->session->userdata('app_login')): ?>
                    <div id="explorer-trigger" class="navbar-item is-icon">
                        <a href="<?= base_url('App/Dashboard') ?>"
                           class="button is-phantom primary-button">
                            <i class="mdi mdi-apps link-icon"></i>
                            <?= ln('FRONT.Panel') ?>
                        </a>
                    </div>
                <?php endif; ?>
            </div>

            <div class="navbar-end">

                <?php foreach ($languages as $lng): ?>
                    <div id="explorer-trigger" class="navbar-item is-icon">
                        <a href="<?= base_url('Language/Change/' . $lng['id'] . '/Front') ?>"
                           class="button is-phantom grey-button">
                            <?= $lng['title'] ?>
                        </a>
                    </div>
                <?php endforeach; ?>


                <div id="account-dropdown" class="navbar-item is-account drop-trigger has-caret">
                    <div class="user-image">
                        <i style="font-size: 28px;
    color: #0e569b;" class="mdi mdi-account-circle"></i>
                    </div>

                    <div class="nav-drop is-account-dropdown">
                        <div class="inner">
                            <div class="nav-drop-body account-items">
                                <a id="profile-link" href="#" class="account-item">
                                    <div class="media">
                                        <div class="media-left">
                                            <div class="image">
                                                <i data-feather="user"></i>
                                            </div>
                                        </div>
                                        <div class="media-content">
                                            <h3><?= $userSettings['name_surname'] ?></h3>
                                            <small><?= $this->session->userdata('department')['title'] ?></small>
                                        </div>
                                        <div class="media-right">
                                            <i data-feather="check"></i>
                                        </div>
                                    </div>
                                </a>

                                <a href="<?= base_url('Front/Auth/Logout') ?>" class="account-item">
                                    <div class="media">
                                        <div class="icon-wrap">
                                            <i data-feather="power"></i>
                                        </div>
                                        <div class="media-content">
                                            <h3><?= ln('AUTH.Log out') ?></h3>
                                            <small><?= ln('AUTH.Log out_sub') ?></small>
                                        </div>
                                    </div>
                                </a>

                                <hr class="account-divider">

                                <?php foreach ($languages as $lng): ?>
                                    <a href="<?= base_url('Language/Change/' . $lng['id'] . '/Front') ?>"
                                       class="account-item">
                                        <div class="media">
                                            <div class="icon-wrap">
                                                <i data-feather="rotate-cw"></i>
                                            </div>
                                            <div class="media-content">
                                                <h3><?= $lng['title'] ?></h3>
                                            </div>
                                        </div>
                                    </a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<nav class="navbar mobile-navbar is-hidden-desktop" aria-label="main navigation">
    <!-- Brand -->
    <div class="navbar-brand">
        <a class="navbar-item" href="<?= base_url('Front') ?>">
            <img src="<?= base_url('assets/front') ?>/img/logo/logo.jpg" alt="">
        </a>

        <div class="navbar-item is-icon">
            <a class="icon-link is-primary is-friends" href="<?= base_url('Front/Feed') ?>">
                <i class="mdi mdi-home"></i>
            </a>
        </div>
        <div class="navbar-item is-icon">
            <a class="icon-link is-primary is-friends" href="<?= base_url('Front/News') ?>">
                <i class="mdi mdi-newspaper"></i>
            </a>
        </div>
        <div class="navbar-item is-icon">
            <a class="icon-link is-primary is-friends" href="<?= base_url('Front/Events') ?>">
                <i class="mdi mdi-calendar-clock"></i>
            </a>
        </div>
        <div class="navbar-item is-icon">
            <a class="icon-link is-primary is-friends" href="<?= base_url('Front/FoodList') ?>">
                <i class="mdi mdi-format-list-bulleted"></i>
            </a>
        </div>

        <?php if ($this->session->userdata('app_login')): ?>
            <div id="mobile-explorer-trigger" class="navbar-item is-icon">
                <a href="<?= base_url('App/Dashboard') ?>" class="icon-link is-primary">
                    <i class="mdi mdi-apps"></i>
                </a>
            </div>
        <?php endif; ?>

        <!-- Mobile menu toggler icon -->
        <div class="navbar-burger">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
    <!-- Navbar mobile menu -->
    <div class="navbar-menu">
        <!-- Account -->
        <div class="navbar-item has-dropdown is-active">
            <div class="navbar-link">
                <small><?= $userSettings['name_surname'] ?></small>
                <span class="is-heading"><?= $this->session->userdata('department')['title'] ?></span>
            </div>

            <!-- Mobile Dropdown -->
            <div class="navbar-dropdown">


                <a href="<?= base_url('Front/Feed') ?>"
                   class="button is-phantom primary-button accent-button <?= current_url() == base_url('Front/Feed') ? 'is-active' : '' ?>">
                    <i class="mdi mdi-home link-icon"></i>
                    <?= ln('FRONT.Feed') ?>
                </a>


                <a href="<?= base_url('Front/News') ?>"
                   class="button is-phantom primary-button accent-button <?= current_url() == base_url('Front/News') ? 'is-active' : '' ?>">
                    <i class="mdi mdi-newspaper link-icon"></i>
                    <?= ln('FRONT.News') ?>
                </a>


                <a href="<?= base_url('Front/Events') ?>"
                   class="button is-phantom primary-button accent-button <?= current_url() == base_url('Front/Events') ? 'is-active' : '' ?>">
                    <i class="mdi mdi-calendar-clock link-icon"></i>
                    <?= ln('FRONT.Events') ?>
                </a>


                <a href="<?= base_url('Front/FoodList') ?>"
                   class="button is-phantom primary-button accent-button <?= current_url() == base_url('Front/FoodList') ? 'is-active' : '' ?>">
                    <i class="mdi mdi-format-list-bulleted link-icon"></i>
                    <?= ln('FRONT.Food List') ?>
                </a>


                <a href="<?= base_url('Front/Profile/Department') ?>"
                   class="button is-phantom primary-button accent-button <?= current_url() == base_url('Front/Profile/Department') ? 'is-active' : '' ?>">
                    <i class="mdi mdi-account-multiple link-icon"></i>
                    <?= ln('FRONT.My Department') ?>
                </a>


                <a href="<?= base_url('Front/Requests') ?>"
                   class="button is-phantom primary-button accent-button <?= current_url() == base_url('Front/Requests') ? 'is-active' : '' ?>">
                    <i class="mdi mdi-walk link-icon"></i>
                    <?= ln('FRONT.Requests') ?>
                </a>


                <?php if ($this->session->userdata('app_login')): ?>
                    <a href="<?= base_url('App/Dashboard') ?>"
                       class="button is-phantom primary-button">
                        <i class="mdi mdi-apps link-icon"></i>
                        <?= ln('FRONT.Panel') ?>
                    </a>
                <?php endif; ?>


                <a href="<?= base_url('Front/Auth/Logout') ?>" class="button is-phantom primary-button">
                    <i class="mdi mdi-logout link-icon"></i>
                    <?= ln('AUTH.Log out') ?>
                </a>
            </div>
        </div>

        <!-- More -->
        <div class="navbar-item has-dropdown">
            <a class="navbar-link">
                <i data-feather="rotate-cw"></i>
                <span class="is-heading"><?= ln('departments.language') ?></span>
            </a>
            <div class="navbar-dropdown">
                <?php foreach ($languages as $lng): ?>
                    <a href="<?= base_url('Language/Change/' . $lng['id'] . '/Front') ?>"
                       class="navbar-item"><?= $lng['title'] ?></a>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</nav>