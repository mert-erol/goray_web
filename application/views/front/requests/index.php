<div class="inbox-wrapper">
    <div class="inbox-wrapper-inner">
        <div class="inbox-left-sidebar">
            <div class="inbox-left-sidebar-inner">
                <div class="compose">
                    <a href="<?= base_url('Front/Requests/Apply/1') ?>" class="button is-fullwidth raised is-bold">
                        <i class="mdi mdi-walk link-icon"></i>
                        <?= ln('FRONT.Leave Apply') ?>
                    </a>
                </div>
                <div class="compose">
                    <a href="<?= base_url('Front/Requests/Apply/2') ?>" class="button is-fullwidth raised is-bold">
                        <i class="mdi mdi-cash-multiple link-icon"></i>
                        <?= ln('FRONT.Expense bill') ?>
                    </a>
                </div>

                <div class="compose">
                    <a href="<?= base_url('Front/Requests/Apply/3') ?>" class="button is-fullwidth raised is-bold">
                        <i class="mdi mdi-airplane-landing link-icon"></i>
                        <?= ln('requests.type_3') ?>
                    </a>
                </div>

                <div class="compose">
                    <a href="<?= base_url('Front/Requests/Apply/4') ?>" class="button is-fullwidth raised is-bold">
                        <i class="mdi mdi-inbox-arrow-down link-icon"></i>
                        <?= ln('requests.type_4') ?>
                    </a>
                </div>

                <div class="compose">
                    <a href="<?= base_url('Front/Requests/Apply/5') ?>" class="button is-fullwidth raised is-bold">
                        <i class="mdi mdi-inbox-arrow-up link-icon"></i>
                        <?= ln('requests.type_5') ?>
                    </a>
                </div>

                <div class="close-menu is-hidden-desktop is-hidden-landscape">
                    <a id="close-compose"><i data-feather="arrow-left"></i> <?= ln('BASE.close') ?></a>
                </div>
            </div>
        </div>
        <div class="inbox-center-container is-both-opened">
            <div class="inbox-center-container-inner">
                <div class="messages">
                    <!-- Messages list -->
                    <div id="inbox-messages" class="inbox-messages has-slimscroll">
                        <div class="inbox-messages-inner">
                            <?php foreach ($request_list as $k => $req): ?>
                                <div id="msg-card-<?= $k ?>" data-preview-id="<?= $k ?>" class="card is-msg">
                                    <div class="card-content">
                                        <div class="msg-header">
                                                <span class="msg-from">
                                                    <small><?= $userSettings['name_surname'] ?></small>
                                                </span>
                                            <span class="msg-attachment">
                                                    <i data-feather="paperclip"></i>
                                                </span>
                                            <span class="msg-timestamp"><?= date_std($req['created_at']) ?></span>
                                        </div>
                                        <div class="msg-snippet">
                                            <span class="tag is-dark"><?= ln('requests.type_' . $req['type']) ?></span>
                                            <span class="tag is-<?= $status_map[$req['status']] ?>"><?= ln('requests.status_' . $req['status']) ?></span>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Messages preview -->
        <!-- /partials/pages/inbox/inbox-message-container.html -->
        <!-- Message Preview -->
        <div class="inbox-message-container">
            <div class="inbox-message-container-inner">

                <!-- Message Previews -->
                <div class="message-body has-slimscroll">

                    <?php foreach ($request_list as $k => $req): ?>
                        <div id="message-preview-<?= $k ?>"
                             class="message-body-inner <?= $k == 0 ? 'is-active' : '' ?>">
                            <div class="box message-preview">
                                <div class="box-inner">
                                    <div class="header">
                                        <div class="meta">
                                            <div class="name"><?= $userSettings['name_surname'] ?></div>
                                            <div class="date"><?= date_std($req['created_at']) ?></div>
                                        </div>
                                        <div class="meta-right">
                                            <div>
                                                <span class="tag is-dark"><?= ln('requests.type_' . $req['type']) ?></span>
                                            </div>
                                            <div>
                                                <span class="tag is-<?= $status_map[$req['status']] ?>"><?= ln('requests.status_' . $req['status']) ?></span>
                                            </div>
                                        </div>
                                    </div>

                                    <hr>
                                    <div class="content">

                                        <?php if ($req['type'] == 1): ?>

                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th><?= ln('requests.start_date') ?></th>
                                                    <th><?= ln('requests.end_date') ?></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td><?= date_std($req['start_date']) ?></td>
                                                    <td><?= date_std($req['end_date']) ?></td>
                                                </tr>
                                                </tbody>
                                            </table>

                                            <p><?= $req['content'] ?></p>

                                        <?php endif; ?>

                                        <?php if ($req['type'] == 2): ?>

                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th><?= ln('food_list.date') ?></th>
                                                    <td><?= date_std($req['start_date']) ?></td>
                                                </tr>
                                                </thead>

                                                </tbody>
                                            </table>

                                            <p><?= $req['content'] ?></p>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>

                </div>
            </div>
        </div>
    </div>

</div>

