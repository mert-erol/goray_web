<div class="view-wrapper ">
    <div class="settings-wrapper">
        <!-- /partials/settings/sections/general-settings.html -->
        <div id="general-settings" class="settings-section is-active">
            <div class="settings-panel">

                <div class="title-wrap">
                    <a class="mobile-sidebar-trigger">
                        <i data-feather="menu"></i>
                    </a>
                    <h2><?= ln('FRONT.Leave Apply') ?></h2>
                </div>

                <div class="settings-form-wrapper">
                    <div class="settings-form">
                        <div class="columns is-multiline">

                            <div class="column is-6">
                                <!--Field-->
                                <div class="field field-group">
                                    <label><?= ln('education.start_date') ?></label>
                                    <div class="control has-icon">
                                        <input id="start-date" class="input my-datetimepicker" type="datetime">

                                        <div class="form-icon">
                                            <i data-feather="calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="column is-6">
                                <!--Field-->
                                <div class="field field-group">
                                    <label><?= ln('education.end_date') ?></label>
                                    <div class="control has-icon">
                                        <input id="end-date" class="input my-datetimepicker" type="datetime">

                                        <div class="form-icon">
                                            <i data-feather="calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="column is-12">
                                <!--Field-->
                                <div class="field field-group">
                                    <label><?= ln('education.content') ?></label>
                                    <div class="control">
                                        <textarea type="text" id="content" class="textarea is-fade" rows="1"
                                                  placeholder=""></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="column is-12">
                                <div class="buttons">
                                    <button class="button is-solid accent-button form-button"><?= ln('BASE.apply') ?></button>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="illustration">
                        <img src="<?= base_url('assets/front') ?>/img/illustrations/settings/5.svg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', (event) => {

        $(document).on('click', '.form-button', function () {

            var dataset = {
                start_date: $('#start-date').val(),
                end_date: $('#end-date').val(),
                content: $('#content').val(),
            };

            $.post(window.location.origin + '/Front/Requests/Apply', dataset, function (response) {
                response = JSON.parse(response);

                if (response.result == 1) {
                    swal.fire({
                        type: 'success',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 1500
                    });

                    window.location.href = window.location.origin + '/Front/Auth/Referrer';
                } else {
                    swal.fire({
                        type: 'error',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 6000
                    });
                }

            });

        });

    });
</script>