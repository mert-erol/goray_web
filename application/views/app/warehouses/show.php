<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Depo Dizaynı
                    <small>Tüm Depolar</small>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="row">


                <?php foreach ($warehouses as $warehouse): ?>
                    <div class="col-md-4">
                        <div class="card" style="width: 100%">
                            <div class="card-header"><?= $warehouse['warehouse_name'] ?></div>
                            <div class="card-body">
                                <?php
                                if (isset($warehouse['sections'])):
                                    foreach ($warehouse['sections'] as $section): ?>
                                        <h4><?= $section['name'] ?></h4>
                                        <?php foreach ($section['shelfs'] as $shelf): ?>
                                            <p><?= $shelf['name'] ?></p>
                                        <?php endforeach;
                                    endforeach;
                                endif;
                                ?>
                            </div>
                        </div>
                    </div>


                <?php endforeach; ?>


            </div>

        </div>
    </div>
</div>

