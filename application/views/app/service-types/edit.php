<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <?= ln('BASE.edit') ?>
                    <span class="badge badge-danger"> <?= lang_data($data['language'])['title'] ?> </span>
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="<?= base_url('App/ServiceTypes') ?>"
                           class="btn btn-dark btn-elevate btn-icon-sm btn-sm">
                            <i class="la la-arrow-left"></i>
                            <?= ln('APPLICATIONS.Service List') ?>
                        </a>
                        <?= $this->load->view('app/base/translations', ['languages' => $languages, 'ref_id' => $data['ref_id'], 'module' => 'ServiceTypes'], TRUE) ?>
                    </div>
                </div>
            </div>
        </div>
        <form id="<?= $appName ?>" action="<?= base_url('App/ServiceTypes/Edit/' . $id) ?>" method="post">
            <div class="kt-portlet__body">

                <div class="form-group row">

                    <?php
                    $textEditorData = '';

                    foreach ($cols as $col):
                        if ($col['type'] == 'datetime'):
                            ?>

                        <?php elseif ($col['name'] == 'method'): ?>
                            <div class="col-lg-2 margin-bottom-15">
                                <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                                <select class="form-control" name="<?= $col['name'] ?>">
                                    <option value="%" <?=$data[$col['name']]=='%'?'selected':''?>>%</option>
                                    <option value="+" <?=$data[$col['name']]=='+'?'selected':''?>>+</option>
                                </select>
                            </div>


                        <?php elseif ($col['name'] == 'password'):

                            ?>
                            <div class="col-lg-3 margin-bottom-15">
                                <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                                <input type="password" name="<?= $col['name'] ?>"
                                       value="<?= md5($data[$col['name']]) ?>"
                                       class="form-control">
                            </div>

                        <?php elseif ($col['type'] == 'text'):
                            $textEditorData = $data[$col['name']];
                            ?>
                            <div class="col-lg-12 margin-bottom-15">
                                <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                                <div name="<?= $col['name'] ?>" class="summernote"></div>
                            </div>

                        <?php elseif ($col['name'] == 'status'): ?>
                            <div class="col-lg-2 margin-bottom-15">
                                <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                                <select class="form-control" name="<?= $col['name'] ?>">
                                    <option value="1" <?= $data[$col['name']] == 1 ? 'selected' : '' ?>><?= ln('BASE.active') ?></option>
                                    <option value="0" <?= $data[$col['name']] == 0 ? 'selected' : '' ?>><?= ln('BASE.passive') ?></option>
                                </select>
                            </div>

                        <?php elseif (in_array($col['type'], array('int', 'float'))): ?>
                            <div class="col-lg-2 margin-bottom-15">
                                <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                                <input type="text" name="<?= $col['name'] ?>" value="<?= $data[$col['name']] ?>"
                                       class="form-control">
                            </div>


                        <?php else: ?>

                            <div class="col-lg-4 margin-bottom-15">
                                <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                                <input type="text" name="<?= $col['name'] ?>" value="<?= $data[$col['name']] ?>"
                                       class="form-control">
                            </div>

                        <?php endif; endforeach; ?>
                </div>
                <div class="kt-form__actions ">


                    <button type="submit" class="btn btn-success btn-elevate btn-icon-sm float-right btn-sm"><i
                                class="la la-save"></i><?= ln('ACTION.update') ?>
                    </button>


                </div>
            </div>
        </form>


    </div>
</div>

<script>

    document.addEventListener("DOMContentLoaded", function (event) {

        $('form[id="<?=$appName?>"]').submit(function (e) {

            e.preventDefault();

            swal.fire({
                title: "<?=ln('MESSAGE.Processing')?>",
                text: "<?=ln('MESSAGE.Wait')?>",
                timer: 99999,
                onOpen: function () {
                    swal.showLoading()
                }
            }).then(function (e) {
                "timer" === e.dismiss && console.log("I was closed by the timer")
            });


            $.post($('form[id="<?=$appName?>"]').attr('action'), $('form[id="<?=$appName?>"]').serializeArray(), function (response) {

                var response = JSON.parse(response);

                console.log(response);

                if (response.result == 1) {
                    swal.fire({
                        type: 'success',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                } else {
                    swal.fire({
                        type: 'error',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 6000
                    });
                }
            });


        });


    });


</script>