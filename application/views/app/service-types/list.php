<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <?= ln('APPLICATIONS.Service List') ?>
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="<?= base_url('App/ServiceTypes/Set') ?>"
                           class="btn btn-dark btn-elevate btn-icon-sm btn-sm">
                            <i class="la la-plus"></i>
                            <?= ln('BASE.add') ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <table class="table table-striped table-bordered table-hover table-checkable" id="pages-categories-table">
                <thead>
                <tr>
                    <?php
                    $columns = '[';
                    foreach ($cols as $col):
                        $columns .= '{ data:"' . $col['name'] . '"},';
                        ?>
                        <th><?= ln($mainTbl . '.' . $col['name']) ?></th>
                    <?php
                    endforeach;
                    $columns = $columns . ']';
                    ?>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


<script>

    var table;
    document.addEventListener("DOMContentLoaded", function (event) {
       table = _base.get_table($("#pages-categories-table"), {
            responsive: !0,
            searchDelay: 500,
            processing: !0,
            serverSide: !0,
            ajax: {url: window.location.origin + '/App/ServiceTypes/Datatables', method: 'post'},
            columns: <?= $columns ?>,
            columnDefs: [
                <?php if(app_auth_check('edit-servicetypes')): ?>
                {
                    targets: 0,
                    title: "#",
                    orderable: !1,
                    render: function (d, t, r) {

                        var edit_btn = '<a href="' + window.location.origin + '/App/ServiceTypes/Edit/' + d + '" class="btn btn-sm btn-clean"><i class="far fa-edit"></i> Edit</a>';
                        var delete_btn = '<button type="button" onclick="delete_service_type(' + r.id + ')" class="btn btn-sm btn-clean text-danger" ><i class="fas fa-times text-danger"></i> Delete</button>';


                        var dropdown = `
                        <div class="dropdown dropdown-inline">
                            <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown" aria-expanded="false">
                                <i class="fas fa-ellipsis-v"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-left" style="display: none;">
                                <ul class="navi flex-column navi-hover">
                                    <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary">Choose an action</li>
                                    <li class="navi-item"> ` + edit_btn + `</li>
                                    <li class="navi-item"> ` + delete_btn + `</li>

                                </ul>
                            </div>
                        </div>`;


                        return dropdown;

                    }
                },
                <?php endif; ?>

                {
                    targets: -3,
                    orderable: !1,
                    render: function (d) {
                        if (d == 0) {
                            return '<span class="badge badge-danger"><?=ln('BASE.passive')?></span>';
                        }
                        if (d == 1) {
                            return '<span class="badge badge-success"><?=ln('BASE.active')?></span>';
                        }
                    }
                }

            ]
        });
    });

    function delete_service_type(id) {


        swal.fire({
            type: 'info',
            title: 'Delete',
            showConfirmButton: true,
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'Close',
            onOpen: function () {
                swal.disableLoading()
            }
        }).then((result) => {
            console.log(result);
            if (result.value) {
                swal.showLoading();
                $.post(window.location.origin + '/App/ServiceTypes/Delete/' + id, {}, function (res) {
                    res = JSON.parse(res);
                    console.log(res);
                    table.ajax.reload();
                    if (res.status == 1) {
                        swal.fire({
                            type: 'success',
                            title: 'Deleted',
                            showConfirmButton: false,
                            timer: 6000,
                            onOpen: function () {
                                swal.disableLoading()
                            }
                        });
                    }

                });
            } else {
                swal.close();
            }

        });


        /*
        $.get(window.location.origin + '/App/Pages/' + page_id, function (res) {
            res = JSON.parse(res);

            if (res.status = 1) {

            } else {

            }

        }); */
    }
</script>