<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <?= ln('meeting_items.survey_id') . ' ' . ln('meeting_items.options') ?>
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <button onclick="modal_trigger('<?= base_url('App/MeetingItems/Set/' . $id) ?>')"
                                class="btn btn-dark btn-elevate btn-icon-sm btn-sm">
                            <i class="la la-plus"></i>
                            <?= ln('BASE.add') ?>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <table class="table table-striped table-bordered table-hover table-checkable" id="department-table">
                <thead>
                <tr>
                    <?php
                    $cols = [['name' => 'id'], ['name' => 'title']];
                    $columns = '[';
                    foreach ($cols as $col):
                        $columns .= '{ data:"' . $col['name'] . '"},';
                        ?>
                        <th><?= ln('survey_items' . '.' . $col['name']) ?></th>
                    <?php
                    endforeach;
                    $columns = $columns . ']';
                    ?>

                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


<script>

    document.addEventListener("DOMContentLoaded", function (event) {
        var table = _base.get_table($("#department-table"), {
            responsive: !0,
            searchDelay: 500,
            processing: !0,
            serverSide: !0,
            ajax: {url: window.location.origin + '/App/MeetingItems/Datatables/<?=$id?>', method: 'post'},
            columns: <?= $columns ?>,
            columnDefs: [{
                targets: 0,
                title: "#",
                orderable: !1,
                render: function (d) {
                    return '<button type="button" onclick="modal_trigger(\'' + window.location.origin + '/App/MeetingItems/Edit/' + d + '\')" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Düzenle">\n<i class="la la-edit"></i>\n</button>';
                }
            }]
        });

        $(".modal").on('hide.bs.modal', function () {
            table.ajax.reload();
        });
    });

</script>