<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <?= ln('BASE.add') ?>
                    <span class="badge badge-danger"> <?= lang_data($this->session->userdata('app_lang_id'))['title'] ?> </span>
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <?php if ($modal): ?>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">x
                            </button>
                        <?php else: ?>
                            <a href="<?= base_url('App/Meetings') ?>"
                               class="btn btn-dark btn-elevate btn-icon-sm btn-sm">
                                <i class="la la-arrow-left"></i>
                                <?= ln('APPLICATIONS.Meetings') ?>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <form id="<?= $appName ?>" action="<?= base_url('App/Meetings/Set') ?>" method="post">
            <div class="kt-portlet__body">

                <div class="form-group row">
                    <?php

                    foreach ($cols as $col):
                        ?>


                        <?php if (in_array($col['name'], array('location'))): ?>
                        <div class="col-lg-12 margin-bottom-15">
                            <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                            <input type="text" name="<?= $col['name'] ?>" class="form-control">
                        </div>

                    <?php elseif (in_array($col['type'], array('varchar'))): ?>
                        <div class="col-lg-8 margin-bottom-15">
                            <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                            <input type="text" name="<?= $col['name'] ?>" class="form-control">
                        </div>

                    <?php elseif ($col['name'] == 'department'): ?>
                        <div class="col-lg-4 margin-bottom-15">
                            <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                            <select class="form-control kt-selectpicker" data-style="btn-warning"
                                    name="<?= $col['name'] ?>" data-size="7"
                                    data-live-search="true">
                                <option value=""><?= ln('BASE.all') ?></option>
                            </select>
                        </div>

                    <?php elseif ($col['name'] == 'start_date'): ?>
                        <div class="col-lg-6 margin-bottom-15">
                            <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                            <input type="text" name="<?= $col['name'] ?>" class="form-control datetimepicker"
                                   autocomplete="off">
                        </div>

                    <?php elseif ($col['name'] == 'end_date'): ?>
                        <div class="col-lg-6 margin-bottom-15">
                            <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                            <input type="text" name="<?= $col['name'] ?>" class="form-control datetimepicker"
                                   autocomplete="off">
                        </div>

                    <?php elseif ($col['type'] == 'datetime'): ?>

                    <?php elseif ($col['type'] == 'text'): ?>
                        <div class="col-lg-12 margin-bottom-15">
                            <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                            <div name="<?= $col['name'] ?>" class="summernote"></div>
                        </div>

                    <?php elseif ($col['name'] == 'status'): ?>
                        <div class="col-lg-2 margin-bottom-15">
                            <label><?= $col['comment'] ?></label>
                            <select class="form-control" name="<?= $col['name'] ?>">
                                <option value="1"><?= ln('BASE.active') ?></option>
                                <option value="0"><?= ln('BASE.passive') ?></option>
                            </select>
                        </div>


                    <?php elseif (in_array($col['type'], array('int', 'float'))): ?>
                        <div class="col-lg-2 margin-bottom-15">
                            <label><?= $col['comment'] ?></label>
                            <input type="text" name="<?= $col['name'] ?>" class="form-control">
                        </div>


                    <?php else: ?>

                        <div class="col-lg-4 margin-bottom-15">
                            <label><?= $col['comment'] ?></label>
                            <input type="text" name="<?= $col['name'] ?>" class="form-control">
                        </div>

                    <?php endif; endforeach; ?>
                </div>
                <div class="kt-form__actions">
                    <button type="submit" class="btn btn-success btn-elevate btn-icon-sm float-right btn-sm"><i
                                class="la la-plus"></i><?= ln('ACTION.add') ?>
                    </button>
                </div>
            </div>
        </form>


    </div>
</div>

<script>

    <?php if (!$modal): ?>
    document.addEventListener("DOMContentLoaded", function (event) {
        <?php endif; ?>

        $('select[name="department"]').html(departments());
        $('form[id="<?=$appName?>"] .summernote').summernote({height: 150});

        $('form[id="<?=$appName?>"]').submit(function (e) {

            e.preventDefault();

            swal.fire({
                title: "<?=ln('MESSAGE.Processing')?>",
                text: "<?=ln('MESSAGE.Wait')?>",
                timer: 99999,
                onOpen: function () {
                    swal.showLoading()
                }
            }).then(function (e) {
                "timer" === e.dismiss && console.log("I was closed by the timer")
            });

            var serializedData = $('form[id="<?=$appName?>"]').serializeArray();
            serializedData.push({
                'name': 'content',
                'value': $('form[id="<?=$appName?>"] .summernote').summernote('code')
            });


            $.post($('form[id="<?=$appName?>"]').attr('action'), serializedData, function (response) {


                console.log($('form[id="<?=$appName?>"]').serializeArray());

                var response = JSON.parse(response);

                if (response.result == 1) {
                    swal.fire({
                        type: 'success',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 1500
                    });

                    <?php if (!$modal): ?>
                    window.location.href = window.location.origin + '/App/Meetings/Edit/' + response.last_id;
                    <?php endif; ?>

                } else {
                    swal.fire({
                        type: 'error',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 6000
                    });
                }


            });


        });

        <?php if (!$modal): ?>
    });
    <?php endif; ?>

</script>