<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <?= ln('APPLICATIONS.Job Titles') ?>
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="<?= base_url('App/Titles/Set') ?>" class="btn btn-dark btn-elevate btn-icon-sm btn-sm">
                            <i class="la la-plus"></i>
                            <?= ln('BASE.add') ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <table class="table table-striped- table-bordered table-hover table-checkable" id="customer-table">
                <thead>
                <tr>
                    <?php
                    $columns = '[';
                    foreach ($cols as $col):
                        if ($col['name'] != 'auths_list'):
                            $columns .= '{ data:"' . $col['name'] . '"},';
                            ?>
                            <th><?= ln($mainTbl . '.' . $col['name']) ?></th>
                        <?php endif; endforeach;
                    $columns = $columns . ']';
                    ?>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


<script>

    document.addEventListener("DOMContentLoaded", function (event) {

        _base.get_table($("#customer-table"), {
            responsive: !0,
            searchDelay: 500,
            processing: !0,
            serverSide: !0,
            ajax: {url: window.location.origin + '/App/Titles/Datatables', method: 'post'},
            columns: <?=$columns?>,
            columnDefs: [
                <?php if(app_auth_check('edit-titles')): ?>
                {
                    targets: 0,
                    title: "#",
                    orderable: !1,
                    render: function (d) {
                        return '<a href="' + window.location.origin + '/App/Titles/Edit/' + d + '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit">\n<i class="la la-edit"></i>\n</a>'
                    }
                },
                <?php endif; ?>
                {
                    targets: -3,
                    orderable: !1,
                    render: function (d) {
                        if (d == 0) {
                            return '<span class="badge badge-danger"><?=ln('BASE.passive')?></span>';
                        }
                        if (d == 1) {
                            return '<span class="badge badge-success"><?=ln('BASE.active')?></span>';
                        }
                    }
                }
            ]
        });

    });


</script>