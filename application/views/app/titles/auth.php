<div class="col-lg-<?= $size ?>">
    <!--begin::Portlet-->
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <?= ln('titles.auths_list') ?>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">

            <?php
            $authsList = strlen($data['auths_list']) > 0 ? json_decode('[' . $data['auths_list'] . ']', true)[0] : [];
            ?>

            <ul class="nav nav-tabs  nav-tabs-line" role="tablist">

                <?php
                $i = 0;
                foreach ($applications as $apps): $i += 1;
                    if ($apps['is_menu'] == 1 && $apps['parent'] == 0):
                        ?>
                        <li class="nav-item">
                            <a class="nav-link <?= $i == 1 ? 'active' : '' ?>" data-toggle="tab"
                               href="#auth_tabs_<?= $apps['name'] ?>" role="tab">
                                <i class="<?= $apps['icon'] ?>"></i>
                                <?= ln('APPLICATIONS.'.$apps['title']) ?></a>
                        </li>

                    <?php
                    endif; endforeach; ?>


            </ul>
            <form action="<?= base_url('App/Titles/SetAuths/' . $id) ?>" id="auths-list">
                <div class="tab-content">

                    <?php

                    $childs = array_filter($applications, function ($val) {

                        if ($val['parent'] != 0) {
                            return $val;
                        }
                    });


                    $i = 0;
                    foreach ($applications as $apps): $i += 1;
                        if ($apps['is_menu'] == 1 && $apps['parent'] == 0):
                            ?>
                            <div class="tab-pane <?= $i == 1 ? 'active' : '' ?>" id="auth_tabs_<?= $apps['name'] ?>"
                                 role="tabpanel">
                                <div class="form-group">
                                    <div class="kt-checkbox-inline">
                                        <?php foreach ($childs as $chd):
                                            if ($chd['parent'] == $apps['id']):
                                                ?>

                                                <label class="">
                                                    <input type="checkbox"
                                                        <?= array_key_exists($chd['name'], $authsList) ? 'checked="checked"' : '' ?>
                                                           name="<?= $chd['name'] ?>">
                                                    <?= $chd['title'] ?>
                                                </label>

                                            <?php endif; endforeach; ?>
                                    </div>
                                </div>
                            </div>

                        <?php
                        endif; endforeach; ?>


                    <div class="kt-form__actions">
                        <button type="submit" class="btn btn-success btn-elevate btn-icon-sm float-right btn-sm"><i
                                    class="la la-save"></i><?= ln('ACTION.update') ?>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script>

    document.addEventListener("DOMContentLoaded", function (event) {

        $('form[id="auths-list"]').submit(function (e) {

            e.preventDefault();

            swal.fire({
                title: "<?=ln('MESSAGE.Processing')?>",
                text: "<?=ln('MESSAGE.Wait')?>",
                timer: 99999,
                onOpen: function () {
                    swal.showLoading()
                }
            }).then(function (e) {
                "timer" === e.dismiss && console.log("I was closed by the timer")
            });


            $.post($('form[id="auths-list"]').attr('action'), $('form[id="auths-list"]').serializeArray(), function (response) {

                var response = JSON.parse(response);

                console.log(response);

                if (response.result == 1) {
                    swal.fire({
                        type: 'success',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                } else {
                    swal.fire({
                        type: 'error',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 6000
                    });
                }
            });


        });


    });


</script>