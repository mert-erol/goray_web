<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <?= ln('BASE.add') . ' ' . ln('client_user.message') ?>
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="<?= base_url('App/ClientUsers') ?>"
                           class="btn btn-dark btn-elevate btn-icon-sm btn-sm">
                            <i class="la la-arrow-left"></i>
                            <?= ln('APPLICATIONS.Users') ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <form id="<?= $appName ?>" action="<?= base_url('App/ClientUsers/Set/' . $customerID) ?>" method="post">
            <div class="kt-portlet__body">

                <div class="form-group row">
                    <?php
                    foreach ($cols as $col):
                        if ($col['type'] == 'datetime' || $col['name']=='file'): ?>

                        <?php elseif ($col['name'] == 'title_ref_id'): ?>
                            <div class="col-lg-3 margin-bottom-15">
                                <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                                <select class="form-control kt-selectpicker" data-style="btn-warning"
                                        name="<?= $col['name'] ?>" data-size="7"
                                        data-live-search="true">
                                    <option value=""><?= ln('BASE.select') ?></option>
                                </select>
                            </div>

                        <?php elseif ($col['name'] == 'locale'): ?>
                            <div class="col-lg-3 margin-bottom-15">
                                <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                                <select class="form-control kt-selectpicker" data-style="btn-info"
                                        name="<?= $col['name'] ?>" data-size="7"
                                        data-live-search="true">
                                    <option value=""><?= ln('BASE.select') ?></option>
                                    <?php foreach ($languages as $lang): ?>
                                        <option value="<?= $lang['id'] ?>"><?= $lang['title'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                        <?php elseif ($col['name'] == 'date'): ?>
                            <div class="col-lg-3 margin-bottom-15">
                                <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                                <input type="text" name="<?= $col['name'] ?>" class="form-control datepicker"
                                       autocomplete="off">
                            </div>

                        <?php elseif ($col['type'] == 'text'): ?>
                            <div class="col-lg-12 margin-bottom-15">
                                <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                                <div name="<?= $col['name'] ?>" class="summernote"></div>
                            </div>

                        <?php elseif ($col['name'] == 'status'): ?>
                            <div class="col-lg-3 margin-bottom-15">
                                <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                                <select class="form-control" name="<?= $col['name'] ?>">
                                    <option value="1"><?= ln('BASE.active') ?></option>
                                    <option value="0"><?= ln('BASE.passive') ?></option>
                                </select>

                            </div>

                        <?php elseif ($col['name'] == 'customer_id'): ?>


                        <?php elseif (in_array($col['type'], array('int', 'float'))): ?>
                            <div class="col-lg-2 margin-bottom-15">
                                <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                                <input type="text" name="<?= $col['name'] ?>" class="form-control">
                            </div>


                        <?php else: ?>

                            <div class="col-lg-3 margin-bottom-15">
                                <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                                <input type="text" name="<?= $col['name'] ?>" class="form-control">
                            </div>

                        <?php endif; endforeach; ?>
                </div>
                <div class="kt-form__actions">
                    <button type="submit" class="btn btn-success btn-elevate btn-icon-sm float-right btn-sm"><i
                                class="la la-plus"></i><?= ln('ACTION.add') ?></button>
                </div>
            </div>
        </form>


    </div>
</div>

<script>

    document.addEventListener("DOMContentLoaded", function (event) {

        $('select[name="title_ref_id"]').html(job_titles(<?=$this->session->userdata('app_lang_id')?>));


        $('form[id="<?=$appName?>"]').submit(function (e) {
            e.preventDefault();
            swal.fire({
                title: "<?=ln('MESSAGE.Processing')?>",
                text: "<?=ln('MESSAGE.Wait')?>",
                timer: 99999,
                onOpen: function () {
                    swal.showLoading()
                }
            }).then(function (e) {
                "timer" === e.dismiss && console.log("I was closed by the timer")
            });

            $.post($('form[id="<?=$appName?>"]').attr('action'), $('form[id="<?=$appName?>"]').serializeArray(), function (response) {
                var response = JSON.parse(response);
                if (response.result == 1) {
                    swal.fire({
                        type: 'success',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 1500
                    });

                    window.location.href = window.location.origin + '/App/ClientUsers/Edit/' + response.last_id;
                } else {
                    swal.fire({
                        type: 'error',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 6000
                    });
                }

            });


        });


    });


</script>