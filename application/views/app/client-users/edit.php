<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <?= ln('BASE.edit') ?>
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="<?= base_url('App/ClientUsers') ?>"
                           class="btn btn-dark btn-elevate btn-icon-sm btn-sm">
                            <i class="la la-arrow-left"></i>
                            <?= ln('APPLICATIONS.Users') ?>
                        </a>

                        <button onclick="modal_trigger('<?= base_url('App/PageSigns/' . $id) ?>')"
                                class="btn btn-dark btn-elevate btn-icon-sm btn-sm">
                            <i class="la la-signature"></i>
                            <?= ln('APPLICATIONS.PageSigns') ?>
                        </button>

                    </div>
                </div>
            </div>
        </div>
        <form id="<?= $appName ?>" action="<?= base_url('App/ClientUsers/Edit/' . $id) ?>" method="post"
              enctype="multipart/form-data">
            <div class="kt-portlet__body">

                <div class="form-group row">
                    <?php if (strlen($data['file']) > 0): ?>
                        <div class="col-md-3">
                            <div class="card">
                                <img class="card-img-top" src="<?= upload_dir() ?>/<?= $data['file'] ?>"
                                     alt="Card image cap">
                            </div>
                        </div>
                    <?php endif; ?>

                    <div class="col-md-9">
                        <div class="row">
                            <?php
                            $textEditorData = '';

                            foreach ($cols as $col):
                                if ($col['type'] == 'datetime'):
                                    ?>

                                <?php elseif ($col['name'] == 'category_ref_id'):
                                    $titl_ref_id = $data[$col['name']];
                                    ?>
                                    <div class="col-lg-3 margin-bottom-15">
                                        <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                                        <select class="form-control kt-selectpicker" data-style="btn-warning"
                                                name="<?= $col['name'] ?>" data-size="7"
                                                data-live-search="true">
                                            <option value=""><?= ln('BASE.select') ?></option>
                                        </select>
                                    </div>

                                <?php elseif ($col['name'] == 'title_ref_id'):
                                    $titl_ref_id = $data[$col['name']];
                                    ?>
                                    <div class="col-lg-3 margin-bottom-15">
                                        <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                                        <select class="form-control kt-selectpicker" data-style="btn-warning"
                                                name="<?= $col['name'] ?>" data-size="7"
                                                data-live-search="true">
                                            <option value=""><?= ln('BASE.select') ?></option>
                                        </select>
                                    </div>


                                <?php elseif ($col['name'] == 'file'): ?>
                                    <div class="col-lg-3 margin-bottom-15">
                                        <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                                        <input type="file" class="form-control" name="<?= $col['name'] ?>">
                                    </div>

                                <?php elseif ($col['name'] == 'locale'): ?>
                                    <div class="col-lg-3 margin-bottom-15">
                                        <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                                        <select class="form-control kt-selectpicker" data-style="btn-info"
                                                name="<?= $col['name'] ?>" data-size="7"
                                                data-live-search="true">
                                            <option value=""><?= ln('BASE.select') ?></option>
                                            <?php foreach ($languages as $lang): ?>
                                                <option <?= $data[$col['name']] == $lang['id'] ? 'selected' : '' ?>
                                                        value="<?= $lang['id'] ?>"><?= $lang['title'] ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>

                                <?php elseif ($col['name'] == 'password'):

                                    ?>
                                    <div class="col-lg-3 margin-bottom-15">
                                        <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                                        <input type="password" name="<?= $col['name'] ?>"
                                               value="<?= md5($data[$col['name']]) ?>"
                                               class="form-control">
                                    </div>

                                <?php elseif ($col['name'] == 'date'): ?>
                                    <div class="col-lg-3 margin-bottom-15">
                                        <label><?= ln($mainTbl . '.' . $col['name']) ?> </label>
                                        <input type="text" name="<?= $col['name'] ?>" class="form-control datepicker"
                                               value="<?= date('d/m/Y', strtotime($data[$col['name']])) ?>">
                                    </div>

                                <?php elseif ($col['type'] == 'text'):
                                    $textEditorData = $data[$col['name']];
                                    ?>
                                    <div class="col-lg-12 margin-bottom-15">
                                        <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                                        <div name="<?= $col['name'] ?>" class="summernote"></div>
                                    </div>

                                <?php elseif ($col['name'] == 'status'): ?>
                                    <div class="col-lg-3 margin-bottom-15">
                                        <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                                        <select class="form-control" name="<?= $col['name'] ?>">
                                            <option value="1" <?= $data[$col['name']] == 1 ? 'selected' : '' ?>><?= ln('BASE.active') ?></option>
                                            <option value="0" <?= $data[$col['name']] == 0 ? 'selected' : '' ?>><?= ln('BASE.passive') ?></option>
                                        </select>

                                    </div>

                                <?php elseif ($col['name'] == 'customer_id'): ?>


                                <?php elseif (in_array($col['type'], array('int', 'float'))): ?>
                                    <div class="col-lg-2 margin-bottom-15">
                                        <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                                        <input type="text" name="<?= $col['name'] ?>" value="<?= $data[$col['name']] ?>"
                                               class="form-control">
                                    </div>


                                <?php else: ?>

                                    <div class="col-lg-3 margin-bottom-15">
                                        <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                                        <input type="text" name="<?= $col['name'] ?>" value="<?= $data[$col['name']] ?>"
                                               class="form-control">
                                    </div>

                                <?php endif; endforeach; ?>
                        </div>
                    </div>


                </div>
                <div class="kt-form__actions">
                    <button type="submit" class="btn btn-brand btn-elevate btn-icon-sm float-right"><i
                                class="la la-save"></i><?= ln('ACTION.update') ?>
                    </button>
                </div>
            </div>
        </form>


    </div>
</div>

<script>

    document.addEventListener("DOMContentLoaded", function (event) {

        $('select[name="title_ref_id"]').html(job_titles(<?=$this->session->userdata('app_lang_id')?>));
        $('select[name="title_ref_id"]').val(<?=$titl_ref_id?>);


        $('form[id="<?=$appName?>"]').submit(function (e) {
            e.preventDefault();
            swal.fire({
                title: "<?=ln('MESSAGE.Processing')?>",
                text: "<?=ln('MESSAGE.Wait')?>",
                timer: 99999,
                onOpen: function () {
                    swal.showLoading()
                }
            }).then(function (e) {
                "timer" === e.dismiss && console.log("I was closed by the timer")
            });

            var form = $(this);
            var formdata = false;
            if (window.FormData) {
                formdata = new FormData(form[0]);
            }

            $.ajax({
                url: $('form[id="<?=$appName?>"]').attr('action'),
                data: formdata ? formdata : form.serialize(),
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function (response, textStatus, jqXHR) {
                    var response = JSON.parse(response);
                    if (response.result == 1) {
                        swal.fire({
                            type: 'success',
                            title: response.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    } else {
                        swal.fire({
                            type: 'error',
                            title: response.message,
                            showConfirmButton: false,
                            timer: 6000
                        });
                    }
                }
            });


        });


    });


</script>