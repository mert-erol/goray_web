<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <?= ln('APPLICATIONS.User List') ?>
                </h3>
            </div>

            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="<?= base_url('App/ClientUsers/Set') ?>"
                           class="btn btn-dark btn-elevate btn-icon-sm btn-sm">
                            <i class="la la-plus"></i>
                            <?= ln('BASE.add') ?>
                        </a>
                    </div>
                </div>
            </div>

        </div>
        <div class="kt-portlet__body">
            <div class="row" id="search-bar">
                <?php foreach ($cols as $k => $cl):
                    if (!in_array($cl['name'], ['password', 'file'])):
                        if ($cl['name'] == 'status'): ?>
                            <div class="col-md-3 col-xs-12">
                                <select class="form-control"
                                        data-col-name="<?= $cl['name'] ?>"
                                        data-index="<?= $k ?>">
                                    <option value="" noselected><?= ln($mainTbl . '.' . $cl['name']) ?></option>
                                    <option value="1"><?= ln('BASE.active') ?></option>
                                    <option value="0"><?= ln('BASE.passive') ?></option>
                                </select>
                            </div>


                        <?php elseif ($cl['name'] == 'locale'): ?>
                            <div class="col-md-3 col-xs-12">
                                <select class="form-control"
                                        data-col-name="<?= $cl['name'] ?>"
                                        data-index="<?= $k ?>">
                                    <option value="" noselected><?= ln($mainTbl . '.' . $cl['name']) ?></option>
                                    <option><?= ln('BASE.turkish') ?></option>
                                    <option><?= ln('BASE.english') ?></option>
                                </select>
                            </div>

                        <?php elseif ($cl['name'] == 'title_ref_id' && 1==2): ?>
                            <div class="col-md-3 col-xs-12">
                                <select class="form-control"
                                        data-col-name="<?= $cl['name'] ?>"
                                        data-index="<?= $k ?>">
                                    <option value="" noselected><?= ln($mainTbl . '.' . $cl['name']) ?></option>
                                </select>
                            </div>

                        <?php else: ?>


                            <div class="col-md-3 col-xs-12">

                                <input class="form-control form-control-sm <?= $cl['name'] == 'date' ? 'datepicker' : '' ?>"
                                       type="text"
                                       data-col-name="<?= $cl['name'] ?>"
                                       placeholder="<?= ln($mainTbl . '.' . $cl['name']) ?>"
                                       data-index="<?= $k ?>"/>
                            </div>


                        <?php endif; endif; endforeach; ?>
                <div class="col-md-3 col-xs-12">

                    <div class="btn-group btn-block" role="group" aria-label="Basic example">
                        <button type="button" id="filter" class="btn btn-secondary btn-sm btn-elevate btn-icon-sm">
                            <i class="la la-filter"></i>
                            <?= ln('BASE.filter') ?>
                        </button>
                        <button type="button" id="clear" class="btn btn-secondary btn-sm btn-elevate btn-icon-sm">
                            <i class="la la-times"></i>
                            <?= ln('BASE.clear') ?>
                        </button>
                    </div>

                </div>
            </div>
            <br>
            <table class="table table-striped- table-bordered table-hover table-checkable" id="users-table">
                <thead>

                <tr>
                    <?php

                    $columns = '[';
                    foreach ($cols as $col):
                        if (!in_array($col['name'], ['file'])):
                            $columns .= '{ data:"' . $col['name'] . '"},';
                            ?>
                            <th><?= ln($mainTbl . '.' . $col['name']) ?></th>
                        <?php
                        endif;
                    endforeach;
                    $columns = $columns . ']';
                    ?>

                </tr>


                </thead>
            </table>
        </div>
    </div>
</div>


<script>

    document.addEventListener("DOMContentLoaded", function (event) {

        $('select[data-col-name="title_ref_id"]').html(job_titles(2, 'title'));

        var table = _base.get_table($("#users-table"), {
            responsive: !0,
            searchDelay: 500,
            processing: !0,
            serverSide: !0,
            ajax: {url: window.location.origin + '/App/ClientUsers/Datatables', method: 'post'},
            columns: <?= $columns ?>,
            columnDefs: [
                {
                    targets: 0,
                    title: "#",
                    orderable: !1,
                    render: function (d) {
                        return '<a href="' + window.location.origin + '/App/ClientUsers/Edit/' + d + '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit">\n<i class="la la-edit"></i>\n</a>'
                    }
                },
                {
                    targets: 4,
                    render: function (d, t, r) {
                        return '<button type="button" data-id="' + r.id + '" data-email="' + r.username + '" class="btn btn-sm renew-password"><?=ln('BASE.renew')?></button>';

                    }
                },
                {
                    targets: -4,
                    orderable: !1,
                    render: function (d) {
                        if (d == 0) {
                            return '<span class="badge badge-danger"><?=ln('BASE.passive')?></span>';
                        }
                        if (d == 1) {
                            return '<span class="badge badge-success"><?=ln('BASE.active')?></span>';
                        }
                    }
                }
            ]
        });


        /*
        $('#search-bar input').on('keyup change', function () {
            $(this).each(function (k, v) {
                var i = $(v).attr('data-index');
                var val = $(v).val();
                table.column(i).search(val).draw();
            });
        });*/

        $(document).on('click', '#filter', function () {
            run_filter();
        });

        $(document).on('click', '#clear', function () {
            run_clear();
        });

        $(document).on('click', '.renew-password', function () {
            var id = $(this).attr('data-id');
            var email = $(this).attr('data-email');
            reset_password(id, email);
        });

        function run_filter() {
            $('#search-bar input').each(function (k, v) {
                var i = $(v).attr('data-index');
                var val = $(v).val();
                table.column(i).search(val).draw();
            });
        }

        function run_clear() {
            $('#search-bar input').each(function (k, v) {
                $(v).val('');
            });
            run_filter();
        }

        function reset_password(id, email) {


            swal.fire({
                title: "<?=ln('MESSAGE.Processing')?>",
                text: "<?=ln('MESSAGE.Wait')?>",
                timer: 99999,
                onOpen: function () {
                    swal.showLoading()
                }
            }).then(function (e) {
                "timer" === e.dismiss && console.log("I was closed by the timer")
            });


            $.post(window.location.origin + '/Front/Auth/ResetPassword', {}, function (response) {
                response = JSON.parse(response);
                if (response.result == 1) {
                    swal.fire({
                        type: 'success',
                        title: response.message + ' ' + email,
                        showConfirmButton: false,
                        timer: 1500
                    });
                } else {
                    swal.fire({
                        type: 'error',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 6000
                    });
                }
            });


        }

    });


</script>