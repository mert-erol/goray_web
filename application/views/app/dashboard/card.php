<?php foreach ($cardData as $card): ?>
    <div class="col-lg-4 col-xl-4 col-md-4 order-lg-1 order-xl-1">
        <div class="kt-portlet kt-callout kt-callout--info kt-callout--diagonal-bg">
            <div class="kt-portlet__body">
                <div class="kt-callout__body">
                    <div class="kt-callout__content">
                        <a href="<?= base_url('App/' . $card['url']) ?>">
                            <span><?= ln('APPLICATIONS.' . $card['parent_title']) ?></span>
                            <h3 class="kt-callout__title"><?= ln('APPLICATIONS.' . $card['title']) ?></h3>
                        </a>
                    </div>
                    <div class="kt-callout__action">
                        <i class="<?= $card['icon'] ?> fa-4x"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>