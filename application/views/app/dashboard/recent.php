<div class="col-lg-<?= $size ?> col-xl-4 order-lg-1 order-xl-1">
    <!--Begin::Portlet-->
    <div class="kt-portlet kt-portlet--height-fluid">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Son Olaylar
                </h3>
            </div>

        </div>
        <div class="kt-portlet__body">
            <!--Begin::Timeline 3 -->
            <div class="kt-timeline-v2">
                <div class="kt-timeline-v2__items  kt-padding-top-25 kt-padding-bottom-30">

                    <?php for ($i = 0; $i < 10; $i++): ?>

                        <div class="kt-timeline-v2__item">
                            <span class="kt-timeline-v2__item-time"><?= date('H:i', strtotime($logs[$i]['created_at'])) ?></span>
                            <div class="kt-timeline-v2__item-cricle">
                                <i class="fa fa-genderless kt-font-dark"></i>
                            </div>
                            <div class="kt-timeline-v2__item-text  kt-padding-top-5">
                                <?= $logs[$i]['text'] ?>
                            </div>
                        </div>

                    <?php endfor; ?>

                </div>
            </div>
            <!--End::Timeline 3 -->
        </div>
    </div>
    <!--End::Portlet-->
</div>