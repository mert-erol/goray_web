<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <?= ln('APPLICATIONS.Category List') ?>
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="<?= base_url('App/PagesCategories/Set') ?>"
                           class="btn btn-dark btn-elevate btn-icon-sm btn-sm">
                            <i class="la la-plus"></i>
                            <?= ln('BASE.add') ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <table class="table table-striped table-bordered table-hover table-checkable" id="pages-categories-table">
                <thead>
                <tr>
                    <?php
                    $columns = '[';
                    foreach ($cols as $col):
                        $columns .= '{ data:"' . $col['name'] . '"},';
                        ?>
                        <th><?= ln($mainTbl . '.' . $col['name']) ?></th>
                    <?php
                    endforeach;
                    $columns = $columns . ']';
                    ?>

                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


<script>

    var table;
    document.addEventListener("DOMContentLoaded", function (event) {
        table = _base.get_table($("#pages-categories-table"), {
            responsive: !0,
            searchDelay: 500,
            processing: !0,
            serverSide: !0,
            ajax: {url: window.location.origin + '/App/PagesCategories/Datatables', method: 'post'},
            columns: <?= $columns ?>,
            columnDefs: [{
                targets: 0,
                title: "#",
                orderable: !1,
                render: function (d, t, r) {


                    var edit_btn = '<a href="' + window.location.origin + '/App/PagesCategories/Edit/' + d + '" class="btn btn-sm btn-clean"><i class="far fa-edit"></i> Edit</a>';
                    var duplicate_btn = '<button type="button" onclick="duplicate_category(' + r.id + ')" class="btn btn-sm btn-clean" ><i class="far fa-copy"></i> Duplicate</button>';
                    var delete_btn = '<button type="button" onclick="delete_category(' + r.id + ')" class="btn btn-sm btn-clean text-danger" ><i class="fas fa-times text-danger"></i> Delete</button>';


                    var dropdown = `
                        <div class="dropdown dropdown-inline">
                            <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown" aria-expanded="false">
                                <i class="fas fa-ellipsis-v"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-left" style="display: none;">
                                <ul class="navi flex-column navi-hover">
                                    <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary">Choose an action</li>
                                    <li class="navi-item"> ` + edit_btn + `</li>
                                    <li class="navi-item"> ` + duplicate_btn + `</li>
                                    <li class="navi-item"> ` + delete_btn + `</li>

                                </ul>
                            </div>
                        </div>`;


                    return dropdown;

                }
            }]
        });
    });

    function duplicate_category(id) {
        $.post(window.location.origin + '/App/PagesCategories/Duplicate/' + id, {}, function (res) {
            res = JSON.parse(res);

            swal.fire({
                title: "<?=ln('MESSAGE.Processing')?>",
                text: "<?=ln('MESSAGE.Wait')?>",
                onOpen: function () {
                    swal.showLoading()
                }
            });


            console.log(res);


            if (res.status == 1) {

                swal.fire({
                    type: 'success',
                    title: '<?=ln('MESSAGE.Duplicated')?>',
                    showConfirmButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Edit Duplicated Category',
                    cancelButtonText: 'Close',
                    onOpen: function () {
                        swal.disableLoading()
                    }
                }).then((result) => {
                    if (result.value) {
                        swal.showLoading();
                        window.location.href = window.location.origin + '/App/PagesCategories/Edit/' + res.insert_id;
                    } else {
                        swal.close();
                    }

                });

            } else {

                swal.fire({
                    type: 'error',
                    title: '<?=ln('MESSAGE.Error')?>',
                    showConfirmButton: false,
                    timer: 6000,
                    onOpen: function () {
                        swal.disableLoading()
                    }
                });
            }

        });
    }

    function delete_category(id) {


        swal.fire({
            type: 'info',
            title: 'Delete',
            showConfirmButton: true,
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'Close',
            onOpen: function () {
                swal.disableLoading()
            }
        }).then((result) => {
            console.log(result);
            if (result.value) {
                swal.showLoading();
                $.post(window.location.origin + '/App/PagesCategories/Delete/' + id, {}, function (res) {
                    res = JSON.parse(res);
                    console.log(res);
                    table.ajax.reload();
                    if (res.status == 1) {
                        swal.fire({
                            type: 'success',
                            title: 'Deleted',
                            showConfirmButton: false,
                            timer: 6000,
                            onOpen: function () {
                                swal.disableLoading()
                            }
                        });
                    }

                });
            } else {
                swal.close();
            }

        });


    }

</script>