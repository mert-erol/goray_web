<div class="btn-group">
    <button type="button"
            class="btn btn-dark btn-elevate btn-icon-sm btn-sm dropdown-toggle dropdown-toggle"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <?= ln('BASE.translations') ?>
    </button>
    <div class="dropdown-menu dropdown-menu-right">
        <?php foreach ($languages as $lng):
            if ($lng['id'] != $data['language']):
                ?>
                <a href="<?= base_url("App/$module/Set/" . $lng['id'] . '/' . $ref_id) ?>"
                   class="dropdown-item"
                   type="button"><?= $lng['title'] ?></a>
            <?php endif; endforeach; ?>
    </div>
</div>