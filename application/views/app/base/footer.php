</div>

</div>
<!-- end:: Content -->

<!--begin::Modal-->
<div class="modal fade" id="auto-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">

        </div>
    </div>
</div>
<!--end::Modal-->


<!-- begin::Global Config(global config for global JS sciprts) -->
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": [
                    "#c5cbe3",
                    "#a1a8c3",
                    "#3d4465",
                    "#3e4466"
                ],
                "shape": [
                    "#f0f3ff",
                    "#d9dffa",
                    "#afb4d4",
                    "#646c9a"
                ]
            }
        }
    };


</script>
<!-- end::Global Config -->

<!--begin::Global Theme Bundle(used by all pages) -->
<script src="<?= base_url('assets/panel') ?>/plugins/global/plugins.bundle.js" type="text/javascript"></script>
<script src="<?= base_url('assets/panel') ?>/js/scripts.bundle.js" type="text/javascript"></script>
<!--end::Global Theme Bundle -->

<!--begin::Page Vendors(used by this page) -->
<script src="<?= base_url('assets/panel') ?>/plugins/custom/fullcalendar/fullcalendar.bundle.js"
        type="text/javascript"></script>

<!--end::Page Vendors -->

<!--begin::Page Scripts(used by this page) -->
<script src="<?= base_url('assets/panel') ?>/js/pages/dashboard.js" type="text/javascript"></script>
<script src="<?= base_url('assets/panel') ?>/plugins/custom/datatables/datatables.bundle.js"
        type="text/javascript"></script>
<script src="<?= base_url('assets/panel') ?>/plugins/custom/datatables/dataTables.fixedColumns.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('assets/panel') ?>/plugins/custom/uppy/uppy.bundle.js" type="text/javascript"></script>
<script src="<?= base_url('assets/panel') ?>/plugins/custom/uppy/tr_TR.min.js" type="text/javascript"></script>
<script src="<?= base_url('assets/panel') ?>/js/pages/custom/inbox/inbox.js" type="text/javascript"></script>
<script src="<?= base_url('assets/panel') ?>/js/pages/crud/forms/editors/quill.js" type="text/javascript"></script>
<script src="<?= base_url('assets/panel') ?>/js/pages/crud/forms/widgets/bootstrap-maxlength.js"
        type="text/javascript"></script>

<script src="<?= base_url('assets/panel') ?>/js/bootstrap-grid.js" type="text/javascript"></script>


<script src="<?= base_url('assets/panel') ?>/js/app.js" type="text/javascript"></script>

<!--begin::Page Scripts(used by this page) -->

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.1/jquery.contextMenu.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.1/jquery.ui.position.js"></script>
<script src="<?= base_url('assets/panel/'); ?>plugins/custom/bootstrap-fileinput/5.0.9/js/fileinput.js"></script>

<script>


    var options = {
        'placement': 'top center',
        'type': 'loader',
        'width': 100,
        'state': 'default',
        'message': 'Loading...'
    };

    var dialog = new KTDialog(options);


    function modal_trigger(url) {
        $.get(url, {modal: 'true'}, function (data) {
            $('.modal-content').html(data);
            $('#auto-modal').modal('show');
        });
    }

    //
    // $(document).on('click', '.modal-engine', function () {
    //
    //     $.get($(this).attr('modal-url'), {modal: 'true'}, function (data) {
    //         $('.modal-content').html(data);
    //         $('#kt_modal_4').modal('toggle');
    //     });
    // });


    function modal_hide() {

        $('#auto-modal').modal('hide');
        $('.modal-content').html('');
    }


    function product_categories() {

        var options = '<option value="">Seçiniz</option>';

        $.ajax({
            url: window.location.origin + '/App/Products/GetCategories',
            method: 'get',
            async: false,
            success: function (response) {
                response = JSON.parse(response);

                $.each(response, function (i, v) {
                    options += '<option value="' + v.id + '">' + v.title + '</option>';
                });

            }
        });

        return options;
    }

    function get_shelfs() {

        var options = '<option value="">Seçiniz</option>';

        $.ajax({
            url: window.location.origin + '/App/Shelfs/GetShelfs',
            method: 'get',
            async: false,
            success: function (response) {
                response = JSON.parse(response);

                $.each(response, function (i, v) {
                    options += '<option value="' + v.id + '">' + v.name + '</option>';
                });

            }
        });

        return options;
    }


    function departments(lang_id) {
        lang_id = lang_id || 0;
        var options = '<option value="0"><?=ln('BASE.all')?></option>';
        $.ajax({
            url: window.location.origin + '/App/Departments/GetDepartments/' + lang_id,
            method: 'get',
            async: false,
            success: function (response) {
                response = JSON.parse(response);

                $.each(response, function (i, v) {
                    options += '<option value="' + v.ref_id + '">' + v.title + '</option>';
                });

            }
        });
        return options;
    }

    function pages_categories(lang_id) {
        lang_id = lang_id || 0;
        var options = '<option value="0"><?=ln('BASE.all')?></option>';
        $.ajax({
            url: window.location.origin + '/App/PagesCategories/GetCategories/' + lang_id,
            method: 'get',
            async: false,
            success: function (response) {
                response = JSON.parse(response);
                $.each(response, function (i, v) {
                    options += '<option value="' + v.ref_id + '">' + v.title + '</option>';
                });

            }
        });
        return options;
    }

    function translation_languages(lang_id) {
        lang_id = lang_id || 0;
        var options = '<option value="0"><?=ln('BASE.Select')?></option>';
        $.ajax({
            url: window.location.origin + '/App/TranslationLanguages/GetLanguages/' + lang_id,
            method: 'get',
            async: false,
            success: function (response) {
                response = JSON.parse(response);
                $.each(response, function (i, v) {
                    options += '<option value="' + v.ref_id + '">' + v.title + '</option>';
                });

            }
        });
        return options;
    }

    function service_types(lang_id) {
        lang_id = lang_id || 0;
        var options = '<option value="0"><?=ln('BASE.Select')?></option>';
        $.ajax({
            url: window.location.origin + '/App/ServiceTypes/GetTypes/' + lang_id,
            method: 'get',
            async: false,
            success: function (response) {
                response = JSON.parse(response);
                $.each(response, function (i, v) {
                    options += '<option value="' + v.ref_id + '">' + v.title + '</option>';
                });

            }
        });
        return options;
    }


    function job_titles(lang_id, value) {
        value = value || 0;
        lang_id = lang_id || 0;
        var options = '<option value="0"><?=ln('BASE.all')?></option>';
        $.ajax({
            url: window.location.origin + '/App/Titles/GetTitles/' + lang_id,
            method: 'get',
            async: false,
            success: function (response) {
                response = JSON.parse(response);
                if (value == 0) {
                    $.each(response, function (i, v) {
                        options += '<option value="' + v.ref_id + '">' + v.title + '</option>';
                    });
                } else {
                    $.each(response, function (i, v) {
                        options += '<option value="' + v.title + '">' + v.title + '</option>';
                    });
                }


            }
        });
        return options;
    }

    function get_pages(lang_id) {
        lang_id = lang_id || 0;
        var options = '<option value="0"><?=ln('BASE.all')?></option>';
        $.ajax({
            url: window.location.origin + '/App/Pages/GetPages/' + lang_id,
            method: 'get',
            async: false,
            success: function (response) {
                response = JSON.parse(response);
                $.each(response, function (i, v) {
                    options += '<option value="' + v.ref_id + '">' + v.title + '</option>';
                });
            }
        });
        return options;
    }

    function client_users() {
        var options = '<option value="0">No Selected</option>';
        $.ajax({
            url: window.location.origin + '/App/ClientUsers/GetClientUsers/',
            method: 'get',
            async: false,
            success: function (response) {
                response = JSON.parse(response);
                $.each(response, function (i, v) {
                    options += '<option value="' + v.id + '">' + v.name_surname + '</option>';
                });

            }
        });
        return options;
    }

    function show_auths(list) {

    }

    function languages() {
        var options = '<option value="0"><?=ln('BASE.select')?></option>';

        $.ajax({
            url: window.location.origin + '/App/Base/Languages',
            method: 'get',
            async: false,
            success: function (response) {
                response = JSON.parse(response);

                $.each(response, function (i, v) {
                    options += '<option value="' + v.id + '">' + v.title + '</option>';
                });

            }
        });

        return options;
    }

    function packages() {

        var options = '<option value="">Seçiniz</option>';

        $.ajax({
            url: window.location.origin + '/App/Packages/Get',
            method: 'get',
            async: false,
            success: function (response) {
                response = JSON.parse(response);

                $.each(response, function (i, v) {
                    options += '<option value="' + v.id + '">' + v.name + '</option>';
                });

            }
        });

        return options;
    }


    function count_lines(type = 1) {
        var arrayOfLines = $('#<?= isset($appName) ? $appName : 'nothing' ?>').find('textarea').val().split('\n');
        var lines = [];
        $.each(arrayOfLines, function (index, item) {
            if (item.length > 0) {
                lines.push(item);
            }
        });

        if (lines.length >= 50001) {
            Swal.fire(
                'Hata!',
                'Tek seferde en fazla 50.000 kayıt ekleyebilirsiniz.',
                'error'
            );
        }

        return type === 1 ? lines.length : lines;
    }

    document.addEventListener("DOMContentLoaded", function (event) {

        var KTBootstrapSelect = {
            init: function () {
                $(".kt-selectpicker").selectpicker()
            }
        };

        jQuery(document).ready(function () {
            KTBootstrapSelect.init()
        });


    });


    (function ($) {
        $.fn.donetyping = function (callback) {
            var _this = $(this);
            var x_timer;
            _this.keyup(function () {
                clearTimeout(x_timer);
                x_timer = setTimeout(clear_timer, 1000);
            });

            function clear_timer() {
                clearTimeout(x_timer);
                callback.call(_this);
            }
        }
    })(jQuery);


    $(".datepicker").datepicker({
        format: "dd/mm/yyyy",
        todayHighlight: !0,
        orientation: "bottom left"
    });

    $(".datetimepicker").datetimepicker({
        format: "dd/mm/yyyy hh:ii",
        todayHighlight: !0,
        autoclose: !0,
        orientation: "bottom left"
    });

    $('.kt-menu__subnav .kt-menu__item--active').parent().parent().parent().addClass('kt-menu__item--open');

</script>
<!--end::Page Scripts -->
</body>
<!-- end::Body -->

</html>
