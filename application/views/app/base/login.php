<!DOCTYPE html>
<html lang="tr">
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<head>
    <meta charset="utf-8"/>

    <title>Yönetim Paneli</title>
    <meta name="description" content="Login page example">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--begin::Fonts -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
    <!--end::Fonts -->


    <!--begin::Page Custom Styles(used by this page) -->
    <link href="<?= base_url('assets/panel') ?>/css/pages/login/login-6.css" rel="stylesheet" type="text/css"/>
    <!--end::Page Custom Styles -->

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="<?= base_url('assets/panel') ?>/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('assets/panel') ?>/css/style.bundle.css" rel="stylesheet" type="text/css"/>
    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->

    <link href="<?= base_url('assets/panel') ?>/css/skins/header/base/light.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('assets/panel') ?>/css/skins/header/menu/light.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('assets/panel') ?>/css/skins/brand/dark.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('assets/panel') ?>/css/skins/aside/dark.css" rel="stylesheet" type="text/css"/>
    <!--end::Layout Skins -->

    <style>
       .kt-login__block .kt-login__title{
            text-shadow: 0px 1px 10px black;
        }

        .btn-brand{
            background-color: #e2d1ca;
            border-color: #d8c3be;
            color: #290a16;
            box-shadow: 0px 0px 20px #AC938D !important;
            border-radius: 0px!important;
            font-weight: bold;
        }

        .form-control{
            padding: 10px!important;
        }
    </style>
</head>
<!-- end::Head -->

<!-- begin::Body -->
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

<!-- begin:: Page -->
<div class="kt-grid kt-grid--ver kt-grid--root">
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v6 kt-login--signin" id="kt_login"
         style="background-image: url('<?= base_url('assets/panel') ?>/media/bg/bg-2.jpg'); background-size:cover">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
            <div class="kt-grid__item  kt-grid__item--order-tablet-and-mobile-2  kt-grid kt-grid--hor kt-login__aside"
                 style="background: rgba(255,255,255,0.3);">
                <div class="kt-login__wrapper">
                    <div class="kt-login__container">
                        <div class="kt-login__body">
                            <div class="kt-login__logo">
                                <a href="#">

                                </a>
                            </div>

                            <div class="kt-login__signin">
                                <div class="kt-login__head">
                                    <h3 class="kt-login__title" style="color:black!important;"><?=ln('FRONT.Panel')?></h3>
                                </div>
                                <div class="kt-login__form">
                                    <form class="kt-form" id="login-form" action="<?= base_url('App/Auth/Login') ?>"
                                          method="POST">
                                        <div class="form-group">
                                            <input class="form-control" type="text" placeholder="Kullanıcı Adı"
                                                   name="username" autocomplete="off">
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control form-control-last" type="password"
                                                   placeholder="Şifre" name="password">
                                        </div>
                                        <div class="kt-login__extra" style="display: none">
                                            <label class="kt-checkbox">
                                                <input type="checkbox" name="remember"> Beni hatırla
                                                <span></span>
                                            </label>
                                            <a style="display: none" href="javascript:;" id="kt_login_forgot">Forget
                                                Password ?</a>
                                        </div>
                                        <div class="kt-login__actions">
                                            <button type="submit"
                                                    class="btn btn-brand btn-pill btn-elevate">Giriş
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="kt-login__signup" style="display: none">
                                <div class="kt-login__head">
                                    <h3 class="kt-login__title">Sign Up</h3>
                                    <div class="kt-login__desc">Enter your details to create your account:</div>
                                </div>
                                <div class="kt-login__form">
                                    <form class="kt-form" action="#">
                                        <div class="form-group">
                                            <input class="form-control" type="text" placeholder="Fullname"
                                                   name="fullname">
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" type="text" placeholder="Email" name="email"
                                                   autocomplete="off">
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control" type="password" placeholder="Password"
                                                   name="password">
                                        </div>
                                        <div class="form-group">
                                            <input class="form-control form-control-last" type="password"
                                                   placeholder="Confirm Password" name="rpassword">
                                        </div>
                                        <div class="kt-login__extra">
                                            <label class="kt-checkbox">
                                                <input type="checkbox" name="agree"> I Agree the <a href="#">terms and
                                                    conditions</a>.
                                                <span></span>
                                            </label>
                                        </div>
                                        <div class="kt-login__actions">
                                            <button id="kt_login_signup_submit"
                                                    class="btn btn-brand btn-pill btn-elevate">Sign Up
                                            </button>
                                            <button id="kt_login_signup_cancel" class="btn btn-outline-brand btn-pill">
                                                Cancel
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="kt-login__forgot" style="display: none">
                                <div class="kt-login__head">
                                    <h3 class="kt-login__title">Forgotten Password ?</h3>
                                    <div class="kt-login__desc">Enter your email to reset your password:</div>
                                </div>
                                <div class="kt-login__form">
                                    <form class="kt-form" action="#">
                                        <div class="form-group">
                                            <input class="form-control" type="text" placeholder="Email" name="email"
                                                   id="kt_email" autocomplete="off">
                                        </div>
                                        <div class="kt-login__actions">
                                            <button id="kt_login_forgot_submit"
                                                    class="btn btn-brand btn-pill btn-elevate">Request
                                            </button>
                                            <button id="kt_login_forgot_cancel" class="btn btn-outline-brand btn-pill">
                                                Cancel
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-login__account" style="display: none">
                    <span class="kt-login__account-msg">
                        Don't have an account yet ?
                    </span>&nbsp;&nbsp;
                        <a href="javascript:;" id="kt_login_signup" class="kt-login__account-link">Sign Up!</a>
                    </div>
                </div>
            </div>

            <div class="kt-grid__item kt-grid__item--fluid kt-grid__item--center kt-grid kt-grid--ver kt-login__content">
                <div class="kt-login__section">
                    <div class="kt-login__block">
                        <h3 class="kt-login__title">EMPLOYEE.CO</h3>
                        <div class="kt-login__desc">
                            v 1.0
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- end:: Page -->


<!-- begin::Global Config(global config for global JS sciprts) -->
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": [
                    "#c5cbe3",
                    "#a1a8c3",
                    "#3d4465",
                    "#3e4466"
                ],
                "shape": [
                    "#f0f3ff",
                    "#d9dffa",
                    "#afb4d4",
                    "#646c9a"
                ]
            }
        }
    };
</script>
<!-- end::Global Config -->

<!--begin::Global Theme Bundle(used by all pages) -->
<script src="<?= base_url('assets/panel') ?>/plugins/global/plugins.bundle.js" type="text/javascript"></script>
<script src="<?= base_url('assets/panel') ?>/js/scripts.bundle.js" type="text/javascript"></script>
<!--end::Global Theme Bundle -->


<!--begin::Page Scripts(used by this page) -->
<script src="<?= base_url('assets/panel') ?>/js/pages/custom/login/login-general.js" type="text/javascript"></script>

<script>

    document.addEventListener("DOMContentLoaded", function (event) {

        $('form[id="<?=$appName?>"]').submit(function (e) {

            e.preventDefault();

            swal.fire({
                title: "<?=ln('MESSAGE.Processing')?>",
                text: "<?=ln('MESSAGE.Wait')?>",
                timer: 99999,
                onOpen: function () {
                    swal.showLoading()
                }
            }).then(function (e) {
                "timer" === e.dismiss && console.log("I was closed by the timer")
            });


            $.post($('form[id="<?=$appName?>"]').attr('action'), $('form[id="<?=$appName?>"]').serializeArray(), function (response) {

                var response = JSON.parse(response);

                console.log(response);

                if (response.result == 1) {
                    swal.fire({
                        type: 'success',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 1500
                    });

                    window.location.href = window.location.origin + '/App/Auth/Referrer';
                } else {
                    swal.fire({
                        type: 'error',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 6000
                    });
                }


            });


        });


    });


</script>
<!--end::Page Scripts -->
</body>
<!-- end::Body -->

</html>
