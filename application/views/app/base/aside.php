<!-- begin:: Page -->
<!-- begin:: Header Mobile -->
<?php $userSessData = $this->session->userdata('app_user_data'); ?>
<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
    <div class="kt-header-mobile__logo">
        <a href="<?= base_url('App') ?>">
            Menü
        </a>
    </div>
    <div class="kt-header-mobile__toolbar">
        <button class="kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler">
            <span></span>
        </button>

        <button class="kt-header-mobile__toggler" id="kt_header_mobile_toggler"><span></span>
        </button>

        <button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler"><i
                    class="flaticon-more"></i>
        </button>
    </div>
</div>
<!-- end:: Header Mobile -->
<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
        <!-- begin:: Aside -->

        <!-- Uncomment this to display the close button of the panel
<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
        -->

        <div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop"
             id="kt_aside">
            <!-- begin:: Aside -->
            <div class="kt-aside__brand kt-grid__item " id="kt_aside_brand">
                <div class="kt-aside__brand-logo">
                    <a href="<?= base_url('App') ?>" class="text-white">
                        <h5>cevir.io | PANEL</h5>
                    </a>
                </div>

                <div class="kt-aside__brand-tools">
                    <button class="kt-aside__brand-aside-toggler" id="kt_aside_toggler">
                        <span><svg xmlns="http://www.w3.org/2000/svg"
                                   width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon points="0 0 24 0 24 24 0 24"/>
                                    <path d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z"
                                          fill="#000000" fill-rule="nonzero"
                                          transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999) "/>
                                    <path d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z"
                                          fill="#000000" fill-rule="nonzero" opacity="0.3"
                                          transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999) "/>
                                </g>
                            </svg></span>
                        <span><svg xmlns="http://www.w3.org/2000/svg"
                                   width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <polygon points="0 0 24 0 24 24 0 24"/>
                                    <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z"
                                          fill="#000000" fill-rule="nonzero"/>
                                    <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z"
                                          fill="#000000" fill-rule="nonzero" opacity="0.3"
                                          transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
                                </g>
                            </svg></span>
                    </button>
                    <!--
        <button class="kt-aside__brand-aside-toggler kt-aside__brand-aside-toggler--left" id="kt_aside_toggler"><span></span></button>
                    -->
                </div>
            </div>
            <!-- end:: Aside -->
            <!-- begin:: Aside Menu -->
            <div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">

                <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1"
                     data-ktmenu-dropdown-timeout="500">
                    <ul class="kt-menu__nav">

                        <li class="kt-menu__item" aria-haspopup="true">
                            <a href="<?= base_url('App/Dashboard') ?>" class="kt-menu__link "> <span
                                        class="kt-menu__link-text">
                                    <span class="kt-menu__link-icon"><i class="fas fa-home"></i></span>
                                    Pano</span></a>
                        </li>


                        <?php
                        foreach ($navMenu as $menu):
                            if ($menu['parent'] == 0):
                                $childs = array();
                                foreach ($navMenu as $child):

                                    if (($menu['id'] == $child['parent']) && app_auth_check($child['name'])):
                                        $childs[] = $child;
                                    endif;
                                endforeach;

                                if (count($childs) == 0 && 1 == 2):
                                    ?>
                                    <li class="kt-menu__item" aria-haspopup="true">
                                        <a href="<?= base_url('App/' . $menu['url']) ?>" class="kt-menu__link "> <span
                                                    class="kt-menu__link-text">
                                                <span class="kt-menu__link-icon"><i
                                                            class="<?= $menu['icon'] ?>"></i></span>
                                                <?= $menu['title'] ?></span></a>
                                    </li>
                                <?php endif; ?>

                                <?php if (count($childs) > 0): ?>

                                <li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true"
                                    data-ktmenu-submenu-toggle="hover">
                                    <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                        <span class="kt-menu__link-icon"><i class="<?= $menu['icon'] ?>"></i></span>
                                        <span class="kt-menu__link-text"><?= ln('APPLICATIONS.' . $menu['title']) ?></span>
                                        <i class="kt-menu__ver-arrow la la-angle-right"></i>
                                    </a>


                                    <div class="kt-menu__submenu "><span class="kt-menu__arrow"></span>
                                        <ul class="kt-menu__subnav">


                                            <?php foreach ($childs as $chd): ?>

                                                <li class="kt-menu__item <?= current_url() == base_url('App/' . $chd['url']) ? 'kt-menu__item--active' : '' ?>"
                                                    aria-haspopup="true">
                                                    <a href="<?= base_url('App/' . $chd['url']) ?>"
                                                       class="kt-menu__link ">
                                                        <i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i>
                                                        <span class="kt-menu__link-text"><?= ln('APPLICATIONS.' . $chd['title']) ?></span></a>
                                                </li>


                                            <?php endforeach;
                                            ?>


                                        </ul>
                                    </div>
                                </li>


                            <?php endif; ?>


                            <?php
                            endif;
                        endforeach;
                        ?>


                        <li class="kt-menu__section ">
                            <h4 class="kt-menu__section-text"><?= ln('APPLICATIONS.Settings') ?></h4>
                            <i class="kt-menu__section-icon flaticon-more-v2"></i>
                        </li>

                        <li class="kt-menu__item" aria-haspopup="true">
                            <a href="<?= base_url('App/ClientUsers/Edit/' . $this->session->userdata('app_user_data')['id']) ?>"
                               class="kt-menu__link "> <span
                                        class="kt-menu__link-text">
                                    <span class="kt-menu__link-icon"><i class="fas fa-user-edit"></i></span>
                                    <?= ln('BASE.profile') ?></span></a>
                        </li>

                        <li class="kt-menu__item" aria-haspopup="true">
                            <a href="<?= base_url('App/Auth/Logout') ?>" class="kt-menu__link "> <span
                                        class="kt-menu__link-text">
                                    <span class="kt-menu__link-icon"><i class="fas fa-sign-out-alt"></i></span>
                                    <?= ln('AUTH.Log out') ?></span></a>
                        </li>
                    </ul>

                </div>
            </div>
            <!-- end:: Aside Menu -->
        </div>
        <!-- end:: Aside -->

        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
            <!-- begin:: Header -->
            <div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">
                <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
                    <div id="kt_header_menu"
                         class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">
                        <ul class="kt-menu__nav ">
                            <li class="kt-menu__item ">
                                <a href="<?= base_url() ?>" class="kt-menu__link ">
                                    <span class="kt-menu__link-text">Portal</span></a>
                            </li>

                            <?php foreach ($languages as $lng):
                                if ($lng['id'] != $this->session->userdata('app_lang_id')):
                                    ?>
                                    <li class="kt-menu__item">
                                        <a href="<?= base_url('Language/Change/' . $lng['id'] . '/App') ?>"
                                           class="kt-menu__link">
                                            <span class="kt-menu__link-text text-info"> <?= $lng['title'] ?></span>
                                        </a>
                                    </li>
                                <?php endif; endforeach; ?>
                        </ul>
                    </div>

                </div>
                <!-- end:: Header Menu -->
                <!-- begin:: Header Topbar -->
                <div class="kt-header__topbar">
                    <div class="kt-header__topbar-item kt-header__topbar-item--user">
                        <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
                            <div class="kt-header__topbar-user">
                                <span class="kt-header__topbar-welcome kt-hidden-mobile"><?= ln('BASE.hello') ?>,</span>
                                <span class="kt-header__topbar-username kt-hidden-mobile"><?= $userSessData['name_surname'] ?></span>
                                <img class="kt-hidden" alt="Pic"
                                     src="<?= base_url('assets/panel') ?>/media/users/300_25.jpg"/>
                                <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                                <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold"><?= strtoupper(substr($userSessData['username'], 0, 1)) ?></span>
                            </div>
                        </div>

                        <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">
                            <!--begin: Head -->
                            <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x"
                                 style="background-image: url(<?= base_url('assets/panel') ?>/media/misc/bg-1.jpg)">
                                <div class="kt-user-card__avatar">
                                    <img class="kt-hidden" alt="Pic"
                                         src="<?= base_url('assets/panel') ?>/media/users/300_25.jpg"/>
                                    <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                                    <span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success"><?= strtoupper(substr($userSessData['username'], 0, 1)) ?></span>
                                </div>
                                <div class="kt-user-card__name">
                                    <?= $userSessData['username'] ?>
                                </div>
                            </div>
                            <!--end: Head -->
                            <!--begin: Navigation -->
                            <div class="kt-notification">
                                <a href="<?= base_url('App/Users/Edit') ?>"
                                   class="kt-notification__item">
                                    <div class="kt-notification__item-icon">
                                        <i class="flaticon2-calendar-3 kt-font-success"></i>
                                    </div>
                                    <div class="kt-notification__item-details">
                                        <div class="kt-notification__item-title kt-font-bold">
                                            <?= ln('BASE.profile') ?>
                                        </div>
                                    </div>
                                </a>

                                <div class="kt-notification__custom">
                                    <a href="<?= base_url('Site/Auth/Logout') ?>"
                                       class="btn btn-label btn-label-brand btn-sm btn-bold"><?= ln('AUTH.Log out') ?></a>


                                </div>
                            </div>
                            <!--end: Navigation -->
                        </div>
                    </div>
                    <!--end: User Bar -->
                </div>
                <!-- end:: Header Topbar -->
            </div>
            <!-- end:: Header -->
            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">


                <!-- begin:: Content -->
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

                    <div class="row">

