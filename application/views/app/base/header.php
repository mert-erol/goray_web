<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<head>
    <meta charset="utf-8"/>

    <title><?= $mainSettings['firm'] ?> | <?= ln('FRONT.Panel') ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--begin::Fonts -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
    <!--end::Fonts -->

    <!--begin::Page Vendors Styles(used by this page) -->
    <link href="<?= base_url('assets/panel') ?>/plugins/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet"
          type="text/css"/>
    <!--end::Page Vendors Styles -->


    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="<?= base_url('assets/panel') ?>/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('assets/panel') ?>/css/style.bundle.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('assets/panel') ?>/css/custom.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('assets/panel') ?>/css/pages/inbox/inbox.css" rel="stylesheet" type="text/css"/>
    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->

    <link href="<?= base_url('assets/panel') ?>/css/skins/header/base/light.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('assets/panel') ?>/css/skins/header/menu/light.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('assets/panel') ?>/css/skins/brand/dark.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('assets/panel') ?>/css/skins/aside/dark.css" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url('assets/panel') ?>/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?= base_url('assets/panel') ?>/plugins/custom/uppy/uppy.bundle.css" rel="stylesheet" type="text/css"/>
    <!--end::Layout Skins -->
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/jquery-contextmenu/2.7.1/jquery.contextMenu.min.css">
    <link href="<?= base_url('assets/panel/'); ?>plugins/custom/bootstrap-fileinput/5.0.9/css/fileinput.min.css"
          media="all"
          rel="stylesheet" type="text/css"/>


</head>
<!-- end::Head -->

<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-aside--enabled kt-aside--fixed">