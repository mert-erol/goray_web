<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Bütçe Raporu
                </h3>
            </div>
        
        </div>

        
<b>D- SATIŞLARIN MALİYETİ(-) 
:</b>
5000.000 tL 
        <div class="kt-portlet__body">

            <table class="table table-striped table-bordered table-hover table-checkable" id="department-table">
                <thead>
                <tr>
                    <?php
                    $columns = '[';
                    foreach ($cols as $col):
                        $columns .= '{ data:"' . $col . '"},';
                        ?>
                        <th><?= $col ?></th>
                    <?php
                    endforeach;
                    $columns = $columns . ']';
                    ?>

                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


<script>

    document.addEventListener("DOMContentLoaded", function (event) {
        _base.get_table($("#department-table"), {
            responsive: !0,
            searchDelay: 500,
            processing: !0,
            serverSide: !0,
            ajax: {url: window.location.origin + '/App/Report/Datatables', method: 'post'},
            columns: <?= $columns ?>
        });
    });

</script>