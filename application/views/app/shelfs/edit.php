<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <?= ln('APPLICATIONS.Edit Shelfs') ?>
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <?php if ($modal): ?>
                            <button type="button"
                                    onclick="modal_trigger('<?= base_url('App/Shelfs/Detail/') ?>/<?= $data['parent'] ?>>')"
                                    class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Detay"><i
                                        class="la la-arrow-left"></i></button>
                        <?php else: ?>
                            <a href="<?= base_url('App/Shelfs') ?>"
                               class="btn btn-dark btn-elevate btn-icon-sm btn-sm">
                                <i class="la la-arrow-left"></i>
                                <?= ln('APPLICATIONS.Shelfs List') ?>
                            </a>
                        <?php endif ?>

                    </div>
                </div>
            </div>
        </div>
        <form id="<?= $appName ?>" action="<?= base_url('App/Shelfs/Edit/' . $id) ?>" method="post">
            <div class="kt-portlet__body">

                <div class="form-group row">

                    <?php
                    $textEditorData = '';

                    foreach ($cols as $col):
                        if (in_array($col['type'], ['datetime'])):


                        elseif ($col['name'] == 'password'):

                            ?>
                            <div class="col-lg-3 margin-bottom-15">
                                <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                                <input type="password" name="<?= $col['name'] ?>"
                                       value="<?= md5($data[$col['name']]) ?>"
                                       class="form-control">
                            </div>

                        <?php elseif ($col['type'] == 'text'):
                            $textEditorData = $data[$col['name']];
                            ?>
                            <div class="col-lg-12 margin-bottom-15">
                                <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                                <div name="<?= $col['name'] ?>" class="summernote"></div>
                            </div>


                        <?php elseif ($col['name'] == 'type' && $data['type'] == 2):  ?>

                        <?php elseif ($col['name'] == 'type' && $data['type'] == 1): ?>


                        <?php elseif ($col['name'] == 'parent' && $data['type'] == 1): ?>

                        <?php elseif ($col['name'] == 'parent' && $data['type'] == 2): ?>
                            <div class="col-lg-2 margin-bottom-15">
                                <label><?= ln('shelfs.parent') ?></label>
                                <select class="form-control" name="<?= $col['name'] ?>">

                                </select>
                            </div>




                        <?php elseif (in_array($col['type'], array('int', 'float'))): ?>
                            <div class="col-lg-2 margin-bottom-15">
                                <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                                <input type="text" name="<?= $col['name'] ?>" value="<?= $data[$col['name']] ?>"
                                       class="form-control">
                            </div>


                        <?php else: ?>

                            <div class="col-lg-4 margin-bottom-15">
                                <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                                <input type="text" name="<?= $col['name'] ?>" value="<?= $data[$col['name']] ?>"
                                       class="form-control">
                            </div>

                        <?php endif; endforeach; ?>
                </div>
                <div class="kt-form__actions ">


                    <button type="submit" class="btn btn-success btn-elevate btn-icon-sm float-right btn-sm"><i
                                class="la la-save"></i><?= ln('ACTION.update') ?>
                    </button>


                </div>
            </div>
        </form>


    </div>
</div>

<script>

    <?php if (!$modal): ?>
    document.addEventListener("DOMContentLoaded", function (event) {
        <?php endif; ?>

        $('select[name="parent"]').html(get_shelfs());
        $('select[name="parent"]').select2();

        <?php if(intval($data['parent']) > 0): ?>
        $('select[name="parent"]').val(<?=$data['parent']?>);
        <?php endif; ?>

        $('form[id="<?=$appName?>"]').submit(function (e) {


            e.preventDefault();

            swal.fire({
                title: "<?=ln('MESSAGE.Processing')?>",
                text: "<?=ln('MESSAGE.Wait')?>",
                timer: 99999,
                onOpen: function () {
                    swal.showLoading()
                }
            }).then(function (e) {
                "timer" === e.dismiss && console.log("I was closed by the timer")
            });


            $.post($('form[id="<?=$appName?>"]').attr('action'), $('form[id="<?=$appName?>"]').serializeArray(), function (response) {

                var response = JSON.parse(response);

                console.log(response);

                if (response.result == 1) {
                    swal.fire({
                        type: 'success',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                } else {
                    swal.fire({
                        type: 'error',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 6000
                    });
                }
            });


        });


        <?php if (!$modal): ?>
    });
    <?php endif; ?>


</script>