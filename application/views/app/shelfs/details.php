<style>
    #sortable {
        list-style-type: none;
        margin: 0;
        padding: 0;
        width: 100%;
    }

    #sortable li {
        border-bottom: 4px solid #e9e9e9;
        padding: 15px;
        cursor: all-scroll;
        font-weight: bold;

        -webkit-box-shadow: 0px 14px 13px -5px rgba(233, 233, 233, 1);
        -moz-box-shadow: 0px 14px 13px -5px rgba(233, 233, 233, 1);
        box-shadow: 0px 14px 13px -5px rgba(233, 233, 233, 1);
    }


    #sortable li i {
        color: rgba(0, 0, 0, 0.5);
        margin-right: 15px;
    }

    #sortable li:hover {
        color: white;
        background: rgba(0, 0, 0, 0.3);
        -webkit-box-shadow: 0px 24px 13px -5px rgba(233, 233, 233, 1);
        -moz-box-shadow: 0px 24px 13px -5px rgba(233, 233, 233, 1);
        box-shadow: 0px 24px 13px -5px rgba(233, 233, 233, 1);
    }

    .shelf-area {
        border: 1px solid rgba(0, 0, 0, 0.1);
        padding: 5px;
    }

    .shelf-area h4 {
        border-bottom: 1px solid rgba(224, 224, 224, 0.4);
        padding: 5px;
        font-size: 16px;
        background: rgba(224, 224, 224, 0.4);
    }

    #sortable li button{
        float: right;
        bottom: 10px;
    }
</style>

<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <?= $parent_data['name'] ?>
                    <small>Raf Detayı</small>
                </h3>
            </div>

            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <?php if ($modal): ?>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">x
                            </button>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-md-4 shelf-area">
                    <h4 class="text-dark text-center">Raf Sıralaması</h4>
                    <ul id="sortable">
                        <?php foreach ($data as $row): ?>
                            <li class="ui-state-default" data-id="<?= $row['id'] ?>"><i
                                        class="fas fa-arrows-alt"></i><?= $row['name'] ?>
                                <button type="button" onclick="modal_trigger('<?=base_url('App/Shelfs/Edit')?>/<?= $row['id'] ?>>')" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Detay"><i class="far fa-edit"></i></button>
                            </li>

                        <?php endforeach; ?>
                    </ul>

                    <button id="sort-save" class="btn btn-dark btn-block d-none">Sıralamayı Kaydet</button>


                </div>
            </div>
        </div>
    </div>
</div>

<script>
    <?php if (!$modal): ?>
    document.addEventListener("DOMContentLoaded", function (event) {
        <?php endif; ?>

        $(function () {
            $("#sortable").sortable();
            $("#sortable").disableSelection();
        });


        $("#sortable").sortable({
            change: function( event, ui ) {
                $('#sort-save').removeClass('d-none');
            }
        });

        $(document).on('click', '#sort-save',function () {
            var sorted_list = get_sorting();
            save_sortings(sorted_list);
        });

        function get_sorting() {
            var sorted = [];
            $.each($('#sortable li'), function (k, v) {
                var id = $(v).attr('data-id');

                if (id != undefined) {
                    sorted.push(id);
                }

            });

            return sorted;
        }

        function save_sortings(sorted_list){
            $.post(window.location.origin + '/App/Shelfs/Detail' , {
                'sorted_list': JSON.stringify(sorted_list)
            }, function (result) {
                if(result.status == 1){
                    $('#sort-save').addClass('d-none');
                    swal.fire({
                        type: 'success',
                        title:'Sıralama kaydedildi',
                        showConfirmButton: false,
                        timer: 1000
                    });
                }
            });
        }

        <?php if (!$modal): ?>
    });
    <?php endif; ?>
</script>