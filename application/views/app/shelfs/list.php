<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <?= ln('APPLICATIONS.Shelfs List') ?>
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="<?= base_url('App/Shelfs/Set') ?>"
                           class="btn btn-dark btn-elevate btn-icon-sm btn-sm">
                            <i class="la la-plus"></i>
                            <?= ln('BASE.add') ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <table class="table table-striped table-bordered table-hover table-checkable" id="shelfs-table">
                <thead>
                <tr>
                    <?php
                    $columns = '[';
                    foreach ($cols as $col):
                        $columns .= '{ data:"' . $col['name'] . '"},';
                        ?>
                        <th><?= ln($mainTbl . '.' . $col['name']) ?></th>
                    <?php
                    endforeach;
                    $columns = $columns . ']';
                    ?>

                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


<script>

    var table;
    <?php if (!$modal): ?>
    document.addEventListener("DOMContentLoaded", function (event) {
        <?php endif; ?>

        table = _base.get_table($("#shelfs-table"), {
            responsive: !0,
            searchDelay: 500,
            processing: !0,
            serverSide: !0,
            ajax: {url: window.location.origin + '/App/Shelfs/Datatables/<?= $warehouse_id ?>' , method: 'post'},
            columns: <?= $columns ?>,
            columnDefs: [
                <?php if(app_auth_check('edit-shelfs')): ?>
                {
                    targets: 0,
                    title: "#",
                    orderable: !1,
                    render: function (d) {
                        return '<a href="' + window.location.origin + '/App/Shelfs/Edit/' + d + '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Düzenle">\n<i class="la la-edit"></i>\n</a>';
                    }
                },
                <?php endif; ?>

                {
                    targets: 2,
                    title:"İşlem",
                    render: function (data,type,row) {
                        return `<button type="button" onclick="modal_trigger('<?=base_url('App/Shelfs/Detail')?>/${row.id}')" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Detay"><i class="far fa-eye"></i></button>`;
                    }
                }
            ]
        });
        <?php if (!$modal): ?>
    });
    <?php endif; ?>

</script>