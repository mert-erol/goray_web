<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <span class="badge badge-dark"><?= ln('requests.type_' . $data['type']) ?></span> <?= ln('BASE.edit') ?>
                </h3>
            </div>
        </div>
        <form id="<?= $appName ?>" action="<?= base_url('App/Requests/Edit/' . $id) ?>" method="post">
            <div class="kt-portlet__body">

                <div class="form-group row">

                    <?php
                    $textEditorData = '';
                    foreach ($cols as $col):
                        ?>


                        <?php if ($col['name'] == 'employee_id'): ?>
                        <div class="col-lg-6 margin-bottom-15">
                            <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                            <br>
                            <b><?= $userData['name_surname'] ?></b>
                            <br>
                            <?= $userData['title'] ?>
                        </div>

                    <?php elseif (in_array($col['type'], array('varchar'))): ?>
                        <div class="col-lg-8 margin-bottom-15">
                            <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                            <input type="text" name="<?= $col['name'] ?>" value="<?= $data[$col['name']] ?>"
                                   class="form-control">
                        </div>

                    <?php elseif ($col['name'] == 'content'): ?>
                        <div class="col-lg-6 margin-bottom-15">
                            <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                            <textarea name="<?= $col['name'] ?>" class="form-control"
                                      disabled><?= $data[$col['name']] ?></textarea>
                        </div>

                    <?php elseif ($col['name'] == 'start_date'): ?>
                        <div class="col-lg-6 margin-bottom-15">
                            <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                            <input type="text" name="<?= $col['name'] ?>" class="form-control datetimepicker"
                                   value="<?= date('d/m/Y H:i', strtotime($data[$col['name']])) ?>"
                                   autocomplete="off">
                        </div>

                    <?php elseif ($col['name'] == 'end_date'): ?>
                        <div class="col-lg-6 margin-bottom-15">
                            <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                            <input type="text" name="<?= $col['name'] ?>" class="form-control datetimepicker"
                                   value="<?= date('d/m/Y H:i', strtotime($data[$col['name']])) ?>"
                                   autocomplete="off">
                        </div>

                    <?php elseif ($col['type'] == 'datetime'): ?>

                    <?php elseif ($col['type'] == 'text'):
                        $textEditorData = $data[$col['name']];
                        ?>
                        <div class="col-lg-12 margin-bottom-15">
                            <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                            <div name="<?= $col['name'] ?>" class="summernote"></div>
                        </div>

                    <?php elseif ($col['name'] == 'status'): ?>
                        <div class="col-lg-2 margin-bottom-15">
                            <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                            <select class="form-control" name="<?= $col['name'] ?>">
                                <option value="0" <?= $data[$col['name']] == 0 ? 'selected' : '' ?>><?= ln('requests.status_0') ?></option>
                                <option value="1" <?= $data[$col['name']] == 1 ? 'selected' : '' ?>><?= ln('requests.status_1') ?></option>
                                <option value="2" <?= $data[$col['name']] == 2 ? 'selected' : '' ?>><?= ln('requests.status_2') ?></option>
                            </select>

                        </div>

                    <?php elseif (in_array($col['type'], array('int', 'float'))): ?>
                        <div class="col-lg-2 margin-bottom-15">
                            <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                            <input type="text" name="<?= $col['name'] ?>" value="<?= $data[$col['name']] ?>"
                                   class="form-control">
                        </div>


                    <?php else: ?>

                        <div class="col-lg-12 margin-bottom-15">
                            <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                            <input type="text" name="<?= $col['name'] ?>" value="<?= $data[$col['name']] ?>"
                                   class="form-control">
                        </div>

                    <?php endif; endforeach; ?>
                </div>
                <div class="kt-form__actions ">


                    <button type="submit" class="btn btn-success btn-elevate btn-icon-sm float-right btn-sm"><i
                                class="la la-save"></i><?= ln('ACTION.update') ?>
                    </button>


                </div>
            </div>
        </form>


    </div>
</div>

<script>

    document.addEventListener("DOMContentLoaded", function (event) {

        $('form[id="<?=$appName?>"]').submit(function (e) {

            e.preventDefault();

            swal.fire({
                title: "<?=ln('MESSAGE.Processing')?>",
                text: "<?=ln('MESSAGE.Wait')?>",
                timer: 99999,
                onOpen: function () {
                    swal.showLoading()
                }
            }).then(function (e) {
                "timer" === e.dismiss && console.log("I was closed by the timer")
            });

            var serializedData = $('form[id="<?=$appName?>"]').serializeArray();


            $.post($('form[id="<?=$appName?>"]').attr('action'), serializedData, function (response) {

                var response = JSON.parse(response);

                console.log(response);

                if (response.result == 1) {
                    swal.fire({
                        type: 'success',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                } else {
                    swal.fire({
                        type: 'error',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 6000
                    });
                }
            });


        });


    });


</script>