<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <?= ln('requests.type_' . $type) ?>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">

            <table class="table table-striped table-bordered table-hover table-checkable" id="department-table">
                <thead>
                <tr>
                    <?php
                    $columns = '[';
                    foreach ($cols as $col):
                        if ($col['name'] != 'type'):
                            $columns .= '{ data:"' . $col['name'] . '"},'; ?>
                            <th><?= ln($mainTbl . '.' . $col['name']) ?></th>
                        <?php
                        endif;
                    endforeach;
                    $columns = $columns . ']';
                    ?>

                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


<script>

    document.addEventListener("DOMContentLoaded", function (event) {
        _base.get_table($("#department-table"), {
            responsive: !0,
            searchDelay: 500,
            processing: !0,
            serverSide: !0,
            ajax: {url: window.location.origin + '/App/Requests/Datatables/<?=$type?>', method: 'post'},
            columns: <?= $columns ?>,
            columnDefs: [
                <?php if(app_auth_check('edit-request')): ?>
                {
                    targets: 0,
                    title: "#",
                    orderable: !1,
                    render: function (d) {
                        return '<a href="' + window.location.origin + '/App/Requests/Edit/' + d + '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Düzenle">\n<i class="la la-edit"></i>\n</a>';
                    }
                },
                <?php endif; ?>
                {
                    targets: -1,
                    orderable: !1,
                    render: function (d) {
                        if (d == 0) {
                            return '<span class="badge badge-warning"><?=ln('requests.status_0')?></span>';
                        } else if (d == 1) {
                            return '<span class="badge badge-success"><?=ln('requests.status_1')?></span>';
                        } else {
                            return '<span class="badge badge-danger"><?=ln('requests.status_2')?></span>';
                        }
                    }
                }
            ]
        });
    });

</script>