<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Stok Sayımı

                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col md-6">
                    <input type="text" id="barcode" class="form-control" placeholder="Barkod"><br>
                </div>
                <div class="col-md-6">
                    <button type="button" id="count" class="btn btn-primary btn-block"><B>SAY</B></button>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
    document.addEventListener("DOMContentLoaded", function (event) {

        $(document).on('click', '#count', function () {
            var barcode = $('#barcode').val();
            $.post(window.location.origin+'/Api/Stock/Count/'+barcode, function () {
                $('#barcode').val('');
                table.ajax.reload();
            });
        });

    });
</script>