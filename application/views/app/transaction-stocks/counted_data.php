<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Sayımı Yapılanlar
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <table class="table table-striped table-bordered table-hover table-checkable" id="pages-categories-table">
                <thead>
                <tr>
                    <?php
                    $columns = '[';
                    $cols = [
                            ['name'=>'id'],
                            ['name'=>'barcode'],
                            ['name'=>'counted'],
                            ['name'=>'created_at'],
                    ];

                    foreach ($cols as $col):
                        $columns .= '{ data:"' . $col['name'] . '"},';
                        ?>
                        <th><?= ln('transaction_count.' . $col['name']) ?></th>
                    <?php
                    endforeach;
                    $columns = $columns . ']';
                    ?>

                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<script>
    var table;
    document.addEventListener("DOMContentLoaded", function (event) {
        table = _base.get_table($("#pages-categories-table"), {
            responsive: !0,
            searchDelay: 500,
            processing: !0,
            serverSide: !0,
            ajax: {url: window.location.origin + '/App/TransactionCount/Datatables', method: 'post'},
            columns: <?= $columns ?>,
        });
    });
</script>