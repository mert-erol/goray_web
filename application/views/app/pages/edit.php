<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <?= ln('BASE.edit') ?>
                    <span class="badge badge-danger"> <?= lang_data($data['language'])['title'] ?> </span>
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="<?= base_url('App/Pages') ?>" class="btn btn-dark btn-elevate btn-icon-sm btn-sm">
                            <i class="la la-arrow-left"></i>
                            <?= ln('APPLICATIONS.Pages') ?>
                        </a>
                        <a href="<?= base_url('App/Pages/Set') ?>" class="btn btn-dark btn-elevate btn-icon-sm btn-sm">
                            <i class="la la-plus"></i>
                            <?= ln('BASE.add') ?>
                        </a>
                        <?= $this->load->view('app/base/translations', ['languages' => $languages, 'ref_id' => $data['ref_id'], 'module' => 'Pages'], TRUE) ?>
                    </div>
                </div>
            </div>
        </div>
        <form id="<?= $appName ?>" action="<?= base_url('App/Pages/Edit/' . $id) ?>" method="post">
            <div class="kt-portlet__body">

                <div class="form-group row">

                    <?php
                    $textEditorData = '';
                    foreach ($cols as $col):
                        ?>

                        <?php if (in_array($col['name'], array('permalink', 'keywords', 'description', 'origin', 'target', 'service', 'faq'))): ?>


                    <?php elseif ($col['name'] == 'category_ref_id'):
                        $category_ref_id = $data[$col['name']];
                        ?>
                        <div class="col-lg-3 margin-bottom-15">
                            <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                            <select class="form-control kt-selectpicker" data-style="btn-warning"
                                    name="<?= $col['name'] ?>" data-size="7" multiple="multiple"
                                    data-live-search="true">
                                <option value="" disabled><?= ln('BASE.all') ?></option>
                            </select>
                        </div>


                    <?php elseif (in_array($col['name'], array('location'))): ?>
                        <div class="col-lg-12 margin-bottom-15">
                            <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                            <input type="text" name="<?= $col['name'] ?>" value="<?= $data[$col['name']] ?>"
                                   class="form-control">
                        </div>

                    <?php elseif (in_array($col['type'], array('varchar'))): ?>
                        <div class="col-lg-6 margin-bottom-15">
                            <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                            <input type="text" name="<?= $col['name'] ?>" value="<?= $data[$col['name']] ?>"
                                   class="form-control">
                        </div>

                    <?php elseif ($col['name'] == 'department'):
                        $department = $data[$col['name']];
                        ?>
                        <div class="col-lg-3 margin-bottom-15">
                            <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                            <select class="form-control kt-selectpicker" data-style="btn-warning"
                                    name="<?= $col['name'] ?>" data-size="7"
                                    data-live-search="true">
                                <option value=""><?= ln('BASE.all') ?></option>
                            </select>
                        </div>


                    <?php elseif ($col['name'] == 'start_date'): ?>
                        <div class="col-lg-6 margin-bottom-15">
                            <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                            <input type="text" name="<?= $col['name'] ?>" class="form-control datetimepicker"
                                   value="<?= date('d/m/Y H:i', strtotime($data[$col['name']])) ?>"
                                   autocomplete="off">
                        </div>

                    <?php elseif ($col['name'] == 'end_date'): ?>
                        <div class="col-lg-6 margin-bottom-15">
                            <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                            <input type="text" name="<?= $col['name'] ?>" class="form-control datetimepicker"
                                   value="<?= date('d/m/Y H:i', strtotime($data[$col['name']])) ?>"
                                   autocomplete="off">
                        </div>

                    <?php elseif ($col['name'] == 'files'): ?>
                        <div class="col-lg-6 margin-bottom-15">
                            <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                            <input type="file" multiple class="form-control" name="<?= $col['name'] ?>[]">
                        </div>


                    <?php elseif ($col['type'] == 'datetime'): ?>

                    <?php elseif ($col['type'] == 'longtext'):
                        $textEditorData = $data[$col['name']];
                        ?>

                        <div class="col-lg-12 margin-bottom-15">
                            <label>Permalink</label>
                            <div class="clearfix d-none" id="permalink-spinner">
                                <p><i class="fas fa-spinner fa-spin"></i> Creating permalink ...</p>
                            </div>
                            <div class="input-group mb-3" id="permalink-area">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"
                                          id="permalink-before"><?= base_url($langData['code']) ?>/</span>
                                </div>
                                <input type="text" name="permalink" value="<?= $data['permalink'] ?>"
                                       class="form-control">
                                <div class="input-group-append" title="Preview Page">
                                    <span class="input-group-text" id="basic-addon3">
                                            <a href="#" id="permalink-url" target="_blank">
                                                <i class="fas fa-external-link-alt"></i>
                                            </a>
                                    </span>
                                </div>
                            </div>
                        </div>


                        <?php
                        $origin_lang_ref_id = $data['origin'];
                        $target_lang_ref_id = $data['target'];
                        $service_ref_id = $data['service'];

                        foreach (['origin', 'target', 'service'] as $static_col): ?>
                            <div class="col-lg-4 margin-bottom-15">
                                <label><?= ln($mainTbl . '.' . $static_col) ?></label>
                                <select class="form-control kt-selectpicker" data-style="btn-warning"
                                        name="<?= $static_col ?>" data-size="7"
                                        data-live-search="true">
                                    <option value=""><?= ln('BASE.select') ?></option>
                                </select>
                            </div>
                        <?php endforeach; ?>

                        <div class="col-lg-12 kt-mb-20">
                            <div class="card">
                                <div class="card-header">SEO</div>
                                <div class="card-body">
                                    <div class="col-lg-12 margin-bottom-15">
                                        <label><?= ln($mainTbl . '.keywords') ?></label>
                                        <input type="text" name="keywords" class="form-control"
                                               value="<?= $data['keywords'] ?>">
                                    </div>
                                    <div class="col-lg-12 margin-bottom-15">
                                        <label><?= ln($mainTbl . '.description') ?></label>
                                        <input type="text" name="description" class="form-control"
                                               value="<?= $data['description'] ?>">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 margin-bottom-15">
                            <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                            <div name="<?= $col['name'] ?>" class="summernote"></div>
                        </div>

                        <div class="col-lg-12 kt-mb-20">
                            <div class="card">
                                <div class="card-header">
                                    <span class="pull-left"><?= ln($mainTbl . '.faq') ?></span>
                                    <button id="faq-add-row" type="button" class="btn btn-dark pull-right btn-sm"><i
                                                class="fas fa-plus"></i> <?= ln('ACTION.add_row') ?>
                                    </button>
                                </div>
                                <div class="card-body" id="faq-inputs">


                                </div>
                            </div>
                        </div>

                    <?php elseif ($col['name'] == 'status'): ?>
                        <div class="col-lg-2 margin-bottom-15 pull-right">
                            <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                            <select class="form-control" name="<?= $col['name'] ?>">
                                <option value="1" <?= $data[$col['name']] == 1 ? 'selected' : '' ?>><?= ln('BASE.active') ?></option>
                                <option value="0" <?= $data[$col['name']] == 0 ? 'selected' : '' ?>><?= ln('BASE.passive') ?></option>
                            </select>

                        </div>

                    <?php elseif (in_array($col['type'], array('int', 'float'))): ?>
                        <div class="col-lg-2 margin-bottom-15">
                            <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                            <input type="text" name="<?= $col['name'] ?>" value="<?= $data[$col['name']] ?>"
                                   class="form-control">
                        </div>

                    <?php else: ?>

                        <div class="col-lg-6 margin-bottom-15">
                            <label><?= ln($mainTbl . '.' . $col['name']) ?></label>
                            <input type="text" name="<?= $col['name'] ?>" value="<?= $data[$col['name']] ?>"
                                   class="form-control">
                        </div>

                    <?php endif; endforeach; ?>


                    <?php $files = json_decode($data['files'], TRUE);
                    if (!empty($files)): ?>
                        <div class="col-md-12">
                            <div class="row">
                                <?php foreach ($files as $file): ?>
                                    <div class="col-md-3">
                                        <div class="card" style="width: 18rem;">
                                            <div class="card-body">
                                                <p class="card-text"><a href="<?= upload_dir() ?>/<?= $file ?>"
                                                                        target="_blank"><?= $file ?></a></p>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="kt-form__actions ">


                    <button type="submit" class="btn btn-success btn-elevate btn-icon-sm float-right btn-sm"><i
                                class="la la-save"></i><?= ln('ACTION.update') ?>
                    </button>


                </div>
            </div>
        </form>


    </div>
</div>

<script>

    document.addEventListener("DOMContentLoaded", function (event) {

        $('select[name="department"]').html(departments(<?=$data['language']?>));
        $('select[name="department"]').val(<?=$department?>);

        $('select[name="category_ref_id"]').html(pages_categories(<?=$data['language']?>));
        $('select[name="category_ref_id"]').val(<?=$category_ref_id?>);

        $('select[name="origin"]').html(translation_languages(<?=$this->session->userdata('app_lang_id')?>));
        $('select[name="origin"]').val(<?=$origin_lang_ref_id?>);

        $('select[name="target"]').html(translation_languages(<?=$this->session->userdata('app_lang_id')?>));
        $('select[name="target"]').val(<?=$target_lang_ref_id?>);

        $('select[name="service"]').html(service_types(<?=$this->session->userdata('app_lang_id')?>));
        $('select[name="service"]').val(<?=$service_ref_id?>);

        $(document).on('click', '#faq-add-row', function () {
            faq_add_row();
        });

        $(document).on('click', '.faq-del-row', function () {
            $(this).parent().parent().parent().remove();
        });

        $('#permalink-url').attr('href', $('#permalink-before').text() + $('input[name="permalink"]').val());


        var exists_faqs = <?= strlen($data['faq']) == 0 ? '[]' : $data['faq'] ?>;
        list_faqs(exists_faqs);

        $('form[id="<?=$appName?>"] .summernote').summernote({
            height: "200px",
            callbacks: {
                onImageUpload: function (files) {
                    url = $(this).data('upload'); //path is defined as data attribute for  textarea
                    myUploadFile(files[0], <?=$id?>, $(this));
                }
            }
        });

        $('form[id="<?=$appName?>"] .summernote').summernote("code", `<?=$textEditorData?>`);
        $('form[id="<?=$appName?>"]').submit(function (e) {

            e.preventDefault();

            swal.fire({
                title: "<?=ln('MESSAGE.Processing')?>",
                text: "<?=ln('MESSAGE.Wait')?>",
                timer: 99999,
                onOpen: function () {
                    swal.showLoading()
                }
            }).then(function (e) {
                "timer" === e.dismiss && console.log("I was closed by the timer")
            });

            var form = $(this);
            var formdata = false;
            formdata = new FormData(form[0]);
            formdata.append('content', $('form[id="<?=$appName?>"] .summernote').summernote('code'));
            formdata.append('category_ref_id', JSON.stringify($('select[name="category_ref_id"]').val()));


            var faqs = [];
            $('#faq-inputs .faq-cards').each((k, v) => {
                var q = $(v).find('.question').val();
                var a = $(v).find('.answer').val();
                var g = {q: q, a: a};
                faqs.push(g);
            });

            formdata.append('faq', JSON.stringify(faqs));

            $.ajax({
                url: $('form[id="<?=$appName?>"]').attr('action'),
                data: formdata,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function (response, textStatus, jqXHR) {
                    var response = JSON.parse(response);
                    if (response.result == 1) {
                        swal.fire({
                            type: 'success',
                            title: response.message,
                            showConfirmButton: false,
                            timer: 1500
                        });
                    } else {
                        swal.fire({
                            type: 'error',
                            title: response.message,
                            showConfirmButton: false,
                            timer: 6000
                        });
                    }
                }
            });
        });

        $('input[name="title"]').donetyping(function () {
            set_permalink($('input[name="title"]').val());
        });

        $('input[name="permalink"]').donetyping(function () {
            set_permalink($('input[name="permalink"]').val());
        });


        if ($('input[name="permalink"]').val() == '') {
            set_permalink($('input[name="title"]').val());
        }

    });


    function set_permalink(title) {

        $('#permalink-area').addClass('d-none');
        $('#permalink-spinner').removeClass('d-none');

        $.post(window.location.origin + '/App/Pages/SetPermalink/<?=$id?>', {title: title}, function (res) {
            res = JSON.parse(res);

            if (res.status == 1) {
                $('input[name="permalink"]').val(res.permalink);
                $('input[name="permalink"]').css("border", "1px solid red");
                setTimeout(function () {
                    $('input[name="permalink"]').css("border", "1px solid black");
                }, 2000);

                $('#permalink-area').removeClass('d-none');
                $('#permalink-spinner').addClass('d-none');

                $('#permalink-url').attr('href', $('#permalink-before').text() + res.permalink);
            }
        })
    }

    function faq_add_row() {
        var input_group = `
        <div class="input-group input-group-sm mb-3 faq-cards">
                                        <textarea data-toggle="tooltip" data-placement="top" title="Question"
                                                  class="form-control question" aria-label="With textarea"></textarea>
                <textarea data-toggle="tooltip" data-placement="top" title="Answer"
                          class="form-control answer" aria-label="With textarea"></textarea>
                <div class="input-group-append">
                                                        <span class="input-group-text" id="inputGroup-sizing-sm">
                                                            <button type="button" class="btn btn-sm text-danger faq-del-row"><i
                                                                        class="fas fa-minus"></i>
                                                            </button>
                                                        </span>
                </div>
            </div>
        `;
        $('#faq-inputs').append(input_group);
        $('[data-toggle="tooltip"]').tooltip();
    }

    function list_faqs(exists_faqs) {
        var groups = '';
        for (var i = 0; i < exists_faqs.length; i++) {
            var a = exists_faqs[i].a, q = exists_faqs[i].q;
            var input_group = `
        <div class="input-group input-group-sm mb-3 faq-cards">
                                        <textarea data-toggle="tooltip" data-placement="top" title="Question"
                                                  class="form-control question" aria-label="With textarea">` + q + `</textarea>
                <textarea data-toggle="tooltip" data-placement="top" title="Answer"
                          class="form-control answer" aria-label="With textarea">` + a + `</textarea>
                <div class="input-group-append">
                                                        <span class="input-group-text" id="inputGroup-sizing-sm">
                                                            <button type="button" class="btn btn-sm text-danger faq-del-row"><i
                                                                        class="fas fa-minus"></i>
                                                            </button>
                                                        </span>
                </div>
            </div>
        `;
            groups += input_group;

        }

        $('#faq-inputs').append(groups);
        $('[data-toggle="tooltip"]').tooltip();

    }


</script>

