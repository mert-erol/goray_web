<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <?= ln('APPLICATIONS.Pages') ?>
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="<?= base_url('App/Pages/Set') ?>" class="btn btn-dark btn-elevate btn-icon-sm btn-sm">
                            <i class="la la-plus"></i>
                            <?= ln('BASE.add') ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <table class="table table-striped table-bordered table-hover table-checkable" id="department-table">
                <thead>
                <tr>
                    <?php
                    $columns = '[';
                    foreach ($cols as $col):
                        if (!in_array($col['name'], ['files', 'permalink', 'keywords', 'description', 'content', 'origin', 'target', 'service', 'faq'])):
                            $columns .= '{ data:"' . $col['name'] . '"},';
                            ?>
                            <th><?= ln($mainTbl . '.' . $col['name']) ?></th>
                        <?php endif;
                    endforeach;
                    $columns = $columns . ']';
                    ?>

                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


<script>

    var table;
    document.addEventListener("DOMContentLoaded", function (event) {
        table = _base.get_table($("#department-table"), {
            responsive: !0,
            searchDelay: 500,
            processing: !0,
            serverSide: !0,
            ajax: {url: window.location.origin + '/App/Pages/Datatables', method: 'post'},
            columns: <?= $columns ?>,
            columnDefs: [
                <?php if(app_auth_check('edit-pages')): ?>
                {
                    targets: 0,
                    title: "#",
                    orderable: !1,
                    render: function (d, t, r) {
                        var edit_btn = '<a href="' + window.location.origin + '/App/Pages/Edit/' + d + '" class="btn btn-sm btn-clean"><i class="far fa-edit"></i> Edit</a>';
                        var duplicate_btn = '<button type="button" onclick="duplicate_page(' + r.id + ')" class="btn btn-sm btn-clean" ><i class="far fa-copy"></i> Duplicate</button>';
                        var delete_btn = '<button type="button" onclick="delete_page(' + r.id + ')" class="btn btn-sm btn-clean text-danger" ><i class="fas fa-times text-danger"></i> Delete</button>';


                        var dropdown = `
                        <div class="dropdown dropdown-inline">
                            <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown" aria-expanded="false">
                                <i class="fas fa-ellipsis-v"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-left" style="display: none;">
                                <ul class="navi flex-column navi-hover">
                                    <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary">Choose an action</li>
                                    <li class="navi-item"> ` + edit_btn + `</li>
                                    <li class="navi-item"> ` + duplicate_btn + `</li>
                                    <li class="navi-item"> ` + delete_btn + `</li>

                                </ul>
                            </div>
                        </div>`;


                        return dropdown;
                    }
                },
                <?php endif; ?>
                {
                    targets: -3,
                    orderable: !1,
                    render: function (d) {
                        if (d == 0) {
                            return '<span class="badge badge-danger"><?=ln('BASE.passive')?></span>';
                        }
                        if (d == 1) {
                            return '<span class="badge badge-success"><?=ln('BASE.active')?></span>';
                        }
                    }
                }
            ]
        });
    });


    function duplicate_page(id) {
        $.post(window.location.origin + '/App/Pages/Duplicate/' + id, {}, function (res) {
            res = JSON.parse(res);

            swal.fire({
                title: "<?=ln('MESSAGE.Processing')?>",
                text: "<?=ln('MESSAGE.Wait')?>",
                onOpen: function () {
                    swal.showLoading()
                }
            });


            console.log(res);


            if (res.status == 1) {

                swal.fire({
                    type: 'success',
                    title: '<?=ln('MESSAGE.Duplicated')?>',
                    showConfirmButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Edit Duplicated Page',
                    cancelButtonText: 'Close',
                    onOpen: function () {
                        swal.disableLoading()
                    }
                }).then((result) => {
                    if (result.value) {
                        swal.showLoading();
                        window.location.href = window.location.origin + '/App/Pages/Edit/' + res.insert_id;
                    } else {
                        swal.close();
                    }

                });

            } else {

                swal.fire({
                    type: 'error',
                    title: '<?=ln('MESSAGE.Error')?>',
                    showConfirmButton: false,
                    timer: 6000,
                    onOpen: function () {
                        swal.disableLoading()
                    }
                });
            }

        });
    }

    function delete_page(page_id) {


        swal.fire({
            type: 'info',
            title: 'Delete',
            showConfirmButton: true,
            showCancelButton: true,
            confirmButtonText: 'Yes',
            cancelButtonText: 'Close',
            onOpen: function () {
                swal.disableLoading()
            }
        }).then((result) => {
            console.log(result);
            if (result.value) {
                swal.showLoading();
                $.post(window.location.origin + '/App/Pages/Delete/' + page_id, {}, function (res) {
                    res = JSON.parse(res);
                    console.log(res);
                    table.ajax.reload();
                    if (res.status == 1) {
                        swal.fire({
                            type: 'success',
                            title: 'Deleted',
                            showConfirmButton: false,
                            timer: 6000,
                            onOpen: function () {
                                swal.disableLoading()
                            }
                        });
                    }

                });
            } else {
                swal.close();
            }

        });


        /*
        $.get(window.location.origin + '/App/Pages/' + page_id, function (res) {
            res = JSON.parse(res);

            if (res.status = 1) {

            } else {

            }

        }); */
    }

</script>