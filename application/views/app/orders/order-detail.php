<div class="col-lg-6">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Order Detail
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="btn-group btn-group btn-group-pill btn-block" role="group">
                    <button type="button" class="btn btn-dark btn-icon" title="Print"
                            onclick="javascript:window.print();">
                        <i class="la la-print"></i>
                    </button>
                    <button type="button" class="btn btn-dark btn-icon" title="Send"
                            onclick="javascript:window.print();">
                        <i class="la la-mail-forward"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <div class="table_desc">
                <div class="cart_page table-responsive">
                    <table class="table table-cards align-items-center">

                        <tr>
                            <th>Origin Language</th>
                            <td><?= $order_data['origin_language'] ?></td>
                        </tr>
                        <tr class="table-divider"></tr>
                        <tr>
                            <th>Target Languages</th>
                            <td><?= $order_data['target_languages'] ?></td>
                        </tr>
                        <tr class="table-divider"></tr>
                        <tr>
                            <th>Services</th>
                            <td><?= $order_data['service_types'] ?></td>
                        </tr>
                        <tr class="table-divider"></tr>

                        <tr>
                            <th>Order Date</th>
                            <td><?= date_std($order_data['created_at']) ?></td>
                        </tr>
                        <tr class="table-divider"></tr>

                        <tr>
                            <th>Estimated Date</th>
                            <td><?= date_std($order_data['estimated_date']) ?></td>
                        </tr>
                        <tr class="table-divider"></tr>

                        <?php if (strlen($order_data['notes']) > 0): ?>
                            <tr>
                                <th>Notes</th>
                                <td><?= $order_data['notes'] ?></td>
                            </tr>
                            <tr class="table-divider"></tr>

                        <?php endif; ?>

                        <tr>
                            <th>Total Word Count</th>
                            <td><?= $order_data['word_count'] ?></td>
                        </tr>
                        <tr class="table-divider"></tr>

                        <?php if (app_auth_check('edit-assign')): ?>
                            <?php if ($order_data['discount'] > 0):
                                ?>
                                <tr class="text-info">
                                    <th>Total Amount</th>
                                    <td><b><?= $order_data['total_amount'] + $order_data['discount'] ?></b></td>
                                </tr>

                                <tr class="text-warning">
                                    <th>Discount</th>
                                    <td><b>- <?= $order_data['discount'] ?></b></td>
                                </tr>

                                <tr class="text-success">
                                    <th>Grand Total</th>
                                    <td><b><?= $order_data['total_amount'] ?></b></td>
                                </tr>

                            <?php else: ?>
                                <tr>
                                    <th>Total Amount</th>
                                    <td><b><?= $order_data['total_amount'] ?></b></td>
                                </tr>
                            <?php endif; ?>
                        <?php endif; ?>


                        <?php if (app_auth_check('edit-assign')): ?>
                            <tr>
                                <th>Status</th>
                                <td>
                                    <?php
                                    $order_stat_map = [
                                        1 => ['Paid', 'btn-success'],
                                        2 => ['No Paid', 'btn-warning'],
                                        3 => ['Canceled', 'btn-danger'],
                                        4 => ['Finished', 'btn-info'],
                                    ];

                                    foreach ($order_stat_map as $k => $stat):
                                        ?>


                                    <?php endforeach; ?>

                                    <select id="order-status" class="form-control kt-selectpicker"
                                            data-style="<?= $order_stat_map[$order_data['status']][1] ?>">
                                        <?php foreach ($order_stat_map as $k => $item):
                                            ?>
                                            <option value="<?= $k ?>" <?= $k == $order_data['status'] ? 'selected="selected"' : '' ?>><?= $item[0] ?></option>
                                        <?php
                                        endforeach; ?>
                                    </select>

                                </td>
                            </tr>
                        <?php endif; ?>


                        <tr class="table-divider"></tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot" style="display: none;">

            <div class="btn-group btn-group btn-group-pill btn-block" role="group">

                <?php

                /*
                for ($i = 0; $i < 5; $i++) {
                    if ($data['status'] != $i) {
                        echo app_order_stats($i, 'button', 'change-stat-button-' . $i, 'data-id="' . $data['id'] . '"');
                    }
                } */
                ?>

            </div>


        </div>
    </div>
</div>


<script>
    document.addEventListener("DOMContentLoaded", function (event) {


        $('select[data-id="employees"]').html(client_users());

        <?php if(count($order_data['assigns']) > 0): ?>
        $('select[data-id="employees"]').val(<?=$order_data['assigns'][0]['client_user']?>);
        <?php endif; ?>

        <?php if(app_auth_check('edit-assign')): ?>
        $(document).on('change', 'select[id="employees"]', function () {
            $.post(window.location.origin + '/App/Orders/AssignTo', {
                order_id:<?=$order_data['id']?>,
                employee_id: $('select[id="employees"]').val()
            });
        });
        <?php endif; ?>


        <?php if (app_auth_check('edit-assign')): ?>
        var status_map = <?=json_encode($order_stat_map)?>;

        $(document).on('change', '#order-status', function () {
            var status = $(this).val();
            var stat_class = status_map[status][1];

            $.post(window.location.origin + '/App/Orders/ChangeStatus/' +<?=$order_data['id']?>, {status: status}, function (res) {
                res = JSON.parse(res);

                if (res.status == 1) {
                    $('button[data-id="order-status"]').removeAttr('class');
                    $('button[data-id="order-status"]').attr('class', 'btn dropdown-toggle ' + stat_class);

                    swal.fire({
                        type: 'success',
                        title: 'Successfully updated!',
                        timer: 1500
                    });
                } else {

                }
            });
        });
        <?php endif; ?>

    });

    function download_files(order_id) {

        $.get(window.location.origin + '/App/Orders/Download/' + order_id, {}, function () {

        });
    }
</script>
