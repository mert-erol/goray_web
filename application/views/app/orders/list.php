<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Orders
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar" style="display: none">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="<?= base_url('App/Orders/Add') ?>" class="btn btn-dark btn-elevate btn-icon-sm btn-sm">
                            <i class="la la-plus"></i>
                            <?= ln('BASE.add') ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <table class="table table-striped table-bordered table-hover table-checkable" id="order-table">
                <thead>
                <tr>
                    <?php
                    $columns = '[';
                    foreach ($cols as $col):
                        if (!in_array($col['name'], ['guid', 'notes', 'user_files', 'free_text', 'discount', 'updated_at'])):
                            $columns .= '{ data:"' . $col['name'] . '"},';
                            ?>
                            <th><?= ln('orders.' . $col['name']) ?></th>
                        <?php endif; endforeach;
                    $columns = $columns . ']';
                    ?>

                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


<script>


    document.addEventListener("DOMContentLoaded", function (event) {

        var status_map = '<?= json_encode(status_map('', NULL, TRUE)) ?>';
        var status_map1 = JSON.parse(status_map);

        console.log(status_map1);

        $("#order-table").DataTable({

            responsive: !0,
            searchDelay: 500,
            processing: !0,
            serverSide: !0,
            ajax: {url: window.location.origin + '/App/Orders/Datatables', method: 'post'},
            columns: <?=$columns?>,
            columnDefs: [
                {
                    targets: 0,
                    title: "#",
                    orderable: !1,
                    render: function (d) {
                        return '<a href="' + window.location.origin + '/App/Orders/Edit/' + d + '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit">\n<i class="la la-edit"></i>\n</a>';
                    }
                },
                {
                    targets: -2,
                    render: function (d) {
                        if (d == 1) return '<span class="badge badge-success">Paid</span>';
                        if (d == 2) return '<span class="badge badge-warning">Waiting</span>';
                        if (d == 3) return '<span class="badge badge-danger">Canceled</span>';
                        if (d == 4) return '<span class="badge badge-info">Finished</span>';
                    }
                }]
        })
    });


</script>