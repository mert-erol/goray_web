
<div class="col-lg-6">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Documents / Text
                </h3>
            </div>

            <div class="kt-portlet__head-toolbar">


                <div class="btn-group btn-group btn-group-pill btn-block" role="group">

                    <a href="<?= base_url('App/Orders/Download/' . $order_data['id']) ?>" class="btn btn-dark "
                       title="Download">
                        <i class="far fa-file-archive"></i> Download
                    </a>


                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <?php if ($order_data['word_count'] > 0): ?>
                <?php $user_files = json_decode($order_data['user_files'], FALSE);
                if (count($user_files) > 0): ?>
                    <ul>
                        <?php foreach ($user_files as $user_file): ?>
                            <li><a href="<?= base_url('uploads/' . $order_data['guid'] . '/' . $user_file) ?>"
                                   target="_blank"><?= $user_file ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                <?php else: echo $order_data['free_text'];
                endif;
            endif; ?>

        </div>
    </div>
</div>