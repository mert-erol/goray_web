<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Yeni Sipariş Ekle
                </h3>
            </div>

            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">

                        <a href="<?= base_url('App/Orders') ?>" class="btn btn-dark btn-elevate btn-icon-sm btn-sm">
                            <i class="la la-ellipsis-v"></i>
                            Siparişler
                        </a>

                        <a href="javascript:;" onclick="modal_trigger('<?= base_url('App/Customers/Set') ?>');" class="btn btn-dark btn-elevate btn-icon-sm btn-sm">
                            <i class="la la-plus"></i>
                            Müşteri Ekle
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="col-lg-12 margin-bottom-15">
                <label>Müşteri</label>
                <select class="form-control kt-selectpicker" data-style="btn-dark"
                        name="customer" data-size="7"
                        data-live-search="true">
                    <option value="">Seçiniz</option>
                </select>
            </div>

            <div class="col-lg-12 margin-bottom-15">
                <label>Ürün Arama</label>
                <select class="form-control kt-selectpicker" data-style="btn-success"
                        name="product" data-size="7"
                        data-live-search="true">
                    <option value="">Seçiniz</option>
                </select>
            </div>

        </div>
    </div>
</div>

<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Eklenen Ürünler
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="col-lg-12 margin-bottom-15">
                eklenen ürünler
            </div>

            <div class="kt-form__actions">
                <button type="submit" class="btn btn-dark btn-elevate btn-icon-sm btn-sm float-right"><i
                            class="la la-check"></i>Siparişi Tamamla
                </button>
            </div>
        </div>


    </div>
</div>


<script>

    document.addEventListener("DOMContentLoaded", function (event) {


        var customer_options = '<option value="">Seçiniz</option>';

        $.ajax({
            url: window.location.origin + '/App/Customers/GetCustomers',
            method: 'get',
            async: false,
            success: function (response) {
                response = JSON.parse(response);

                $.each(response, function (i, v) {
                    customer_options += '<option value="' + v.id + '">' + v.title + '</option>';
                });

            }
        });

        $('select[name="customer"]').html(customer_options);


        var product_options = '<option value="">Seçiniz</option>';

        $.ajax({
            url: window.location.origin + '/App/Products/GetProducts',
            method: 'get',
            async: false,
            success: function (response) {
                response = JSON.parse(response);

                $.each(response, function (i, v) {
                    product_options += '<option value="' + v.id + '">' + v.product_name + '</option>';
                });

            }
        });

        $('select[name="product"]').html(product_options);


        $('select[name="product"]').change(function () {
            var prod_id = $(this).val();
            modal_trigger('<?=base_url('App/Products/Show/')?>' + prod_id);

        });


    });


</script>