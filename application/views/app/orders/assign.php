<div class="col-lg-6">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Assign
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">

            <div class="form-group">
                <label for="">Employee</label>
                <select class="form-control kt-selectpicker" data-style="btn-info" data-id="employees" <?= app_auth_check('edit-assign') ? 'id="employees"' : '' ?> <?= app_auth_check('edit-assign') ? '' : 'disabled' ?>>
                </select>
            </div>

            <div id="assign-detail">

                <?php if (count($order_data['assigns']) > 0): ?>

                    <div class="form-group">
                        <div class="card">
                            <div class="card-body">
                                <label for=""><b>Notes</b></label> <br>
                                <?php if (app_auth_check('edit-assign')): ?>
                                    <?= $order_data['assigns'][0]['notes'] ?>
                                <?php else: ?>
                                    <textarea id="assign-notes" class="form-control" cols="30"
                                              rows="3"><?= $order_data['assigns'][0]['notes'] ?></textarea>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (count($order_data['assigns']) > 0): ?>
                    <div class="form-group">
                        <div class="card">
                            <div class="card-body">
                                <label for=""><b>Assign Date</b></label> <br>
                                <?= date_std($order_data['assigns'][0]['created_at']) ?>
                            </div>
                        </div>
                        <?php if (app_auth_check('edit-assign') == 0): ?>
                            <div class="card">
                                <div class="card-body">
                                    <label for=""><b>Updated Date</b></label> <br>
                                    <?= date_std($order_data['assigns'][0]['updated_at']) ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if (app_auth_check('edit-assign') == 0): ?>
                            <div class="card">
                                <div class="card-body">
                                    <label for=""><b>Status</b></label> <br>
                                    <select class="form-control kt-selectpicker" data-style="btn-warning" id="status">
                                        <option value="1" <?= $order_data['assigns'][0]['status'] == 1 ? ' selected' : '' ?>>
                                            Waiting
                                        </option>
                                        <option value="2" <?= $order_data['assigns'][0]['status'] == 2 ? ' selected' : '' ?>>
                                            Translating
                                        </option>
                                        <option value="3" <?= $order_data['assigns'][0]['status'] == 3 ? ' selected' : '' ?>>
                                            Completed
                                        </option>
                                    </select>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            </div>

        </div>


        <?php if (app_auth_check('edit-assign') == 0): ?>
            <div class="kt-portlet__foot">
                <div class="btn-group btn-group btn-group-pill btn-block" role="group">
                    <button id="update-assign" class="btn btn-success pull-right">Update</button>
                </div>
            </div>
        <?php endif; ?>

    </div>
</div>

<script>

    document.addEventListener("DOMContentLoaded", function (event) {
        <?php if(app_auth_check('edit-assign') == 0): ?>
        $(document).on('click', '#update-assign', function () {
            $.post(window.location.origin + '/App/Orders/UpdateAssign', {
                order_id:<?=$order_data['id']?>,
                notes: $('#assign-notes').val(),
                status: $('#status').val()
            }, function () {
                swal.fire({
                    type: 'success',
                    title: 'Successfully updated!',
                    timer: 1500
                });
            });
        });

        <?php endif; ?>

    });


</script>