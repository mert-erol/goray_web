<?php if (!app_auth_check('edit-assign')): ?>
    <div class="col-lg-6">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Upload Files
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">


                <input type="file" multiple
                       class="filesInput btn btn-default btn-round btn-block btn-lg showFilesInput"
                       id="files-input" name="files[]" style="" name="attachments"/>

            </div>
        </div>
    </div>


    <script>
        document.addEventListener("DOMContentLoaded", function (event) {
            $("#files-input").fileinput({
                uploadUrl: window.location.origin + "/App/Orders/FileUpload/" +<?=$order_data['id']?>,
                maxFileCount: 99,
                showRemove: false,
                mainClass: "input-group-lg",
                <?php
                if(count($order_data['exist_files']) > 0): ?>
                validateInitialCount: true,
                overwriteInitial: false,
                initialPreviewAsData: true,
                initialPreview: <?=$order_data['initialPreview']?>,
                initialPreviewConfig: <?=$order_data['initialPreviewConfig']?>,
                <?php  endif; ?>
                allowedFileExtensions: ['png', 'jpg', 'jpeg', 'tiff', 'bmp', 'gif', 'tif', 'html', 'csv', 'txt', 'eml', 'json', "htm", "rtf", "docx", "doc", "xlsx", "xls", "ppt", "pptx", "pdf"]
            });

            $(document).on('click', '.fileinput-upload-button', function () {
                $("#files-input").fileinput('upload');
            });

            $(document).on('click', '.kv-file-remove', function () {
                var deleted_file_name = $(this).parent().parent().parent().find('.file-footer-caption').attr('title');
                console.log(deleted_file_name);
                $.post(window.location.origin + '/App/Orders/FileDelete/' +<?=$order_data['id']?>, {key: deleted_file_name}, function () {
                });
            });

        });

    </script>
<?php endif; ?>