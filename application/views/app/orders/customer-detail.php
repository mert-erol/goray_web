<?php if (app_auth_check('edit-assign')): ?>
    <div class="col-lg-6">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Customer Detail
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">

                <div class="table_desc">
                    <div class="cart_page table-responsive">
                        <table class="table table-cards align-items-center">
                            <tr>
                                <th>Customer Name</th>
                                <td><b><?= $order_data['client_user']['name_surname'] ?></b></td>
                            </tr>
                            <tr>
                                <th>Customer Phone</th>
                                <td><?= $order_data['client_user']['phone'] ?></td>
                            </tr>
                            <tr>
                                <th>Customer E-mail</th>
                                <td><?= $order_data['client_user']['username'] ?></td>
                            </tr>

                            <tr>
                                <th>Registration Date</th>
                                <td><?= date_std($order_data['client_user']['created_at'], 'Y-m-d H:i:s') ?></td>
                            </tr>
                        </table>
                    </div>
                </div>


            </div>
        </div>
    </div>
<?php endif; ?>