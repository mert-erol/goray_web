<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <?= ln('APPLICATIONS.PageSigns') ?>
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">

            <table class="table table-striped table-bordered table-hover table-checkable" id="department-table">
                <thead>
                <tr>
                    <?php
                    $columns = '[';
                    foreach ($cols as $col):
                        if ($col['name'] != 'files'):
                            $columns .= '{ data:"' . $col['name'] . '"},';
                            ?>
                            <th><?= ln($mainTbl . '.' . $col['name']) ?></th>
                        <?php endif;
                    endforeach;
                    $columns = $columns . ']';
                    ?>

                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

<script>

    <?php if (!$modal): ?>
    document.addEventListener("DOMContentLoaded", function (event) {
        <?php endif; ?>
        _base.get_table($("#department-table"), {
            responsive: !0,
            searchDelay: 500,
            processing: !0,
            serverSide: !0,
            ajax: {url: window.location.origin + '/App/PageSigns/Datatables/' +<?=$client_user_id?>, method: 'post'},
            columns: <?= $columns ?>,
        });
        <?php if (!$modal): ?>
    });
    <?php endif; ?>

</script>