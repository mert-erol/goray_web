<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Yetkili Listesi
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="<?=base_url('App/Users/Set')?>" class="btn btn-dark btn-elevate btn-icon-sm btn-sm">
                            <i class="la la-plus"></i>
                            <?=ln('BASE.add')?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <table class="table table-striped- table-bordered table-hover table-checkable" id="customer-table">
                <thead>
                <tr>
                    <?php
                    $columns = '[';
                    foreach ($cols as $col):
                        $columns .= '{ data:"' . $col['name'] . '"},';
                        ?>
                        <th><?= ln($mainTbl . '.' . $col['name']) ?></th>
                    <?php endforeach;
                    $columns = $columns . ']';
                    ?>

                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


<script>

    document.addEventListener("DOMContentLoaded", function (event) {

        _base.get_table($("#customer-table"), {
            responsive: !0,
            searchDelay: 500,
            processing: !0,
            serverSide: !0,
            ajax: { url :window.location.origin + '/App/Users/Datatables', method:'post' },
            columns: <?=$columns?>,
            columnDefs: [{
                targets: 0,
                title: "#",
                orderable: !1,
                render: function (d) {
                    return '<a href="'+window.location.origin+'/App/Users/Edit/'+d+'" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit">\n<i class="la la-edit"></i>\n</a>'
                }
            },{
                targets: 2,
                visible: 0
            }]
        });

    });


</script>