<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Ürün Listesi
                </h3>
            </div>


            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">

                        <button type="button" onclick="modal_trigger('<?=base_url('App/Products/Categories')?>')" class="btn btn-dark btn-elevate btn-icon-sm btn-sm">
                            <i class="la la-ellipsis-h"></i>
                            Kategoriler
                        </button>
                        <a href="<?=base_url('App/Products/Set')?>" class="btn btn-dark btn-elevate btn-icon-sm btn-sm">
                            <i class="la la-plus"></i>
                            Ürün Ekle
                        </a>

                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <table class="table table-striped- table-bordered table-hover table-checkable" id="customer-table">
                <thead>
                <tr>
                    <?php
                    $columns = '[';
                    foreach ($cols as $col):
                        $columns .= '{ data:"' . $col['name'] . '" },';
                        ?>
                        <th><?= $col['comment'] ?></th>
                    <?php endforeach;
                    $columns = $columns . ']';

                    ?>

                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


<script>

    document.addEventListener("DOMContentLoaded", function (event) {

        $("#customer-table").DataTable({
            responsive: !0,
            searchDelay: 500,
            processing: !0,
            serverSide: !0,
            ajax: { url :window.location.origin + '/App/Products/Datatables', method:'post' },
            columns: <?=$columns?>,
            columnDefs: [{
                targets: 0,
                title: "#",
                orderable: !1,
                render: function (d) {
                    return '<a href="'+window.location.origin+'/App/Products/Edit/'+d+'" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit">\n<i class="la la-edit"></i>\n</a>'
                }
            }],
            "language": {
                "search": "<b>Ürün Sorgula</b>"
            }
        })

    });


</script>