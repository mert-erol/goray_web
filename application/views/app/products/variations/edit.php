<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Ürün Varyasyonu Düzenleme
                </h3>
            </div>


            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">

                        <?php if ($modal): ?>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">x
                            </button>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>


        <div class="kt-portlet__body">


            <form id="<?= $appName ?>" action="<?= base_url('App/Products/EditVariations/' . $data['id']) ?>"
                  method="post">
                <div class="form-group row">
                    <div class="col-lg-4 margin-bottom-15">
                        <label>Birim</label>
                        <input type="text" name="unit" class="form-control" placeholder="Ö: MT"
                               value="<?= $data['unit'] ?>">
                    </div>

                    <div class="col-lg-4 margin-bottom-15">
                        <label>Tip</label>
                        <input type="text" name="type" class="form-control" placeholder="Ö: 0.75"
                               value="<?= $data['type'] ?>">
                    </div>

                    <div class="col-lg-4 margin-bottom-15">
                        <label>Fiyat</label>
                        <input type="text" name="price" class="form-control" placeholder="Ö: 75.99"
                               value="<?= $data['price'] ?>">
                    </div>

                    <div class="col-lg-12 margin-bottom-15">
                        <label>Açıklama</label>
                        <input type="text" name="name" class="form-control" placeholder="Ö: Kırmızı"
                               value="<?= $data['name'] ?>">
                    </div>

                    <div class="col-lg-3 margin-bottom-15">
                        <button type="button" id="<?= $appName ?>-delete" data-id="<?= $data['id'] ?>"
                                class="btn btn-danger btn-elevate btn-icon-sm "><i
                                    class="la la-times"></i>Varyasyonu Sil
                        </button>
                    </div>

                    <div class="col-lg-9 margin-bottom-15">
                        <button type="submit" class="btn btn-success btn-elevate btn-icon-sm float-right btn-sm"><i
                                    class="la la-save"></i>Varyasyonu Güncelle
                        </button>
                    </div>


                </div>
            </form>


        </div>


    </div>
</div>

<script>

    <?php if(!$modal): ?>
    document.addEventListener("DOMContentLoaded", function (event) {
        <?php endif; ?>

        $('form[id="<?=$appName?>"]').submit(function (e) {

            e.preventDefault();

            swal.fire({
                title: "<?=ln('MESSAGE.Processing')?>",
                text: "<?=ln('MESSAGE.Wait')?>",
                timer: 99999,
                onOpen: function () {
                    swal.showLoading()
                }
            }).then(function (e) {
                "timer" === e.dismiss && console.log("I was closed by the timer")
            });


            var serializedData = $('form[id="<?=$appName?>"]').serializeArray();

            $.post($('form[id="<?=$appName?>"]').attr('action'), serializedData, function (response) {

                var response = JSON.parse(response);

                if (response.result == 1) {
                    // $('form[id="<?=$appName?>"] button[type="submit"]').remove();
                    swal.fire({
                        type: 'success',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 1500
                    }).then(function (e) {
                        modal_hide();
                        $("#variation-table").DataTable().ajax.reload();
                        "timer" === e.dismiss && console.log("I was closed by the timer")
                    });
                } else {
                    swal.fire({
                        type: 'error',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 6000
                    });
                }
            });
        });


        $(document).on('click', '#<?=$appName?>-delete', function () {

            Swal.fire({
                title: 'Emin misiniz?',
                text: "Kalıcı olarak silinecektir.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Evet',
                cancelButtonText: 'Hayır',
                cancelButtonColor: '#d33'
            }).then((result) => {
                if (result.value) {
                    $.post(window.location.origin + '/App/Products/DeleteVariation', {varId: $(this).attr('data-id')}, function (response) {
                       
                        if (response == 1) {
                            modal_hide();
                            $("#variation-table").DataTable().ajax.reload();
                            Swal.fire(
                                'Silindi!',
                                'Kayıt kalıcı olarak silindi.',
                                'success'
                            )
                        } else {
                            Swal.fire(
                                'Silinemedi!',
                                'Lütfen sistem yöneticinize başvurun.',
                                'error'
                            )
                        }

                    });
                }
            });
        });



        <?php if(!$modal): ?>
    });
    <?php endif; ?>

</script>
