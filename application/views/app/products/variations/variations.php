<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Varyasyonlar
                </h3>
            </div>

        </div>


        <div class="kt-portlet__body">
            <div class="kt-scroll" data-scroll="true" data-height="475" data-scrollbar-shown="true">

            <form id="product-variations" action="<?= base_url('App/Products/SetVariations/' . $id) ?>"
                  method="post">
                <div class="form-group row">
                    <div class="col-lg-6 margin-bottom-15">
                        <label>Birim</label>
                        <input type="text" name="unit" class="form-control" placeholder="Örn: Metre">
                    </div>

                    <div class="col-lg-6 margin-bottom-15">
                        <label>Tip</label>
                        <input type="text" name="type" class="form-control" placeholder="Örn: 0.75">
                    </div>

                    <div class="col-lg-12 margin-bottom-15">
                        <label>Fiyat</label>
                        <input type="text" name="price" class="form-control" placeholder="Örn: 75.99">
                    </div>

                    <div class="col-lg-12 margin-bottom-15">
                        <label>Açıklama</label>
                        <input type="text" name="name" class="form-control" placeholder="Örn: Kırmızı">
                    </div>

                    <div class="col-lg-12 margin-bottom-15">
                        <button type="submit" class="btn btn-brand btn-elevate btn-icon-sm btn-block"><i
                                    class="la la-plus"></i>Varyasyon Ekle
                        </button>
                    </div>
                </div>
            </form>


                <table class="table table-striped table-bordered table-hover table-checkable"
                       id="variation-table">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Tip</th>
                        <th>Birim</th>
                        <th>Fiyat</th>
                        <th>Açıklama</th>
                        <th></th>
                    </tr>
                    </thead>
                </table>

            </div>


        </div>
    </div>
</div>


<script>

    document.addEventListener("DOMContentLoaded", function (event) {


        var variations_table = $("#variation-table").DataTable({
            dom: 't',
            responsive: !0,
            searchDelay: 500,
            processing: !0,
            serverSide: !0,
            searching: 0,
            ajax: {
                url: window.location.origin + '/App/Products/VariationsDatatables/' +<?=$id?>,
                method: 'post'
            },
            columns: [{data: 'id'}, {data: 'type'}, {data: 'unit'}, {data: 'price'}, {data: 'name'}, {data: 'id'}],
            columnDefs: [{
                targets: -1,
                title: "",
                orderable: !1,
                render: function (d) {
                    return '<a href="javascript:;" onclick="modal_trigger(\'<?=base_url('App/Products/EditVariations')?>/' + d + '\')">' +
                        '<i class="fas fa-edit"></i> Düzenle</a>';

                }
            },
                {
                    targets: 0,
                    visible: false
                }
            ]
        });

        variations_table.ajax.reload();


        $('form[id="product-variations"]').submit(function (e) {

            e.preventDefault();

            swal.fire({
                title: "<?=ln('MESSAGE.Processing')?>",
                text: "<?=ln('MESSAGE.Wait')?>",
                timer: 99999,
                onOpen: function () {
                    swal.showLoading()
                }
            }).then(function (e) {
                "timer" === e.dismiss && console.log("I was closed by the timer")
            });

            var serializedData = $('form[id="product-variations"]').serializeArray();

            $.post($('form[id="product-variations"]').attr('action'), serializedData, function (response) {

                var response = JSON.parse(response);

                if (response.result == 1) {
                    variations_table.ajax.reload();
                    swal.fire({
                        type: 'success',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 1500
                    }).then(function (e) {
                        "timer" === e.dismiss && console.log("I was closed by the timer")
                    });
                } else {
                    swal.fire({
                        type: 'error',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 6000
                    });
                }
            });
        });


    });


</script>
