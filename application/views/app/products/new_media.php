<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--height-fluid">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Multimedya Ekle
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="DashboardContainer"></div>
        </div>
    </div>
</div>

<script>
    document.addEventListener("DOMContentLoaded", function (event) {

        const Dashboard = Uppy.Dashboard;
        const GoogleDrive = Uppy.GoogleDrive;
        const Dropbox = Uppy.Dropbox;
        const Instagram = Uppy.Instagram;
        const Webcam = Uppy.Webcam;
        const Url = Uppy.Url;
        const Xhr = Uppy.XHRUpload;

        const uppy = Uppy.Core({
            debug: true,
            locale: Uppy.locales.tr_TR,
            autoProceed: false,
            restrictions: {
                maxFileSize: 10000000,
                allowedFileTypes: ['image/*', 'video/*']
            }
        })
            .use(Dashboard, {
                trigger: '.UppyModalOpenerBtn',
                inline: true,
                target: '.DashboardContainer',
                replaceTargetContent: true,
                showProgressDetails: true,
                note: 'En büyük multimedya boyutu 10 Mb',
                height: 470,
                metaFields: [
                    {id: 'name', name: 'Name', placeholder: 'Dosya Adı'},
                    {id: 'caption', name: 'Caption', placeholder: 'Açıklama'}
                ],
                browserBackButtonClose: true
            })
            //.use(GoogleDrive, {target: Dashboard, companionUrl: window.location.origin})
            // .use(Dropbox, {target: Dashboard, companionUrl: window.location.origin})
            // .use(Instagram, {target: Dashboard, companionUrl: window.location.origin})
            .use(Webcam, {target: Dashboard})
            .use(Url, {target: Dashboard, companionUrl: ' https://companion.uppy.io'})
            .use(Xhr, {endpoint: window.location.origin + '/App/Products/Media/' + $('form[id="<?=$appName?>"]').data('id')})

        uppy.on('complete', result => {
            autoTable();
        })


    });


</script>