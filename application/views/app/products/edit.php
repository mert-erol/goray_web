<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Ürün Güncelleme
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="<?= base_url('App/Products') ?>" class="btn btn-dark btn-elevate btn-icon-sm btn-sm">
                            <i class="la la-arrow-left"></i>
                            Ürün Listesi
                        </a>
                        <a href="<?= base_url('App/Products/Set') ?>" class="btn btn-dark btn-elevate btn-icon-sm btn-sm">
                            <i class="la la-plus"></i>
                            <?=ln('BASE.add')?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <form id="<?= $appName ?>" action="<?= base_url('App/Products/Edit/' . $id) ?>" data-id="<?= $id ?>"
              method="post">
            <div class="kt-portlet__body">

                <div class="form-group row">

                    <?php
                    $textEditorData = '';
                    foreach ($cols as $col):
                        if ($col['type'] == 'datetime'):
                            ?>
                            <div class="col-lg-2 margin-bottom-15">
                                <label><?= $col['comment'] ?></label>
                                <input type="text" name="<?= $col['name'] ?>" value="<?= $data[$col['name']] ?>"
                                       class="form-control datepicker_1">
                            </div>


                        <?php elseif ($col['type'] == 'text'):
                            $textEditorData = $data[$col['name']];
                            ?>
                            <div class="col-lg-12 margin-bottom-15">
                                <label><?= $col['comment'] ?></label>
                                <div name="<?= $col['name'] ?>" class="summernote"></div>
                            </div>

                        <?php elseif (in_array($col['type'], array('int', 'float'))): ?>
                            <div class="col-lg-2 margin-bottom-15">
                                <label><?= $col['comment'] ?></label>
                                <input type="text" name="<?= $col['name'] ?>" value="<?= $data[$col['name']] ?>"
                                       class="form-control">
                            </div>

                        <?php elseif ($col['name'] == 'category'):
                            $category = $data[$col['name']];
                            ?>
                            <div class="col-lg-3 margin-bottom-15">
                                <label><?= $col['comment'] ?></label>
                                <select class="form-control kt-selectpicker" data-style="btn-warning"
                                        name="<?= $col['name'] ?>" data-size="7"
                                        data-live-search="true">
                                    <option value="">Seçiniz</option>
                                </select>
                            </div>

                        <?php elseif ($col['name'] == 'status'): ?>
                            <div class="col-lg-2 margin-bottom-15">
                                <label><?= $col['comment'] ?></label>
                                <select class="form-control" name="<?= $col['name'] ?>">
                                    <option value="1" <?= $data[$col['name']] == 1 ? 'selected' : '' ?>><?=ln('BASE.active')?></option>
                                    <option value="0" <?= $data[$col['name']] == 0 ? 'selected' : '' ?>><?=ln('BASE.passive')?></option>
                                </select>

                            </div>

                        <?php else: ?>

                            <div class="col-lg-3 margin-bottom-15">
                                <label><?= $col['comment'] ?></label>
                                <input type="text" name="<?= $col['name'] ?>" value="<?= $data[$col['name']] ?>"
                                       class="form-control">
                            </div>

                        <?php endif; endforeach; ?>
                </div>
                <div class="kt-form__actions">
                    <button type="button" id="<?= $appName ?>-delete" data-id="<?= $data['id'] ?>"
                            class="btn btn-danger btn-elevate btn-icon-sm">
                        <i class="la la-times"></i> Sil
                    </button>
                    <button type="submit" class="btn btn-success btn-elevate btn-icon-sm float-right btn-sm"><i
                                class="la la-save"></i> Güncelle
                    </button>
                </div>
            </div>
        </form>


    </div>
</div>

<script>

    document.addEventListener("DOMContentLoaded", function (event) {

        $('select[name="category"]').html(product_categories());
        $('select[name="category"]').val(<?=$category?>);

        $('form[id="<?=$appName?>"]').submit(function (e) {

            e.preventDefault();

            swal.fire({
                title: "<?=ln('MESSAGE.Processing')?>",
                text: "<?=ln('MESSAGE.Wait')?>",
                timer: 99999,
                onOpen: function () {
                    swal.showLoading()
                }
            }).then(function (e) {
                "timer" === e.dismiss && console.log("I was closed by the timer")
            });

            var serializedData = $('form[id="<?=$appName?>"]').serializeArray();
            serializedData.push({
                'name': 'content',
                'value': $('form[id="<?=$appName?>"] .summernote').summernote('code')
            });

            $.post($('form[id="<?=$appName?>"]').attr('action'), serializedData, function (response) {

                var response = JSON.parse(response);

                console.log(response);

                if (response.result == 1) {
                    swal.fire({
                        type: 'success',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 1500
                    });
                } else {
                    swal.fire({
                        type: 'error',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 6000
                    });
                }
            });


        });

        $('form[id="<?=$appName?>"] .summernote').summernote({height: 150});
        $('form[id="<?=$appName?>"] .summernote').summernote("code", '<?=$textEditorData?>');


        $(document).on('click', '#<?=$appName?>-delete', function () {

            Swal.fire({
                title: 'Emin misiniz?',
                text: "Kalıcı olarak silinecektir.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Evet',
                cancelButtonText: 'Hayır',
                cancelButtonColor: '#d33'
            }).then((result) => {
                if (result.value) {
                    $.post(window.location.origin + '/App/Products/Delete', {prodId: $(this).attr('data-id')}, function (response) {

                        if (response == 1) {
                            Swal.fire(
                                'Silindi!',
                                'Kayıt kalıcı olarak silindi.',
                                'success'
                            )
                        } else {
                            Swal.fire(
                                'Silinemedi!',
                                'Lütfen sistem yöneticinize başvurun.',
                                'error'
                            )
                        }

                    });
                }
            });
        });

    });


</script>