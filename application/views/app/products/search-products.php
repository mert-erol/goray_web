<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    <?= $data[0]['product_name'] ?>
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <button type="button" class="btn btn-secondary btn-elevate btn-icon-sm" data-dissmis="modal">
                            <i class="la la-times"></i>
                            Kapat
                        </button>

                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <?php if (count($data) > 0): ?>
                <div class="col-lg-12 margin-bottom-15">
                    <table class="table variations-table">
                        <thead>
                        <th>Tip</th>
                        <th>Ürün</th>
                        <th>Birim</th>
                        <th>Fiyat</th>
                        <th>Miktar</th>
                        <th>Açıklama</th>
                        <th></th>
                        </thead>
                        <tbody>
                        <?php foreach ($data[0]['variations'] as $var): ?>
                            <tr class="var-row-<?= $var['id'] ?>" data-id="<?= $var['id'] ?>">
                                <td class="grouped-product-list label prod-type"><?= $var['type'] ?></td>
                                <td class="grouped-product-list label prod-name"><?= $var['name'] ?></td>
                                <td class="grouped-product-list label prod-unit"><?= $var['unit'] ?></td>
                                <td class="grouped-product-list price prod-price"><?= $var['price'] ?></td>
                                <td class="grouped-product-list quantity prod-quantity">
                                    <input min="1" max="99999" type="number"></td>
                                <td class="grouped-product-list label prod-content">
                                    <input type="text"></td>
                                <td class="grouped-product-list label">
                                    <a href="javascript:;" class="add-basket" data-id="<?= $var['id'] ?>"><i
                                                class="fa fa-plus"></i>
                                        Ekle </a></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>


                </div>

            <?php else: ?>

                Sistem Hatası
            <?php endif; ?>


        </div>
    </div>
</div>