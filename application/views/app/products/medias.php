<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--height-fluid">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Multimedyalar
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <a href="javascript:;" onclick="autoTable()" class="btn btn-dark btn-elevate btn-icon-sm btn-sm">
                            <i class="la la-refresh"></i>
                            Yenile
                        </a>

                    </div>
                </div>
            </div>
        </div>

        <div class="kt-portlet__body">
            <div class="kt-scroll" id="<?= $appName ?>_medias" data-scroll="true" data-height="500"
                 data-scrollbar-shown="true">

                <table class="table table-striped table-bordered table-hover table-checkable"
                       id="<?= $appName ?>_media_list">
                    <tbody>
                    <tr>
                        <td>#</td>
                        <td>#</td>
                        <td>#</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script>


    function autoTable() {
        $("#<?=$appName?>_media_list").KTDatatable({
            data: {
                type: "remote",
                source: {
                    read: {
                        url: window.location.origin + '/App/Products/Media/' + $('form[id="<?=$appName?>"]').data('id'),
                        params: {
                            'app_name': '<?=explode('-', $appName)[1]?>'
                        },
                        map: function (t) {

                            var e = t;
                            return void 0 !== t.data && (e = t.data), e
                        }
                    }
                },
                pageSize: 10,
                serverPaging: !0,
                serverFiltering: !0,
                serverSorting: !0
            },
            layout: {
                scroll: !1,
                footer: !1
            },
            sortable: !0,
            pagination: !0,
            search: {
                input: $("#generalSearch")
            },
            columns: [{
                field: "id",
                title: "#",
                sortable: "asc",
                width: 60,
                type: "number",
                selector: !1,
                textAlign: "center",
                template: function (r, i, d) {
                    return '<a href="#" class="kt-media">' +
                        '<img src="<?=base_url('uploads/' . explode('-', $appName)[1])?>/' + r.name + '" alt="image">' +
                        '</a>';
                }
            }, {
                field: "name",
                title: "İçerik"
            }, {
                field: "process",
                title: "İşlem",
                template: function (r, i, d) {
                    init();
                    if (r.status == 0) {
                        return '<button type="button" id="<?=$appName?>-set-featured" data-id="' + r.id + '" class="btn btn-dark btn-icon btn-sm"><i class="fa fa-check"></i></button><button type="button" id="<?=$appName?>-delete" data-id="' + r.id + '" class="btn btn-dark btn-icon btn-sm"><i class="fa fa-times"></i></button>';
                    } else {
                        return '<button type="button" id="<?=$appName?>-asd" class="btn btn-danger btn-icon btn-sm"><i class="fa fa-check"></i></button>';
                    }
                }
            }]
        })
    }

    document.addEventListener("DOMContentLoaded", function (event) {
        autoTable();
        init();
    });

    function init() {
        $(document).on('click', '#<?=$appName?>-set-featured', function () {


            $.post(window.location.origin + '/App/Base/SetFeaturedMedia', {mediaId: $(this).attr('data-id')}, function (response) {
                autoTable();
                if (response == 1) {
                    Swal.fire(
                        'Tamamlandı!',
                        'Öne çıkarılmış görsel olarak tanımlandı.',
                        'success'
                    )
                } else {
                    Swal.fire(
                        'Hata!',
                        'Lütfen sistem yöneticinize başvurun.',
                        'error'
                    )
                }
            });


        });

        $(document).on('click', '#<?=$appName?>-delete', function () {

            Swal.fire({
                title: 'Emin misiniz?',
                text: "Kalıcı olarak silinecektir.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Evet',
                cancelButtonText: 'Hayır',
                cancelButtonColor: '#d33'
            }).then((result) => {
                if (result.value) {
                    $.post(window.location.origin + '/App/Base/DeleteMedia', {mediaId: $(this).attr('data-id')}, function (response) {
                        autoTable();
                        if (response == 1) {
                            Swal.fire(
                                'Silindi!',
                                'Dosya kalıcı olarak silindi.',
                                'success'
                            )
                        } else {
                            Swal.fire(
                                'Silinemedi!',
                                'Lütfen sistem yöneticinize başvurun.',
                                'error'
                            )
                        }

                    });
                }
            });


        });
    }


</script>