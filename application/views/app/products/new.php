<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Yeni Ürün Ekle
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <button type="button" onclick="modal_trigger('<?= base_url('App/Products/SetCategory') ?>')"
                           class="btn btn-dark btn-elevate btn-icon-sm btn-sm">
                            <i class="la la-plus"></i>
                            Kategori Ekle
                        </button>
                        <a href="<?= base_url('App/Products') ?>" class="btn btn-dark btn-elevate btn-icon-sm btn-sm">
                            <i class="la la-arrow-left"></i>
                            Ürün Listesi
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <form id="<?= $appName ?>" action="<?= base_url('App/Products/Set') ?>" method="post">
            <div class="kt-portlet__body">

                <div class="form-group row">
                    <?php
                    foreach ($cols as $col):
                        if ($col['type'] == 'datetime'):
                            ?>
                            <div class="col-lg-2 margin-bottom-15">
                                <label><?= $col['comment'] ?></label>
                                <input type="text" name="<?= $col['name'] ?>" class="form-control datepicker_1">
                            </div>


                        <?php elseif ($col['name'] == 'category'): ?>
                            <div class="col-lg-3 margin-bottom-15">
                                <label><?= $col['comment'] ?></label>
                                <select class="form-control kt-selectpicker" data-style="btn-warning"
                                        name="<?= $col['name'] ?>" data-size="7"
                                        data-live-search="true">
                                    <option value="">Seçiniz</option>
                                </select>
                            </div>

                        <?php elseif ($col['name'] == 'status'): ?>
                            <div class="col-lg-2 margin-bottom-15">
                                <label><?= $col['comment'] ?></label>
                                <select class="form-control" name="<?= $col['name'] ?>">
                                    <option value="1"><?=ln('BASE.active')?></option>
                                    <option value="0"><?=ln('BASE.passive')?></option>
                                </select>

                            </div>

                     
                        <?php elseif ($col['type'] == 'text'): ?>
                            <div class="col-lg-12 margin-bottom-15">
                                <label><?= $col['comment'] ?></label>
                                <div name="<?= $col['name'] ?>" class="summernote"></div>
                            </div>

                        <?php elseif (in_array($col['type'], array('int', 'float'))): ?>
                            <div class="col-lg-2 margin-bottom-15">
                                <label><?= $col['comment'] ?></label>
                                <input type="text" name="<?= $col['name'] ?>" class="form-control">
                            </div>


                        <?php else: ?>

                            <div class="col-lg-3 margin-bottom-15">
                                <label><?= $col['comment'] ?></label>
                                <input type="text" name="<?= $col['name'] ?>" class="form-control">
                            </div>

                        <?php endif; endforeach; ?>
                </div>
                <div class="kt-form__actions">
                    <button type="submit" class="btn btn-success btn-elevate btn-icon-sm float-right btn-sm"><i
                                class="la la-plus"></i><?=ln('BASE.add')?>
                    </button>
                </div>
            </div>
        </form>


    </div>
</div>

<script>

    document.addEventListener("DOMContentLoaded", function (event) {


        $('select[name="category"]').html(product_categories());


        $('form[id="<?=$appName?>"]').submit(function (e) {

            e.preventDefault();

            swal.fire({
                title: "<?=ln('MESSAGE.Processing')?>",
                text: "<?=ln('MESSAGE.Wait')?>",
                timer: 99999,
                onOpen: function () {
                    swal.showLoading()
                }
            }).then(function (e) {
                "timer" === e.dismiss && console.log("I was closed by the timer")
            });

            var serializedData = $('form[id="<?=$appName?>"]').serializeArray();
            serializedData.push({
                'name': 'content',
                'value': $('form[id="<?=$appName?>"] .summernote').summernote('code')
            });

            $.post($('form[id="<?=$appName?>"]').attr('action'), serializedData, function (response) {

                var response = JSON.parse(response);

                if (response.result == 1) {
                    $('form[id="<?=$appName?>"] button[type="submit"]').remove();
                    swal.fire({
                        type: 'success',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 1500
                    }).then(function (e) {

                        window.location.href = window.location.origin + '/App/Products/Edit/' + response.last_id;

                        "timer" === e.dismiss && console.log("I was closed by the timer")
                    });
                } else {
                    swal.fire({
                        type: 'error',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 6000
                    });
                }
            });
        });


        $('form[id="<?=$appName?>"] .summernote').summernote({height: 150});



    });


</script>