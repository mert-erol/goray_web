<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Ürün Kategorileri
                </h3>
            </div>


            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">

                        <?php if ($modal): ?>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">x
                            </button>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>

        <form id="<?= $appName ?>" action="<?= base_url('App/Products/SetCategory') ?>" method="post">
            <div class="kt-portlet__body">

                <div class="form-group row">
                    <label>Yeni Kategori Ekle</label>
                    <div class="input-group">
                        <input type="text" name="title" class="form-control" placeholder="Kategori Adı">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-brand btn-elevate btn-icon-sm float-right"><i
                                        class="la la-plus text-white"></i>Ekle
                            </button>
                        </div>
                    </div>

                </div>


            </div>
        </form>

    </div>
</div>

<script>

    <?php if(!$modal): ?>
    document.addEventListener("DOMContentLoaded", function (event) {
        <?php endif; ?>

        $('form[id="<?=$appName?>"]').submit(function (e) {

            e.preventDefault();

            swal.fire({
                title: "<?=ln('MESSAGE.Processing')?>",
                text: "<?=ln('MESSAGE.Wait')?>",
                timer: 99999,
                onOpen: function () {
                    swal.showLoading()
                }
            }).then(function (e) {
                "timer" === e.dismiss && console.log("I was closed by the timer")
            });


            var serializedData = $('form[id="<?=$appName?>"]').serializeArray();

            $.post($('form[id="<?=$appName?>"]').attr('action'), serializedData, function (response) {

                var response = JSON.parse(response);

                if (response.result == 1) {
                    // $('form[id="<?=$appName?>"] button[type="submit"]').remove();
                    swal.fire({
                        type: 'success',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 1500
                    }).then(function (e) {

                        <?php if(!$modal): ?>
                        $("#category-table").DataTable().ajax.reload();
                        <?php else: ?>
                        $('.modal').modal('hide');
                        <?php endif; ?>

                        "timer" === e.dismiss && console.log("I was closed by the timer")
                    });
                } else {
                    swal.fire({
                        type: 'error',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 6000
                    });
                }
            });
        });
        <?php if(!$modal): ?>
    });
    <?php endif; ?>

</script>
