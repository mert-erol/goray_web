<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Kategori Listesi
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <table class="table table-striped- table-bordered table-hover table-checkable" id="category-table">
                <thead>
                <tr>
                    <?php
                    $columns = '[';
                    foreach ($cols as $col):
                        $columns .= '{ data:"' . $col['name'] . '"},';
                        ?>
                        <th><?= $col['comment'] ?></th>
                    <?php endforeach;
                    $columns = $columns . ']';
                    ?>

                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


<script>

    <?php if(!$modal): ?>
    document.addEventListener("DOMContentLoaded", function (event) {
        <?php endif; ?>


        $("#category-table").DataTable({
            responsive: !0,
            searchDelay: 500,
            processing: !0,
            serverSide: !0,
            ajax: {url: window.location.origin + '/App/Products/CategoriesDatatables', method: 'post'},
            columns: <?=$columns?>,
            columnDefs: [{
                targets: 0,
                title: "#",
                orderable: !1,
                render: function (d) {
                   // return '<a href="javascript:;" onclick="alert('+d+')">EDIT</a>';
                    return '<button type="button" onclick="modal_trigger(\'<?=base_url('App/Products/Category')?>/'+d+'\'); return false;" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit">\n<i class="la la-edit"></i>\n</button>'
                }
            }]
        })
        <?php if(!$modal): ?>
    });
    <?php endif; ?>


</script>