<div class="col-lg-<?= $size ?>">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Kategori Düzenleme
                </h3>
            </div>


            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">

                        <?php if ($modal): ?>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">x
                            </button>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>

        <form id="<?= $appName ?>" action="<?= base_url('App/Products/Category/' . $data['id']) ?>" method="post">
            <div class="kt-portlet__body">

                <div class="form-group row">
                    <div class="input-group">
                        <input type="text" name="title" class="form-control" value="<?= $data['title'] ?>">
                        <div class="input-group-append">
                            <button type="button" id="<?= $appName ?>-delete" data-id="<?= $data['id'] ?>"
                                    class="btn btn-brand btn-danger btn-icon-sm float-right"><i
                                        class="la la-times text-white"></i>Sil
                            </button>

                            <button type="submit" class="btn btn-brand btn-success btn-icon-sm float-right"><i
                                        class="la la-save text-white"></i>Güncelle
                            </button>
                        </div>
                    </div>

                </div>


            </div>
        </form>

    </div>
</div>

<script>

    <?php if(!$modal): ?>
    document.addEventListener("DOMContentLoaded", function (event) {
        <?php endif; ?>

        $('form[id="<?=$appName?>"]').submit(function (e) {

            e.preventDefault();

            swal.fire({
                title: "<?=ln('MESSAGE.Processing')?>",
                text: "<?=ln('MESSAGE.Wait')?>",
                timer: 99999,
                onOpen: function () {
                    swal.showLoading()
                }
            }).then(function (e) {
                "timer" === e.dismiss && console.log("I was closed by the timer")
            });


            var serializedData = $(this).serializeArray();


            $.post($(this).attr('action'), serializedData, function (response) {

                var response = JSON.parse(response);

                if (response.result == 1) {
                    // $('form[id="<?=$appName?>"] button[type="submit"]').remove();
                    swal.fire({
                        type: 'success',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 1500
                    }).then(function (e) {

                        <?php if(!$modal): ?>
                        $("#category-table").DataTable().ajax.reload();
                        <?php else: ?>
                        $('.modal').modal('hide');
                        <?php endif; ?>

                        "timer" === e.dismiss && console.log("I was closed by the timer")
                    });
                } else {
                    swal.fire({
                        type: 'error',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 6000
                    });
                }
            });
        });


        $(document).on('click', '#<?=$appName?>-delete', function () {

            Swal.fire({
                title: 'Emin misiniz?',
                text: "Kalıcı olarak silinecektir.",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Evet',
                cancelButtonText: 'Hayır',
                cancelButtonColor: '#d33'
            }).then((result) => {
                if (result.value) {
                    var id = $(this).attr('data-id');
                    $.post(window.location.origin + '/App/Products/Category/' + id + '/Delete', {id: id}, function (response) {

                        if (response == 1) {
                            Swal.fire(
                                'Silindi!',
                                'Kayıt kalıcı olarak silindi.',
                                'success'
                            );

                            <?php if($modal): ?>
                            $('.modal').modal('hide');
                            $("#category-table").DataTable().ajax.reload();
                            <?php endif; ?>

                        } else {
                            Swal.fire(
                                'Silinemedi!',
                                'Lütfen sistem yöneticinize başvurun.',
                                'error'
                            );
                        }

                    });
                }
            });
        });


        <?php if(!$modal): ?>
    });
    <?php endif; ?>

</script>
