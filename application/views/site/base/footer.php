<footer class="footer ">

    <div class="container">
        <div class="row row-grid align-items-center my-md">
            <div class="col-lg-6">
                <h3 class="text-primary font-weight-light mb-2">Thank you for supporting us!</h3>
                <h4 class="mb-0 font-weight-light">Let's get in touch on any of these platforms.</h4>
            </div>
            <div class="col-lg-6 text-lg-center btn-wrapper">
                <a target="_blank" href="https://twitter.com/ceviriofficial"
                   class="btn btn-neutral btn-icon-only btn-twitter btn-round btn-lg" data-toggle="tooltip"
                   data-original-title="Follow us">
                    <i class="fa fa-twitter"></i>
                </a>
                <a target="_blank" href="https://www.facebook.com/ceviriofficial"
                   class="btn btn-neutral btn-icon-only btn-facebook btn-round btn-lg" data-toggle="tooltip"
                   data-original-title="Like us">
                    <i class="fa fa-facebook-square"></i>
                </a>
                <a target="_blank" href="https://www.instagram.com/cevir.io/"
                   class="btn btn-neutral btn-icon-only btn-dribbble btn-lg btn-round" data-toggle="tooltip"
                   style="background:#95a5a6;border-color:#95a5a6;" data-original-title="Follow us">
                    <i class="fa fa-instagram"></i>
                </a>

                <a target="_blank" href="http://linkedin.com/company/cevir-io"
                   class="btn btn-neutral btn-icon-only btn-dribbble btn-lg btn-round"
                   style="background:#1abc9c;border-color:#1abc9c;" data-toggle="tooltip"
                   data-original-title="Follow us">
                    <i class="fa fa-linkedin"></i>
                </a>

            </div>
        </div>
        <hr>
        <div class="row align-items-center justify-content-md-between">
            <div class="col-md-6">
                <div class="copyright">
                    &copy; 2020 <a href="/" target="_blank"></a>
                </div>
            </div>
            <div class="col-md-6">
                <ul class="nav nav-footer justify-content-end">
                    <li class="nav-item">
                        <a href="#" class="nav-link" target="_blank">Home</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link" target="_blank">About Us</a>
                    </li>
                    <li class="nav-item">
                        <a href="/en/contact" class="nav-link" target="_blank">Contact</a>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</footer>

<!-- Customizer modal -->
<div class="modal fade fixed-right" id="modal-customizer" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-vertical" role="document">
        <form class="modal-content" id="form-customizer">
            <div class="modal-body">
                <!-- Close -->
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-toggle="tooltip"
                        data-placement="left" title="Close Customizer">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="text-center mx-auto mt-4 mb-5" style="width: 80px;">
                    <img alt="Image placeholder"
                         src="<?= base_url('assets/site/img'); ?>/icons/essential/detailed/Click.svg"
                         class="svg-inject img-fluid">
                </div>
                <h5 class="text-center mb-2">Choose (your) Purpose</h5>
                <p class="text-center mb-4">
                    Customize your preview experience by selecting skin colors and modes.
                </p>
                <hr class="mb-4">
                <!-- Skin color -->
                <h6 class="mb-1">Skin color</h6>
                <p class="small text-muted mb-3">
                    Set a new theme color palette.
                </p>
                <div class="btn-group-toggle row mx-0 mb-5" data-toggle="buttons">
                    <label class="btn btn-sm btn-neutral active col mb-2">
                        <input type="radio" name="skin" value="default" checked>
                        Default
                    </label>
                    <label class="btn btn-sm btn-neutral col-6 mb-2 mr-0">
                        <input type="radio" name="skin" value="blue">
                        Blue
                    </label>
                    <!-- <label class="btn btn-sm btn-neutral col mb-2">
                <input type="radio" name="skin" value="blue">
                Blue
            </label>
            <label class="btn btn-sm btn-neutral col-6 mb-2 mr-0">
                <input type="radio" name="skin" value="red">
                Red
            </label> -->
                </div>
                <!-- Skin mode -->
                <h6 class="mb-1">Skin mode</h6>
                <p class="small text-muted mb-3">
                    Set the theme's mode: light or dark.
                </p>
                <div class="btn-group-toggle row mx-0 mb-4" data-toggle="buttons">
                    <label class="btn btn-sm btn-neutral active flex-fill mb-2 mr-2">
                        <input type="radio" name="mode" value="light" checked>
                        <i class="far fa-sun mr-2"></i> Light
                    </label>
                    <label class="btn btn-sm btn-neutral flex-fill mb-2 mr-2">
                        <input type="radio" name="mode" value="dark">
                        <i class="far fa-moon mr-2"></i> Dark
                    </label>
                </div>
            </div>
            <div class="modal-footer border-0">
                <button type="submit" class="btn btn-block btn-primary mt-auto">
                    Preview
                </button>
            </div>
        </form>
    </div>
</div>
<!-- Core JS - includes jquery, bootstrap, popper, in-view and sticky-kit -->
<script src="<?= base_url('assets/site/js'); ?>/purpose.core.js"></script>
<!-- Page JS -->
<script src="<?= base_url('assets/site/libs'); ?>/swiper/dist/js/swiper.min.js"></script>
<!-- Purpose JS -->
<script src="<?= base_url('assets/site/js'); ?>/purpose.js"></script>
<!-- Demo JS - remove it when starting your project -->
<script src="<?= base_url('assets/site/js'); ?>/demo.js"></script>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js"></script>


<script src="<?= base_url('assets/site'); ?>/libs/bootstrap-fileinput/5.0.9/js/plugins/piexif.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('assets/site'); ?>/libs/bootstrap-fileinput/5.0.9/js/plugins/sortable.min.js"
        type="text/javascript"></script>
<script src="<?= base_url('assets/site'); ?>/libs/bootstrap-fileinput/5.0.9/js/plugins/purify.min.js"
        type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="<?= base_url('assets/site'); ?>/libs/bootstrap-fileinput/5.0.9/js/fileinput.js"></script>
<script src="<?= base_url('assets/site'); ?>/libs/bootstrap-fileinput/5.0.9/themes/fa/theme.js"></script>
<script src="<?= base_url('assets/site'); ?>/libs/bootstrap-fileinput/5.0.9/js/locales/LANG.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-136783901-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-136783901-1');
</script>

<script>
    $('select').select2();
</script>

</body>


</html>
