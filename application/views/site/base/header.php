<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description"
          content="<?= get_sess('meta_description') ?>">
    <meta name="keywords"
          content="<?= get_sess('meta_keywords') ?>">
    <meta name="author" content="artificial.works">
    <title><?= strlen(get_sess('title')) > 0 ? get_sess('title') . ' - ' : '' ?>CEVIR.IO </title>
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url("assets/site/img/favicon"); ?>/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url("assets/site/img/favicon"); ?>/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url("assets/site/img/favicon"); ?>/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url("assets/site/img/favicon"); ?>/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114"
          href="<?= base_url("assets/site/img/favicon"); ?>/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120"
          href="<?= base_url("assets/site/img/favicon"); ?>/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144"
          href="<?= base_url("assets/site/img/favicon"); ?>/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152"
          href="<?= base_url("assets/site/img/favicon"); ?>/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180"
          href="<?= base_url("assets/site/img/favicon"); ?>/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"
          href="<?= base_url("assets/site/img/favicon"); ?>/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32"
          href="<?= base_url("assets/site/img/favicon"); ?>/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96"
          href="<?= base_url("assets/site/img/favicon"); ?>/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16"
          href="<?= base_url("assets/site/img/favicon"); ?>/favicon-16x16.png">
    <link rel="manifest" href="<?= base_url("assets/site/img/favicon"); ?>/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= base_url("assets/site/img/favicon"); ?>/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- Font Awesome 5 -->
    <link rel="stylesheet" href="<?= base_url("assets/site/libs"); ?>/@fortawesome/fontawesome-pro/css/all.min.css">
    <!-- Page CSS -->
    <link rel="stylesheet" href="<?= base_url("assets/site/libs"); ?>/swiper/dist/css/swiper.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Purpose CSS -->
    <link rel="stylesheet" href="<?= base_url("assets/site/css"); ?>/purpose.css" id="stylesheet">
    <link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@3/dark.css" rel="stylesheet">

    <link href="<?= base_url('assets/site/'); ?>libs/bootstrap-fileinput/5.0.9/css/fileinput.min.css" media="all"
          rel="stylesheet" type="text/css"/>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet"/>


    <style>
        .select2-container .select2-selection--single {
            height: inherit !important;
        }

        #service-type .btn-group .btn {
            margin: 5px;
            border-radius: 0px;
        }
    </style>

</head>

<body>
<!-- <div class="alert alert-danger bg-gradient-danger text-white fixed-top alert-flush alert-dismissible    border-0 shadow-lg fade show mb-0" role="alert">
    <div class="container">
        The heat of the summer comes with a <strong>35% discount</strong>. Use the <strong>SUMMER35</strong> code until June 15h and get started with this UI Kit to build your next amazing website.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span class="text-white opacity-10" aria-hidden="true">&times;</span>
        </button>
    </div>
</div> -->
<header class="header bg-dark">
    <!-- Topbar -->
            