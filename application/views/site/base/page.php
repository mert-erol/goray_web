<div class="main-content page">
    <?php if (app_auth_check('edit-pages')): ?>
        <div class="customizer">
            <a href="<?= base_url('App/Pages/Edit/' . $page_data['id']) ?>"
               title="Edit Page"
               class="btn btn-lg btn-danger btn-icon-only rounded-circle hover-scale-110 shadow-lg mr-3 d-none d-lg-inline-block">
                <span class="btn-inner--icon"><i class="far fa-edit"></i></span>
            </a>

        </div>
    <?php endif; ?>
    <section class="slice slice-lg bg-gradient-dark" style="padding-top: 205px;" data-offset-top="#header-main">
        <div class="section-inner bg-gradient-primary"></div>
        <!-- SVG illustration -->
        <div class="pt-7 position-absolute middle right-0 col-lg-7 col-xl-6 d-none d-lg-block">
            <figure class="w-100" style="max-width: 1000px;">
                <img alt="Image placeholder" src="<?= base_url('assets/site/img') ?>/svg/illustrations/work-chat.svg"
                     class="svg-inject img-fluid" style="height: 1000px;">
            </figure>
        </div>
        <!-- SVG background -->
        <div class="bg-absolute-cover bg-size--contain d-flex align-items-center">
            <figure class="w-100 d-none d-lg-block">
                <img alt="Image placeholder" src="<?= base_url('assets/site/img') ?>/svg/backgrounds/bg-4.svg"
                     class="svg-inject" style="height: 1000px;">
            </figure>
        </div>
        <!-- Hero container -->


        <div class="container py-5 pt-lg-6 d-flex align-items-center position-relative zindex-100">

            <div class="container py-lg-md d-flex">
                <div class="col px-0">
                    <div class="row">
                        <div class="col-lg-12">
                            <h3 class="text-white"><?= $page_data['title'] ?><span></span></h3>
                            <p class="lead  text-white"><?= $page_data['description'] ?></p>


                            <form action="" method="post">
                                <div class="row">


                                    <div class="col-md-4">
                                        <div class="form-group">


                                            <select name="" id="" class="form-control form-control-sm">
                                                <?php foreach ($translationSettings['languages'] as $language): ?>
                                                    <option <?= $page_data['origin'] == $language['ref_id'] ? 'selected' : '' ?>
                                                            value="<?= $language['ref_id'] ?>"> <?= $language['title'] ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select name="" id="" class="form-control form-control-sm">
                                                <?php foreach ($translationSettings['languages'] as $language): ?>
                                                    <option <?= $page_data['target'] == $language['ref_id'] ? 'selected' : '' ?>
                                                            value="<?= $language['ref_id'] ?>"> <?= $language['title'] ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select name="" id="" class="form-control form-control-sm">
                                                <?php foreach ($translationSettings['serviceTypes'] as $service): ?>
                                                    <option
                                                        <?= $page_data['service'] == $service['ref_id'] ? 'selected' : '' ?>
                                                            value="<?= $service['ref_id'] ?>"> <?= $service['title'] ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>


                                <div class="btn-wrapper ">
                                    <a href="<?= base_url('order') ?>" class="btn btn-info btn-icon mb-3 mb-sm-0">
                                        <span class="btn-inner--icon"><i class="fa fa-chevron-right"></i></span>
                                        <span class="btn-inner--text">Click to place your order !</span>
                                    </a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </section>
    <?= $page_data['content'] ?>

    <section class="bg-gradient-warning">
        <div class="container">
            <div class="row">
                <div class="col-md-12 kt-mb-20 faq-card">
                    <div class="d-flex px-3">
                        <div>
                            <div class="icon icon-lg icon-shape bg-gradient-white shadow rounded-circle text-primary">
                                <i class="far fa-question text-primary"></i>
                            </div>
                        </div>
                        <div class="pl-4">
                            <h4 class="display-4 text-white">Frequently Asked Questions</h4>
                        </div>
                    </div>

                </div>

                <?php $faqs = json_decode($page_data['faq'], true);
                foreach ($faqs as $faq):
                    ?>
                    <div class="col-md-12">
                        <div class="card shadow hover-shadow-lg border-0 position-relative zindex-100">
                            <div class="card-body py-5">
                                <div class="d-flex align-items-start">
                                    <div class="icon-text">
                                        <h3 class="h5 text-success"><?= $faq['q'] ?></h3>
                                        <p class=" mb-0"><?= $faq['a'] ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php endforeach; ?>
            </div>

        </div>
    </section>
    <section class="section section-lg bg-gradient-default">
        <div class="container pt-lg pb-300">
            <div class="row text-center justify-content-center">
                <div class="col-lg-10">
                    <h2 class="display-5 text-white">Comments</h2>
                </div>
            </div>


        </div>
        <!-- SVG separator -->
        <div class="separator separator-bottom separator-skew zindex-100">
            <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1"
                 xmlns="http://www.w3.org/2000/svg">
                <polygon class="fill-white" points="2560 0 2560 100 0 100"></polygon>
            </svg>
        </div>
    </section>
    <section class="section section-lg pt-lg-0 mt--200 no-background-color">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="row row-grid">

                        <?php foreach ($page_comments as $comment): ?>
                            <div class="col-lg-3">
                                <div class="card card-lift--hover shadow border-0">
                                    <div class="card-body py-5">
                                        <div class="icon icon-shape icon-shape-success rounded-circle mb-4">
                                            <i class="fas fa-quote-left"></i>
                                        </div>
                                        <!-- <h6 class="text-success text-uppercase">Build Something</h6> -->
                                        <p class="description mt-3"><?= $comment['comment'] ?></p>
                                        <div>
                                            <?php for ($i = 0; $i < $comment['star']; $i++): ?>
                                                <i class="fa fa-star" style="color:gold;"></i>
                                            <?php endfor; ?>
                                        </div>

                                        <div style="margin-top:5px;">
                                            <span class="badge badge-pill badge-success"><?= $comment['author'] ?></span>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>


                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
          
          

