<!-- Main navbar -->
<nav class="navbar navbar-main navbar-expand-lg navbar-transparent navbar-dark navbar-collapsed" id="navbar-main">
    <div class="container px-lg-0">
        <!-- Logo -->
        <a class="navbar-brand mr-lg-5" href="<?= base_url() ?>">
            <img alt="Image placeholder" src="<?= base_url('assets/site/img'); ?>/logo.png" id="navbar-logo"
                 style="height: 50px;">
        </a>
        <!-- Navbar collapse trigger -->
        <button class="navbar-toggler pr-0" type="button" data-toggle="collapse" data-target="#navbar-main-collapse"
                aria-controls="navbar-main-collapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Navbar nav -->
        <div class="collapse navbar-collapse" id="navbar-main-collapse">
            <ul class="navbar-nav align-items-lg-center">
                <!-- Home - Overview  -->
                <li class="nav-item active">
                    <a class="nav-link" href="<?= base_url() ?>">About</a>
                </li>
                <!-- Pages menu -->
                <li class="nav-item dropdown dropdown-animate" data-toggle="hover">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">Services</a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-arrow p-0">
                        <ul class="list-group list-group-flush">
                            <?php $services = get_pages(['category' => [3]]);
                            foreach ($services as $service):
                                if (strlen($service['title']) > 0):
                                    ?>
                                    <li>
                                        <a href="<?= base_url($service['permalink']) ?>"
                                           class="list-group-item list-group-item-action">
                                            <div class="media d-flex align-items-center">
                                                <!-- Media body -->
                                                <div class="media-body ml-3">
                                                    <h6 class="mb-1"><?= $service['title'] ?></h6>
                                                </div>
                                            </div>
                                        </a>

                                    </li>
                                <?php endif; endforeach; ?>

                        </ul>

                    </div>
                </li>
                <li class="nav-item d-lg-none d-xl-block">
                    <a class="nav-link" href="<?= base_url('order') ?>">Order</a>
                </li>

            </ul>
            <div id="navbar-top-main" class="navbar-top  navbar-dark bg-dark">
                <div class="container px-0">
                    <div class="navbar-nav align-items-center">
                        <div class="d-none d-lg-inline-block">
                            <hr width="310" style="border: 1px solid transparent;"></hr>
                        </div>
                        <div style="display: none">
                            <ul class="nav">
                                <li class="nav-item dropdown ml-lg-2">
                                    <a class="nav-link px-0" href="#" role="button" data-toggle="dropdown"
                                       aria-haspopup="true" aria-expanded="false" data-offset="0,10">
                                        <img alt="Image placeholder"
                                             src="<?= base_url('assets/site/img'); ?>/icons/flags/us.svg">
                                        <span class="d-none d-lg-inline-block">United States</span>
                                        <span class="d-lg-none">EN</span>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-sm">
                                        <a href="#" class="dropdown-item"><img alt="Image placeholder"
                                                                               src="<?= base_url('assets/site/img'); ?>/icons/flags/es.svg">Spanish</a>
                                        <a href="#" class="dropdown-item"><img alt="Image placeholder"
                                                                               src="<?= base_url('assets/site/img'); ?>/icons/flags/ro.svg">Romanian</a>
                                        <a href="#" class="dropdown-item"><img alt="Image placeholder"
                                                                               src="<?= base_url('assets/site/img'); ?>/icons/flags/gr.svg">Greek</a>
                                    </div>
                                </li>
                            </ul>
                        </div>

                        <?php if ($this->session->userdata('front_login')): ?>
                            <div class="ml-auto">
                                <ul class="nav">
                                    <?php if ($this->session->userdata('app_login')): ?>
                                        <li class="nav-item">
                                            <a class="nav-link"
                                               href="<?= base_url('App/Dashboard') ?>"><?= ln('FRONT.Panel') ?></a>
                                        </li>
                                    <?php endif; ?>

                                    <li class="nav-item">
                                        <a class="nav-link" href="<?= base_url() ?>"><i
                                                    class="far fa-shopping-cart"></i></a>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link pr-0" href="#" data-toggle="dropdown" aria-haspopup="true"
                                           aria-expanded="false">
                                            <?= $userSettings['name_surname'] ?>
                                            <i class="far fa-user-circle"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                            <h6 class="dropdown-header">User menu</h6>
                                            <a class="dropdown-item" href="<?= base_url('account') ?>">
                                                <i class="far fa-user"></i>Account
                                            </a>
                                            <a class="dropdown-item" href="<?= base_url('account/orders') ?>">
                                                <i class="far fa-envelope"></i>Orders
                                            </a>
                                            <div class="dropdown-divider" role="presentation"></div>
                                            <a class="dropdown-item" href="<?= base_url('logout') ?>">
                                                <i class="far fa-sign-out-alt"></i>Sign out
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                        <?php else: ?>

                            <div class="ml-auto">
                                <ul class="nav">
                                    <li class="nav-item">
                                        <a class="nav-link"
                                           href="<?= base_url('login') ?>"><?= ln('AUTH.Login') ?></a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link"
                                           href="<?= base_url('register') ?>">Register</a>
                                    </li>
                                </ul>
                            </div>

                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>
</header>