<!-- Omnisearch -->
<div id="omnisearch" class="omnisearch">
    <div class="container">
        <!-- Search form -->
        <form class="omnisearch-form">
            <div class="form-group">
                <div class="input-group input-group-merge input-group-flush">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-search"></i></span>
                    </div>
                    <input type="text" class="form-control" placeholder="Type and hit enter ...">
                </div>
            </div>
        </form>
        <div class="omnisearch-suggestions">
            <h6 class="heading">Search Suggestions</h6>
            <div class="row">
                <div class="col-sm-6">
                    <ul class="list-unstyled mb-0">
                        <li>
                            <a class="list-link" href="#">
                                <i class="far fa-search"></i>
                                <span>macbook pro</span> in Laptops
                            </a>
                        </li>
                        <li>
                            <a class="list-link" href="#">
                                <i class="far fa-search"></i>
                                <span>iphone 8</span> in Smartphones
                            </a>
                        </li>
                        <li>
                            <a class="list-link" href="#">
                                <i class="far fa-search"></i>
                                <span>macbook pro</span> in Laptops
                            </a>
                        </li>
                        <li>
                            <a class="list-link" href="#">
                                <i class="far fa-search"></i>
                                <span>beats pro solo 3</span> in Headphones
                            </a>
                        </li>
                        <li>
                            <a class="list-link" href="#">
                                <i class="far fa-search"></i>
                                <span>smasung galaxy 10</span> in Phones
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="main-content">
    <!-- Header (v1) -->
    <section class="slice slice-lg bg-gradient-dark" style="padding-top: 205px;" data-offset-top="#header-main">
        <div class="section-inner bg-gradient-primary"></div>
        <!-- SVG illustration -->
        <div class="pt-7 position-absolute middle right-0 col-lg-7 col-xl-6 d-none d-lg-block">
            <figure class="w-100" style="max-width: 1000px;">
                <img alt="Image placeholder" src="<?= base_url('assets/site/img') ?>/svg/illustrations/work-chat.svg"
                     class="svg-inject img-fluid" style="height: 1000px;">
            </figure>
        </div>
        <!-- SVG background -->
        <div class="bg-absolute-cover bg-size--contain d-flex align-items-center">
            <figure class="w-100 d-none d-lg-block">
                <img alt="Image placeholder" src="<?= base_url('assets/site/img') ?>/svg/backgrounds/bg-4.svg"
                     class="svg-inject" style="height: 1000px;">
            </figure>
        </div>
        <!-- Hero container -->


        <div class="container py-5 pt-lg-6 d-flex align-items-center position-relative zindex-100">

            <div class="container py-lg-md d-flex">
                <div class="col px-0">
                    <div class="row">
                        <div class="col-lg-12">
                            <h3 class="text-white">Professional Translation Services<span></span></h3>
                            <p class="lead  text-white">Have your document translated into more than
                                70 languages by professional translators.</p>


                            <form action="" method="post">
                                <div class="row">


                                    <div class="col-md-4">
                                        <div class="form-group">


                                            <select name="" id="" class="form-control form-control-sm">
                                                <?php foreach ($translationSettings['languages'] as $language): ?>
                                                    <option value="<?= $language['ref_id'] ?>" <?= $language['title'] == 'English' ? 'selected' : '' ?>> <?= $language['title'] ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select name="" id="" class="form-control form-control-sm">
                                                <?php foreach ($translationSettings['languages'] as $language): ?>
                                                    <option value="<?= $language['ref_id'] ?>" <?= $language['title'] == 'Spanish' ? 'selected' : '' ?>> <?= $language['title'] ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select name="" id="" class="form-control form-control-sm">
                                                <?php foreach ($translationSettings['serviceTypes'] as $service): ?>
                                                    <option value="<?= $service['ref_id'] ?>"> <?= $service['title'] ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>

                                </div>


                                <div class="btn-wrapper ">
                                    <a href="<?= base_url('order') ?>" class="btn btn-info btn-icon mb-3 mb-sm-0">
                                        <span class="btn-inner--icon"><i class="fa fa-chevron-right"></i></span>
                                        <span class="btn-inner--text">Click to place your order !</span>
                                    </a>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </section>
    <section>
        <div class="position-relative p-5 bg-dark">
            <a href="#" class="tongue tongue-bottom tongue-primary">
                <i class="fas fa-angle-down"></i>
            </a>
        </div>
    </section>
    <!-- Features (v1) -->
    <section id="sct-page-examples" class="slice bg-section-secondary">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <div class="card text-center hover-shadow-lg hover-translate-y-n10">
                        <div class="px-4 py-5">
                            <img alt="Image placeholder"
                                 src="<?= base_url('assets/site/img') ?>/icons/essential/detailed/Code_2.svg"
                                 class="svg-inject" style="height: 70px;">
                        </div>
                        <div class="px-4 pb-5">


                            <h6 class="text-primary text-uppercase">Online Certified Translation</h6>
                            <p class="description mt-3">Once you have submitted your documents, specified the language
                                pair, and made your payment online, we will assign the translation job to an expert
                                translator.</p>
                            <div>
                                <span class="badge badge-pill badge-primary">Fast</span>
                                <span class="badge badge-pill badge-primary">Responsive</span>
                                <span class="badge badge-pill badge-primary">User-friendly</span>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="card text-center hover-shadow-lg hover-translate-y-n10">
                        <div class="px-4 py-5">
                            <img alt="Image placeholder"
                                 src="<?= base_url('assets/site/img') ?>/icons/essential/detailed/Code.svg"
                                 class="svg-inject" style="height: 70px;">
                        </div>
                        <div class="px-4 pb-5">
                            <h6 class="text-success text-uppercase">Professional Translation Service</h6>
                            <p class="description mt-3">Translations that are checked by our proofreaders and project
                                managers will be delivered on your terms, always on time, in any desired file
                                format.</p>
                            <div>
                                <span class="badge badge-pill badge-success">Professional</span>
                                <span class="badge badge-pill badge-success">Reliable</span>
                                <span class="badge badge-pill badge-success">High-quality</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="card text-center hover-shadow-lg hover-translate-y-n10">
                        <div class="px-4 py-5">
                            <img alt="Image placeholder"
                                 src="<?= base_url('assets/site/img') ?>/icons/essential/detailed/Secure_Files.svg"
                                 class="svg-inject" style="height: 70px;">
                        </div>
                        <div class="px-4 pb-5">
                            <h6 class="text-warning text-uppercase">SEO Friendly Translation</h6>
                            <p class="description mt-3">Heavily invested in SEO? Don't let a poorly made translation
                                affect your rankings on search engines. We make sure that your translation is SEO
                                friendly.</p>
                            <div>
                                <span class="badge badge-pill badge-warning">Multilingual SEO | Optimized Translation</span>
                                <span class="badge badge-pill badge-warning">SEO Localization</span>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

    </section>
    <!-- Features (v2) -->
    <section class="slice slice-lg">
        <div class="container">
            <div class="row row-grid justify-content-around align-items-center">
                <div class="col-lg-5 order-lg-2">
                    <div class=" pr-lg-4">
                        <h5 class=" h3">Code-friendly Translation
                        </h5>
                        <p class="lead my-4">Cevir.io delivers professional localization service and lets you translate
                            or localize mobile apps, websites, and marketing material with its continuous localization
                            solutions.</p>
                        <ul class="list-unstyled">
                            <li class="py-2">
                                <div class="d-flex align-items-center">
                                    <div>
                                        <div class="icon icon-shape icon-primary icon-sm rounded-circle mr-3">
                                            <i class="far fa-store-alt"></i>
                                        </div>
                                    </div>
                                    <div>
                                                <span class="h6 mb-0">No need to worry about missing strings.
</span>
                                    </div>
                                </div>
                            </li>
                            <li class="py-2">
                                <div class="d-flex align-items-center">
                                    <div>
                                        <div class="icon icon-shape icon-warning icon-sm rounded-circle mr-3">
                                            <i class="far fa-palette"></i>
                                        </div>
                                    </div>
                                    <div>
                                                <span class="h6 mb-0">We are aware of the character limitations.
</span>
                                    </div>
                                </div>
                            </li>
                            <li class="py-2">
                                <div class="d-flex align-items-center">
                                    <div>
                                        <div class="icon icon-shape icon-success icon-sm rounded-circle mr-3">
                                            <i class="far fa-cog"></i>
                                        </div>
                                    </div>
                                    <div>
                                                <span class="h6 mb-0">Terminology management at its best.
</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 order-lg-1">
                    <img alt="Image placeholder" src="<?= base_url('assets/site/img') ?>/theme/light/presentation-1.png"
                         class="img-fluid img-center">
                </div>
            </div>
        </div>
    </section>
    <!-- Features (v3) -->

    <!-- Features (v4) -->
    <section class="slice slice-lg bg-section-secondary overflow-hidden">

    </section>
    <!-- Features (v5) -->
    <section class="slice slice-xl has-floating-items bg-gradient-primary" id=sct-call-to-action><a
                href="#sct-call-to-action" class="tongue tongue-up tongue-section-secondary" data-scroll-to>
            <i class="far fa-angle-up"></i>
        </a>
        <div class="container">
            <div class="row row-grid align-items-center">
                <div class="col-md-6 order-lg-2 ml-lg-auto">
                    <div class="position-relative pl-md-5">
                        <img src="<?= base_url('assets/site/img') ?>/ill-2.svg" class="img-center img-fluid"
                             alt="image">
                    </div>
                </div>
                <div class="col-lg-6 order-lg-1">
                    <div class="d-flex px-3">
                        <div>
                            <div class="icon icon-lg icon-shape bg-gradient-white shadow rounded-circle text-primary">
                                <i class="ni ni-building text-primary"></i>
                            </div>
                        </div>
                        <div class="pl-2">
                            <h4 class="display-5 text-white">Effective Project Management</h4>
                            <p class="text-white">We have developed the responsive end-user panel of Cevir.io in a way
                                that ensures a smooth user experience.</p>
                        </div>
                    </div>
                    <div class="card shadow shadow-lg--hover mt-5">
                        <div class="card-body">
                            <div class="d-flex px-3">
                                <div>
                                    <div class="icon icon-shape bg-gradient-success rounded-circle text-white">
                                        <i class="ni ni-satisfied"></i>
                                    </div>
                                </div>
                                <div class="pl-4">
                                    <h5 class="title text-success">Keep track of your orders</h5>
                                    <p>You may effortlessly check what stage your order is at 24/7 through our
                                        platform.</p>
                                    <!-- <a href="#" class="text-success">Learn more</a> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card shadow shadow-lg--hover mt-5">
                        <div class="card-body">
                            <div class="d-flex px-3">
                                <div>
                                    <div class="icon icon-shape bg-gradient-warning rounded-circle text-white">
                                        <i class="ni ni-active-40"></i>
                                    </div>
                                </div>
                                <div class="pl-4">
                                    <h5 class="title text-warning">100% Satisfaction</h5>
                                    <p>We offer unlimited revisions in case of any dispute and make sure that all of our
                                        clients are satisfied to the fullest.</p>
                                    <!-- <a href="#" class="text-warning">Learn more</a> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- SVG separator -->

    </section>
    <!-- Testimonials (v1) -->
    <section class="slice slice-lg bg-section-secondary overflow-hidden">
        <div class="container">
            <div class="row row-grid align-items-center">
                <div class="col-md-6">
                    <div class="card bg-default shadow border-0">
                        <img src="<?= base_url('assets/site/img') ?>/img-1-1200x1000.jpg" class="card-img-top"
                             alt="image">
                        <blockquote class="card-blockquote">
                            <svg preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 583 95"
                                 class="svg-bg">
                                <polygon points="0,52 583,95 0,95" class="fill-default"/>
                                <polygon points="0,42 583,95 683,0 0,95" opacity=".2" class="fill-default"/>
                            </svg>
                            <h4 class="display-3 font-weight-bold text-white">Revolutionary</h4>
                            <p class="lead text-italic text-white">Cevir.io aims to build a sustainable business model
                                that has various advantages for both the translator and the client.</p>
                        </blockquote>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="pl-md-5">
                        <div class="icon icon-lg icon-shape icon-shape-warning shadow rounded-circle mb-5">
                            <i class="ni ni-settings"></i>
                        </div>
                        <h3>Join our expert translator team!</h3>
                        <p class="lead">Here at Cevir.io translation company, we truly admire your work. Submit the
                            translator application form below to join our professional translator team!</p>
                        <p>We will only get in touch with you when there is an online translation job available. We will
                            never assign you freelance translation jobs which are not relevant to your field of
                            expertise!</p>
                        <p>Thanks to our translation memory, you will not waste your time translating the same lines
                            over and over again. </p>
                        <a href="/en/translator-application" class="font-weight-bold text-warning mt-5">Apply Now</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Features (v7) -->

    <!-- Call to action (v10) -->
    <section class="slice slice-lg">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <h1>Links</h1>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <ul class="menu links-menu">
                        <?php
                        $random_pages = get_pages(['category' => [4], 'limit' => 15]);

                        foreach (array_slice($random_pages, 0, 5) as $page): ?>
                            <li><a href="<?= base_url($page['permalink']) ?>" target="_blank"><?= $page['title'] ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="menu links-menu">
                        <?php
                        foreach (array_slice($random_pages, 5, 10) as $page): ?>
                            <li><a href="<?= base_url($page['permalink']) ?>" target="_blank"><?= $page['title'] ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="menu links-menu">
                        <?php
                        foreach (array_slice($random_pages, 10, 15) as $page): ?>
                            <li><a href="<?= base_url($page['permalink']) ?>" target="_blank"><?= $page['title'] ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</div>
