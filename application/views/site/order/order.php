<!-- Omnisearch -->

<section class="section section-lg section section-lg section-shaped">
    <div class="container">
        <div class="row justify-content-center text-center ">
            <div class="col-lg-8">
                <h3 class="display-4" style="margin-top:10px;"><i class="ni ni-single-copy-04"
                                                                  style="color:#34495e;"></i> Order Details</h3>
            </div>
        </div>
    </div>

</section>
<section class="slice slice-lg bg-section-secondary" style="margin-top:80px;">

    <div class="container">
        <div class="row" style="margin-top:-160px;">
            <div class="col-lg-8">
                <div class="card bg-gradient-secondary shadow entry-area">
                    <div class="card-body">
                        <h4 class="mb-1">Text Entry</h4>
                        <div class="nav-wrapper">
                            <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text"
                                role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0 active" id="file-upload-tab"
                                       data-toggle="tab" href="#tabs-icons-text-1" role="tab"
                                       data-type="file"
                                       aria-controls="tabs-icons-text-1" aria-selected="true"><i
                                                class="ni ni-cloud-upload-96 mr-2"></i>Upload Document / Photo</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link mb-sm-3 mb-md-0" id="free-text-tab" data-toggle="tab"
                                       href="#tabs-icons-text-2" data-type="text" role="tab"
                                       aria-controls="tabs-icons-text-2"
                                       aria-selected="false"><i class="ni ni-ruler-pencil mr-2"></i>Type Text</a>
                                </li>

                            </ul>
                        </div>
                        <div class="" style="background:transparent;">
                            <div class="card-body" style="padding:0px;">
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel"
                                         aria-labelledby="file-upload-tab">


                                        <input type="file" multiple
                                               class="filesInput btn btn-default btn-round btn-block btn-lg showFilesInput"
                                               id="files-input" name="files[]" style="" name="attachments"/>


                                        <p class="description showText" style="margin-top:10px;margin-bottom:-10px;">
                                            * You can upload multiple files.</p>

                                    </div>
                                    <div class="tab-pane fade" id="tabs-icons-text-2" role="tabpanel"
                                         aria-labelledby="free-text-tab">
                                        <div class="form-group mb-4">
                                            <textarea class="form-control form-control-alternative" id="std-text"
                                                      name="name" rows="4"
                                                      cols="80"
                                                      placeholder="Type a message..."><?= isset($order_data['free_text']) ? $order_data['free_text'] : '' ?></textarea>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="card bg-gradient-secondary shadow">
                    <div class="card-body">
                        <h4 class="mb-1">Language Selection</h4>

                        <div class="form-group" style="margin-top:15px;">
                            <label for="prigin-language">Origin Language</label>
                            <select id="origin-language" class="form-control">
                                <option value="">Select</option>
                                <?php foreach ($translationSettings['languages'] as $language): ?>
                                    <option value="<?= $language['ref_id'] ?>"> <?= $language['title'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div class="form-group" style="margin-top:15px;">
                            <label for="prigin-language">Target Languages</label>
                            <select id="target-language" class="form-control" multiple>
                                <?php foreach ($translationSettings['languages'] as $language): ?>
                                    <option value="<?= $language['ref_id'] ?>"> <?= $language['title'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>


                </div>

                <div class="card bg-gradient-secondary shadow">
                    <div class="card-body">
                        <h4 class="mb-1">Service Type</h4>
                        <div id="service-type">
                            <div class="btn-group" data-toggle="buttons">

                                <?php foreach ($translationSettings['serviceTypes'] as $service): ?>

                                    <label for="chc-<?= $service['title'] ?>-<?= $service['id'] ?>"
                                           class="btn btn-primary">
                                        <input id="chc-<?= $service['title'] ?>-<?= $service['id'] ?>"
                                               type="radio"
                                               name="service-types[]"
                                               value="<?= $service['ref_id'] ?>"
                                               method="<?= $service['method'] ?>"
                                               title="<?= $service['title'] ?>"
                                               method-value="<?= $service['method_value'] ?>">
                                        <?= $service['title'] ?>
                                    </label>

                                <?php endforeach; ?>
                            </div>
                        </div>


                    </div>
                </div>

                <div class="card bg-gradient-secondary shadow" style="display: none;">
                    <div class="card-body">
                        <h4 class="mb-1">Category</h4>

                        <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist"
                            style="margin-top:15px;">
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 active" id="file-upload-tab" data-toggle="tab"
                                   href="#tabs-icons-text-4" role="tab" aria-controls="tabs-icons-text-1"
                                   aria-selected="true">Professional Translator</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="free-text-tab" data-toggle="tab"
                                   href="#tabs-icons-text-5" role="tab" aria-controls="tabs-icons-text-2"
                                   aria-selected="false">Certified Translator</a>
                            </li>


                        </ul>


                    </div>
                </div>

                <div class="card bg-gradient-secondary shadow" style="display: none">
                    <div class="card-body">
                        <h4 class="mb-1">Translation Duration</h4>

                        <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist"
                            style="margin-top:15px;">
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0 active" id="file-upload-tab" data-toggle="tab"
                                   href="#tabs-icons-text-4" role="tab" aria-controls="tabs-icons-text-1"
                                   aria-selected="true">Standard Translation</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link mb-sm-3 mb-md-0" id="free-text-tab" data-toggle="tab"
                                   href="#tabs-icons-text-5" role="tab" aria-controls="tabs-icons-text-2"
                                   aria-selected="false">Quick Translation</a>
                            </li>
                        </ul>


                    </div>
                </div>

                <div class="card bg-gradient-secondary shadow" style="display: none">
                    <div class="card-body">
                        <h4 class="mb-1">Additional Services</h4>

                        <div class="custom-control custom-checkbox mb-3" style="margin-top:15px;">
                            <input class="custom-control-input" id="customCheck1" type="checkbox">
                            <label class="custom-control-label" for="customCheck1" style="">I need DTP Services for my
                                documents.</label>
                        </div>


                    </div>
                </div>

                <div class="card bg-gradient-secondary shadow">
                    <div class="card-body">
                        <h4 class="mb-1">Notes</h4>
                        <div class="form-group mb-4" style="margin-top:15px;">
                            <textarea class="form-control form-control-alternative" name="name" rows="4" cols="80"
                                      placeholder="Type a note..."
                                      id="order-notes"><?= isset($order_data['notes']) ? $order_data['notes'] : '' ?></textarea>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 " id="summary-card">
                <div class="card bg-gradient-secondary shadow position-sticky sticky-top">
                    <div class="card-body">
                        <h4 class="mb-1">Summary</h4>
                        <ul style="padding-left:18px;">
                            <li>
                                Total Words: <span class="total-words">0</span>
                            </li>
                            <li> Origin Language: <span class="origin-language"></span></li>
                            <li> Target Language(s): <span class="target-languages"></span></li>
                            <li> Service Type(s): <span class="service-types"></span></li>
                            <li> Estimated Date: <span class="estimated-date"></span></li>
                        </ul>


                        <h5 class="mb-1">Total: <span style="position:relative;float:right;"
                                                      class="total-amount">0.00 </span></h5>

                        <h5 class="mb-1 text-warning discount-row" style="display: none">Discount: <span
                                    style="position:relative;float:right;"
                                    class="total-discount">- 0.00 </span></h5>

                        <h5 class="mb-1 text-success grand-total-row" style="display: none">Grand Total: <span
                                    style="position:relative;float:right;"
                                    class="grand-total">0.00 </span></h5>


                        <div id="coupon-area" style="margin-bottom:10px;">
                            <button type="button"
                                    style="display: none" id="coupon-text"
                                    data-valid="false"
                                    class="btn btn-dark btn-block btn-sm">
                                Do you have a coupon code ?
                            </button>

                            <label for="" class="text-danger" style="display: none">
                                <fa class="fas fa-info"></fa>
                                The coupon code is invalid.
                            </label>
                            <input type="text" class="form-control form-control-sm" id="coupon-input"
                                   style="display:none"
                                   placeholder="Coupon Code...">
                        </div>
                        <div>
                            <button type="button" id="get-order" get-order="false"
                                    class="btn btn-primary btn-block btn-lg">
                                Calculate
                            </button>

                            <a href="<?= base_url('account/orders') ?>"
                               style="display: none" id="place-order"
                               class="btn btn-dark btn-block btn-lg">
                                Place Order
                            </a>


                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>


</section>


<script>

    document.addEventListener("DOMContentLoaded", function (event) {


        $(document).ready(function () {
            $("#files-input").fileinput({
                uploadUrl: window.location.origin + "/File/Upload",
                maxFileCount: 99,
                showRemove: false,
                mainClass: "input-group-lg",
                <?php if(count($exist_files) > 0): ?>
                validateInitialCount: true,
                overwriteInitial: false,
                initialPreviewAsData: true,
                initialPreview: <?=$initialPreview?>,
                initialPreviewConfig: <?=$initialPreviewConfig?>,
                <?php  endif; ?>
                allowedFileExtensions: ['png', 'jpg', 'jpeg', 'tiff', 'bmp', 'gif', 'tif', 'html', 'csv', 'txt', 'eml', 'json', "htm", "rtf", "docx", "doc", "xlsx", "xls", "ppt", "pptx", "pdf"]
            });
        });

        $(document).on('click', '.fileinput-upload-button', function () {
            place_order.hide();
            $("#files-input").fileinput('upload');
        });


        $(document).on('change', '#origin-language', function () {
            place_order.hide();
            var org_lang = $('#origin-language').val();
            if (org_lang.length > 0) {
                $('#target-language').html(get_target_langs(org_lang));
                $('#target-language option[value="' + org_lang + '"]').attr('disabled', 'disabled');
                $('#target-language option[value!="' + org_lang + '"]').removeAttr('disabled');
            }
        });

        $(document).on('change', '#origin-language,#target-language', function (e) {
            e.preventDefault();
            place_order.hide();
            $('span.origin-language').html($('#origin-language option:selected').text());
            $('span.target-languages').html(get_selected_target_languages());
        });

        $(document).on('click', '#get-order', function () {
            calculate();

            if ($(this).attr('get-order')) {

            }
        });


        $(document).on('click', '.kv-file-remove', function () {
            var deleted_file_name = $(this).parent().parent().parent().find('.file-footer-caption').attr('title');
            $.post(window.location.origin + '/order/file-delete', {key: deleted_file_name}, function () {
            });
            place_order.hide();
        });

        $(document).on('change', 'input[name^="service-types"]', function () {
            var styps = get_service_types(true);
            var styps_string = '';
            for (var i = 0; i < styps.length; i++) {
                styps_string += styps[i].toString() + (i != styps.length - 1 ? ',' : '');
            }
            $('span.service-types').html(styps_string);
            place_order.hide();
        });

        $(document).on('click', '#coupon-text', function () {
            coupon.get_input();
        });


        (function ($) {
            $.fn.donetyping = function (callback) {
                var _this = $(this);
                var x_timer;
                _this.keyup(function () {
                    clearTimeout(x_timer);
                    x_timer = setTimeout(clear_timer, 3000);
                });

                function clear_timer() {
                    clearTimeout(x_timer);
                    callback.call(_this);
                }
            }
        })(jQuery);

        $('#std-text').donetyping(function (callback) {
            if (get_selected_target_languages().length > 0) {
                var std_txt_cnt = get_std_text_word_count();
                place_order.hide();
                calculate(std_txt_cnt);
            } else {
                swal.close();
                swal.fire({
                    type: 'error',
                    title: 'Please select target language',
                    showConfirmButton: true
                });
            }
        });

        $(document).on('change', '#origin-language,#target-language,#std-text,input[name^="service-types"]', function () {
            $('#get-order').html('Calculate');
        });




        <?php if(isset($order_data['origin_language'])): ?>
        $('#origin-language').val(<?=$order_data['origin_language']?>);
        $('#origin-language').change();
        <?php endif; ?>

        <?php if(isset($order_data['origin_language'])): ?>
        $('#target-language').val(<?=$order_data['target_languages']?>);
        $('#target-language').change();
        <?php endif; ?>

        <?php if(isset($order_data['service_types'])):
        foreach (json_decode($order_data['service_types'], FALSE) as $stypes):
        ?>
        $('input[name^="service-types"][value="<?=$stypes?>"]').parent().click();
        <?php endforeach; endif; ?>

        <?php if(isset($order_data['free_text'])):
        if(strlen($order_data['free_text']) > 0): ?>
        $('#free-text-tab').tab('show');
        calculate();
        <?php endif; endif; ?>

        <?php if(isset($order_data['user_files'])):
        if(count(json_decode($order_data['user_files'], FALSE)) > 0): ?>
        calculate();
        <?php endif; endif; ?>
    });


    function get_target_langs(lang_ref_id) {
        lang_ref_id = lang_ref_id || 0;
        var options = '';
        $.ajax({
            url: window.location.origin + '/Site/Order/GetScenarios/' + lang_ref_id,
            method: 'get',
            async: false,
            success: function (response) {
                response = JSON.parse(response);
                $.each(response, function (i, v) {
                    options += '<option value="' + v.ref_id + '">' + v.title + '</option>';
                });

            }
        });
        return options;
    }

    function calculate(std_txt_cnt) {

        var tab_type = $('.entry-area').find('.nav-link.active').data('type');

        if (tab_type == 'file') {
            std_txt_cnt = -1;
        } else {
            std_txt_cnt = get_std_text_word_count() || 0;
        }

        var origin_language = $('#origin-language').val();
        var target_language = $('#target-language').val();

        if (origin_language.length == 0) {
            swal.close();
            swal.fire({
                type: 'error',
                title: 'Please select origin language',
                showConfirmButton: true,
            });
        }

        else if (target_language.length > 0) {
            var data_set = {
                'origin_language': origin_language,
                'target_languages': target_language,
                'service_types': get_service_types(),
                'std_word_count': std_txt_cnt,
                'free_text': $('#std-text').val(),
                'order_notes': $('#order-notes').val(),
                'coupon_code': $('#coupon-input').val()
            };

            swal.close();
            swal.fire({
                title: "<?=ln('MESSAGE.Processing')?>",
                text: "<?=ln('MESSAGE.Wait')?>",
                timer: 999999,
                onOpen: function () {
                    swal.showLoading()
                }
            }).then(function (e) {
                "timer" === e.dismiss && console.log("I was closed by the timer")
            });

            $.post(window.location.origin + '/calculate', data_set, function (response) {
                response = JSON.parse(response);
                console.log(response);
                if (response.total_words == 0) {
                    swal.close();
                    swal.fire({
                        title: "Please enter text or upload files",
                        text: "The entry can´t be null",
                        showConfirmButton: true
                    });
                } else {
                    swal.close();
                    $('span.total-words').html(response.total_words);
                    $('span.total-amount').html(response.total_amount.toFixed(2));
                    $('span.estimated-date').html(response.estimated_date);

                    $('span.origin-language').html($('#origin-language option:selected').text());
                    $('span.target-languages').html(get_selected_target_languages());


                    if (response.discount > 0) {
                        coupon.valid();

                        $('.discount-row').css('display', 'inherit');
                        $('.discount-row span.total-discount').text(response.discount.toFixed(2));
                        $('span.total-amount').html((parseFloat(response.total_amount.toFixed(2)) + parseFloat(response.discount.toFixed(2))).toFixed(2));

                        $('.grand-total-row').css('display', 'inherit');
                        $('.grand-total-row span.grand-total').text(response.total_amount.toFixed(2));

                    } else {
                        coupon.invalid();

                        $('.discount-row').css('display', 'none');
                        $('.grand-total-row').css('display', 'none');
                    }

                    place_order.show();
                }
            });


        }
        else {
            swal.close();
            swal.fire({
                type: 'error',
                title: 'Please select target language',
                showConfirmButton: true,
                timer: 6000
            });
        }
    }

    function get_selected_target_languages() {
        var target_languages = $('#target-language').val();
        var text = '';
        for (var i = 0; i < target_languages.length; i++) {
            text += $('#target-language option[value="' + target_languages[i] + '"]').text();
            text += i != target_languages.length - 1 ? ',' : '';

        }
        return text;
    }

    function get_service_types(text) {
        text = text || false;
        var stypes = $('input[name^="service-types"]:checked').map(function (idx, elem) {
            return !text ? $(elem).val() : $(elem).attr('title');
        }).get();
        return stypes;
    }

    function get_std_text_word_count() {
        var words = $('#std-text').val().split(" ");
        for (var i = 0; i < words.length; i++) {
            while (words[i] === "") {
                words.splice(i, 1);
            }
        }
        return words.length;
    }

    var place_order = {
        show: function () {
            $('#place-order').css('display', 'inherit');
            if ($('#coupon-text').attr('data-valid') == 'false') {
                $('#coupon-text').css('display', 'inherit');
            }
        },
        hide: function () {
            $('#place-order').css('display', 'none');
            $('#coupon-text').css('display', 'none');
        }
    };

    var coupon = {
        get_input: function () {
            $('#coupon-text').css('display', 'none');
            $('#coupon-area #coupon-input').css('display', 'inherit');
            place_order.hide();
        },

        valid: function () {
            coupon.hide_button();
            coupon.hide_input();
            $('#coupon-text').attr('data-valid', 'true');
            $('#coupon-area').find('label').removeClass('text-danger').addClass('text-success').html('<i class="fas fa-check"></i> The discount applied.');
            $('#coupon-area label').css('display', 'inherit');
        },

        invalid: function () {
            $('#coupon-area').find('label').removeClass('text-success').addClass('text-danger').html('<i class="fas fa-times"></i> The coupon code is invalid');
        },

        hide_input: function () {
            $('#coupon-input').css('display', 'none');
        },

        hide_button: function () {
            $('#coupon-text').css('display', 'none');
        }

    };


</script>

          
          

      