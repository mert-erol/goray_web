<!-- Omnisearch -->


<div class="main-content">
    <section class="slice slice-lg min-vh-100 d-flex align-items-center bg-section-secondary">
        <!-- SVG background -->
        <div class="bg-absolute-cover bg-size--contain d-none d-lg-block">
            <figure class="w-100">
                <img alt="Image placeholder" src="<?= base_url('assets/site/img'); ?>/svg/backgrounds/bg-3.svg"
                     class="svg-inject">
            </figure>
        </div>
        <div class="container py-5 px-md-0 d-flex align-items-center">
            <div class="w-100">
                <div class="row row-grid justify-content-center justify-content-lg-between align-items-center">
                    <div class="col-sm-8 col-lg-6 col-xl-5 order-lg-2">
                        <div class="card shadow zindex-100 mb-0">
                            <div class="card-body px-md-5 py-5">
                                <div class="mb-5">
                                    <h6 class="h3">Register</h6>
                                    <p class="text-muted mb-0">Sign up to your account to continue.</p>
                                </div>
                                <span class="clearfix"></span>
                                <form role="form">
                                    <div class="form-group">
                                        <label class="form-control-label">Email address</label>
                                        <div class="input-group input-group-merge">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="far fa-user"></i></span>
                                            </div>
                                            <input type="email" class="form-control" id="username"
                                                   placeholder="name@example.com">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Full Name</label>
                                        <div class="input-group input-group-merge">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="far fa-user"></i></span>
                                            </div>
                                            <input type="email" class="form-control" id="name-surname"
                                                   placeholder="John Doe">
                                        </div>
                                    </div>
                                    <div class="form-group mb-4">
                                        <label class="form-control-label">Password</label>
                                        <div class="input-group input-group-merge">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="far fa-key"></i></span>
                                            </div>
                                            <input type="password" class="form-control" id="password"
                                                   placeholder="********">
                                            <div class="input-group-append">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Confirm password</label>
                                        <div class="input-group input-group-merge">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="far fa-key"></i></span>
                                            </div>
                                            <input type="password" class="form-control" id="password-confirm"
                                                   placeholder="********">
                                        </div>
                                    </div>
                                    <div class="my-4">
                                        <div class="custom-control custom-checkbox mb-3">
                                            <input type="checkbox" class="custom-control-input" id="check-terms">
                                            <label class="custom-control-label" for="check-terms">I agree to the <a
                                                        href="#">terms and conditions</a></label>
                                        </div>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="check-privacy">
                                            <label class="custom-control-label" for="check-privacy">I agree to the <a
                                                        href="#">privacy policy</a></label>
                                        </div>
                                    </div>
                                    <div class="mt-4">
                                        <button id="register" type="button"
                                                class="btn btn-sm btn-primary btn-icon rounded-pill">
                                            <span class="btn-inner--text">Create my account</span>
                                            <span class="btn-inner--icon"><i
                                                        class="far fa-long-arrow-alt-right"></i></span>
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <div class="card-footer px-md-5">
                                <small>Do you have already an account ?</small>
                                <a href="<?= base_url('login') ?>" class="small font-weight-bold">Login
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 order-lg-1 d-none d-lg-block">

                        <h3 class="h2 mb-4">Don't have an account yet?</h3>

                        <a href="<?= base_url('register') ?>"
                           class="btn btn-sm btn-primary btn-icon rounded-pill">

                            Create an account in seconds

                            <span class="btn-inner--icon"><i
                                        class="far fa-plus"></i></span>
                        </a>


                    </div>
                </div>
            </div>
        </div>
    </section>
</div>


<script>

    document.addEventListener("DOMContentLoaded", function (event) {

        $(document).on('click', '#register', function (e) {

            e.preventDefault();

            swal.fire({
                title: "<?=ln('MESSAGE.Processing')?>",
                text: "<?=ln('MESSAGE.Wait')?>",
                timer: 99999,
                onOpen: function () {
                    swal.showLoading()
                }
            }).then(function (e) {
                "timer" === e.dismiss && console.log("I was closed by the timer")
            });

            var dataset = {
                username: $('#username').val(),
                name_surname: $('#name-surname').val(),
                password: $('#password').val(),
                password_confirm: $('#password-confirm').val()
            };


            $.post(window.location.origin + '/register', dataset, function (response) {

                var response = JSON.parse(response);

                if (response.result == 1) {
                    swal.fire({
                        type: 'success',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 1500
                    });

                    window.location.href = window.location.origin + '/referrer';
                } else {
                    swal.fire({
                        type: 'error',
                        title: response.message,
                        showConfirmButton: false,
                        timer: 6000
                    });
                }
            });
        });
    });
</script>


