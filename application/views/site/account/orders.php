<div class="main-content">
    <!-- Header (account) -->
    <section class="header-account-page bg-primary d-flex align-items-end" data-offset-top="#header-main">
        <!-- Header container -->
        <div class="container pt-4 pt-lg-0">
            <div class="row">
                <div class=" col-lg-12">
                    <!-- Salute + Small stats -->
                    <div class="row align-items-center mb-4">
                        <div class="col-md-5 mb-4 mb-md-0">
                            <span class="h2 mb-0 text-white d-block">Hello, <?= $userSettings['name_surname'] ?></span>

                            <span class="text-white">Have a nice day!</span>
                        </div>
                        <div class="col-auto flex-fill d-none d-xl-block">
                            <ul class="list-inline row justify-content-lg-end mb-0">
                                <li class="list-inline-item col-sm-4 col-md-auto px-3 my-2 mx-0">
                    <span class="badge badge-dot text-white">
                      <i class="bg-success"></i>Sales
                    </span>
                                    <a class="d-sm-block h5 text-white font-weight-bold pl-2" href="#">
                                        20.5%
                                        <small class="far fa-angle-up text-success"></small>
                                    </a>
                                </li>
                                <li class="list-inline-item col-sm-4 col-md-auto px-3 my-2 mx-0">
                    <span class="badge badge-dot text-white">
                      <i class="bg-warning"></i>Tasks
                    </span>
                                    <a class="d-sm-block h5 text-white font-weight-bold pl-2" href="#">
                                        5.7%
                                        <small class="far fa-angle-up text-warning"></small>
                                    </a>
                                </li>
                                <li class="list-inline-item col-sm-4 col-md-auto px-3 my-2 mx-0">
                    <span class="badge badge-dot text-white">
                      <i class="bg-danger"></i>Sales
                    </span>
                                    <a class="d-sm-block h5 text-white font-weight-bold pl-2" href="#">
                                        -3.24%
                                        <small class="far fa-angle-down text-danger"></small>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Account navigation -->
                    <div class="d-flex">
                        <a href="#" class="btn btn-icon btn-group-nav shadow btn-neutral">
                            <span class="btn-inner--icon"><i class="far fa-user"></i></span>
                            <span class="btn-inner--text d-none d-md-inline-block">My Profile</span>
                        </a>
                        <div class="btn-group btn-group-nav shadow ml-auto" role="group" aria-label="Basic example">
                            <div class="btn-group" role="group">
                                <button id="btn-group-settings" type="button" class="btn btn-neutral btn-icon"
                                        data-toggle="dropdown" data-offset="0,8" aria-haspopup="true"
                                        aria-expanded="false">
                                    <span class="btn-inner--icon"><i class="far fa-sliders-h"></i></span>
                                    <span class="btn-inner--text d-none d-sm-inline-block"><a
                                                href="profile.php">Account</a></span>
                                </button>

                            </div>

                            <div class="btn-group" role="group">
                                <button id="btn-group-listing" type="button"
                                        class="btn btn-neutral btn-icon rounded-right" data-toggle="dropdown"
                                        data-offset="0,8" aria-haspopup="true" aria-expanded="false">
                                    <span class="btn-inner--icon"><i class="far fa-list-ul"></i></span>
                                    <span class="btn-inner--text d-none d-sm-inline-block"><a
                                                href="orders.php">Orders</a></span>
                                </button>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="slice bg-section-secondary">
        <div class="container">
            <!-- Section title -->
            <div class="actions-toolbar py-2 mb-4">
                <div class="actions-search" id="actions-search">
                    <div class="input-group input-group-merge input-group-flush">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-transparent"><i class="far fa-search"></i></span>
                        </div>
                        <input type="text" class="form-control form-control-flush" placeholder="Type and hit enter ...">
                        <div class="input-group-append">
                            <a href="#" class="input-group-text bg-transparent" data-action="search-close"
                               data-target="#actions-search"><i class="far fa-times"></i></a>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-between align-items-center">
                    <div class="col">
                        <h5 class="mb-1">Orders</h5>
                        <p class="text-sm text-muted mb-0 d-none d-md-block">
                            Manage pending orders and track invoices.</p>
                    </div>
                </div>
            </div>
            <!-- Orders table -->
            <div class="table-responsive">
                <table class="table table-cards align-items-center">
                    <thead>
                    <tr>
                        <th scope="col">Origin</th>
                        <th scope="col">Target</th>
                        <th scope="col" class="sort">Date</th>
                        <th scope="col" class="sort">Word Count</th>
                        <th scope="col" class="sort">Services</th>
                        <th scope="col" class="sort">Value</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    foreach ($order_data as $ord): ?>

                        <tr>
                            <td>
                                <span class="value text-sm mb-0"><?= $ord['origin_language'] ?></span>
                            </td>
                            <td>
                                <span class="value text-sm mb-0"><?= $ord['target_languages'] ?></span>
                            </td>
                            <td class="order">
                                <span class="h6 text-sm font-weight-bold mb-0"><?= date_std($ord['created_at']) ?></span>
                                <span class="d-block text-sm text-muted">ORDER ID #<?= $ord['id'] ?></span>
                            </td>
                            <td>
                                <span class="client"><?= $ord['word_count'] ?></span>
                            </td>
                            <td>
                                <span class="client"><?= $ord['service_types'] ?></span>
                            </td>
                            <td>
                                <span class="value text-sm mb-0"><?= $ord['total_amount'] ?></span>
                            </td>

                            <td>
                                <div class="d-flex align-items-center justify-content-end">

                                    <?php if ($ord['status'] == 2): ?>
                                        <button type="button" class="btn btn-sm btn-soft-warning btn-icon rounded-pill">
                                            <span class="btn-inner--icon"><i class="far fa-plus"></i></span>
                                            <span class="btn-inner--text">Pay now</span>
                                        </button>
                                    <?php endif; ?>

                                    <?php if ($ord['status'] == 1): ?>
                                        <button type="button" class="btn btn-sm btn-soft-success btn-icon rounded-pill">
                                            <span class="btn-inner--icon"><i class="far fa-check"></i></span>
                                            <span class="btn-inner--text">Paid: <?= date_std($ord['updated_at']) ?></span>
                                        </button>
                                    <?php endif; ?>


                                    <?php if ($ord['status'] == 3): ?>
                                        <button type="button" class="btn btn-sm btn-soft-danger btn-icon rounded-pill">
                                            <span class="btn-inner--icon"><i class="far fa-times"></i></span>
                                            <span class="btn-inner--text">Canceled</span>
                                        </button>
                                    <?php endif; ?>

                                    <!-- Actions -->
                                    <div class="actions ml-3">
                                        <a href="<?= base_url('account/order/' . $ord['id']) ?>"
                                           class="action-item mr-2 order-detail"
                                           data-toggle="tooltip" title="Show Detail">
                                            <i class="far fa-folder-open"></i>
                                        </a>


                                    </div>
                                </div>
                            </td>
                        </tr>

                        <tr class="table-divider"></tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>

        </div>
    </section>
</div>


<script>

    document.addEventListener("DOMContentLoaded", function (event) {


    });
</script>