<div class="main-content">


    <section class="slice bg-section-secondary">
        <div class="container">
            <!-- Section title -->
            <div class="actions-toolbar py-2 mb-4">
                <div class="actions-search" id="actions-search">
                    <div class="input-group input-group-merge input-group-flush">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-transparent"><i class="far fa-search"></i></span>
                        </div>
                        <input type="text" class="form-control form-control-flush" placeholder="Type and hit enter ...">
                        <div class="input-group-append">
                            <a href="#" class="input-group-text bg-transparent" data-action="search-close"
                               data-target="#actions-search"><i class="far fa-times"></i></a>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-between align-items-center">
                    <div class="col">
                        <h5 class="mb-1">Order No #<?= $order_data['id'] ?></h5>
                        <p class="text-sm text-muted mb-0 d-none d-md-block">Manage pending order and track
                            invoices.</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <div>
                                <h5 class="card-title mb-0">Documents / Text</h5>
                            </div>
                        </div>
                        <div class="card-body">
                            <p class="card-text">
                                <?php if ($order_data['word_count'] > 0): ?>
                                <?php $user_files = json_decode($order_data['user_files'], FALSE);
                                if (count($user_files) > 0): ?>
                            <ul>
                                <?php foreach ($user_files as $user_file): ?>

                                    <li><a href="<?= base_url('uploads/' . $order_data['guid'] . '/' . $user_file) ?>"
                                           target="_blank"><?= $user_file ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                            <?php else: echo $order_data['free_text'];
                            endif;
                            endif; ?>
                            </p>

                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <div>
                                <h5 class="card-title mb-0">Order Status</h5>
                            </div>
                        </div>
                        <div class="card-body">
                            <?php if ($order_data['status'] == 2): ?>
                                <button type="button" class="btn btn-sm btn-soft-warning btn-icon rounded-pill">
                                    <span class="btn-inner--icon"><i class="far fa-plus"></i></span>
                                    <span class="btn-inner--text">Pay now</span>
                                </button>
                            <?php endif; ?>

                            <?php if ($order_data['status'] == 1): ?>
                                <button type="button" class="btn btn-sm btn-soft-success btn-icon rounded-pill">
                                    <span class="btn-inner--icon"><i class="far fa-check"></i></span>
                                    <span class="btn-inner--text">Paid: <?= date_std($ord['updated_at']) ?></span>
                                </button>
                            <?php endif; ?>


                            <?php if ($order_data['status'] == 3): ?>
                                <button type="button" class="btn btn-sm btn-soft-danger btn-icon rounded-pill">
                                    <span class="btn-inner--icon"><i class="far fa-times"></i></span>
                                    <span class="btn-inner--text">Canceled</span>
                                </button>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <div>
                                <h5 class="card-title mb-0">Order Detail</h5>
                            </div>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-cards align-items-center">
                                    <tr>
                                        <th>Origin Language</th>
                                        <td><?= $order_data['origin_language'] ?></td>
                                    </tr>
                                    <tr class="table-divider"></tr>
                                    <tr>
                                        <th>Target Languages</th>
                                        <td><?= $order_data['target_languages'] ?></td>
                                    </tr>
                                    <tr class="table-divider"></tr>

                                    <?php if (strlen($order_data['service_types']) > 0): ?>
                                        <tr>
                                            <th>Services</th>
                                            <td><?= $order_data['service_types'] ?></td>
                                        </tr>
                                        <tr class="table-divider"></tr>
                                    <?php endif; ?>
                                    <tr>
                                        <th>Order Date</th>
                                        <td><?= date_std($order_data['created_at']) ?></td>
                                    </tr>
                                    <tr class="table-divider"></tr>

                                    <tr>
                                        <th>Estimated Date</th>
                                        <td><?= date_std($order_data['estimated_date']) ?></td>
                                    </tr>
                                    <tr class="table-divider"></tr>

                                    <?php if (strlen($order_data['notes']) > 0): ?>
                                        <tr>
                                            <th>Notes</th>
                                            <td><?= $order_data['notes'] ?></td>
                                        </tr>
                                        <tr class="table-divider"></tr>

                                    <?php endif; ?>

                                    <tr>
                                        <th>Total Word Count</th>
                                        <td><?= $order_data['word_count'] ?></td>
                                    </tr>
                                    <tr class="table-divider"></tr>

                                    <?php if ($order_data['discount'] > 0):
                                        ?>
                                        <tr class="text-info">
                                            <th>Total Amount</th>
                                            <td><b><?= $order_data['total_amount'] + $order_data['discount'] ?></b></td>
                                        </tr>

                                        <tr class="text-warning">
                                            <th>Discount</th>
                                            <td><b>- <?= $order_data['discount'] ?></b></td>
                                        </tr>

                                        <tr class="text-success">
                                            <th>Grand Total</th>
                                            <td><b><?= $order_data['total_amount'] ?></b></td>
                                        </tr>

                                    <?php else: ?>
                                        <tr>
                                            <th>Total Amount</th>
                                            <td><b><?= $order_data['total_amount'] ?></b></td>
                                        </tr>
                                    <?php endif; ?>


                                    <tr class="table-divider"></tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </section>
</div>