

  <!-- Omnisearch -->
  <div id="omnisearch" class="omnisearch">
    <div class="container">
      <!-- Search form -->
      <form class="omnisearch-form">
        <div class="form-group">
          <div class="input-group input-group-merge input-group-flush">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="far fa-search"></i></span>
            </div>
            <input type="text" class="form-control" placeholder="Type and hit enter ...">
          </div>
        </div>
      </form>
      <div class="omnisearch-suggestions">
        <h6 class="heading">Search Suggestions</h6>
        <div class="row">
          <div class="col-sm-6">
            <ul class="list-unstyled mb-0">
              <li>
                <a class="list-link" href="#">
                  <i class="far fa-search"></i>
                  <span>macbook pro</span> in Laptops
                </a>
              </li>
              <li>
                <a class="list-link" href="#">
                  <i class="far fa-search"></i>
                  <span>iphone 8</span> in Smartphones
                </a>
              </li>
              <li>
                <a class="list-link" href="#">
                  <i class="far fa-search"></i>
                  <span>macbook pro</span> in Laptops
                </a>
              </li>
              <li>
                <a class="list-link" href="#">
                  <i class="far fa-search"></i>
                  <span>beats pro solo 3</span> in Headphones
                </a>
              </li>
              <li>
                <a class="list-link" href="#">
                  <i class="far fa-search"></i>
                  <span>smasung galaxy 10</span> in Phones
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="main-content">
    <!-- Header (account) -->
    <section class="header-account-page bg-primary d-flex align-items-end" data-offset-top="#header-main">
      <!-- Header container -->
      <div class="container pt-4 pt-lg-0">
        <div class="row">
          <div class=" col-lg-12">
            <!-- Salute + Small stats -->
            <div class="row align-items-center mb-4">
              <div class="col-md-5 mb-4 mb-md-0">
                <span class="h2 mb-0 text-white d-block">Morning, Heather</span>
                <span class="text-white">Have a nice day!</span>
              </div>
              <div class="col-auto flex-fill d-none d-xl-block">
                <ul class="list-inline row justify-content-lg-end mb-0">
                  <li class="list-inline-item col-sm-4 col-md-auto px-3 my-2 mx-0">
                    <span class="badge badge-dot text-white">
                      <i class="bg-success"></i>Sales
                    </span>
                    <a class="d-sm-block h5 text-white font-weight-bold pl-2" href="#">
                      20.5%
                      <small class="far fa-angle-up text-success"></small>
                    </a>
                  </li>
                  <li class="list-inline-item col-sm-4 col-md-auto px-3 my-2 mx-0">
                    <span class="badge badge-dot text-white">
                      <i class="bg-warning"></i>Tasks
                    </span>
                    <a class="d-sm-block h5 text-white font-weight-bold pl-2" href="#">
                      5.7%
                      <small class="far fa-angle-up text-warning"></small>
                    </a>
                  </li>
                  <li class="list-inline-item col-sm-4 col-md-auto px-3 my-2 mx-0">
                    <span class="badge badge-dot text-white">
                      <i class="bg-danger"></i>Sales
                    </span>
                    <a class="d-sm-block h5 text-white font-weight-bold pl-2" href="#">
                      -3.24%
                      <small class="far fa-angle-down text-danger"></small>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <!-- Account navigation -->
            <div class="d-flex">
              <a href="#" class="btn btn-icon btn-group-nav shadow btn-neutral">
                <span class="btn-inner--icon"><i class="far fa-user"></i></span>
                <span class="btn-inner--text d-none d-md-inline-block">My Profile</span>
              </a>
              <div class="btn-group btn-group-nav shadow ml-auto" role="group" aria-label="Basic example">
                <div class="btn-group" role="group">
                  <button id="btn-group-settings" type="button" class="btn btn-neutral btn-icon" data-toggle="dropdown" data-offset="0,8" aria-haspopup="true" aria-expanded="false">
                    <span class="btn-inner--icon"><i class="far fa-sliders-h"></i></span>
                    <span class="btn-inner--text d-none d-sm-inline-block"><a href="profile.php">Account</a></span>
                  </button>
              
                </div>
             
                <div class="btn-group" role="group">
                  <button id="btn-group-listing" type="button" class="btn btn-neutral btn-icon rounded-right" data-toggle="dropdown" data-offset="0,8" aria-haspopup="true" aria-expanded="false">
                    <span class="btn-inner--icon"><i class="far fa-list-ul"></i></span>
                    <span class="btn-inner--text d-none d-sm-inline-block"><a href="orders.php">Orders</a></span>
                  </button>
               
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="slice">
      <div class="container">
        <div class="row row-grid">
          <div class="col-lg-9 order-lg-2">
            <!-- Change avatar -->
            <div class="card bg-gradient-warning hover-shadow-lg">
              <div class="card-body py-3">
                <div class="row row-grid align-items-center">
                  <div class="col-lg-8">
                    <div class="media align-items-center">
                      <a href="#" class="avatar avatar-lg rounded-circle mr-3">
                        <img alt="Image placeholder" src="assets/img/theme/light/team-1-800x800.jpg">
                      </a>
                      <div class="media-body">
                        <h5 class="text-white mb-0">Heather Wright</h5>
                        <div>
                          <form>
                            <input type="file" name="file-1[]" id="file-1" class="custom-input-file custom-input-file-link" data-multiple-caption="{count} files selected" multiple />
                            <label for="file-1">
                              <span class="text-white">Change avatar</span>
                            </label>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-auto flex-fill mt-4 mt-sm-0 text-sm-right d-none d-lg-block">
                    <a href="#" class="btn btn-sm btn-white rounded-pill btn-icon shadow">
                      <span class="btn-inner--icon"><i class="far fa-fire"></i></span>
                      <span class="btn-inner--text">Upgrade to Pro</span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <!-- General information form -->
            <div class="actions-toolbar py-2 mb-4">
              <h5 class="mb-1">General information</h5>
              <p class="text-sm text-muted mb-0">You can help us, by filling your data, create you a much better experience using our website.</p>
            </div>
            <form>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="form-control-label">First name</label>
                    <input class="form-control" type="text" placeholder="Enter your first name">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="form-control-label">Last name</label>
                    <input class="form-control" type="text" placeholder="Also your last name">
                  </div>
                </div>
              </div>
              <div class="row align-items-center">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="form-control-label">Birthday</label>
                    <input type="text" class="form-control" data-toggle="date" placeholder="Select your birth date">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="form-control-label">Gender</label>
                    <select class="form-control" data-toggle="select">
                      <option value="1">Female</option>
                      <option value="2">Male</option>
                      <option value="2">Rather not say</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="form-control-label">Email</label>
                    <input class="form-control" type="email" placeholder="name@exmaple.com">
                    <small class="form-text text-muted mt-2">This is the main email address that we'll send notifications to. <a href="account-notifications.html">Manage you notifications</a> in order to receive only the thing that matter to you most.</small>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="form-control-label">Phone</label>
                    <input class="form-control" type="text" placeholder="+40-777 245 549">
                  </div>
                </div>
              </div>
              <!-- Address -->
              <div class="pt-5 mt-5 delimiter-top">
                <div class="actions-toolbar py-2 mb-4">
                  <h5 class="mb-1">Address details</h5>
                  <p class="text-sm text-muted mb-0">Fill in your address info for upcoming orders or payments.</p>
                </div>
                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label class="form-control-label">Address</label>
                      <input class="form-control" type="text" placeholder="Enter your home address">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="form-control-label">City</label>
                      <input class="form-control" type="text" placeholder="City">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="form-control-label">Country</label>
                      <select class="form-control" data-toggle="select" title="Country" data-live-search="true" data-live-search-placeholder="Country">
                        <option>Romania</option>
                        <option>United Stated</option>
                        <option>France</option>
                        <option>Greece</option>
                        <option>Italy</option>
                        <option>Norway</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Skills -->
              <div class="pt-5 mt-5 delimiter-top">
                <div class="actions-toolbar py-2 mb-4">
                  <h5 class="mb-1">Skills</h5>
                  <p class="text-sm text-muted mb-0">Show off you skills using our tags input control.</p>
                </div>
                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <label class="sr-only">Skills</label>
                      <input type="text" class="form-control" value="HTML, CSS3, Bootstrap, Photoshop, VueJS" data-toggle="tags" placeholder="Type here..." />
                    </div>
                  </div>
                </div>
              </div>
              <!-- Description -->
              <div class="pt-5 mt-5 delimiter-top">
                <div class="actions-toolbar py-2 mb-4">
                  <h5 class="mb-1">About me</h5>
                  <p class="text-sm text-muted mb-0">Use this field to let others know you better.</p>
                </div>
                <div class="row">
                  <div class="col">
                    <div class="form-group">
                      <div class="form-group">
                        <label class="form-control-label">Bio</label>
                        <textarea class="form-control" placeholder="Tell us a few words about yourself" rows="3"></textarea>
                        <small class="form-text text-muted mt-2">You can @mention other users and organizations to link to them.</small>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Save changes buttons -->
              <div class="pt-5 mt-5 delimiter-top text-center">
                <button type="button" class="btn btn-sm btn-primary">Save changes</button>
                <button type="button" class="btn btn-link text-muted">Cancel</button>
              </div>
            </form>
          </div>
          <div class="col-lg-3 order-lg-1">
            <div data-toggle="sticky" data-sticky-offset="30" data-negative-margin=".card-profile-cover">
              <div class="card">
                <div class="card-header py-3">
                  <span class="h6">Settings</span>
                </div>
                <div class="list-group list-group-sm list-group-flush">
                  <a href="account-profile.html" class="list-group-item list-group-item-action d-flex justify-content-between">
                    <div>
                      <i class="far fa-user-circle mr-2"></i>
                      <span>Profile</span>
                    </div>
                    <div>
                      <i class="far fa-angle-right"></i>
                    </div>
                  </a>
                  <a href="account-settings.html" class="list-group-item list-group-item-action d-flex justify-content-between">
                    <div>
                      <i class="far fa-cog mr-2"></i>
                      <span>Settings</span>
                    </div>
                    <div>
                      <i class="far fa-angle-right"></i>
                    </div>
                  </a>
                  <a href="account-billing.html" class="list-group-item list-group-item-action d-flex justify-content-between">
                    <div>
                      <i class="far fa-credit-card mr-2"></i>
                      <span>Billing</span>
                    </div>
                    <div>
                      <i class="far fa-angle-right"></i>
                    </div>
                  </a>
                  <a href="account-notifications.html" class="list-group-item list-group-item-action d-flex justify-content-between">
                    <div>
                      <i class="far fa-bell mr-2"></i>
                      <span>Notifications</span>
                    </div>
                    <div>
                      <i class="far fa-angle-right"></i>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
