<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'Language';
$route['404_override'] = 'Language/show_404';
$route['translate_uri_dashes'] = FALSE;

$route['home'] = 'Site/Site';

$route['order'] = 'Site/Order';
$route['order/file-delete'] = 'Site/Order/FileDelete';

$route['calculate'] = 'Site/Order/Calculate';
$route['login'] = 'Site/Auth/Login';
$route['logout'] = 'Site/Auth/Logout';
$route['register'] = 'Site/Auth/Register';
$route['referrer'] = 'Site/Auth/Referrer';

$route['account'] = 'Site/Account/MyAccount';
$route['account/orders'] = 'Site/Account/Orders';
$route['account/order/(:num)'] = 'Site/Account/Order/$1';

$route['File/(:any)'] = 'Site/File/Upload';

$route['App'] = 'App/Auth';
$route['App/Requests/(:num)'] = 'App/Requests/index/$1';
$route['App/Shelfs/(:num)'] = 'App/Shelfs/index/$1';

$controller_dir = opendir(APPPATH . "controllers/App");

while (($file = readdir($controller_dir)) !== false) {
    if (substr($file, -4) == ".php") {
        $route[substr($file, 0, -4) . "(.*)"] = substr($file, 0, -4) . "$1";
    } elseif (substr($file, -5) == ".php/") {
        $route[substr($file, 0, -5) . "(.*)"] = substr($file, 0, -5) . "$1";
    }
}

$language_dir = opendir(FCPATH . "assets/languages");

while (($file = readdir($language_dir)) !== false) {
    if (!is_dir($file)) {
        $route[$file . "(.*)"] = 'Site/Site/pages$1';
    }
}










 