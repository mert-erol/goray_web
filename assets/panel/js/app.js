function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}


var _base_functions = function () {

    this.test = function () {
        return 'here';
    };

    var lang = getCookie('app_lang_dir');
    lang = lang == "" ? 'turkish' : lang;
    //console.log('language ' + lang);

    var langString = {
        "sDecimal": ",",
        "sEmptyTable": "Tabloda herhangi bir veri mevcut değil",
        "sInfo": "<b>_TOTAL_</b> kayıttan _START_ - _END_ arasındaki kayıtlar gösteriliyor",
        "sInfoEmpty": "Kayıt yok",
        "sInfoFiltered": "(_MAX_ kayıt içerisinden bulunan)",
        "sInfoPostFix": "",
        "sInfoThousands": ".",
        "sLengthMenu": "_MENU_",
        "sLoadingRecords": "Yükleniyor...",
        "sProcessing": "İşleniyor...",
        "sSearch": "Ara:",
        "sZeroRecords": "Eşleşen kayıt bulunamadı",
        "oPaginate": {
            "sFirst": "İlk",
            "sLast": "Son",
            "sNext": "Sonraki",
            "sPrevious": "Önceki"
        },
        "oAria": {
            "sSortAscending": ": artan sütun sıralamasını aktifleştir",
            "sSortDescending": ": azalan sütun sıralamasını aktifleştir"
        },
        "select": {
            "rows": {
                "_": "%d kayıt seçildi",
                "0": "",
                "1": "1 kayıt seçildi"
            }
        }
    };

    if (lang != 'turkish') {
        langString = {
            "sDecimal": ",",
            "sEmptyTable": "No records",
            "sInfo": "Showing _START_ - _END_ of <b>_TOTAL_</b>",
            "sInfoEmpty": "No data",
            "sInfoFiltered": "(of _MAX_)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_",
            "sLoadingRecords": "Loading...",
            "sProcessing": "Processing...",
            "sSearch": "Search:",
            "sZeroRecords": "Not found",
            "oPaginate": {
                "sFirst": "First",
                "sLast": "Last",
                "sNext": "Next",
                "sPrevious": "Prev"
            },
            "oAria": {
                "sSortAscending": ": Ascending",
                "sSortDescending": ": Descending"
            },
            "select": {
                "rows": {
                    "_": "%d row selected",
                    "0": "",
                    "1": "1 row selected"
                }
            }
        };
    }

    this.get_table = function (e, settings) {
        $.extend(true, $.fn.dataTable.defaults, {
            dom: "<'row'><'row'<'col-md-1'l><'col-md-4'B><'col-md-7'f>r>t<'row'<'col-md-6'i><'col-md-6'p>>",
            "lengthMenu": [[10, 50, 100, 250, 500, 1000], [10 + ' kayıt', 50 + ' kayıt', 100 + ' kayıt', 250 + ' kayıt', 500 + ' kayıt', 1000 + ' kayıt']],
            "language": langString,
            "buttons": [
                {
                    extend: 'colvis',
                    text: '<i class="la la-eye"></i> ' + lang == 'turkish' ? 'Görünürlük' : 'Visibility',
                    className: 'btn dark btn-outline btn-sm'
                },
                {
                    extend: 'collection',
                    text: '<i class="la la-external-link-square"></i> ' + lang == 'turkish' ? 'Dışa Aktar' : 'Export',
                    className: 'btn dark btn-outline btn-sm',
                    buttons: [
                        {
                            extend: 'copy',
                            titleAttr: 'Kopyala',
                            text: '<i class="la la-copy"></i> ' + lang == 'turkish' ? 'Panoya Kopyala' : 'Copy',
                        },
                        {
                            extend: 'excel',
                            titleAttr: 'Excele aktar',
                            text: '<i class="la la-file-excel-o"></i> ' + lang == 'turkish' ? 'Excel İndir' : 'Create Excel',
                        },
                        {
                            extend: 'pdf',
                            titleAttr: 'PDF indir',
                            text: '<i class="la la-file-pdf-o"></i> ' + lang == 'turkish' ? 'Pdf Oluştur' : 'Get PDF',
                        }
                    ]
                }
            ]
        });
        return e.DataTable(settings);
    };
};

var _base = new _base_functions();


;(function ($) {
    $.fn.extend({
        donetyping: function (callback, timeout) {
            timeout = timeout || 1e3; // 1 second default timeout
            var timeoutReference,
                doneTyping = function (el) {
                    if (!timeoutReference) return;
                    timeoutReference = null;
                    callback.call(el);
                };
            return this.each(function (i, el) {
                var $el = $(el);
                // Chrome Fix (Use keyup over keypress to detect backspace)
                // thank you @palerdot
                $el.is(':input') && $el.on('keyup keypress paste', function (e) {
                    // This catches the backspace button in chrome, but also prevents
                    // the event from triggering too preemptively. Without this line,
                    // using tab/shift+tab will make the focused element fire the callback.
                    if (e.type == 'keyup' && e.keyCode != 8) return;

                    // Check if timeout has been set. If it has, "reset" the clock and
                    // start over again.
                    if (timeoutReference) clearTimeout(timeoutReference);
                    timeoutReference = setTimeout(function () {
                        // if we made it here, our timeout has elapsed. Fire the
                        // callback
                        doneTyping(el);
                    }, timeout);
                }).on('blur', function () {
                    // If we can, fire the event since we're leaving the field
                    doneTyping(el);
                });
            });
        }
    });


})(jQuery);

function myUploadFile(file, path, editor) {
    url = window.location.origin + '/App/Pages/Upload/' + path;
    var data = new FormData();
    data.append("file", file);
    var request = new XMLHttpRequest();
    request.open('POST', url, true);
    request.onload = function () {
        if (request.status >= 200 && request.status < 400) {
            // Success!
            var resp = request.responseText;
            editor.summernote('insertImage', resp);
            console.log(resp);
        } else {
            // We reached our target server, but it returned an error
            var resp = request.responseText;
            console.log(resp);
        }
    };
    request.onerror = function (jqXHR, textStatus, errorThrown) {
        // There was a connection error of some sort
        console.log(jqXHR);
    };
    request.send(data);
}

$(function () {
    $.contextMenu({
        selector: '.note-editable',
        callback: function (key, options) {
            if (key == 'add_row') {

                $('.summernote').summernote('pasteHTML', '<p></p>');
            }

        },
        items: {
            "add_row": {name: "Add Row After", icon: "add"},
            "sep1": "---------",
            "quit": {
                name: "Quit", icon: function () {
                    return 'context-menu-icon context-menu-icon-quit';
                }
            }
        }
    });
});